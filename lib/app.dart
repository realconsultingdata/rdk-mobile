import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/diseases.dart';
import 'package:rdk_mobile/providers/establishments.dart';
import 'package:rdk_mobile/providers/expert_centers.dart';
import 'package:rdk_mobile/providers/genes.dart';
import 'package:rdk_mobile/providers/nh_age_onsets.dart';
import 'package:rdk_mobile/providers/phenotypes.dart';
import 'package:rdk_mobile/providers/rdk_sources.dart';
import 'package:rdk_mobile/screens/compliances_screen.dart';
import 'package:rdk_mobile/screens/cookie_policy_screen.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/screens/diagnostic_tutorial_screen.dart';
import 'package:rdk_mobile/screens/disease_info_screen.dart';
import 'package:rdk_mobile/screens/disease_pnds_viewer.dart';
import 'package:rdk_mobile/screens/embedded_webview_screen.dart';
import 'package:rdk_mobile/screens/expert_center_info_screen.dart';
import 'package:rdk_mobile/screens/gene_info_screen.dart';
import 'package:rdk_mobile/screens/info_service_screen.dart';
import 'package:rdk_mobile/screens/legal_notices_screen.dart';
import 'package:rdk_mobile/screens/map_screen.dart';
import 'package:rdk_mobile/screens/partner_screen.dart';
import 'package:rdk_mobile/screens/phenotype_info_screen.dart';
import 'package:rdk_mobile/screens/legal_menu_screen.dart';
import 'package:rdk_mobile/screens/privacy_policy_screen.dart';
import 'package:rdk_mobile/screens/sources_screen.dart';
import 'package:rdk_mobile/screens/sponsor_screen.dart';
import 'package:rdk_mobile/screens/tab_screen.dart';
import 'package:rdk_mobile/screens/user_guide_viewer.dart';
import 'package:rdk_mobile/utils/device_type.dart';
import 'package:rdk_mobile/widgets/ios_update_notification_message.dart';
import 'package:syncfusion_localizations/syncfusion_localizations.dart';
import 'package:upgrader/upgrader.dart';
import 'package:url_launcher/url_launcher.dart';

import 'flavor_config.dart';
import 'helpers/database_helper.dart';

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();

  static GlobalKey<NavigatorState> appKey = GlobalKey<NavigatorState>();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  DatabaseHelper dbHelper;

  @override
  void initState() {
    super.initState();
    dbHelper = DatabaseHelper();
    dbHelper.initDB().whenComplete(() async {
      setState(() {});
    });
    WidgetsBinding.instance.addObserver(this);
    if (WidgetsBinding.instance.lifecycleState != null &&
        WidgetsBinding.instance.lifecycleState == AppLifecycleState.resumed) {
      Posthog().capture(eventName: 'app-open');
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        Posthog().capture(eventName: 'app-open');
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final Link httpLink = HttpLink(dotenv.env['SERVER_HOST']);
    final ValueNotifier<GraphQLClient> client =
        ValueNotifier(GraphQLClient(link: httpLink, cache: GraphQLCache()));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Diseases(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Genes(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Phenotypes(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => NhAgeOnsets(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => ExpertCenters(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Establishments(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => BookmarkDiseases(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => CurrentLocale(),
        ),
        // ChangeNotifierProvider(
        //   create: (ctx) => BasePageController(),
        // ),
        ChangeNotifierProvider(
          create: (ctx) => RdkSources(),
        ),
      ],
      child: GraphQLProvider(
        client: client,
        child: MaterialApp(
          // title: 'RDK Mobile',
          debugShowCheckedModeBanner: false,
          navigatorKey: MyApp.appKey,
          localizationsDelegates: [
            FlutterI18nDelegate(
              translationLoader: FileTranslationLoader(
                  useCountryCode: false,
                  fallbackFile: 'en',
                  basePath: 'assets/i18n'),
              missingTranslationHandler: (key, locale) {
                print(
                    "--- Missing Key: $key, languageCode: ${locale.languageCode}");
              },
            ),
            SfGlobalLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('en'),
            Locale('fr'),
          ],
          theme: ThemeData(
            primaryColor: const Color(0xFF0EA5E9),
            canvasColor: const Color(0xFFF5F5F9),
            fontFamily: 'Inter',
            textTheme: ThemeData.light().textTheme.copyWith(
                headline3:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                headline4:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                headline5:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                headline6:
                    const TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                bodyText1:
                    const TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                bodyText2:
                    const TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                subtitle1: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                ),
                subtitle2: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                    color: Colors.white),
                button:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
            appBarTheme: const AppBarTheme(
              titleTextStyle: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 26,
                fontFamily: 'Inter',
                color: Color(0xFF1E293B),
                height: 1.15,
              ),
            ),
          ),
          home: Platform.isIOS && FlavorConfig.isProduction()
              ? UpgradeAlert(
                  child: TabScreen(),
                  upgrader: Upgrader(
                    dialogStyle: UpgradeDialogStyle.cupertino,
                    durationUntilAlertAgain: Duration.zero,
                    showIgnore: false,
                    showLater: false,
                    showReleaseNotes: false,
                    messages: IOSUpdateNotificationMessage(),
                    onUpdate: () {
                      launchUrl(Uri.parse(dotenv.env['APPSTORE_URL']));
                      return true;
                    },
                  ),
                )
              : TabScreen(),
          routes: {
            DiseaseInfoScreen.routeName: (ctx) => DiseaseInfoScreen(),
            GeneInfoScreen.routeName: (ctx) => GeneInfoScreen(),
            PhenotypeInfoScreen.routeName: (ctx) => PhenotypeInfoScreen(),
            ExpertCenterInfoScreen.routeName: (ctx) => ExpertCenterInfoScreen(),
            SourcesScreen.routeName: (ctx) => SourcesScreen(),
            PartnerScreen.routeName: (ctx) => PartnerScreen(),
            SponsorScreen.routeName: (ctx) => SponsorScreen(),
            InfoServiceScreen.routeName: (ctx) => InfoServiceScreen(),
            UserGuideViewer.routeName: (ctx) => UserGuideViewer(),
            EmbeddedWebviewScreen.reportBugRouteNameEn: (ctx) =>
                EmbeddedWebviewScreen(
                    key: const ValueKey('report_bug'),
                    title: 'pages.improve_rdk',
                    url: dotenv.env['CLICKUP_BUG_REPORT_EN']),
            EmbeddedWebviewScreen.reportBugRouteNameFr: (ctx) =>
                EmbeddedWebviewScreen(
                    key: const ValueKey('report_bug'),
                    title: 'pages.improve_rdk',
                    url: dotenv.env['CLICKUP_BUG_REPORT_FR']),
            EmbeddedWebviewScreen.requestFeatureRouteNameEn: (ctx) =>
                EmbeddedWebviewScreen(
                    key: const ValueKey('request_feature'),
                    title: 'common.text.request_feature',
                    url: dotenv.env['CLICKUP_FEATURE_REQUEST_EN']),
            EmbeddedWebviewScreen.requestFeatureRouteNameFr: (ctx) =>
                EmbeddedWebviewScreen(
                    key: const ValueKey('request_feature'),
                    title: 'common.text.request_feature',
                    url: dotenv.env['CLICKUP_FEATURE_REQUEST_FR']),
            EmbeddedWebviewScreen.evaluation: (ctx) => EmbeddedWebviewScreen(
                key: const ValueKey('evaluation'),
                title: 'common.text.evaluation',
                url: dotenv.env['EVALUATION_URL']),
            LegalMenuScreen.routeName: (ctx) => const LegalMenuScreen(),
            LegalNoticesScreen.routeName: (ctx) => const LegalNoticesScreen(),
            PrivacyPolicyScreen.routeName: (ctx) => const PrivacyPolicyScreen(),
            CookiePolicyScreen.routeName: (ctx) => const CookiePolicyScreen(),
            CompliancesScreen.routeName: (ctx) => const CompliancesScreen(),
            DiseasePndsViewer.routeName: (ctx) => DiseasePndsViewer(),
            DiagnosticTutorialScreen.routeName: (ctx) =>
                DiagnosticTutorialScreen(),
            MapScreen.routeName: (ctx) => MapScreen(),
          },
        ),
      ),
    );
  }
}

DeviceType getDeviceType() {
  final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
  return data.size.shortestSide < 550 ? DeviceType.Phone : DeviceType.Tablet;
}
