import 'package:flutter/cupertino.dart';
import 'package:rdk_mobile/models/disease.dart';

class DiagnosticDisease {
  final Disease disease;
  final num scoring;

  DiagnosticDisease({
    @required this.disease,
    this.scoring,
  });

  factory DiagnosticDisease.fromJson(Map<String, dynamic> json) {
    return DiagnosticDisease(
        disease: Disease.fromJson(json['disease']), scoring: json['scoring']);
  }
}
