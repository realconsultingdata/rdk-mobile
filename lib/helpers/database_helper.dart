import 'package:path/path.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _databaseHelper = DatabaseHelper._();

  DatabaseHelper._();

  Database db;

  factory DatabaseHelper() {
    return _databaseHelper;
  }

  Future<void> initDB() async {
    String path = await getDatabasesPath();
    db = await openDatabase(
      join(path, 'rdk_db.db'),
      onCreate: (database, version) async {
        await database.execute(
          """
            CREATE TABLE bookmark_diseases (
              orpha_number TEXT PRIMARY KEY,
              name TEXT NOT NULL
            )
          """,
        );
      },
      version: 1,
    );
  }

  Future<int> insertBookmarkDisease(Disease disease) async {
    int result = await db.insert('bookmark_diseases', disease.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return result;
  }

  Future<Disease> findBookmarkDisease(String orphaNumber) async {
    final List<Map<String, Object>> queryResult = await db.query(
      'bookmark_diseases',
      where: "orpha_number = ?",
      whereArgs: [orphaNumber],
    );
    if (queryResult.isNotEmpty) {
      return Disease.fromMap(queryResult.first);
    }
    return null;
  }

  Future<List<Disease>> retrieveBookmarkDiseases() async {
    final List<Map<String, Object>> queryResult =
        await db.query('bookmark_diseases');
    return queryResult.map((e) => Disease.fromMap(e)).toList();
  }

  Future<int> deleteBookmarkDisease(String orphaNumber) async {
    int result = await db.delete(
      'bookmark_diseases',
      where: "orpha_number = ?",
      whereArgs: [orphaNumber],
    );
    return result;
  }
}
