import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:rdk_mobile/app.dart';

import 'flavor_config.dart';

void main() async {
  await dotenv.load(fileName: '.env.staging');
  FlavorConfig(flavor: Flavor.DEV, color: Colors.deepOrange);
  runApp(MyApp());
}

