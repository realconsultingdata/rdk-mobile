import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:rdk_mobile/app.dart';

import 'flavor_config.dart';

void main() async {
  await dotenv.load();
  FlavorConfig(flavor: Flavor.PRODUCTION, color: Colors.green);
  runApp(MyApp());
}