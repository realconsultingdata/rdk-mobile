import 'package:flutter/foundation.dart';

import 'network.dart';

class Author {
  final String firstName;
  final String lastName;
  final String title;
  final Network network;

  Author({
    @required this.firstName,
    @required this.lastName,
    this.title,
    this.network,
  });

  factory Author.fromJson(Map<String, dynamic> json) {
    Network network;
    if (json['network'] != null) {
      network = Network.fromJson(json['network']);
    }

    return Author(
        firstName: json['firstName'],
        lastName: json['lastName'],
        title: json['title'],
        network: network);
  }
}
