import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/author.dart';
import 'package:rdk_mobile/models/disease_hierarchy.dart';
import 'package:rdk_mobile/models/disease_new_expert_center.dart';
import 'package:rdk_mobile/models/disease_pnds.dart';
import 'package:rdk_mobile/models/disease_synonym.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/models/external_content.dart';
import 'package:rdk_mobile/models/gene_disease.dart';
import 'package:rdk_mobile/models/natural_history.dart';
import 'package:rdk_mobile/models/phenotype_disease.dart';
import 'package:rdk_mobile/models/prevalence.dart';
import 'package:rdk_mobile/models/text_section.dart';

import 'disease_relation.dart';

class Disease {
  final String orphaNumber;
  final String name;
  String contents;
  String disorderGroup;
  String disorderType;

  // final List<Gene> genes;
  List<GeneDisease> genes;
  List<PhenotypeDisease> phenotypes;
  List<DiseaseSynonym> synonyms;
  List<NaturalHistory> histories;
  List<Prevalence> prevalences;
  List<DiseaseRelation> relations;
  List<DiseaseTranslation> translations;
  List<DiseaseHierarchy> hierarchies;
  List<DiseaseNewExpertCenter> newExpertCenters;
  List<Disease> preferentialParents;

  // final List<NewExpertCenter> newExpertCenters;
  List<DiseasePnds> diseasePnds;
  List<TextSection> textSections;
  List<Author> authors;
  List<ExternalContent> externalContents;
  int geneCount;
  int phenotypeCount;
  int academyCount;
  double scoring;

  Disease({
    @required this.orphaNumber,
    @required this.name,
    this.contents,
    this.disorderGroup,
    this.disorderType,
    this.genes,
    this.phenotypes,
    this.synonyms,
    this.histories,
    this.prevalences,
    this.relations,
    this.translations,
    this.hierarchies,
    this.newExpertCenters,
    this.diseasePnds,
    this.textSections,
    this.authors,
    this.externalContents,
    this.preferentialParents,
    this.geneCount,
    this.phenotypeCount,
    this.academyCount,
    this.scoring,
  });

  factory Disease.fromJson(Map<String, dynamic> json) {
    List<GeneDisease> genes = [];
    if (json['genes'] != null && json['genes']['totalCount'] > 0) {
      for (var gene in json['genes']['data']) {
        genes.add(GeneDisease.fromJson(gene));
      }
    }

    List<PhenotypeDisease> phenotypes = [];
    if (json['phenotypes'] != null && json['phenotypes']['totalCount'] > 0) {
      for (var data in json['phenotypes']['data']) {
        phenotypes.add(PhenotypeDisease.fromJson(data));
      }
    }

    List<DiseaseSynonym> synonyms = [];
    if (json['synonyms'] != null && json['synonyms']['totalCount'] > 0) {
      for (var syn in json['synonyms']['data']) {
        synonyms.add(DiseaseSynonym.fromJson(syn));
      }
    }

    List<NaturalHistory> histories = [];
    if (json['histories'] != null && json['histories']['totalCount'] > 0) {
      for (var hist in json['histories']['data']) {
        histories.add(NaturalHistory.fromJson(hist));
      }
    }

    List<Prevalence> prevalences = [];
    if (json['epidemiology'] != null &&
        json['epidemiology']['totalCount'] > 0) {
      for (var prevalence in json['epidemiology']['data']) {
        prevalences.add(Prevalence.fromJson(prevalence));
      }
    }

    List<DiseaseRelation> relations = [];
    if (json['relation'] != null && json['relation']['totalCount'] > 0) {
      for (var relation in json['relation']['data']) {
        relations.add(DiseaseRelation.fromJson(relation));
      }
    }

    List<DiseaseTranslation> translations = [];
    if (json['translations'] != null &&
        json['translations']['totalCount'] > 0) {
      for (var translation in json['translations']['data']) {
        translations.add(DiseaseTranslation.fromJson(translation));
      }
    }

    List<DiseaseHierarchy> hierarchies = [];
    if (json['hierarchy'] != null && json['hierarchy']['totalCount'] > 0) {
      for (var data in json['hierarchy']['data']) {
        hierarchies.add(DiseaseHierarchy.fromJson(data));
      }
    }

    List<DiseaseNewExpertCenter> newExpertCenters = [];
    if (json['newExpertCenters'] != null &&
        json['newExpertCenters']['totalCount'] > 0) {
      for (var data in json['newExpertCenters']['data']) {
        newExpertCenters.add(DiseaseNewExpertCenter.fromJson(data));
      }
    }

    List<DiseasePnds> diseasePnds = [];
    if (json['diseasePnds'] != null && json['diseasePnds']['totalCount'] > 0) {
      for (var data in json['diseasePnds']['data']) {
        diseasePnds.add(DiseasePnds.fromJson(data));
      }
    }

    List<TextSection> textSections = [];
    if (json['textSections'] != null &&
        json['textSections']['totalCount'] > 0) {
      for (var data in json['textSections']['data']) {
        textSections.add(TextSection.fromJson(data));
      }
    }

    List<Author> authors = [];
    if (json['authors'] != null && json['authors']['totalCount'] > 0) {
      for (var data in json['authors']['data']) {
        authors.add(Author.fromJson(data));
      }
    }

    List<ExternalContent> externalContents = [];
    if (json['externalContents'] != null &&
        json['externalContents']['totalCount'] > 0) {
      for (var data in json['externalContents']['data']) {
        externalContents.add(ExternalContent.fromJson(data));
      }
    }

    List<Disease> preferentialParents = [];
    if (json['preferentialParents'] != null &&
        json['preferentialParents']['totalCount'] > 0) {
      for (var data in json['preferentialParents']['data']) {
        preferentialParents.add(Disease.fromJson(data));
      }
    }

    return Disease(
      orphaNumber: json['orphaNumber'],
      name: json['name'],
      contents: json['contents'],
      disorderGroup: json['disorderGroup'],
      disorderType: json['disorderType'],
      genes: genes,
      phenotypes: phenotypes,
      synonyms: synonyms,
      histories: histories,
      prevalences: prevalences,
      relations: relations,
      translations: translations,
      hierarchies: hierarchies,
      newExpertCenters: newExpertCenters,
      diseasePnds: diseasePnds,
      textSections: textSections,
      authors: authors,
      externalContents: externalContents,
      preferentialParents: preferentialParents,
      geneCount: json['genes'] == null ? 0 : json['genes']['totalCount'],
      phenotypeCount:
          json['phenotypes'] == null ? 0 : json['phenotypes']['totalCount'],
      academyCount: 0,
      scoring: json['scoring'] ?? 0,
    );
  }

  Disease.fromMap(Map<String, dynamic> res)
      : orphaNumber = res["orpha_number"],
        name = res["name"];

  Map<String, Object> toMap() {
    return {'orpha_number': orphaNumber, 'name': name};
  }
}
