import 'package:flutter/foundation.dart';

import 'disease.dart';

class DiseaseHierarchy {
  final List<DiseaseHierarchyNode> hierarchy;

  DiseaseHierarchy({
    @required this.hierarchy,
  });

  factory DiseaseHierarchy.fromJson(Map<String, dynamic> json) {
    List<DiseaseHierarchyNode> hierarchy = [];
    for (var node in json['hierarchy']) {
      hierarchy.add(DiseaseHierarchyNode.fromJson(node));
    }
    return DiseaseHierarchy(hierarchy: hierarchy);
  }
}

class DiseaseHierarchyNode {
  final Disease disease;
  final int nodeLevel;

  DiseaseHierarchyNode({
    @required this.disease,
    @required this.nodeLevel,
  });

  factory DiseaseHierarchyNode.fromJson(Map<String, dynamic> json) {
    return DiseaseHierarchyNode(
        disease: Disease.fromJson(json['self']), nodeLevel: json['nodeLevel']);
  }
}
