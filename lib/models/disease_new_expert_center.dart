import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';

class DiseaseNewExpertCenter {
  final Disease disease;
  final NewExpertCenter expertCenter;
  final String level;
  final bool isPrefParent;

  DiseaseNewExpertCenter({
    this.disease,
    this.expertCenter,
    this.level,
    this.isPrefParent,
  });

  factory DiseaseNewExpertCenter.fromJson(Map<String, dynamic> json) {
    Disease disease;
    if (json['disease'] != null) {
      disease = Disease.fromJson(json['disease']);
    }

    NewExpertCenter expertCenter;
    if (json['expertCenter'] != null) {
      expertCenter = NewExpertCenter.fromJson(json['expertCenter']);
    }

    return DiseaseNewExpertCenter(
      disease: disease,
      expertCenter: expertCenter,
      level: json['level'],
      isPrefParent: json['isPreferentialParent'],
    );
  }
}
