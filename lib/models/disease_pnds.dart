import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/disease_pnds_document.dart';

class DiseasePnds {
  final String id;
  final String title;
  final String hyperLink;
  final String validationMonthDate;
  final String minValidationDate;
  final String seniority;
  final List<DiseasePndsDocument> documents;

  DiseasePnds({
    @required this.id,
    @required this.title,
    this.hyperLink,
    this.validationMonthDate,
    this.minValidationDate,
    this.seniority,
    this.documents,
  });

  factory DiseasePnds.fromJson(Map<String, dynamic> json) {
    List<DiseasePndsDocument> documents = [];
    if (json['diseasePndsDocuments'] != null &&
        json['diseasePndsDocuments']['totalCount'] > 0) {
      for (var doc in json['diseasePndsDocuments']['data']) {
        documents.add(DiseasePndsDocument.fromJson(doc));
      }
    }

    return DiseasePnds(
      id: json['id'],
      title: json['title'],
      hyperLink: json['hyperLink'],
      validationMonthDate: json['validationMonthDate'],
      minValidationDate: json['minValidationDate'],
      seniority: json['seniority'],
      documents: documents,
      // geneCount: json['genes'] == null ? 0 : json['genes']['totalCount'],
      // phenotypeCount:
      //     json['phenotypes'] == null ? 0 : json['phenotypes']['totalCount'],
      // academyCount: 0,
      // scoring: json['scoring'] ?? 0,
    );
  }
}
