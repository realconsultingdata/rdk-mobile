import 'package:flutter/foundation.dart';

class DiseasePndsDocument {
  final String id;
  final String name;
  final String hyperLink;

  DiseasePndsDocument({
    @required this.id,
    @required this.name,
    this.hyperLink,
  });

  factory DiseasePndsDocument.fromJson(Map<String, dynamic> json) {
    return DiseasePndsDocument(
      id: json['id'],
      name: json['name'],
      hyperLink: json['hyperLink'],
    );
  }
}
