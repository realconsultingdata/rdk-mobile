import 'package:flutter/foundation.dart';

class DiseaseRelation {
  final String reference;
  final String source;
  final String mapping;
  final String mappingIcd;

  DiseaseRelation({
    @required this.reference,
    @required this.source,
    @required this.mapping,
    this.mappingIcd,
  });

  factory DiseaseRelation.fromJson(Map<String, dynamic> json) {
    return DiseaseRelation(
        reference: json['reference'],
        source: json['source'],
        mapping: json['mapping'],
        mappingIcd: json['mappingIcd']);
  }
}
