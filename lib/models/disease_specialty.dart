import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/disease_specialty_translation.dart';

class DiseaseSpecialty {
  final int id;
  final String specialtyLabel;
  final List<DiseaseSpecialtyTranslation> translations;

  DiseaseSpecialty({
    @required this.id,
    @required this.specialtyLabel,
    this.translations,
  });

  factory DiseaseSpecialty.fromJson(Map<String, dynamic> json) {
    List<DiseaseSpecialtyTranslation> translations = [];
    if (json['translations'] != null &&
        json['translations']['totalCount'] > 0) {
      for (var translation in json['translations']['data']) {
        translations.add(DiseaseSpecialtyTranslation.fromJson(translation));
      }
    }

    return DiseaseSpecialty(
      id: json['id'],
      specialtyLabel: json['specialtyLabel'],
      translations: translations,
    );
  }
}
