import 'package:flutter/foundation.dart';

class DiseaseSpecialtyTranslation {
  final String language;
  final String label;

  DiseaseSpecialtyTranslation({
    @required this.label,
    @required this.language,
  });

  factory DiseaseSpecialtyTranslation.fromJson(Map<String, dynamic> json) {
    return DiseaseSpecialtyTranslation(
      label: json['label'],
      language: json['language'],
    );
  }
}
