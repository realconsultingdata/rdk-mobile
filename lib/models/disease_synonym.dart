import 'package:flutter/foundation.dart';

class DiseaseSynonym {
  final String language;
  final String synonym;

  DiseaseSynonym({@required this.language, @required this.synonym});

  factory DiseaseSynonym.fromJson(Map<String, dynamic> json) {
    return DiseaseSynonym(language: json['language'], synonym: json['synonym']);
  }
}
