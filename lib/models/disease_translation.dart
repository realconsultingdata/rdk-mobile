import 'package:flutter/foundation.dart';

class DiseaseTranslation {
  final String name;
  final String contents;
  final String disorderType;
  final String disorderGroup;
  final String textSectionType;
  final String language;

  DiseaseTranslation(
      {@required this.name,
      @required this.contents,
      @required this.language,
      this.disorderType,
      this.disorderGroup,
      this.textSectionType});

  factory DiseaseTranslation.fromJson(Map<String, dynamic> json) {
    return DiseaseTranslation(
      name: json['name'],
      contents: json['contents'],
      language: json['language'],
      disorderType: json['disorderType'],
      disorderGroup: json['disorderGroup'],
      textSectionType: json['textSectionType'],
    );
  }
}
