import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/institution.dart';

class Establishment {
  final String id;
  final String label;
  final String odId;
  final String legalId;
  final String city;
  final String zipCode;
  final String category;
  final double longitude;
  final double latitude;
  final String streetNumber;
  final String streetIndex;
  final String streetType;
  final String streetLabel;
  final int departmentCode;
  final String departmentName;
  final String region;
  final List<Institution> institutions;

  Establishment(
      {@required this.id,
      @required this.label,
      this.odId,
      this.legalId,
      this.city,
      this.zipCode,
      this.category,
      this.longitude,
      this.latitude,
      this.streetNumber,
      this.streetIndex,
      this.streetType,
      this.streetLabel,
      this.departmentCode,
      this.departmentName,
      this.region,
      this.institutions});

  factory Establishment.fromJson(Map<String, dynamic> json) {
    List<Institution> institutions = [];
    if (json['institutions'] != null &&
        json['institutions']['totalCount'] > 0) {
      for (var data in json['institutions']['data']) {
        institutions.add(Institution.fromJson(data));
      }
    }

    return Establishment(
      id: json['id'],
      label: json['label'],
      odId: json['odId'],
      legalId: json['legalId'],
      city: json['city'],
      zipCode: json['zipCode'],
      category: json['category'],
      longitude: json['longitude'],
      latitude: json['latitude'],
      streetNumber: json['streetNumber'],
      streetIndex: json['streetIndex'],
      streetType: json['streetType'],
      streetLabel: json['streetLabel'],
      departmentCode: json['departmentCode'],
      departmentName: json['departmentName'],
      region: json['region'],
      institutions: institutions,
    );
  }
}
