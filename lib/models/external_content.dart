import 'package:flutter/foundation.dart';

import 'network.dart';

class ExternalContent {
  final String id;
  final String lang;
  final String journal;
  final String title;
  final String citation;
  final String doi;
  final String url;
  final String validationDate;
  final String online;
  final String type;
  final List<Network> networks;

  ExternalContent({
    @required this.id,
    @required this.lang,
    @required this.journal,
    @required this.title,
    @required this.type,
    this.citation,
    this.doi,
    this.url,
    this.validationDate,
    this.online,
    this.networks,
  });

  factory ExternalContent.fromJson(Map<String, dynamic> json) {
    List<Network> networks = [];
    if (json['networks'] != null &&
        json['networks']['totalCount'] > 0) {
      for (var data in json['networks']['data']) {
        networks.add(Network.fromJson(data));
      }
    }

    return ExternalContent(
        id: json['id'],
        lang: json['lang'],
        journal: json['journal'],
        title: json['title'],
        type: json['type'],
        citation: json['citation'],
        doi: json['doi'],
        url: json['url'],
        validationDate: json['validationDate'],
        online: json['online'],
        networks: networks);
  }
}
