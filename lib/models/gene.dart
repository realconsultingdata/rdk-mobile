import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/gene_synonym.dart';

import 'gene_disease.dart';

class Gene {
  final String symbol;
  final String name;
  final String type;
  final String locus;
  // final List<Disease> diseases;
  final List<GeneDisease> diseases;
  final List<GeneSynonym> synonyms;
  final int diseaseCount;
  final int academyCount;

  Gene(
      {@required this.symbol,
      @required this.name,
      this.type,
      this.locus,
      this.diseases,
      this.synonyms,
      this.diseaseCount,
      this.academyCount});

  factory Gene.fromJson(Map<String, dynamic> json) {
    List<GeneDisease> diseases = [];
    if (json['diseases'] != null && json['diseases']['totalCount'] > 0) {
      for (var disease in json['diseases']['data']) {
        diseases.add(GeneDisease.fromJson(disease));
      }
    }

    List<GeneSynonym> synonyms = [];
    if (json['synonyms'] != null && json['synonyms']['totalCount'] > 0) {
      for (var syn in json['synonyms']['data']) {
        synonyms.add(GeneSynonym.fromJson(syn));
      }
    }

    return Gene(
      symbol: json['symbol'],
      name: json['name'],
      type: json['type'],
      locus: json['locus'],
      diseases: diseases,
      synonyms: synonyms,
      diseaseCount:
          json['diseases'] == null ? 0 : json['diseases']['totalCount'],
      academyCount: 0,
    );
  }
}
