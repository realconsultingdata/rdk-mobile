import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/gene.dart';

class GeneDisease {
  final Disease disease;
  final Gene gene;
  final Disease childDisease;
  final String associationType;
  final String associationStatus;

  GeneDisease({
    @required this.disease,
    @required this.gene,
    this.childDisease,
    this.associationType,
    this.associationStatus,
  });

  factory GeneDisease.fromJson(Map<String, dynamic> json) {
    Disease disease;
    if (json['disease'] != null) {
      disease = Disease.fromJson(json['disease']);
    }

    Gene gene;
    if (json['gene'] != null) {
      gene = Gene.fromJson(json['gene']);
    }

    Disease childDisease;
    if (json['childDisease'] != null) {
      childDisease = Disease.fromJson(json['childDisease']);
    }

    return GeneDisease(
        disease: disease,
        gene: gene,
        childDisease: childDisease,
        associationType: json['associationType'],
        associationStatus: json['associationStatus']);
  }
}
