import 'package:flutter/foundation.dart';

class GeneSynonym {
  final String synonym;

  GeneSynonym({@required this.synonym});

  factory GeneSynonym.fromJson(Map<String, dynamic> json) {
    return GeneSynonym(synonym: json['geneSynonym']);
  }
}
