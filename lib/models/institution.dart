import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/network.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';

class Institution {
  final String id;
  final String departmentName;
  final String departmentAcronym;
  final String upperLevelAffiliation;
  final String upperLevelAffiliationAcronym;
  final String validationDate;
  final String contact1;
  final String contact2;
  final String fax;
  final String email;
  final String url;
  final Establishment establishment;
  final List<NewExpertCenter> expertCenters;
  final List<Network> networks;

  Institution(
      {@required this.id,
      @required this.departmentName,
      this.departmentAcronym,
      this.upperLevelAffiliation,
      this.upperLevelAffiliationAcronym,
      this.validationDate,
      this.contact1,
      this.contact2,
      this.fax,
      this.email,
      this.url,
      this.establishment,
      this.expertCenters,
      this.networks});

  factory Institution.fromJson(Map<String, dynamic> json) {
    Establishment establishment;
    if (json['establishment'] != null) {
      establishment = Establishment.fromJson(json['establishment']);
    }

    List<NewExpertCenter> expertCenters = [];
    if (json['expertCenters'] != null &&
        json['expertCenters']['totalCount'] > 0) {
      for (var data in json['expertCenters']['data']) {
        expertCenters.add(NewExpertCenter.fromJson(data));
      }
    }

    List<Network> networks = [];
    if (json['networks'] != null && json['networks']['totalCount'] > 0) {
      for (var data in json['networks']['data']) {
        networks.add(Network.fromJson(data));
      }
    }

    return Institution(
      id: json['id'],
      departmentName: json['departmentName'],
      departmentAcronym: json['departmentAcronym'],
      upperLevelAffiliation: json['upperLevelAffiliation'],
      upperLevelAffiliationAcronym: json['upperLevelAffiliationAcronym'],
      validationDate: json['validationDate'],
      contact1: json['contact1'],
      contact2: json['contact2'],
      fax: json['fax'],
      email: json['email'],
      url: json['url'],
      establishment: establishment,
      expertCenters: expertCenters,
      networks: networks,
    );
  }
}
