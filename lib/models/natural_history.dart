import 'package:flutter/foundation.dart';

class NaturalHistory {
  final String avgAgeOnset;
  final String inheritanceType;

  NaturalHistory({
    @required this.avgAgeOnset,
    @required this.inheritanceType,
  });

  factory NaturalHistory.fromJson(Map<String, dynamic> json) {
    return NaturalHistory(
        avgAgeOnset: json['avgAgeOnset'],
        inheritanceType: json['inheritanceType']);
  }
}
