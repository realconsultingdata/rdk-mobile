import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/institution.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/models/status_flag.dart';

class Network {
  final String id;
  final String name;
  final String nameEn;
  final String url;
  final String validationDate;
  final String subType;
  final String filiereAcronym;
  final bool isErn;
  final List<NewExpertCenter> expertCenters;
  final List<Institution> institutions;
  final List<StatusFlag> statusFlags;

  Network({
    @required this.id,
    @required this.name,
    this.nameEn,
    this.url,
    this.validationDate,
    this.subType,
    this.expertCenters,
    this.institutions,
    this.statusFlags,
    this.filiereAcronym,
    this.isErn,
  });

  factory Network.fromJson(Map<String, dynamic> json) {
    List<NewExpertCenter> expertCenters = [];
    if (json['expertCenters'] != null &&
        json['expertCenters']['totalCount'] > 0) {
      for (var data in json['expertCenters']['data']) {
        expertCenters.add(NewExpertCenter.fromJson(data));
      }
    }

    List<Institution> institutions = [];
    if (json['institutions'] != null &&
        json['institutions']['totalCount'] > 0) {
      for (var data in json['institutions']['data']) {
        institutions.add(Institution.fromJson(data));
      }
    }

    List<StatusFlag> statusFlags = [];
    if (json['statusFlags'] != null && json['statusFlags']['totalCount'] > 0) {
      for (var data in json['statusFlags']['data']) {
        statusFlags.add(StatusFlag.fromJson(data));
      }
    }

    return Network(
      id: json['id'],
      name: json['name'],
      nameEn: json['nameEn'],
      url: json['url'],
      validationDate: json['validationDate'],
      subType: json['subType'],
      expertCenters: expertCenters,
      institutions: institutions,
      statusFlags: statusFlags,
      filiereAcronym: json['filiereAcronym'],
      isErn: json['isErn'],
    );
  }
}
