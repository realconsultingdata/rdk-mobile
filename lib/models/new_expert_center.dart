import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/disease_specialty.dart';
import 'package:rdk_mobile/models/status_flag.dart';

import 'disease_new_expert_center.dart';
import 'institution.dart';
import 'network.dart';

class NewExpertCenter {
  final String id;
  final String name;
  final String nameEn;
  final String url;
  final String expertLink;
  final String validationDate;
  final List<DiseaseNewExpertCenter> diseases;
  // final List<Disease> diseases;
  final List<Institution> institutions;
  final List<Network> networks;
  final List<StatusFlag> statusFlags;
  final List<DiseaseSpecialty> specialties;

  NewExpertCenter({
    @required this.id,
    @required this.name,
    this.nameEn,
    this.url,
    this.expertLink,
    this.validationDate,
    this.diseases,
    this.institutions,
    this.networks,
    this.statusFlags,
    this.specialties,
  });

  factory NewExpertCenter.fromJson(Map<String, dynamic> json) {
    List<DiseaseNewExpertCenter> diseases = [];
    if (json['diseases'] != null && json['diseases']['totalCount'] > 0) {
      for (var data in json['diseases']['data']) {
        diseases.add(DiseaseNewExpertCenter.fromJson(data));
      }
    }

    List<Institution> institutions = [];
    if (json['institutions'] != null &&
        json['institutions']['totalCount'] > 0) {
      for (var data in json['institutions']['data']) {
        institutions.add(Institution.fromJson(data));
      }
    }

    List<Network> networks = [];
    if (json['networks'] != null && json['networks']['totalCount'] > 0) {
      for (var data in json['networks']['data']) {
        networks.add(Network.fromJson(data));
      }
    }

    List<StatusFlag> statusFlags = [];
    if (json['statusFlags'] != null && json['statusFlags']['totalCount'] > 0) {
      for (var data in json['statusFlags']['data']) {
        statusFlags.add(StatusFlag.fromJson(data));
      }
    }

    List<DiseaseSpecialty> specialties = [];
    if (json['specialties'] != null && json['specialties']['totalCount'] > 0) {
      for (var data in json['specialties']['data']) {
        specialties.add(DiseaseSpecialty.fromJson(data));
      }
    }

    return NewExpertCenter(
        id: json['id'],
        name: json['name'],
        nameEn: json['nameEn'],
        url: json['url'],
        expertLink: json['expertLink'],
        validationDate: json['validationDate'],
        diseases: diseases,
        institutions: institutions,
        networks: networks,
        statusFlags: statusFlags,
        specialties: specialties);
  }
}
