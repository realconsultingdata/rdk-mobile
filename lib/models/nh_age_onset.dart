import 'package:flutter/foundation.dart';

class NhAgeOnset {
  final String id;
  final String averageAgeOnsetEn;
  final String averageAgeOnsetFr;

  NhAgeOnset(
      {@required this.id, this.averageAgeOnsetEn, this.averageAgeOnsetFr});

  factory NhAgeOnset.fromJson(Map<String, dynamic> json) {
    return NhAgeOnset(
        id: json['id'],
        averageAgeOnsetEn: json['averageAgeOnsetEn'],
        averageAgeOnsetFr: json['averageAgeOnsetFr']);
  }
}
