import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/phenotype_disease.dart';
import 'package:rdk_mobile/models/phenotype_hierarchy.dart';
import 'package:rdk_mobile/models/phenotype_synonym.dart';
import 'package:rdk_mobile/models/phenotype_translation.dart';

class Phenotype {
  final String id;
  final String hpoTerm;
  final String contents;
  final String category;
  final List<PhenotypeDisease> diseases;
  final List<PhenotypeSynonym> synonyms;
  final List<PhenotypeHierarchy> hierarchies;
  final List<PhenotypeTranslation> translations;
  final int diseaseCount;
  final int academyCount;

  Phenotype({
    @required this.id,
    @required this.hpoTerm,
    this.contents,
    this.category,
    this.diseases,
    this.synonyms,
    this.hierarchies,
    this.translations,
    this.diseaseCount,
    this.academyCount,
  });

  factory Phenotype.fromJson(Map<String, dynamic> json) {
    List<PhenotypeDisease> diseases = [];
    if (json['diseases'] != null && json['diseases']['totalCount'] > 0) {
      for (var data in json['diseases']['data']) {
        diseases.add(PhenotypeDisease.fromJson(data));
      }
    }

    List<PhenotypeSynonym> synonyms = [];
    if (json['synonyms'] != null && json['synonyms']['totalCount'] > 0) {
      for (var syn in json['synonyms']['data']) {
        synonyms.add(PhenotypeSynonym.fromJson(syn));
      }
    }

    List<PhenotypeHierarchy> hierarchies = [];
    if (json['hierarchy'] != null && json['hierarchy']['totalCount'] > 0) {
      for (var data in json['hierarchy']['data']) {
        hierarchies.add(PhenotypeHierarchy.fromJson(data));
      }
    }

    List<PhenotypeTranslation> translations = [];
    if (json['translations'] != null &&
        json['translations']['totalCount'] > 0) {
      for (var translation in json['translations']['data']) {
        translations.add(PhenotypeTranslation.fromJson(translation));
      }
    }

    return Phenotype(
      id: json['id'],
      hpoTerm: json['hpoTerm'],
      contents: json['contents'],
      category: json['category'],
      diseases: diseases,
      synonyms: synonyms,
      hierarchies: hierarchies,
      translations: translations,
      diseaseCount:
          json['diseases'] == null ? 0 : json['diseases']['totalCount'],
      academyCount: 0,
    );
  }
}
