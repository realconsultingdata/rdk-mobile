import 'package:rdk_mobile/models/phenotype.dart';

import 'disease.dart';

class PhenotypeDisease {
  final Phenotype phenotype;
  final Disease disease;
  final String diagnostic;
  final String frequency;

  PhenotypeDisease({
    this.phenotype,
    this.disease,
    this.diagnostic,
    this.frequency,
  });

  factory PhenotypeDisease.fromJson(Map<String, dynamic> json) {
    Phenotype phenotype;
    if (json['phenotype'] != null) {
      phenotype = Phenotype.fromJson(json['phenotype']);
    }

    Disease disease;
    if (json['disease'] != null) {
      disease = Disease.fromJson(json['disease']);
    }

    return PhenotypeDisease(
      phenotype: phenotype,
      disease: disease,
      diagnostic: json['diagnostic'],
      frequency: json['frequency'],
    );
  }
}
