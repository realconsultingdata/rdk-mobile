import 'package:flutter/foundation.dart';

import 'phenotype.dart';

class PhenotypeHierarchy {
  final List<PhenotypeHierarchyNode> hierarchy;

  PhenotypeHierarchy({
    @required this.hierarchy,
  });

  factory PhenotypeHierarchy.fromJson(Map<String, dynamic> json) {
    List<PhenotypeHierarchyNode> hierarchy = [];
    for (var node in json['hierarchy']) {
      hierarchy.add(PhenotypeHierarchyNode.fromJson(node));
    }
    return PhenotypeHierarchy(hierarchy: hierarchy);
  }
}

class PhenotypeHierarchyNode {
  final Phenotype phenotype;
  final int nodeLevel;

  PhenotypeHierarchyNode({
    @required this.phenotype,
    @required this.nodeLevel,
  });

  factory PhenotypeHierarchyNode.fromJson(Map<String, dynamic> json) {
    return PhenotypeHierarchyNode(
        phenotype: Phenotype.fromJson(json['self']), nodeLevel: json['nodeLevel']);
  }
}
