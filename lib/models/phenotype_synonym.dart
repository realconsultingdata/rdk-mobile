import 'package:flutter/foundation.dart';

class PhenotypeSynonym {
  final String language;
  final String synonym;
  final String translationSource;

  PhenotypeSynonym(
      {@required this.language,
      @required this.synonym,
      this.translationSource});

  factory PhenotypeSynonym.fromJson(Map<String, dynamic> json) {
    return PhenotypeSynonym(
        language: json['language'],
        synonym: json['synonym'],
        translationSource: json['translationSource']);
  }
}
