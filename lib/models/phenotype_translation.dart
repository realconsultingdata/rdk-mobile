import 'package:flutter/foundation.dart';

class PhenotypeTranslation {
  final String hpoTerm;
  final String contents;
  final String language;
  final String category;
  final String hpoTermTranslationSource;
  final String contentsTranslationSource;

  PhenotypeTranslation({
    @required this.hpoTerm,
    this.contents,
    this.language,
    this.category,
    this.hpoTermTranslationSource,
    this.contentsTranslationSource,
  });

  factory PhenotypeTranslation.fromJson(Map<String, dynamic> json) {
    return PhenotypeTranslation(
      hpoTerm: json['hpoTerm'],
      contents: json['contents'],
      language: json['language'],
      category: json['category'],
      hpoTermTranslationSource: json['hpoTermTranslationSource'],
      contentsTranslationSource: json['contentsTranslationSource'],
    );
  }
}
