import 'package:flutter/foundation.dart';

class Prevalence {
  final String prevalenceGeographic;
  final String prevalenceType;
  final String prevalenceQualification;
  final String prevalenceClass;
  final String validationStatus;
  final num valMoy;

  Prevalence({
    @required this.prevalenceGeographic,
    @required this.prevalenceType,
    @required this.prevalenceQualification,
    @required this.prevalenceClass,
    @required this.validationStatus,
    @required this.valMoy,
  });

  factory Prevalence.fromJson(Map<String, dynamic> json) {
    return Prevalence(
        prevalenceGeographic: json['prevalenceGeographic'],
        prevalenceType: json['prevalenceType'],
        prevalenceQualification: json['prevalenceQualification'],
        prevalenceClass: json['prevalenceClass'],
        validationStatus: json['validationStatus'],
        valMoy: json['valMoy']);
  }
}
