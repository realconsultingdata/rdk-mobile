import 'package:flutter/foundation.dart';

class Publication {
  final String pmId;
  final String title;
  final String link;
  final String source;
  final String abstract;
  final List<String> authorList;
  final List<String> types;
  final String publishedDate;

  Publication({
    @required this.pmId,
    @required this.title,
    this.link,
    this.source,
    this.abstract,
    this.authorList,
    this.types,
    this.publishedDate,
  });
}
