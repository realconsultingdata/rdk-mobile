import 'package:flutter/foundation.dart';

class RdkSource {
  final String name;
  final String abbr;
  final String country;
  final String countryCode;
  final String sourceTypeEn;
  final String sourceTypeFr;
  final DateTime updatedAt;
  final String aboutEn;
  final String aboutFr;
  final String link;
  final List<String> usedCategories;

  RdkSource({
    @required this.name,
    @required this.abbr,
    this.country,
    this.countryCode,
    this.sourceTypeEn,
    this.sourceTypeFr,
    this.updatedAt,
    this.aboutEn,
    this.aboutFr,
    this.link,
    this.usedCategories,
  });
}
