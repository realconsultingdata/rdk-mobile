import 'package:flutter/foundation.dart';

class StatusFlag {
  final String id;
  final String name;

  StatusFlag({
    @required this.id,
    @required this.name,
  });

  factory StatusFlag.fromJson(Map<String, dynamic> json) {
    return StatusFlag(
      id: json['id'],
      name: json['name'],
    );
  }
}
