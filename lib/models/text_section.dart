import 'package:flutter/foundation.dart';

class TextSection {
  final String lang;
  final String type;
  final String contents;
  final int order;
  final String validationDate;

  TextSection({
    @required this.lang,
    @required this.type,
    @required this.contents,
    this.order,
    this.validationDate,
  });

  factory TextSection.fromJson(Map<String, dynamic> json) {
    return TextSection(
        lang: json['lang'],
        type: json['type'],
        contents: json['contents'],
        order: json['order'],
        validationDate: json['validationDate']);
  }
}
