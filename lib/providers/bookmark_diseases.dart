import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/helpers/database_helper.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/providers/diseases.dart';

class BookmarkDiseases with ChangeNotifier {
  List<Disease> _diseases = [];
  DatabaseHelper _dbHelper = DatabaseHelper();

  List<Disease> get diseases => _diseases;

  Future<int> addDisease(BuildContext context, Disease disease) async {
    int result = await _dbHelper.insertBookmarkDisease(disease);
    await getDiseases(context);
    notifyListeners();
    return result;
  }

  Future<void> getDiseases(BuildContext context) async {
    _diseases = await _dbHelper.retrieveBookmarkDiseases();
    await Provider.of<Diseases>(context, listen: false).getBookmarkDiseases(
        context: context,
        orphaNumbers: _diseases.map((disease) => disease.orphaNumber).toList());
    // notifyListeners();
  }

  Future<int> deleteDisease(BuildContext context, String orphaNumber) async {
    int result = await _dbHelper.deleteBookmarkDisease(orphaNumber);
    await getDiseases(context);
    notifyListeners();
    return result;
  }

  Future<Disease> findDiseaseById(String orphaNumber) async {
    Disease disease = await _dbHelper.findBookmarkDisease(orphaNumber);
    // notifyListeners();
    return disease;
  }
}
