import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../app.dart';

class CurrentLocale with ChangeNotifier {
  Locale _currentLocale;

  Locale currentLocale() {
    _currentLocale = FlutterI18n.currentLocale(MyApp.appKey.currentContext);
    return _currentLocale;
  }

  void setLocale(Locale locale) async {
    _currentLocale = locale;
    await FlutterI18n.refresh(MyApp.appKey.currentContext, _currentLocale);
    notifyListeners();
  }
}
