import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rdk_mobile/dto/diagnostic_disease.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_translation.dart';

class Diseases with ChangeNotifier {
  List<DiagnosticDisease> _diagnosticDiseases = [];
  List<Disease> _bookmarkDiseases = [];
  int _dataCount = 0;
  int _bookmarkCount = 0;

  List<DiagnosticDisease> get diagnosticDiseases {
    return [..._diagnosticDiseases];
  }

  int get dataCount {
    return _dataCount;
  }

  int get bookmarkCount {
    return _bookmarkCount;
  }

  String _searchString = '';

  UnmodifiableListView<Disease> bookmarkDiseases(String language) {
    if (_searchString.isEmpty) {
      return UnmodifiableListView(_bookmarkDiseases);
    } else {
      if (num.tryParse(_searchString) != null) {
        return UnmodifiableListView(_bookmarkDiseases.where((disease) =>
            disease.orphaNumber.contains(_searchString) ||
            disease.name.contains(_searchString)));
      }
      if (language == 'en') {
        return UnmodifiableListView(_bookmarkDiseases.where((disease) =>
            disease.name.toLowerCase().contains(_searchString.toLowerCase()) ||
            disease.synonyms.any((syn) =>
                syn.language == 'en' &&
                syn.synonym
                    .toLowerCase()
                    .contains(_searchString.toLowerCase()))));
      } else {
        return UnmodifiableListView(_bookmarkDiseases.where((disease) {
          DiseaseTranslation translation = disease.translations.firstWhere(
              (trans) => trans.language == language,
              orElse: () => null);
          if (translation == null) {
            return disease.name
                    .toLowerCase()
                    .contains(_searchString.toLowerCase()) ||
                disease.synonyms.any((syn) =>
                    syn.language == 'en' &&
                    syn.synonym
                        .toLowerCase()
                        .contains(_searchString.toLowerCase()));
          } else {
            return removeDiacritics(translation.name)
                    .toLowerCase()
                    .contains(removeDiacritics(_searchString).toLowerCase()) ||
                disease.synonyms.any((syn) =>
                    syn.language == language &&
                    removeDiacritics(syn.synonym).toLowerCase().contains(
                        removeDiacritics(_searchString).toLowerCase()));
          }
        }));
      }
    }
  }

  void changeSearchString(String searchString) {
    _searchString = searchString;
    notifyListeners();
  }

  // static FutureOr<Iterable<Disease>> fetchDiseases(
  //     BuildContext context, String pattern, String language) {
  //   const String searchDiseaseQuery = """
  //     query searchDisease(\$param: String!, \$lang: String) {
  //       searchDisease(name: \$param, lang: \$lang) {
  //         totalCount
  //         data {
  //           orphaNumber
  //           name
  //           disorderType
  //           disorderGroup
  //           translations {
  //             totalCount
  //             data {
  //               name
  //               language
  //             }
  //           }
  //           synonyms {
  //             totalCount
  //             data {
  //               synonym
  //               language
  //             }
  //           }
  //         }
  //       }
  //     }
  //   """;
  //
  //   try {
  //     GraphQLClient client = GraphQLProvider.of(context).value;
  //     final result = client.query(
  //       QueryOptions(
  //         document: gql(searchDiseaseQuery),
  //         variables: {'param': pattern, 'lang': language},
  //       ),
  //     );
  //     List<Disease> loadedDiseases = [];
  //     return result.then((res) {
  //       if (res.hasException) {
  //         throw HttpStatus.internalServerError;
  //       }
  //       if (res.data['searchDisease'] != null) {
  //         List diseases = res.data['searchDisease']['data'];
  //         for (var element in diseases) {
  //           loadedDiseases.add(Disease.fromJson(element));
  //         }
  //       }
  //       return loadedDiseases;
  //     });
  //   } catch (error) {
  //     throw (error);
  //   }
  // }

  Future<Disease> getDiseaseById(BuildContext context, String orphaNumber,
      String language, bool withChildGene) async {
    const String getDiseaseQuery = """
      query orphaDiseaseById(\$orphaNumber: ID!, \$lang: String!, \$withChild: Boolean) {
          orphaDisease(orphaNumber: \$orphaNumber) {
              orphaNumber
              name
              contents
              disorderGroup
              disorderType
              synonyms {
                  totalCount
                  data {
                      language
                      synonym
                  }
              }
              preferentialParents {
                  totalCount
                  data {
                      orphaNumber
                      name
                      translations {
                          totalCount
                          data {
                              language
                              name
                          }
                      }
                  }
              }
              textSections(lang: \$lang) {
                  totalCount
                  data {
                      lang
                      type
                      order
                      contents
                      validationDate
                  }
              }
              externalContents(onlyEmergencyGuidelines: true) {
                  totalCount
                  data {
                      title
                      lang
                      journal
                      url
                      validationDate
                      online
                      networks {
                          totalCount
                          data {
                              id
                              name
                          }
                      }
                  }
              }
              authors {
                  totalCount
                  data {
                      firstName
                      lastName
                      title
                      network {
                          id
                          name
                          nameEn
                      }
                  }
              }
              relation {
                  totalCount
                  data {
                      reference
                      source
                      mapping
                      mappingIcd
                  }
              }
              epidemiology {
                  totalCount
                  data {
                      prevalenceGeographic
                      prevalenceType
                      prevalenceQualification
                      prevalenceClass
                      validationStatus
                      valMoy
                  }
              }
              histories {
                  totalCount
                  data {
                      avgAgeOnset
                      inheritanceType
                  }
              }
              phenotypes {
                  totalCount
                  data {
                      phenotype {
                          id
                          hpoTerm
                          category
                          translations {
                              totalCount
                              data {
                                  hpoTerm
                                  language
                                  category
                                  hpoTermTranslationSource
                              }
                          }
                      }
                      diagnostic
                      frequency
                  }
              }
              genes(withChild: \$withChild) {
                  totalCount
                  data {
                      gene {
                          symbol
                          name
                          type
                          synonyms {
                              totalCount
                              data {
                                  synonymSymbol
                                  geneSynonym
                              }
                          }
                      }
                      childDisease {
                          orphaNumber
                          name
                          translations {
                              totalCount
                              data {
                                  language
                                  name
                                  contents
                                  disorderGroup
                                  disorderType
                              }
                          }
                      }
                      associationStatus
                      associationType(lang: \$lang)
                  }
              }
              translations {
                  totalCount
                  data {
                      language
                      name
                      contents
                      disorderGroup
                      disorderType
                  }
              }
              hierarchy {
                  totalCount
                  data {
                      hierarchy {
                          self {
                              orphaNumber
                              name
                              contents
                              disorderGroup
                              disorderType
                              translations {
                                  totalCount
                                  data {
                                      name
                                      contents
                                      disorderGroup
                                      disorderType
                                      language
                                  }
                              }
                          }
                      nodeLevel
                      }
                  }
              }
              diseasePnds {
                  totalCount
                  data {
                      id
                      title
                      seniority
                      minValidationDate
                      validationMonthDate
                      hyperLink
                      diseasePndsDocuments {
                          totalCount
                          data {
                              id
                              name
                              hyperLink
                          }
                      }
                  }
              }
              newExpertCenters{
                  totalCount
                  data{
                      level
                      isPreferentialParent
                      expertCenter {
                          id
                          name
                          nameEn
                          url
                          expertLink
                          validationDate
                          statusFlags {
                              totalCount
                              data{
                                  id
                                  name
                              }
                          }
                          specialties{
                              totalCount
                              data{
                                  id
                                  specialtyLabel
                                  translations {
                                      totalCount
                                      data{
                                          label 
                                          language
                                      }
                                  }
                              }
                          } 
                          networks {
                              totalCount
                              data{
                                  id
                                  nameEn
                                  name
                              }
                          }
                          institutions{
                              totalCount
                              data{
                                  id
                                  departmentName
                                  upperLevelAffiliation
                                  upperLevelAffiliationAcronym
                                  contact1
                                  contact2
                                  fax
                                  email
                                  establishment{
                                      id
                                      odId
                                      legalId 
                                      privateId 
                                      otherId 
                                      category 
                                      city 
                                      zipCode 
                                      label 
                                      ugaId 
                                      longitude 
                                      latitude
                                      departmentCode
                                      departmentName
                                      region
                                      comments
                                      odLabel
                                      legalEntityId
                                      streetNumber
                                      streetIndex
                                      streetType
                                      streetLabel
                                  }
                              }
                          }
                      }
                  }
              }
          }
      }
    """;
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getDiseaseQuery),
          variables: {
            'orphaNumber': orphaNumber,
            'lang': language,
            'withChild': withChildGene
          },
        ),
      );
      Disease loadedDisease;
      if (result.hasException) {
        print(result.exception);
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['orphaDisease'] != null) {
        loadedDisease = Disease.fromJson(result.data['orphaDisease']);
        return loadedDisease;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }

  Future<void> getDiagnosticDiseases(
      {@required BuildContext context,
      @required List phenotypeIds,
      @required List geneSymbols,
      List ageOnsetIds,
      String operator,
      String language,
      int take,
      int skip}) async {
    const String getDiagnosticQuery = """
    query getDiagnosticDiseases(
        \$symbols: [String!]
        \$phenotypeIds: [[String!]!]
        \$phenotypeFilterIds: [ID!]
        \$ageOnsetIds: [String!]
        \$operator: String
        \$take: Int!
        \$skip: Int!
    ) {
        getDiagnosticDiseases(
            geneSymbols: \$symbols
            phenotypeIds: \$phenotypeIds
            ageOnsetIds: \$ageOnsetIds
            operator: \$operator
            take: \$take
            skip: \$skip
        ) {
            totalCount
            data {
                scoring
                disease {
                    orphaNumber
                    name
                    disorderGroup
                    translations {
                        totalCount
                        data {
                            name
                            language
                            disorderGroup
                        }
                    }
                    phenotypes(hpoIds: \$phenotypeFilterIds) {
                        totalCount
                        data {
                            diagnostic
                            frequency
                        }
                    }
                }
            }
        }
    }
    """;
    if (phenotypeIds.isEmpty && geneSymbols.isEmpty && ageOnsetIds.isEmpty) {
      _diagnosticDiseases = [];
      notifyListeners();
      return;
    }
    List phenotypeFilterIds = [];
    for (List ids in phenotypeIds) {
      phenotypeFilterIds.addAll(ids);
    }
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getDiagnosticQuery),
          variables: {
            'symbols': geneSymbols,
            'phenotypeIds': phenotypeIds,
            'phenotypeFilterIds': phenotypeFilterIds,
            'ageOnsetIds': ageOnsetIds,
            'operator': operator,
            'take': take,
            'skip': skip,
            'lang': language
          },
        ),
      );
      List<DiagnosticDisease> loadedDiseases = [];
      if (result.hasException) {
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['getDiagnosticDiseases'] != null) {
        if (result.data['getDiagnosticDiseases']['totalCount'] == 0) {
          _diagnosticDiseases = [null];
          _dataCount = result.data['getDiagnosticDiseases']['totalCount'];
        } else {
          List data = result.data['getDiagnosticDiseases']['data'];
          _dataCount = result.data['getDiagnosticDiseases']['totalCount'];
          for (var element in data) {
            loadedDiseases.add(DiagnosticDisease.fromJson(element));
          }
          if (skip == 0) {
            _diagnosticDiseases = loadedDiseases;
          } else {
            _diagnosticDiseases.addAll(loadedDiseases);
          }
        }
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    } finally {
      notifyListeners();
    }
  }

  Future<void> getBookmarkDiseases(
      {@required BuildContext context,
      @required List<String> orphaNumbers}) async {
    const String getBookmarkDiseasesQuery = """
      query orphaDiseasesByIds(
        \$orphaNumbers: [ID!]!
      ) {
        orphaDiseasesByIds(orphaNumbers: \$orphaNumbers) {
          totalCount
          data {
            orphaNumber
            name
            contents
            disorderGroup
            synonyms {
              totalCount
              data {
                synonym
                language
              }
            }
            translations {
              totalCount
              data {
                name
                contents
                disorderGroup
                language
              }
            }
          }
        }
      }
    """;
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getBookmarkDiseasesQuery),
          variables: {'orphaNumbers': orphaNumbers},
        ),
      );
      List<Disease> loadedDiseases = [];
      if (result.hasException) {
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['orphaDiseasesByIds'] != null) {
        if (result.data['orphaDiseasesByIds']['totalCount'] == 0) {
          _bookmarkDiseases = [];
          _bookmarkCount = 0;
        } else {
          List data = result.data['orphaDiseasesByIds']['data'];
          _bookmarkCount = result.data['orphaDiseasesByIds']['totalCount'];
          for (var element in data) {
            loadedDiseases.add(Disease.fromJson(element));
          }
          _bookmarkDiseases = List.from(loadedDiseases);
        }
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    } finally {
      notifyListeners();
    }
  }
}
