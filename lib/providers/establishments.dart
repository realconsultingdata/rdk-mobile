import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rdk_mobile/models/establishment.dart';

class Establishments with ChangeNotifier {
  List<Establishment> _establishments = [];

  List<Establishment> get establishments {
    return [..._establishments];
  }

  // Future<void> fetchExpertCenters(
  //     BuildContext context, String pattern, int take, int skip) async {
  //   const String searchExpertCenterQuery = """
  //     query searchCenter(\$label: String!, \$take: Int!, \$skip: Int!) {
  //       searchCenter(label: \$label, take: \$take, skip: \$skip) {
  //         totalCount
  //         data {
  //           id
  //           odId
  //           label
  //           zipCode
  //           city
  //           affiliation {
  //             totalCount
  //             data {
  //               expertCenterReference
  //               expertCenterSubsidiaryLabel
  //               expertCenterResponsibleName
  //               expertCenterType
  //             }
  //           }
  //         }
  //       }
  //     }
  //   """;
  //
  //   try {
  //     GraphQLClient client = GraphQLProvider.of(context).value;
  //     QueryResult result = await client.query(QueryOptions(
  //         document: gql(searchExpertCenterQuery),
  //         variables: {'label': pattern, 'take': take, 'skip': skip}));
  //     List<ExpertCenter> loadedCenters = [];
  //     if (result.hasException) {
  //       if (result.exception.linkException.originalException
  //           is SocketException) {
  //         throw HttpStatus.gatewayTimeout;
  //       }
  //       throw HttpStatus.internalServerError;
  //     }
  //     if (result.data['searchCenter'] != null) {
  //       if (result.data['searchCenter']['totalCount'] == 0) {
  //         _expertCenters = [null];
  //       } else {
  //         List centers = result.data['searchCenter']['data'];
  //         for (var element in centers) {
  //           loadedCenters.add(ExpertCenter.fromJson(element));
  //         }
  //         _expertCenters = loadedCenters;
  //       }
  //     } else {
  //       throw HttpStatus.notFound;
  //     }
  //   } catch (error) {
  //     throw (error);
  //   } finally {
  //     notifyListeners();
  //   }
  // }

  Future<Establishment> getEstablishmentById(
      BuildContext context, String id) async {
    const String getEstablishmentsQuery = """
      query getEstablishment(\$id: ID!) {
        establishment(id: \$id) {
          id
          label
          odId
          city
          zipCode
          longitude
          latitude
          streetNumber
          streetIndex
          streetType
          streetLabel
          institutions {
            totalCount
            data {
              id
              departmentName
              email
              contact1
              contact2
              expertCenters {
                totalCount
                data {
                  id
                  name
                  nameEn
                  statusFlags {
                    totalCount
                    data {
                      id
                      name
                    }
                  }
                  diseases {
                    totalCount
                    data {
                      level
                      disease {
                        orphaNumber
                        name
                        disorderGroup
                        disorderType
                        translations {
                          totalCount
                          data {
                            language
                            name
                            disorderGroup
                            disorderType
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    """;
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getEstablishmentsQuery),
          variables: {'id': id},
        ),
      );
      Establishment loadedEstablishment;
      if (result.hasException) {
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['establishment'] != null) {
        loadedEstablishment =
            Establishment.fromJson(result.data['establishment']);
        return loadedEstablishment;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }
}
