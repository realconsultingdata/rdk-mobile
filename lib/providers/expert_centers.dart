import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';

class ExpertCenters with ChangeNotifier {
  List<NewExpertCenter> _expertCenters = [];

  List<NewExpertCenter> get expertCenters {
    return [..._expertCenters];
  }

  Future<NewExpertCenter> getExpertCenterById(
      BuildContext context, String id) async {
    const String getExpertCenterQuery = """
      query getExpertCenterById(\$id: ID!) {
        newExpertCenter(id: \$id) {
          id
          name
          nameEn
          url
          validationDate
          expertLink
          specialties{
            totalCount
            data{
              id
              specialtyLabel
              translations {
                totalCount
                data{
                  label 
                  language
                }
              }
            }
          }
          statusFlags {
            totalCount
            data {
              id
              name
            }
          }
          diseases{
            totalCount
            data{
              disease{
                orphaNumber
                name
                contents
                disorderGroup
                synonyms {
                  totalCount
                  data {
                    language
                    synonym
                  }
                }
                translations {
                  totalCount
                  data {
                    language
                    name
                    contents
                    disorderGroup
                  }
                }
              }
            }
          }
          networks {
            totalCount
            data {
              id
              name
              nameEn
              url
              filiereAcronym
              isErn
            }
          }
          institutions {
            totalCount
            data {
              id
              departmentName
              contact1
              contact2
              email
              establishment {
                id
                label
                odLabel
                streetNumber
                streetIndex
                streetType
                streetLabel
                city
                zipCode
                longitude
                latitude
              }
            }
          }
        }
      }
    """;
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getExpertCenterQuery),
          variables: {'id': id},
        ),
      );
      NewExpertCenter loadedCenter;
      if (result.hasException) {
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['newExpertCenter'] != null) {
        loadedCenter = NewExpertCenter.fromJson(result.data['newExpertCenter']);
        return loadedCenter;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }
}
