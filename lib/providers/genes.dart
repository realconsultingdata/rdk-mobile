import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rdk_mobile/models/gene.dart';

class Genes with ChangeNotifier {
  static FutureOr<Iterable<Gene>> fetchGenes(
      BuildContext context, String pattern) {
    const String searchGeneQuery = """
      query searchGene(\$param: String!) {
        searchGene(name: \$param) {
            totalCount
            data {
                symbol
                name
            }
        }
      }
    """;

    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      final result = client.query(
        QueryOptions(
          document: gql(searchGeneQuery),
          variables: {'param': pattern},
        ),
      );
      List<Gene> loadedGenes = [];
      return result.then((res) {
        if (res.hasException) {
          if (res.exception.linkException.originalException
              is SocketException) {
            throw HttpStatus.gatewayTimeout;
          }
          throw HttpStatus.internalServerError;
        }
        if (res.data['searchGene'] != null) {
          List genes = res.data['searchGene']['data'];
          for (var element in genes) {
            loadedGenes
                .add(Gene(symbol: element['symbol'], name: element['name']));
          }
        }
        return loadedGenes;
      });
    } catch (error) {
      throw (error);
    }
  }

  Future<Gene> getGeneBySymbol(
      BuildContext context, String symbol, String language) async {
    const String getGeneQuery = """
    query geneBySymbol(\$symbol: String!) {
      gene(symbol: \$symbol) {
          symbol
          name
          type
          synonyms {
            totalCount
            data {
              geneSynonym
              synonymSymbol
            }
          }
          diseases {
            totalCount
            data {
              disease {
                orphaNumber
                name
                contents
                disorderGroup
                disorderType
                synonyms {
                  totalCount
                  data {
                    language
                    synonym
                  }
                }
                translations {
                  totalCount
                  data {
                    language
                    name
                    contents
                    disorderGroup
                    disorderType
                  }
                }
              }
            }
          }
      }
    }
    """;
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getGeneQuery),
          variables: {
            'symbol': symbol,
            'lang': language,
          },
        ),
      );
      Gene loadedGene;
      if (result.hasException) {
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['gene'] != null) {
        loadedGene = Gene.fromJson(result.data['gene']);
        return loadedGene;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }

  Future<Gene> getGeneForDiagnostic(BuildContext context, String symbol) async {
    const String getGeneQuery = """
    query geneBySymbol(\$symbol: String!) {
      gene(symbol: \$symbol) {
          symbol
          name
      }
    }
    """;
    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getGeneQuery),
          variables: {'symbol': symbol},
        ),
      );
      Gene loadedGene;
      if (result.hasException) {
        if (result.exception.linkException.originalException
            is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['gene'] != null) {
        loadedGene = Gene.fromJson(result.data['gene']);
        return loadedGene;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }
}
