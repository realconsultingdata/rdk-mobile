import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rdk_mobile/models/nh_age_onset.dart';

class NhAgeOnsets with ChangeNotifier {
  Future<Iterable<NhAgeOnset>> getAllAges(BuildContext context) {
    const String getAllAgeOnsetsQuery = """
      query nhAverageAgeOnsets {
        nhAverageAgeOnsets {
          totalCount
          data {
            id
            averageAgeOnsetEn
            averageAgeOnsetFr
          }
        }
      }
    """;

    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      final result = client.query(
        QueryOptions(
          document: gql(getAllAgeOnsetsQuery),
        ),
      );
      List<NhAgeOnset> loadedAges = [];
      return result.then((res) {
        if (res.hasException) {
          if (res.exception.linkException.originalException
              is SocketException) {
            throw HttpStatus.gatewayTimeout;
          }
          throw HttpStatus.internalServerError;
        }
        if (res.data['nhAverageAgeOnsets'] != null) {
          List ages = res.data['nhAverageAgeOnsets']['data'];
          for (var element in ages) {
            loadedAges.add(NhAgeOnset(
                id: element['id'],
                averageAgeOnsetEn: element['averageAgeOnsetEn'],
                averageAgeOnsetFr: element['averageAgeOnsetFr']));
          }
        }
        return loadedAges;
      });
    } catch (error) {
      throw (error);
    }
  }
}
