import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rdk_mobile/models/phenotype.dart';

class Phenotypes with ChangeNotifier {
  static FutureOr<Iterable<Phenotype>> fetchPhenotype(
      BuildContext context, String pattern, String language) {
    const String searchPhenotypeQuery = """
      query searchPhenotype(\$param: String!, \$lang: String) {
        searchPhenotype(hpoTerm: \$param, lang: \$lang) {
          totalCount
          data {
            id
            hpoTerm
            translations {
                totalCount
                data {
                  language
                  hpoTerm
                }
            }
            synonyms {
                totalCount
                data {
                  language
                  synonym
                }
            }
          }
        }
      }
    """;

    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      final result = client.query(
        QueryOptions(
          document: gql(searchPhenotypeQuery),
          variables: {'param': pattern, 'lang': language},
        ),
      );
      List<Phenotype> loadedPhenotypes = [];
      return result.then((res) {
        if (res.hasException) {
          if (res.exception.linkException.originalException is SocketException) {
            throw HttpStatus.gatewayTimeout;
          }
          throw HttpStatus.internalServerError;
        }
        if (res.data['searchPhenotype'] != null) {
          List phenotypes = res.data['searchPhenotype']['data'];
          for (var element in phenotypes) {
            loadedPhenotypes
                .add(Phenotype.fromJson(element));
          }
        }
        return loadedPhenotypes;
      });
    } catch (error) {
      throw (error);
    }
  }

  Future<Phenotype> getPhenotypeById(
      BuildContext context, String id, String language) async {
    const String getPhenotypeQuery = """
    query phenotypeById(\$id: ID!) {
        phenotype(id: \$id) {
            id
            hpoTerm
            contents
            category
            synonyms {
                totalCount
                data {
                    synonym
                    language
                    translationSource
                }
            }
            hierarchy {
                totalCount
                data {
                    hierarchy {
                        self {
                            id
                            hpoTerm
                            translations {
                                totalCount
                                data {
                                    hpoTerm
                                    language
                                }
                            }
                        }
                        nodeLevel
                    }
                }
            }
            translations {
                totalCount
                data {
                    language
                    hpoTerm
                    contents
                    category
                    hpoTermTranslationSource
                    contentsTranslationSource
                }
            }
            diseases {
                totalCount
                data {
                    disease {
                        orphaNumber
                        name
                        contents
                        disorderGroup
                        disorderType
                        synonyms {
                            totalCount
                            data {
                                language
                                synonym
                            }
                        }
                        translations {
                            totalCount
                            data {
                                language
                                name
                                contents
                                disorderGroup
                                disorderType
                            }
                        }
                    }
                    frequency
                    diagnostic
                }
            }
        }
    }
    """;

    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getPhenotypeQuery),
          variables: {'id': id, 'lang': language},
        ),
      );
      Phenotype loadedPhenotype;
      if (result.hasException) {
        if (result.exception.linkException.originalException is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['phenotype'] != null) {
        loadedPhenotype = Phenotype.fromJson(result.data['phenotype']);
        return loadedPhenotype;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }

  Future<Phenotype> getPhenotypeForDiagnostic(
      BuildContext context, String id) async {
    const String getPhenotypeQuery = """
    query phenotypeById(\$id: ID!) {
        phenotype(id: \$id) {
            id
            hpoTerm
            translations {
                totalCount
                data {
                  language
                  hpoTerm
                }
            }
            synonyms {
                totalCount
                data {
                  language
                  synonym
                }
            }
        }
    }
    """;

    try {
      GraphQLClient client = GraphQLProvider.of(context).value;
      QueryResult result = await client.query(
        QueryOptions(
          document: gql(getPhenotypeQuery),
          variables: {'id': id},
        ),
      );
      Phenotype loadedPhenotype;
      if (result.hasException) {
        if (result.exception.linkException.originalException is SocketException) {
          throw HttpStatus.gatewayTimeout;
        }
        throw HttpStatus.internalServerError;
      }
      if (result.data['phenotype'] != null) {
        loadedPhenotype = Phenotype.fromJson(result.data['phenotype']);
        return loadedPhenotype;
      } else {
        throw HttpStatus.notFound;
      }
    } catch (error) {
      throw (error);
    }
  }
}
