import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:rdk_mobile/models/publication.dart';
import 'package:xml/xml.dart';

class Publications {
  static Future<Map<String, dynamic>> fetchPublications(
      {String searchTerm, String type, int offset, int limit}) async {
    String idsURI = '';
    switch (type) {
      case 'all':
        idsURI =
            'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?retmode=json&retstart=$offset&retmax=$limit&db=pubmed&sort=pub+date&term=($searchTerm)%20AND%20%22last%2010%20years%22[dp]%20%20AND%20(Case%20Reports[pt:noexp]%20%20OR%20Review[pt:noexp]%20)';
        break;
      case 'review':
        idsURI =
            'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?retmode=json&retstart=$offset&retmax=$limit&db=pubmed&sort=pub+date&term=($searchTerm)%20AND%20%22last%2010%20years%22[dp]%20%20AND%20(Review[pt:noexp])';
        break;
      case 'case_reports':
        idsURI =
            'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?retmode=json&retstart=$offset&retmax=$limit&db=pubmed&sort=pub+date&term=($searchTerm)%20AND%20%22last%2010%20years%22[dp]%20%20AND%20(Case%20Reports[pt:noexp])';
    }
    final idsResponse = await http.get(Uri.parse(idsURI));
    if (idsResponse.statusCode == 200) {
      final LinkedHashMap res = jsonDecode(idsResponse.body)['esearchresult'];
      final articlesResponse = await http.get(Uri.parse(
          'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?&db=pubmed&id=${res['idlist']}&rettype=xml&retstart=0'));
      if (articlesResponse.statusCode == 200) {
        final articleData = XmlDocument.parse(articlesResponse.body);
        List xmlPubList =
            articleData.getElement('PubmedArticleSet').children.toList();
        List<Publication> loadedPublications = [];
        for (XmlNode pub in xmlPubList) {
          if (pub.getElement('MedlineCitation') != null) {
            String pubDate;
            if (pub
                    .getElement('MedlineCitation')
                    .getElement('Article')
                    .getElement('Journal')
                    .getElement('JournalIssue')
                    .getElement('PubDate')
                    .getElement('MedlineDate') !=
                null) {
              pubDate = pub
                  .getElement('MedlineCitation')
                  .getElement('Article')
                  .getElement('Journal')
                  .getElement('JournalIssue')
                  .getElement('PubDate')
                  .getElement('MedlineDate')
                  .text;
            } else if (pub
                    .getElement('MedlineCitation')
                    .getElement('Article')
                    .getElement('Journal')
                    .getElement('JournalIssue')
                    .getElement('PubDate')
                    .getElement('Month') !=
                null) {
              pubDate = pub
                      .getElement('MedlineCitation')
                      .getElement('Article')
                      .getElement('Journal')
                      .getElement('JournalIssue')
                      .getElement('PubDate')
                      .getElement('Year')
                      .text +
                  ' ' +
                  pub
                      .getElement('MedlineCitation')
                      .getElement('Article')
                      .getElement('Journal')
                      .getElement('JournalIssue')
                      .getElement('PubDate')
                      .getElement('Month')
                      .text;
            } else {
              pubDate = pub
                  .getElement('MedlineCitation')
                  .getElement('Article')
                  .getElement('Journal')
                  .getElement('JournalIssue')
                  .getElement('PubDate')
                  .getElement('Year')
                  .text;
            }
            loadedPublications.add(
              Publication(
                  pmId:
                      pub.getElement('MedlineCitation').getElement('PMID').text,
                  title: pub
                      .getElement('MedlineCitation')
                      .getElement('Article')
                      .getElement('ArticleTitle')
                      .text,
                  link:
                      'https://pubmed.ncbi.nlm.nih.gov/${pub.getElement('MedlineCitation').getElement('PMID').text}',
                  source: pub
                      .getElement('MedlineCitation')
                      .getElement('Article')
                      .getElement('Journal')
                      .getElement('ISOAbbreviation')
                      .text,
                  abstract: pub
                      .findAllElements('AbstractText')
                      .map((node) => node.text)
                      .join(' '),
                  authorList: pub.findAllElements('Author').map((node) {
                    if (node.getElement('LastName') != null) {
                      return node.getElement('LastName').text +
                          (node.getElement('Initials') != null
                              ? (' ' + node.getElement('Initials').text + '.')
                              : '');
                    } else if (node.getElement('CollectiveName') != null) {
                      return node.getElement('CollectiveName').text;
                    }
                  }).toList(),
                  types: pub
                      .findAllElements('PublicationType')
                      .map((node) => node.text)
                      .toList(),
                  publishedDate: pubDate),
            );
          }
        }
        return {'count': res['count'], 'data': loadedPublications};
      } else {
        throw HttpStatus.internalServerError;
      }
    } else {
      throw HttpStatus.internalServerError;
    }
  }
}
