import 'package:flutter/foundation.dart';
import 'package:rdk_mobile/models/rdk_source.dart';

class RdkSources with ChangeNotifier {
  final List<RdkSource> _rdkSources = [
    RdkSource(
      name: 'Orphanet',
      abbr: 'ORPHA',
      country: 'France',
      countryCode: 'fr',
      sourceTypeEn: 'Public',
      sourceTypeFr: 'Publique',
      updatedAt: DateTime(2023, 6),
      aboutEn:
          'Orphanet is a unique resource, gathering and improving knowledge on rare diseases so as to improve the diagnosis, care and treatment of patients with rare diseases. Orphanet aims to provide high-quality information on rare diseases, and ensure equal access to knowledge for all stakeholders. Orphanet also maintains the Orphanet rare disease nomenclature (ORPHAnumber), essential in improving the visibility of rare diseases in health and research information systems. Orphanet was established in France by the INSERM (French National Institute for Health and Medical Research) in 1997. This initiative became a European endeavour from 2000, supported by grants from the European Commission: Orphanet has gradually grown to a Consortium of 40 countries, within Europe and across the globe.',
      aboutFr:
          'Orphanet est une ressource unique, rassemblant et améliorant la connaissance sur les maladies rares, afin de faciliter et perfectionner le diagnostic, le soin et le traitement des patients atteints de maladies rares. L’objectif d’Orphanet est de fournir des informations de haute qualité sur les maladies rares et de permettre le même accès à la connaissance pour toutes les parties prenantes. Orphanet développe également la nomenclature d’Orphanet sur les maladies rares (code ORPHA), essentielle à l’amélioration de la visibilité des maladies rares dans les systèmes d’information de santé et de recherche. Orphanet a été créé en France par l’INSERM (Institut national de la santé et de la recherche médicale) en 1997. Cette initiative est devenue un effort européen à partir de l’an 2000, financée par des contrats de la Commission européenne : Orphanet s’est progressivement transformé en un Consortium de 40 pays, répartis en Europe et à travers le monde.',
      link: 'https://www.orpha.net/',
      usedCategories: [
        'common.tabs.diseases',
        'common.tabs.symptoms',
        'common.tabs.genes',
        'pages.screening_tools'
      ],
    ),
    // RdkSource(
    //   name: 'Online Mendelian Inheritance in Man®',
    //   abbr: 'OMIM',
    //   country: 'France',
    //   countryCode: 'fr',
    //   sourceTypeEn: 'Public source',
    //   sourceTypeFr: 'Source publique',
    //   updatedAt: DateTime(2022, 2, 15),
    //   aboutEn:
    //       'OMIM is a comprehensive, authoritative compendium of human genes and genetic phenotypes that is freely available and updated daily. OMIM is authored and edited at the McKusick-Nathans Institute of Genetic Medicine, Johns Hopkins University School of Medicine, under the direction of Dr. Ada Hamosh.',
    //   aboutFr:
    //       'OMIM is a comprehensive, authoritative compendium of human genes and genetic phenotypes that is freely available and updated daily. OMIM is authored and edited at the McKusick-Nathans Institute of Genetic Medicine, Johns Hopkins University School of Medicine, under the direction of Dr. Ada Hamosh.',
    //   link: 'https://www.omim.org/',
    //   usedCategories: [
    //     'common.tabs.diseases',
    //     'common.tabs.symptoms',
    //     'common.tabs.genes',
    //     'pages.screening_tools'
    //   ],
    // ),
    RdkSource(
      name: 'The Human Phenotype Ontology',
      abbr: 'HPO',
      sourceTypeEn: 'Public',
      sourceTypeFr: 'Publique',
      updatedAt: DateTime(2023, 6),
      aboutEn:
          """The Human Phenotype Ontology (HPO) project provides an ontology of medically relevant phenotypes, disease-phenotype annotations, and the algorithms that operate on these. The HPO can be used to support differential diagnostics, translational research, and a number of applications in computational biology by providing the means to compute over the clinical phenotype. The HPO is being used for computational deep phenotyping and precision medicine as well as integration of clinical data into translational research. Deep phenotyping can be defined as the precise and comprehensive analysis of phenotypic abnormalities in which the individual components of the phenotype are observed and described. The HPO is being increasingly adopted as a standard for phenotypic abnormalities by diverse groups such as international rare disease organizations, registries, clinical labs, biomedical resources, and clinical software tools and will thereby contribute toward nascent efforts at global data exchange for identifying disease etiologies (Köhler et al, 2017).

The HPO currently contains over 13,000 terms arranged in a directed acyclic graph and are connected by is-a (subclass-of) edges, such that a term represents a more specific or limited instance of its parent term(s). All relationships in the HPO are is-a relationships, i.e. simple class-subclass relationships. For instance, Abnormal lens morphology is-a Abnormal eye morphology. The relationships are transitive, meaning that they are inherited up all paths to the root. Phenotypic abnormality is the main subontology of the HPO and contains descriptions of clinical abnormalities. Additional subontologies are provided to describe inheritance patterns, onset/clinical course and modifiers of abnormalities.""",
      aboutFr:
          """Le projet Human Phenotype Ontology (HPO) fournit une ontologie des phénotypes médicalement pertinents, des annotations de phénotypes de maladies et des algorithmes qui fonctionnent sur ceux-ci. L'HPO peut être utilisée pour soutenir les diagnostics différentiels, la recherche translationnelle et un certain nombre d'applications en biologie computationnelle en fournissant les moyens de calculer le phénotype clinique. Le HPO est utilisé pour le phénotypage profond computationnel et la médecine de précision, ainsi que pour l'intégration des données cliniques dans la recherche translationnelle. Le phénotypage profond peut être défini comme l'analyse précise et complète des anomalies phénotypiques dans laquelle les composants individuels du phénotype sont observés et décrits. Le HPO est de plus en plus adopté comme norme pour les anomalies phénotypiques par divers groupes tels que les organisations internationales de maladies rares, les registres, les laboratoires cliniques, les ressources biomédicales et les outils logiciels cliniques, et contribuera ainsi aux efforts naissants d'échange mondial de données pour identifier les étiologies des maladies (Köhler et al, 2017).

Le HPO contient actuellement plus de 13 000 termes disposés dans un graphe acyclique dirigé et reliés par des arêtes is-a (sous-classe de), de sorte qu'un terme représente une instance plus spécifique ou plus limitée de son ou ses termes parents. Toutes les relations dans le HPO sont des relations is-a, c'est-à-dire des relations simples classe-sous-classe. Par exemple, Abnormal lens morphology est un Abnormal eye morphology. Les relations sont transitives, ce qui signifie qu'elles sont héritées par tous les chemins jusqu'à la racine. L'anomalie phénotypique est la principale sous-ontologie du HPO et contient des descriptions d'anomalies cliniques. Des sous-ontologies supplémentaires sont fournies pour décrire les schémas d'hérédité, l'apparition/évolution clinique et les modificateurs des anomalies.""",
      link: 'https://hpo.jax.org/',
      usedCategories: ['common.tabs.symptoms', 'pages.screening_tools'],
    ),
    RdkSource(
      name: 'Fichier National des Etablissements Sanitaires et Sociaux',
      abbr: 'FINESS',
      country: 'France',
      countryCode: 'fr',
      sourceTypeEn: 'Public',
      sourceTypeFr: 'Publique',
      aboutEn:
          'FINESS ensures the registration of all health establishments with an EJ (legal entity) which holds the rights to carry out activities for one or more establishments and an ET corresponding to a geographical location. An ET is necessarily linked to an EJ. FINESS contains 4 levels of nomenclature that distinguish between hospitals, specialized establishments, reception establishments, but also pharmacies and biology laboratories. FINESS also contains qualifying information on the type of activity (cancer treatment, transplants, etc.) and the type of clientele (care capacities for specific pathologies).',
      aboutFr:
          'FINESS assure l’enregistrement de tous les établissements de santé avec une EJ (entité juridique) qui détient les droits d’exercer des activités pour un ou plusieurs établissements et une ET correspondant à un emplacement géographique. Une ET est nécessairement liée à une EJ. FINESS contient 4 niveaux de nomenclature qui distinguent les hôpitaux, les établissements spécialisés, les établissements d’accueil, mais aussi les pharmacies et les laboratoires de biologie. FINESS contient également des informations qualifiantes sur le type d’activité (traitement du cancer, greffes, etc.) et le type de clientèle (capacités de soins pour des pathologies spécifiques).',
      link: 'http://finess.sante.gouv.fr/',
      usedCategories: [
        'common.tabs.expert_centers',
      ],
    ),
    // RdkSource(
    //   name: 'DeepL Translator',
    //   abbr: 'DeepL',
    //   link: 'https://www.deepl.com/translator',
    //   usedCategories: [
    //     'common.tabs.symptoms',
    //   ],
    // ),
    RdkSource(
      name: 'PUBMED - National Library of Medicine',
      abbr: 'PUBMED',
      sourceTypeEn: 'Public',
      sourceTypeFr: 'Publique',
      aboutEn:
          'PubMed is a free resource supporting the search and retrieval of biomedical and life sciences literature with the aim of improving health–both globally and personally. PubMed® comprises more than 35 million citations for biomedical literature from MEDLINE, life science journals, and online books. Citations may include links to full text content from PubMed Central and publisher web sites.',
      aboutFr:
          "PubMed est une ressource gratuite permettant la recherche et la récupération de la littérature biomédicale et des sciences de la vie dans le but d'améliorer la santé, tant au niveau mondial que personnel. PubMed® comprend plus de 35 millions de citations pour la littérature biomédicale provenant de MEDLINE, de revues de sciences de la vie et de livres en ligne. Les citations peuvent inclure des liens vers le texte intégral de PubMed Central et des sites Web des éditeurs.",
      link: 'https://pubmed.ncbi.nlm.nih.gov/',
      usedCategories: [
        'common.tabs.publications',
      ],
    ),
    RdkSource(
      name: 'HAS - Haute Autorité de Santé',
      abbr: 'HAS',
      sourceTypeEn: 'Public',
      sourceTypeFr: 'Publique',
      aboutEn:
          'The HAS is an independent French public authority that contributes to the regulation of the healthcare system through quality. It carries out its missions in the fields of evaluation of health products, professional practices, organization of care and public health.',
      aboutFr:
          "La HAS est une autorité publique indépendante qui contribue à la régulation du système de santé par la qualité. Elle exerce ses missions dans les champs de l'évaluation des produits de santé, des pratiques professionnelles, de l’organisation des soins et de la santé publique.",
      link: 'https://www.has-sante.fr/',
      usedCategories: [
        'common.tabs.diseases',
        'common.text.pnds',
      ],
    ),
  ];

  List<RdkSource> get rdkSources {
    return [..._rdkSources];
  }
}
