import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';
import 'package:flutter_searchbox/flutter_searchbox.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/diseases.dart';
import 'package:rdk_mobile/screens/disease_info_screen.dart';
import 'package:rdk_mobile/widgets/bookmark/disease_bookmark_card.dart';
import 'package:rdk_mobile/widgets/browsing/browsing_expert_center_card.dart';
import 'package:rdk_mobile/widgets/search_input_no_suggestion.dart';
import 'package:searchbase/searchbase.dart';

class BookmarkScreen extends StatefulWidget {
  const BookmarkScreen({Key key}) : super(key: key);

  @override
  _BookmarkScreenState createState() => _BookmarkScreenState();
}

class _BookmarkScreenState extends State<BookmarkScreen>
    with TickerProviderStateMixin {
  bool _isLoading = true;
  final TextEditingController _searchController = TextEditingController();

  Locale currentLocale;
  List<Disease> _diseases = [];
  List<Disease> _sortedDiseases = [];

  @override
  void initState() {
    _searchController.addListener(() {
      setState(() {});
    });
    Provider.of<BookmarkDiseases>(context, listen: false)
        .getDiseases(context)
        .then((_) {
      setState(() {
        _isLoading = false;
      });
    });
    super.initState();
  }

  Widget buildSearchBar() {
    return SearchInputNoSuggestion(
      searchController: _searchController,
      onChanged: () async {
        Provider.of<Diseases>(context, listen: false)
            .changeSearchString(_searchController.text);
      },
      height: 50,
      padding: EdgeInsets.zero,
      contentPadding: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: const Color(0xFFFDFDFF),
      ),
      prefixIcon: const Icon(
        PhosphorIcons.magnifyingGlass,
        size: 20,
        color: Color(0xFF8F91AC),
      ),
      placeholderStyle: const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: Color(0xFFADAFCA),
        height: 1.2,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    _diseases = Provider.of<Diseases>(context)
        .bookmarkDiseases(currentLocale.languageCode);
    _sortedDiseases = List.from(_diseases);
    _sortedDiseases.sort((a, b) {
      DiseaseTranslation aTranslation = a.translations.firstWhere(
          (element) => element.language == currentLocale.languageCode,
          orElse: () => null);
      DiseaseTranslation bTranslation = b.translations.firstWhere(
          (element) => element.language == currentLocale.languageCode,
          orElse: () => null);
      if (currentLocale.languageCode != 'en' &&
          aTranslation != null &&
          bTranslation != null) {
        return aTranslation.name.compareTo(bTranslation.name);
      }
      return a.name.compareTo(b.name);
    });
    return Stack(
      children: [
        _isLoading
            ? Center(
                child: CircularProgressIndicator(
                  color: Theme.of(context).primaryColor,
                ),
              )
            : (_diseases.isEmpty
                ? Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).padding.top + 80,
                      ),
                      Center(
                        child: Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: const Color(0xFFECECF2),
                          ),
                          child: Icon(
                            _searchController.text.isNotEmpty
                                ? PhosphorIcons.magnifyingGlassBold
                                : PhosphorIcons.bookmarkSimpleBold,
                            size: 44,
                            color: const Color(0xFF64748B),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 24),
                        child: _searchController.text.isEmpty
                            ? RichText(
                                textScaleFactor:
                                    MediaQuery.of(context).textScaleFactor,
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF64748B),
                                        height: 1.5),
                                    children: [
                                      TextSpan(
                                        text: FlutterI18n.translate(context,
                                            'common.text.bookmark_text_1'),
                                      ),
                                      const WidgetSpan(
                                        child: Icon(
                                          PhosphorIcons.bookmarkSimple,
                                          size: 24,
                                          color: Color(0xFF64748B),
                                        ),
                                        style: TextStyle(height: 1.5),
                                      ),
                                      TextSpan(
                                        text: FlutterI18n.translate(context,
                                            'common.text.bookmark_text_2'),
                                      )
                                    ]),
                              )
                            : Text(
                                FlutterI18n.translate(
                                    context, 'common.text.no_result_found'),
                                style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF1E293B),
                                  height: 1.5,
                                ),
                              ),
                      )
                    ],
                  )
                : ListView.builder(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top + 24,
                        bottom: MediaQuery.of(context).padding.bottom + 90),
                    itemCount: _diseases.length,
                    itemBuilder: (context, index) => GestureDetector(
                      behavior: HitTestBehavior.deferToChild,
                      onTap: () {
                        Posthog().capture(
                          eventName: 'search-resource',
                          properties: {
                            'eventType': 'click',
                            'resourceType': 'disease',
                            'resourceId': _diseases[index].orphaNumber,
                            'resourceName': _diseases[index].name,
                            'searchInput': _searchController.text,
                            '\$screen_name': 'Bookmarks'
                          },
                        );
                        Navigator.of(context).pushNamed(
                          DiseaseInfoScreen.routeName,
                          arguments: {
                            'orphaNumber': _diseases[index].orphaNumber,
                            'backBtn':
                                FlutterI18n.translate(context, 'pages.bookmarks'),
                          },
                        );
                      },
                      child: DiseaseBookmarkCard(diseaseData: _diseases[index]),
                    ),
                  )),
        Align(
          alignment: Alignment.bottomCenter,
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
              child: Container(
                padding: const EdgeInsets.only(
                  top: 8,
                  left: 16,
                  right: 16,
                ),
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom),
                width: MediaQuery.of(context).size.width,
                color: const Color(0xFFECECF2).withOpacity(0.8),
                child: buildSearchBar(),
              ),
            ),
          ),
        )
      ],
    );
  }
}
