import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class CompliancesScreen extends StatelessWidget {
  static const routeName = '/compliances';

  const CompliancesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: 'Compliances',
    );
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('legal_compliances'),
              title: FlutterI18n.translate(context, 'common.text.compliances')),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 15,
                right: 15,
                bottom: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 15,),
                Card(
                  elevation: 0,
                  shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 12),
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          width: double.infinity,
                          height: 90,
                          decoration: BoxDecoration(
                            color: const Color(0xFFF9F9FA),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Image.asset(
                            'assets/icons/ce_logo.png',
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Text(
                          FlutterI18n.translate(
                              context, 'common.text.ce_title'),
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF3E3F5E),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        Text(
                          FlutterI18n.translate(
                              context, 'common.text.ce_contents'),
                          style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.6),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 0,
                  shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 12),
                          padding: const EdgeInsets.symmetric(vertical: 35),
                          width: double.infinity,
                          height: 90,
                          decoration: BoxDecoration(
                            color: const Color(0xFFF9F9FA),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Image.asset(
                            'assets/icons/ansm_logo.png',
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        const Text(
                          'ANSM',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF3E3F5E),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        Text(
                          FlutterI18n.translate(
                              context, 'common.text.ansm_contents'),
                          style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.6),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
