import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class CookiePolicyScreen extends StatelessWidget {
  static const routeName = '/cookie-policy';

  final String _cookiePolicy = """
    <h1>POLITIQUE COOKIES</h1>

    <p>Version de novembre 2023</p>

    <h2>1. Cookies </h2>
    <p>Parmi les données collectées par la Société, certaines sont recueillies par le biais de traceurs ou cookies. </p>

    <h2>1.1	Qu’est-ce qu’un cookie ? </h2>
    <p>Un cookie ou traceur est un petit fichier stocké par un serveur dans le terminal de navigation (ordinateur, téléphone, tablette.) d’un utilisateur lorsqu’il visite un site internet ou application. Le cookie est ensuite associé à un domaine web (c’est-à-dire dans la majorité des cas à l’ensemble des pages d’un même site web ou application).  Ce fichier peut permettre d’identifier l’appareil de l’utilisateur à chaque fois qu’il visite un site internet ou application.</p>

    <p>La plupart des sites internet utilisent des cookies et c’est le cas de RDK.</p>

    <p>Un cookie va généralement contenir les informations suivantes : </p>

    <ul>
    	<li>Le nom du site internet d’où il provient,</li>
    	<li>Un numéro unique attaché au terminal de navigation </li>
    </ul>

    <h2>1.2	A quoi servent les cookies ?</h2>
    <p>La Société utilise des traceurs afin d’obtenir des données notamment pour les finalités suivants :</p>
    <ul>
    	<li>Générer des statistiques de fréquentation utiles à l'amélioration de RDK</li>
    	<li>Limiter les disfonctionnements et bugs de navigation ou d’accès à RDK</li>
    </ul>

    <h2>1.3	Est-il obligatoire d’accepter tous les cookies ? </h2>
    <p>Les cookies dits « essentiels » seront installés automatiquement sur votre terminal puisqu’ils sont nécessaires au bon fonctionnement du site internet/ de l’application et permettent de limiter les disfonctionnements. Ces cookies contribuent également à rendre le site/ l’application utilisable en activant des fonctions de base comme la navigation de page et l'accès aux zones sécurisées du site. RDK ne peut pas fonctionner correctement sans ces cookies. Vous ne pouvez pas refuser l’installation de ces cookies. </p>
    <p>En revanche, vous êtes libre d’accepter l’utilisation des cookies optionnels, notamment pour la mesure d’audience et de performance ou les cookies de nos partenaires (listés ci-dessous). </p>

    <p>Si vous refusez les cookies, certaines pages ou fonctionnalités de RDK peuvent ne pas fonctionner sur votre appareil. </p>

    <h2>1.4	Quels cookies sont utilisés sur RDK? </h2>
    <p>La Société utilise des cookies essentiels et des cookies optionnels.</p>
    <p>Parmi les cookies optionnels, as we know utilise des cookies optionnels suivants : les cookies de mesure d’audience et de  statistiques afin de connaître l'utilisation et les performances de RDK, l’origine des internautes, d'établir des statistiques, des volumes de fréquentation et d'utilisation de divers éléments (contenus visités, parcours…) aux fins d'améliorer l'intérêt et l'ergonomie des services proposés (les pages ou les rubriques les plus souvent consultées, les articles les plus lus ...). </p>
    <p>Les données obtenues sont pseudonymisées et ne sont pas transmises à des tiers.</p>

    <h2>1.5	Pendant combien de temps les cookies sont-ils stockés sur mon appareil ? </h2>
    <p>Les cookies de session restent stockés sur votre appareil uniquement pendant la durée de session internet. </p>
    <p>Les cookies persistants sont stockés sur le disque dur de votre appareil jusqu’à ce que vous les supprimiez ou qu’ils atteignent leur date d’expiration. Ces cookies peuvent, par exemple, être utilisés pour se souvenir de vos préférences quand vous naviguez sur RDK. Les données collectées via les cookies sont conservées pendant une durée de vingt-cinq (25) mois maximum et la durée de vie de ces cookies ne dépassera pas treize (13) mois à compter de leur dépôt sur votre terminal.</p>

    <h2>1.6	Que puis-je faire pour gérer les cookies stockés sur mon appareil ?</h2>
    <p>Les cookies intégrés dans les pages et contenus que vous avez consultés pourront être stockés temporairement dans un espace dédié de votre terminal.</p>
    <p>La plupart des navigateurs acceptent les cookies par défaut. Cependant, vous pouvez modifier le paramétrage et la gestion des cookies de votre navigateur, vous pouvez modifier les réglages directement depuis menu d’aide ou paramétrage du navigateur.</p>

    <p>Par exemple :</p>
    <ul>
    	<li>Pour Internet Explorer : https://support.microsoft.com/fr-fr/help/17442/windows-internet-explorer-delete-manage-cookies</li>
    	<li>Pour Safari : https://support.apple.com/fr-fr/HT201265</li>
    	<li>Pour Google Chrome : https://support.google.com/chrome/answer/95647?hl=fr&hlrm=en&safe=on</li>
    	<li>Pour Firefox : https://support.mozilla.org/fr/kb/activer-desactiver-cookies</li>
    	<li>Pour Opera : https://help.opera.com/Windows/10.20/fr/cookies.html</li>
    </ul>
    <p>Nous attirons votre attention sur le fait que votre opposition à l’installation ou à l’utilisation d’un cookie sera prise en compte par l’installation d’un « cookie de refus » sur votre terminal. Aussi, veillez à ne pas supprimer ce cookie de refus si vous souhaitez que votre choix soit pris en compte.</p>

    <h2>1.7	Quels sont mes droits sur les données collectées par les cookies ?</h2>
    <p>Vous pouvez exercer les droits prévus dans la politique de confidentialité accessible <a href="https://rdk.asweknow.com/legale/politique">ici.</a> Vous pouvez également modifier le paramétrage de votre navigateur à tout moment comme expliqué ci-dessus. </p>
  """;

  const CookiePolicyScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: 'Cookie Policy',
    );
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('legal_cookie_policy'),
              title:
                  FlutterI18n.translate(context, 'common.tabs.cookie_policy')),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 15,
                right: 15,
                bottom: 30),
            child: Html(
              data: _cookiePolicy,
              onLinkTap: (link, ctx, attributes, element) {
                launchUrl(Uri.parse(link));
              },
              style: {
                "body": Style(
                  margin: EdgeInsets.zero,
                  padding: EdgeInsets.zero,
                  lineHeight: const LineHeight(1.5),
                ),
                "a": Style(color: Theme.of(context).primaryColor),
              },
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
