import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/dto/diagnostic_disease.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/diseases.dart';
import 'package:rdk_mobile/screens/diagnostic_tutorial_screen.dart';
import 'package:rdk_mobile/widgets/diagnostic/diagnostic_modal_bottom_sheet.dart';
import 'package:rdk_mobile/widgets/gene/disease_card.dart';

import 'disease_info_screen.dart';

class DiagnosticScreen extends StatefulWidget {
  @override
  _DiagnosticScreenState createState() => _DiagnosticScreenState();
}

class _DiagnosticScreenState extends State<DiagnosticScreen>
    with TickerProviderStateMixin {
  bool _isLoading = false;
  bool _isFetching = false;
  bool _showRelevanceInfo = false;
  Locale currentLocale;
  List<dynamic> selectedPhenotypeIds = [];
  List<dynamic> selectedPhenotypeAndChildrenIds = [];
  List<dynamic> selectedGeneSymbols = [];
  Set<String> selectedAgeIds = {};
  List<String> selectedPhenotypes = [];
  List<String> selectedGenes = [];
  List<String> selectedAgeLabels = [];

  bool _showLongBtnFirstTime = true;

  AnimationController _animationController;
  Animation _animation;

  static const _pageSize = 10;

  void setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  void setFetching(bool loading) {
    setState(() {
      _isFetching = loading;
    });
  }

  Widget buildResult(List<DiagnosticDisease> data, int totalDataCount) {
    if (data.length == 1 && data.contains(null)) {
      return Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        alignment: Alignment.center,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: const Color(0xFFECECF2),
                ),
                child: const Icon(
                  PhosphorIcons.magnifyingGlassBold,
                  size: 44,
                  color: Color(0xFF64748B),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
                child: Text(
                  FlutterI18n.translate(context, 'common.text.no_result_found'),
                  style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF1E293B),
                      height: 1.5),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Column(
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 10, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${totalDataCount.toString()} ${FlutterI18n.plural(context, 'common.text.result.values', totalDataCount).toLowerCase()}',
                  style: TextStyle(
                      fontWeight:
                          Theme.of(context).textTheme.headline4.fontWeight,
                      fontSize: Theme.of(context).textTheme.headline4.fontSize,
                      color: const Color(0xFF1E293B),
                      height: 1.5),
                ),
                TextButton(
                  child: Text(
                    FlutterI18n.translate(
                        context, 'common.buttons.how_it_works'),
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.underline,
                      color: Theme.of(context).primaryColor,
                      height: 1.5,
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      _showRelevanceInfo = !_showRelevanceInfo;
                    });
                  },
                ),
              ],
            ),
          ),
          if (_showRelevanceInfo)
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              margin: const EdgeInsets.only(bottom: 20),
              child: Text(
                FlutterI18n.translate(context, 'common.text.relevance_info'),
                style: TextStyle(
                  fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
                  fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                  height: 1.5,
                  color: const Color(0xFF8487AC),
                ),
              ),
            ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
            margin: const EdgeInsets.only(bottom: 15),
            alignment: Alignment.centerLeft,
            child: Wrap(
              // mainAxisAlignment: MainAxisAlignment.start,
              spacing: 20,
              runSpacing: 8,
              children: [
                RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Container(
                          margin: const EdgeInsets.only(right: 4),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 2),
                          decoration: BoxDecoration(
                            color: const Color(0xFFA17DEF).withOpacity(0.1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(4)),
                          ),
                          child: Text(
                            currentLocale.languageCode == 'fr' ? 'SP' : 'PS',
                            style: const TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFFA17DEF),
                              height: 1.3,
                            ),
                          ),
                        ),
                      ),
                      TextSpan(
                        text: FlutterI18n.translate(
                            context, 'common.text.pathognomonic_sign'),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF8487AC),
                          height: 1.2,
                        ),
                      ),
                    ],
                  ),
                ),
                // SizedBox(
                //   width: MediaQuery.of(context).size.width / 15,
                // ),
                RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Container(
                          margin: const EdgeInsets.only(right: 4),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 2),
                          decoration: BoxDecoration(
                            color: const Color(0xFFF55A8D).withOpacity(0.1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(4)),
                          ),
                          child: Text(
                            currentLocale.languageCode == 'fr' ? 'CD' : 'DC',
                            style: const TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFFF55A8D),
                              height: 1.3,
                            ),
                          ),
                        ),
                      ),
                      TextSpan(
                        text: FlutterI18n.translate(
                            context, 'common.text.diagnostic_criterion'),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF8487AC),
                          height: 1.2,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
            itemCount: data.length,
            itemBuilder: (ctx, i) => GestureDetector(
              behavior: HitTestBehavior.deferToChild,
              onTap: () {
                Posthog().capture(
                  eventName: 'see-details',
                  properties: {
                    'eventType': 'click',
                    'resourceType': 'disease',
                    'resourceId': data[i].disease.orphaNumber,
                    'resourceName': data[i].disease.name,
                    'phenotypeIds': selectedPhenotypeIds
                        .expand((element) => element)
                        .toList(),
                    'geneSymbols': selectedGeneSymbols,
                    'ageOnsetLabel': selectedAgeLabels,
                    '\$screen_name': 'Screening'
                  },
                );
                bool filterAdultCentre = false;
                bool filterChildrenCentre = false;
                if (selectedAgeIds
                    .intersection({'1', '3', '4', '6', '7'}).isNotEmpty) {
                  filterChildrenCentre = true;
                }
                if (selectedAgeIds.intersection({'2', '9'}).isNotEmpty) {
                  filterAdultCentre = true;
                }
                Navigator.of(context).pushNamed(
                  DiseaseInfoScreen.routeName,
                  arguments: {
                    'orphaNumber': data[i].disease.orphaNumber,
                    'filterAdultCentre': filterAdultCentre,
                    'filterChildrenCentre': filterChildrenCentre,
                    'backBtn':
                        FlutterI18n.translate(context, 'pages.screening_tools'),
                  },
                );
              },
              child: DiseaseCard(
                key: ObjectKey(data[i].disease.orphaNumber),
                disease: data[i].disease,
                scoring: data[i].scoring,
                parentWidget: 'diagnostic_screen',
              ),
            ),
          ),
          _isFetching
              ? CircularProgressIndicator(
                  color: Theme.of(context).primaryColor,
                )
              : const SizedBox.shrink(),
          SizedBox(
            height: MediaQuery.of(context).padding.bottom + 70,
          ),
        ],
      );
    }
  }

  void fetchDiagnosticData(int pageKey) async {
    setFetching(true);
    Provider.of<Diseases>(context, listen: false)
        .getDiagnosticDiseases(
            context: context,
            phenotypeIds: selectedPhenotypeAndChildrenIds,
            geneSymbols: selectedGeneSymbols,
            ageOnsetIds: selectedAgeIds.toList(),
            operator: 'AND',
            language: currentLocale.languageCode,
            take: _pageSize,
            skip: pageKey)
        .then((_) {
      setFetching(false);
    }).catchError((error) async {
      // widget.setLoading(false);
      setFetching(false);
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Text(
            FlutterI18n.translate(context, 'common.errors.an_error_occured'),
            style: Theme.of(context).textTheme.headline5,
          ),
          content: Text(
            FlutterI18n.translate(
                context, 'common.errors.something_went_wrong'),
            style: Theme.of(context).textTheme.bodyText2,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                FlutterI18n.translate(context, 'common.buttons.close'),
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    final List<DiagnosticDisease> diagnosticData =
        Provider.of<Diseases>(context).diagnosticDiseases;
    final int dataCount = Provider.of<Diseases>(context).dataCount;

    if (diagnosticData.isNotEmpty) {
      if (_isLoading) {
        setLoading(false);
      }
      return Stack(children: [
        _isLoading
            ? Center(
                child: CircularProgressIndicator(
                  color: Theme.of(context).primaryColor,
                ),
              )
            : NotificationListener<ScrollEndNotification>(
                onNotification: (scrollEnd) {
                  final metrics = scrollEnd.metrics;
                  if (metrics.atEdge) {
                    bool isTop = metrics.pixels == 0;
                    if (!isTop) {
                      if (diagnosticData.length < dataCount) {
                        fetchDiagnosticData(diagnosticData.length);
                      }
                    }
                  }
                  return true;
                },
                child: SingleChildScrollView(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: buildResult(diagnosticData, dataCount),
                ),
              ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).padding.bottom + 24, right: 24),
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                Container(
                  height: 60,
                  alignment: Alignment.centerRight,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 8,
                        primary: Theme.of(context).primaryColor,
                        padding: const EdgeInsets.all(16),
                        shadowColor: const Color.fromRGBO(95, 178, 237, 0.2),
                        shape: _animationController.value == 0
                            ? RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100),
                              )
                            : const CircleBorder(),
                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        animationDuration: const Duration(milliseconds: 800),
                        alignment: Alignment.center),
                    onPressed: () async {
                      showModalBottomSheet(
                        context: context,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(24),
                              topLeft: Radius.circular(24)),
                        ),
                        enableDrag: true,
                        elevation: 100,
                        isScrollControlled: true,
                        backgroundColor: const Color(0xFFF5F5F9),
                        builder: (ctx) {
                          return StatefulBuilder(builder: (context, setState) {
                            return DiagnosticModalBottomSheet(
                              selectedPhenotypeIds: selectedPhenotypeIds,
                              selectedPhenotypeAndChildrenIds:
                                  selectedPhenotypeAndChildrenIds,
                              selectedGeneSymbols: selectedGeneSymbols,
                              selectedPhenotypes: selectedPhenotypes,
                              selectedGenes: selectedGenes,
                              selectedAgeIds: selectedAgeIds,
                              selectedAgeLabels: selectedAgeLabels,
                              setLoadingScreen: setLoading,
                            );
                          });
                        },
                      );
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: _animation.value,
                          // Uses to hide widget when flex is going to 0
                          child: SizedBox(
                            width: _animationController.value == 0
                                ? null
                                : double.parse(_animation.value.toString()) /
                                    1.5,
                            child: Text(
                              FlutterI18n.translate(context,
                                  'common.buttons.change_your_filters_here'),
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              style: const TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        Flexible(
                          flex: 10,
                          child: Padding(
                            padding: _animationController.value != 1
                                ? const EdgeInsets.only(left: 8)
                                : EdgeInsets.zero,
                            child: const Icon(
                              PhosphorIcons.funnelBold,
                              color: Colors.white,
                              size: 24,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    color: const Color(0xFFEF4444)
                        .withOpacity(_animationController.value),
                    borderRadius: BorderRadius.circular(100),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    (selectedPhenotypeIds.length +
                            selectedGeneSymbols.length +
                            selectedAgeIds
                                .where((id) => id != '5' && id != '8')
                                .length)
                        .toString(),
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                      color:
                          Colors.white.withOpacity(_animationController.value),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]);
    }
    return CustomScrollView(
      physics: const NeverScrollableScrollPhysics(),
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height / 8,
                ),
                Text(
                  FlutterI18n.translate(context, 'common.text.disclaimer') +
                      ' ' +
                      FlutterI18n.translate(
                          context, 'common.text.disclaimer_content'),
                  style: const TextStyle(
                    height: 1.5,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFFADAFCA),
                  ),
                  textAlign: TextAlign.start,
                ),
                Container(
                  alignment: MediaQuery.of(context).size.shortestSide < 600
                      ? Alignment.centerLeft
                      : Alignment.center,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        alignment: Alignment.centerLeft),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.buttons.how_it_works'),
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        decoration: TextDecoration.underline,
                        color: Theme.of(context).primaryColor,
                        height: 1.5,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed(DiagnosticTutorialScreen.routeName);
                    },
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 3.5),
                const Expanded(child: SizedBox.shrink()),
                ElevatedButton(
                  child: Text(
                    FlutterI18n.translate(
                        context, 'common.buttons.start_my_screening'),
                    style: const TextStyle(
                        fontWeight: FontWeight.w700, fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      elevation: 8,
                      primary: Theme.of(context).primaryColor,
                      padding: const EdgeInsets.symmetric(
                          vertical: 18, horizontal: 24),
                      shadowColor: const Color.fromRGBO(95, 178, 237, 0.2),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      )),
                  onPressed: () {
                    if (_showLongBtnFirstTime) {
                      _animationController = AnimationController(
                          duration: const Duration(milliseconds: 300),
                          vsync: this);
                      _animation = IntTween(begin: 50, end: 0)
                          .animate(_animationController);
                      _animation.addListener(() => setState(() {}));
                    }
                    showModalBottomSheet(
                      context: context,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(24),
                            topLeft: Radius.circular(24)),
                      ),
                      enableDrag: true,
                      elevation: 100,
                      isScrollControlled: true,
                      backgroundColor: const Color(0xFFF5F5F9),
                      builder: (context) {
                        return StatefulBuilder(builder: (context, setState) {
                          return Listener(
                            onPointerDown: (_) =>
                                FocusManager.instance.primaryFocus.unfocus(),
                            child: DiagnosticModalBottomSheet(
                              selectedPhenotypeIds: selectedPhenotypeIds,
                              selectedPhenotypeAndChildrenIds:
                                  selectedPhenotypeAndChildrenIds,
                              selectedGeneSymbols: selectedGeneSymbols,
                              selectedPhenotypes: selectedPhenotypes,
                              selectedGenes: selectedGenes,
                              selectedAgeIds: selectedAgeIds,
                              selectedAgeLabels: selectedAgeLabels,
                              setLoadingScreen: setLoading,
                            ),
                          );
                        });
                      },
                    ).whenComplete(() {
                      Timer(const Duration(milliseconds: 3000), () async {
                        if (_showLongBtnFirstTime) {
                          await _animationController.forward();
                          setState(() {
                            _showLongBtnFirstTime = false;
                          });
                          _animationController.dispose();
                        }
                      });
                    });
                  },
                ),
                const SizedBox(
                  height: 32,
                ),
                Text(
                  FlutterI18n.translate(
                      context, 'common.footer.co_developed_with'),
                  style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF64748B),
                  ),
                ),
                Container(
                    height: MediaQuery.of(context).size.height / 20,
                    margin: const EdgeInsets.only(top: 8, bottom: 32),
                    child: ColorFiltered(
                      colorFilter: const ColorFilter.mode(
                        Color(0xFFF5F5F9),
                        BlendMode.saturation,
                      ),
                      child: Image.asset(
                        'assets/icons/orphanet.png',
                        fit: BoxFit.fitHeight,
                      ),
                    )),
                const Expanded(child: SizedBox.shrink()),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
