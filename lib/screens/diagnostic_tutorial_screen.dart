import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:story_view/story_view.dart';

class DiagnosticTutorialScreen extends StatelessWidget {
  static const routeName = '/diagnostic-tutorial';

  final StoryController _storyController = StoryController();

  DiagnosticTutorialScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    return Scaffold(
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: StoryView(
          controller: _storyController,
          inline: true,
          indicatorColor: Theme.of(context).primaryColor,
          onComplete: () {
            Navigator.of(context).pop();
          },
          onVerticalSwipeComplete: (Direction direction) {
            Navigator.of(context).pop();
          },
          storyItems: [
            StoryItem(
              Container(
                padding: const EdgeInsets.all(20),
                color: const Color(0xFFF5F5F9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top + 120,
                          bottom: 120),
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.welcome_text'),
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: const EdgeInsets.all(15),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/tutorial_welcome_logo.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    const SizedBox(
                      height: 60,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.introduction_text'),
                        style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                  ],
                ),
              ),
              duration: const Duration(seconds: 5),
            ),
            StoryItem(
              Container(
                padding: const EdgeInsets.all(20),
                color: const Color(0xFFF5F5F9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top + 120,
                          bottom: 60),
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.select_step_1'),
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(bottom: 60),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/tutorial_1_${currentLocale.languageCode}.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.select_step_2'),
                        style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                  ],
                ),
              ),
              duration: const Duration(seconds: 5),
            ),
            StoryItem(
              Container(
                padding: const EdgeInsets.all(20),
                color: const Color(0xFFF5F5F9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top + 120,
                          bottom: 32),
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.search_step_1'),
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(bottom: 10),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/tutorial_2_${currentLocale.languageCode}.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 5,
                      margin: const EdgeInsets.only(bottom: 35),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/tutorial_add_button_${currentLocale.languageCode}.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.search_step_2'),
                        style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                  ],
                ),
              ),
              duration: const Duration(seconds: 5),
            ),
            StoryItem(
              Container(
                padding: const EdgeInsets.all(20),
                color: const Color(0xFFF5F5F9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top + 120,
                          bottom: 60),
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.see_result_step'),
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      margin: const EdgeInsets.only(bottom: 10),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/tutorial_3_${currentLocale.languageCode}.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ],
                ),
              ),
              duration: const Duration(seconds: 5),
            ),
            StoryItem(
              Container(
                padding: const EdgeInsets.all(20),
                color: const Color(0xFFF5F5F9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top + 120,
                          bottom: 60),
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.tutorial.last_step'),
                        style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B)),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(bottom: 10),
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/tutorial_4_${currentLocale.languageCode}.png',
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ],
                ),
              ),
              duration: const Duration(seconds: 5),
            ),
          ],
        ),
      ),
    );
  }
}
