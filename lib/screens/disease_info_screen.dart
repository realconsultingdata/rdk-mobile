import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_hierarchy.dart';
import 'package:rdk_mobile/models/disease_synonym.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/diseases.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/disease/disease_gene_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/disease_hierarchy_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/disease_information_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/disease_expert_center_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/disease_phenotype_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/disease_rare_disease_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/prevalence_tab_view.dart';
import 'package:rdk_mobile/widgets/disease/publication_tab_view.dart';
import 'package:rdk_mobile/widgets/tab_bar_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class DiseaseInfoScreen extends StatefulWidget {
  static const routeName = '/disease-info';

  const DiseaseInfoScreen({Key key}) : super(key: key);

  @override
  _DiseaseInfoScreenState createState() => _DiseaseInfoScreenState();
}

class _DiseaseInfoScreenState extends State<DiseaseInfoScreen>
    with TickerProviderStateMixin {
  var _isInit = true;
  var _isLoading = false;

  Disease _loadedDisease;
  DiseaseTranslation _translation;
  List<DiseaseSynonym> _synonyms;
  List<Disease> _relatedDiseases = [];
  TabController _tabController;
  final TextEditingController _expCenterSearchController =
      TextEditingController();

  List<String> tabTitles = [];
  List<Widget> tabBarViews = [];
  Locale currentLocale;

  bool isBookmarked = false;

  String backBtn = '';

  // @override
  // void initState() {
  //   _tabController = TabController(length: tabTitles.length, vsync: this);
  //   super.initState();
  // }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
      final arguments = ModalRoute.of(context).settings.arguments as Map;
      final dynamic orphaNumber = arguments['orphaNumber'];
      backBtn = arguments['backBtn'];
      final bool filterAdultCentre = arguments['filterAdultCentre'] ?? false;
      final bool filterChildrenCentre =
          arguments['filterChildrenCentre'] ?? false;
      Provider.of<BookmarkDiseases>(context, listen: false)
          .findDiseaseById(orphaNumber)
          .then((data) {
        if (data != null) {
          setState(() {
            isBookmarked = true;
          });
        }
      });
      Provider.of<Diseases>(context)
          .getDiseaseById(
              context, orphaNumber.toString(), currentLocale.languageCode, true)
          .then((data) {
        _loadedDisease = data;
        if (currentLocale.languageCode != 'en') {
          _translation = _loadedDisease.translations.firstWhere(
              (translation) =>
                  translation.language == currentLocale.languageCode,
              orElse: () => null);
        }
        _synonyms =
            _loadedDisease.synonyms.where((syn) => syn.synonym != '').toList();
        for (DiseaseHierarchy hierarchy in _loadedDisease.hierarchies) {
          for (DiseaseHierarchyNode node in hierarchy.hierarchy) {
            if (node.disease.orphaNumber != orphaNumber) {
              Disease existedDisease = _relatedDiseases.firstWhere(
                  (item) => item.orphaNumber == node.disease.orphaNumber,
                  orElse: () => null);
              if (existedDisease == null) {
                _relatedDiseases.add(node.disease);
              }
            }
          }
        }
        Posthog().screen(
            screenName: 'Disease Info - ${_loadedDisease.orphaNumber}',
            properties: {
              'resourceType': 'disease',
              'resourceId': _loadedDisease.orphaNumber,
              'resourceName': _loadedDisease.name,
            });
        if (_loadedDisease.disorderGroup == 'Group of disorders') {
          tabTitles = [
            'common.tabs.summary',
            'common.tabs.diseases',
            'common.tabs.expert_centers',
            'common.tabs.publications',
            'common.tabs.classification',
          ];
          _tabController = TabController(length: tabTitles.length, vsync: this);
          tabBarViews = [
            DiseaseInformationTabView(
              loadedDisease: _loadedDisease,
              synonyms: _synonyms,
              tabController: _tabController,
              expCenterSearchController: _expCenterSearchController,
            ),
            DiseaseRareDiseaseTabView(diseases: _relatedDiseases),
            DiseaseExpertCenterTabView(
              expertCenters: _loadedDisease.newExpertCenters,
              diseaseOrphaNumber: _loadedDisease.orphaNumber,
              searchController: _expCenterSearchController,
              filterAdultCentre: filterAdultCentre,
              filterChildrenCentre: filterChildrenCentre,
            ),
            PublicationTabView(
              diseaseName: _loadedDisease.name,
              synonyms: _synonyms
                  .where((syn) => syn.language == 'en')
                  .map((e) => e.synonym)
                  .toList(),
            ),
            DiseaseHierarchyTabView(
              hierarchies: _loadedDisease.hierarchies,
              orphaNumber: _loadedDisease.orphaNumber,
            ),
          ];
        } else {
          tabTitles = [
            'common.tabs.summary',
            'common.tabs.symptoms',
            'common.tabs.expert_centers',
            'common.tabs.publications',
            'common.tabs.genes',
            'common.tabs.epidemiology',
            'common.tabs.classification',
          ];
          _tabController = TabController(length: tabTitles.length, vsync: this);
          tabBarViews = [
            DiseaseInformationTabView(
              loadedDisease: _loadedDisease,
              synonyms: _synonyms,
              tabController: _tabController,
              expCenterSearchController: _expCenterSearchController,
            ),
            DiseasePhenotypeTabView(
                phenotypeDiseases: _loadedDisease.phenotypes),
            DiseaseExpertCenterTabView(
              expertCenters: _loadedDisease.newExpertCenters,
              diseaseOrphaNumber: _loadedDisease.orphaNumber,
              searchController: _expCenterSearchController,
              filterAdultCentre: filterAdultCentre,
              filterChildrenCentre: filterChildrenCentre,
            ),
            PublicationTabView(
              diseaseName: _loadedDisease.name,
              synonyms: _synonyms
                  .where((syn) => syn.language == 'en')
                  .map((e) => e.synonym)
                  .toList(),
            ),
            DiseaseGeneTabView(geneDiseases: _loadedDisease.genes),
            PrevalenceTabView(prevalenceList: _loadedDisease.prevalences),
            DiseaseHierarchyTabView(
              hierarchies: _loadedDisease.hierarchies,
              orphaNumber: _loadedDisease.orphaNumber,
            ),
          ];
        }
        setState(() {
          _isLoading = false;
        });
      }).catchError((error) async {
        print(error);
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.connection_error'
                      : 'common.errors.an_error_occured'),
              style: Theme.of(context).textTheme.headline5,
            ),
            content: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.unable_access_server'
                      : 'common.errors.something_went_wrong'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            actions: <Widget>[
              TextButton(
                child: Text(
                  FlutterI18n.translate(context, 'common.buttons.go_back'),
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              )
            ],
          ),
        );
        Navigator.of(context).pop();
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _expCenterSearchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            if (connected) {
              return child;
            } else {
              return Scaffold(
                appBar: CustomAppBar(
                  key: const ValueKey('no_connection_appbar'),
                  title: 'RDK',
                ),
                body: NoConnectionScreen(),
              );
            }
          },
          child: _isLoading
              ? Scaffold(
                  appBar: CustomAppBar(
                    key: const ValueKey('disease_info'),
                    backBtnText: backBtn,
                    isBookmarked: isBookmarked,
                  ),
                  body: Center(
                    child: CircularProgressIndicator(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                )
              : Listener(
                  onPointerDown: (_) =>
                      FocusManager.instance.primaryFocus.unfocus(),
                  child: Scaffold(
                    extendBodyBehindAppBar: true,
                    appBar: CustomAppBar(
                      key: const ValueKey('disease_info'),
                      backBtnText: backBtn,
                      tabBar: TabBarCard(
                        tabController: _tabController,
                        tabTitles: tabTitles,
                      ),
                      isBookmarked: isBookmarked,
                      setBookmark: () {
                        if (!isBookmarked) {
                          Provider.of<BookmarkDiseases>(context, listen: false)
                              .addDisease(context, _loadedDisease)
                              .then((value) {
                            if (value != null) {
                              setState(() {
                                isBookmarked = true;
                              });
                            }
                          });
                        } else {
                          Provider.of<BookmarkDiseases>(context, listen: false)
                              .deleteDisease(
                                  context, _loadedDisease.orphaNumber)
                              .then((value) {
                            if (value > 0) {
                              setState(() {
                                isBookmarked = false;
                              });
                            }
                          });
                        }
                      },
                    ),
                    body: NotificationListener<OverscrollIndicatorNotification>(
                      onNotification:
                          (OverscrollIndicatorNotification overScroll) {
                        overScroll.disallowGlow();
                        return false;
                      },
                      child: NestedScrollView(
                        headerSliverBuilder: (context, value) {
                          return [
                            SliverToBoxAdapter(
                              child: Container(
                                height: 32,
                              ),
                            ),
                          ];
                        },
                        body: TabBarView(
                          controller: _tabController,
                          children: tabBarViews,
                        ),
                      ),
                    ),
                  ),
                ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
