import 'package:flutter/material.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_pnds_document.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../flavor_config.dart';

class DiseasePndsViewer extends StatelessWidget {
  static const routeName = '/disease-pnds-viewer';

  const DiseasePndsViewer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<CurrentLocale>(context).currentLocale();
    final DiseasePndsDocument document =
        ModalRoute.of(context).settings.arguments;
    Posthog().screen(
      screenName: 'Disease PNDS - ${document.name}',
    );
    return Stack(
      children: [
        Scaffold(
          appBar: CustomAppBar(
            key: const ValueKey('disease_pnds_viewer_screen'),
            title: document.name,
          ),
          body: Padding(
            padding: EdgeInsets.only(
                top: 15,
                left: 15,
                right: 15,
                bottom: MediaQuery.of(context).padding.bottom + 10),
            child: SfPdfViewer.network(
              document.hyperLink,
            ),
          ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
