import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class EmbeddedWebviewScreen extends StatefulWidget {
  static const reportBugRouteNameEn = '/report-bug-en';
  static const reportBugRouteNameFr = '/report-bug-fr';
  static const requestFeatureRouteNameEn = '/request-feature-en';
  static const requestFeatureRouteNameFr = '/request-feature-fr';
  static const evaluation = '/evaluation';
  final String title;
  final String url;

  const EmbeddedWebviewScreen({Key key, this.title, this.url})
      : super(key: key);

  @override
  _EmbeddedWebviewScreenState createState() => _EmbeddedWebviewScreenState();
}

class _EmbeddedWebviewScreenState extends State<EmbeddedWebviewScreen> {
  bool isLoading = true;

  @override
  void initState() {
    if (widget.key.toString().contains('report_bug')) {
      Posthog().screen(
        screenName: 'Report',
      );
    } else if (widget.key.toString().contains('request_feature')) {
      Posthog().screen(
        screenName: 'Request Feature',
      );
    } else if (widget.key.toString().contains('evaluation')) {
      Posthog().screen(
        screenName: 'Evaluation',
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            if (connected) {
              return child;
            } else {
              return Scaffold(
                appBar: CustomAppBar(
                  key: const ValueKey('no_connection_appbar'),
                  title: 'RDK',
                ),
                body: NoConnectionScreen(),
              );
            }
          },
          child: Scaffold(
            appBar: CustomAppBar(
                key: widget.key,
                title: FlutterI18n.translate(context, widget.title)),
            body: Stack(
              children: [
                WebView(
                  initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onPageFinished: (finish) {
                    setState(() {
                      isLoading = false;
                    });
                  },
                ),
                if (isLoading)
                  Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height,
                    color: const Color(0xFFF5F5F9),
                    child: Center(
                      child: CircularProgressIndicator(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
