import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/expert_centers.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/expert_center/center_disease_tab_view.dart';
import 'package:rdk_mobile/widgets/expert_center/center_expert_center_tab_view.dart';
import 'package:rdk_mobile/widgets/expert_center/center_information_tab_view.dart';
import 'package:rdk_mobile/widgets/tab_bar_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class ExpertCenterInfoScreen extends StatefulWidget {
  static const routeName = '/expert-center-info';

  @override
  _ExpertCenterInfoScreenState createState() => _ExpertCenterInfoScreenState();
}

class _ExpertCenterInfoScreenState extends State<ExpertCenterInfoScreen>
    with TickerProviderStateMixin {
  var _isInit = true;
  var _isLoading = false;

  NewExpertCenter _loadedExpertCenter;
  TabController _tabController;
  List<String> tabTitles = [
    'common.tabs.summary',
    'common.tabs.diseases',
  ];
  Locale currentLocale;

  @override
  void initState() {
    _tabController = TabController(length: tabTitles.length, vsync: this);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
      // String establishmentId = ModalRoute.of(context).settings.arguments;
      String expertCenterId = ModalRoute.of(context).settings.arguments;
      Provider.of<ExpertCenters>(context)
          .getExpertCenterById(context, expertCenterId)
          .then((data) {
        _loadedExpertCenter = data;
        Posthog().screen(
            screenName: 'Expert Center Info - ${_loadedExpertCenter.id}',
            properties: {
              'resourceType': 'expert_center',
              'resourceId': _loadedExpertCenter.id
            });
        setState(() {
          _isLoading = false;
        });
      }).catchError((error) async {
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.connection_error'
                      : 'common.errors.an_error_occured'),
              style: Theme.of(context).textTheme.headline5,
            ),
            content: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.unable_access_server'
                      : 'common.errors.something_went_wrong'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            actions: <Widget>[
              TextButton(
                child: Text(
                  FlutterI18n.translate(context, 'common.buttons.go_back'),
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              )
            ],
          ),
        );
        Navigator.of(context).pop();
      });
    }
    //   Provider.of<Establishments>(context)
    //       .getEstablishmentById(context, establishmentId)
    //       .then((data) {
    //     _loadedEstablishment = data;
    //     Posthog().screen(
    //         screenName: 'Expert Center Info - ${_loadedEstablishment.id}',
    //         properties: {
    //           'resourceType': 'expert_center',
    //           'resourceId': _loadedEstablishment.id
    //         });
    //     setState(() {
    //       _isLoading = false;
    //     });
    //   }).catchError((error) async {
    //     await showDialog(
    //       context: context,
    //       builder: (ctx) => AlertDialog(
    //         shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(10),
    //         ),
    //         title: Text(
    //           FlutterI18n.translate(
    //               context,
    //               error == HttpStatus.gatewayTimeout
    //                   ? 'common.errors.connection_error'
    //                   : 'common.errors.an_error_occured'),
    //           style: Theme.of(context).textTheme.headline5,
    //         ),
    //         content: Text(
    //           FlutterI18n.translate(
    //               context,
    //               error == HttpStatus.gatewayTimeout
    //                   ? 'common.errors.unable_access_server'
    //                   : 'common.errors.something_went_wrong'),
    //           style: Theme.of(context).textTheme.bodyText2,
    //         ),
    //         actions: <Widget>[
    //           TextButton(
    //             child: Text(
    //               FlutterI18n.translate(context, 'common.buttons.go_back'),
    //               style: TextStyle(color: Theme.of(context).primaryColor),
    //             ),
    //             onPressed: () {
    //               Navigator.of(ctx).pop();
    //             },
    //           )
    //         ],
    //       ),
    //     );
    //     Navigator.of(context).pop();
    //   });
    // }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Stack(
      children: [
        OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            if (connected) {
              return child;
            } else {
              return Scaffold(
                appBar: CustomAppBar(
                  key: const ValueKey('no_connection_appbar'),
                  title: 'RDK',
                ),
                body: NoConnectionScreen(),
              );
            }
          },
          child: Scaffold(
            extendBodyBehindAppBar: true,
            appBar: CustomAppBar(
              key: const ValueKey('expert_center_info'),
              title: _isLoading
                  ? FlutterI18n.translate(context, 'pages.model_info',
                      translationParams: {
                          'model': FlutterI18n.plural(
                              context, 'models.expert_center.title', 1)
                        })
                  : language == 'fr'
                      ? _loadedExpertCenter.name
                      : _loadedExpertCenter.nameEn,
              tabBar: TabBarCard(
                tabController: _tabController,
                tabTitles: tabTitles,
              ),
            ),
            body: _isLoading
                ? Center(
                    child: CircularProgressIndicator(
                      color: Theme.of(context).primaryColor,
                    ),
                  )
                : NotificationListener<OverscrollIndicatorNotification>(
                    onNotification:
                        (OverscrollIndicatorNotification overScroll) {
                      overScroll.disallowGlow();
                      return false;
                    },
                    child: NestedScrollView(
                      headerSliverBuilder: (context, value) {
                        return [
                          SliverToBoxAdapter(
                            child: Container(
                              height: 32,
                            ),
                          ),
                        ];
                      },
                      body: TabBarView(
                        controller: _tabController,
                        children: [
                          CenterInformationTabView(
                              loadedExpertCenter: _loadedExpertCenter),
                          CenterDiseaseTabView(
                            diseaseList: _loadedExpertCenter.diseases
                                .map((d) => d.disease)
                                .toList(),
                          ),
                        ],
                      ),
                    ),
                  ),
          ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
