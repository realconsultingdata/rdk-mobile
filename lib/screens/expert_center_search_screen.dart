import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_searchbox/flutter_searchbox.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/expert_center_info_screen.dart';
import 'package:rdk_mobile/widgets/browsing/browsing_expert_center_card.dart';
import 'package:rdk_mobile/widgets/search_input_no_suggestion.dart';
import 'package:searchbase/searchbase.dart';

class ExpertCenterSearchScreen extends StatefulWidget {
  const ExpertCenterSearchScreen({Key key}) : super(key: key);

  @override
  _ExpertCenterSearchScreenState createState() =>
      _ExpertCenterSearchScreenState();
}

class _ExpertCenterSearchScreenState extends State<ExpertCenterSearchScreen>
    with TickerProviderStateMixin {
  bool _isLoading = true;
  final TextEditingController _searchController = TextEditingController();

  Locale currentLocale;

  String searchIndex;

  List _searchResult = [];

  @override
  void initState() {
    _searchController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void fetchData(SearchController searchWidget) async {
    if (currentLocale.languageCode == 'en') {
      searchWidget.index = dotenv.env['APPBASE_EXPERT_CENTER_EN_INDEX'];
    } else if (currentLocale.languageCode == 'fr') {
      searchWidget.index = dotenv.env['APPBASE_EXPERT_CENTER_FR_INDEX'];
    }
    searchWidget.setValue(_searchController.text);
    await searchWidget.triggerDefaultQuery();
    if (mounted) {
      setState(() {
        _isLoading = false;
        _searchResult = searchWidget.suggestions;
      });
    }
  }

  void initialFetchData(SearchController searchWidget) async {
    searchWidget.setValue(_searchController.text);
    await searchWidget.triggerDefaultQuery();
    if (mounted) {
      setState(() {
        _isLoading = false;
        _searchResult = searchWidget.suggestions;
      });
    }
  }

  Widget buildSearchBar() {
    if (currentLocale.languageCode == 'en') {
      searchIndex = dotenv.env['APPBASE_EXPERT_CENTER_EN_INDEX'];
    } else if (currentLocale.languageCode == 'fr') {
      searchIndex = dotenv.env['APPBASE_EXPERT_CENTER_FR_INDEX'];
    }
    List dataField = [
      {'field': 'expert_center_name', 'weight': 5},
      {'field': 'label', 'weight': 3},
      {'field': 'specialty_label', 'weight': 2},
      {'field': 'city', 'weight': 1},
      {'field': 'zip_code', 'weight': 1},
      {'field': 'department_name', 'weight': 1},
      {'field': 'region', 'weight': 1},
    ];

    var searchBaseInstance = SearchBase(
      searchIndex,
      dotenv.env['APPBASE_HOST'],
      dotenv.env['APPBASE_CREDENTIAL'],
    );
    return SearchBaseProvider(
      searchbase: searchBaseInstance,
      child: SearchWidgetConnector(
        id: 'center-search',
        enablePopularSuggestions: false,
        triggerQueryOnInit: false,
        size: 50,
        subscribeTo: [],
        fuzziness: 'AUTO',
        showDistinctSuggestions: true,
        dataField: dataField,
        builder: (BuildContext context, SearchController searchWidget) =>
            SearchInputNoSuggestion(
          searchController: _searchController,
          initialLoad: () async {
            initialFetchData(searchWidget);
          },
          onChanged: () async {
            if (mounted) {
              setState(() {
                _isLoading = true;
              });
            }
            fetchData(searchWidget);
          },
          height: 50,
          padding: EdgeInsets.zero,
          contentPadding: const EdgeInsets.symmetric(vertical: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: const Color(0xFFFDFDFF),
          ),
          prefixIcon: const Icon(
            PhosphorIcons.magnifyingGlass,
            size: 20,
            color: Color(0xFF8F91AC),
          ),
          placeholderStyle: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color(0xFFADAFCA),
            height: 1.2,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();

    return Stack(
      children: [
        _isLoading
            ? Center(
                child: CircularProgressIndicator(
                  color: Theme.of(context).primaryColor,
                ),
              )
            : (_searchResult.isEmpty
                ? Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: const Color(0xFFECECF2),
                          ),
                          child: const Icon(
                            PhosphorIcons.magnifyingGlassBold,
                            size: 44,
                            color: Color(0xFF64748B),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 55, vertical: 8),
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.text.no_result_found'),
                            style: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xFF1E293B),
                                height: 1.5),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  )
                : ListView.builder(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top + 24,
                        bottom: MediaQuery.of(context).padding.bottom + 90),
                    itemCount: _searchResult.length,
                    itemBuilder: (context, index) => GestureDetector(
                      behavior: HitTestBehavior.deferToChild,
                      onTap: () {
                        Posthog().capture(
                          eventName: 'search-resource',
                          properties: {
                            'eventType': 'click',
                            'resourceType': 'expert-center',
                            'resourceId':
                                _searchResult[index].source['expert_center_id'],
                            'resourceName': _searchResult[index]
                                .source['expert_center_name'],
                            'searchInput': _searchController.text,
                            '\$screen_name': 'Search'
                          },
                        );
                        Navigator.of(context).pushNamed(
                          ExpertCenterInfoScreen.routeName,
                          arguments:
                              _searchResult[index].source['expert_center_id'],
                        );
                      },
                      child: BrowsingExpertCenterCard(
                          expertCenterData: _searchResult[index]),
                    ),
                  )),
        Align(
          alignment: Alignment.bottomCenter,
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
              child: Container(
                padding: const EdgeInsets.only(
                  top: 8,
                  left: 16,
                  right: 16,
                ),
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom),
                width: MediaQuery.of(context).size.width,
                color: const Color(0xFFECECF2).withOpacity(0.8),
                child: buildSearchBar(),
              ),
            ),
          ),
        )
      ],
    );
  }
}
