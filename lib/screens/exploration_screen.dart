import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_searchbox/flutter_searchbox.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/gene_info_screen.dart';
import 'package:rdk_mobile/screens/phenotype_info_screen.dart';
import 'package:rdk_mobile/widgets/browsing/disease_exploration_card.dart';
import 'package:rdk_mobile/widgets/browsing/gene_exploration_card.dart';
import 'package:rdk_mobile/widgets/browsing/phenotype_exploration_card.dart';
import 'package:rdk_mobile/widgets/search_input_no_suggestion.dart';
import 'package:searchbase/searchbase.dart';

import 'disease_info_screen.dart';

class ExplorationScreen extends StatefulWidget {
  final TabController tabController;

  const ExplorationScreen({Key key, @required this.tabController})
      : super(key: key);

  @override
  _ExplorationScreenState createState() => _ExplorationScreenState();
}

class _ExplorationScreenState extends State<ExplorationScreen> {
  bool _isLoading = true;
  // TabController _tabController;
  final TextEditingController _searchController = TextEditingController();

  Locale currentLocale;

  List _searchDiseaseResult = [];
  List _searchSymptomResult = [];
  List _searchGeneResult = [];

  @override
  void initState() {
    widget.tabController.addListener(() {
      _searchController.clear();
    });
    _searchController.addListener(() {
      setState(() {});
    });
    Provider.of<BookmarkDiseases>(context, listen: false).getDiseases(context);
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void fetchData(SearchController searchWidget) async {
    searchWidget.setValue(_searchController.text);
    if (widget.tabController.index == 0) {
      if (currentLocale.languageCode == 'en') {
        searchWidget.index = dotenv.env['APPBASE_DISEASE_EN_INDEX'];
      } else if (currentLocale.languageCode == 'fr') {
        searchWidget.index = dotenv.env['APPBASE_DISEASE_FR_INDEX'];
      }
      searchWidget.setDataField([
        {
          'field': _searchController.text.isEmpty ? 'name.keyword' : 'name',
          'weight': 5
        },
        {'field': 'synonym', 'weight': 2},
        {'field': 'orpha_number', 'weight': 1}
      ]);
    } else if (widget.tabController.index == 1) {
      if (currentLocale.languageCode == 'en') {
        searchWidget.index = dotenv.env['APPBASE_PHENOTYPE_EN_INDEX'];
      } else if (currentLocale.languageCode == 'fr') {
        searchWidget.index = dotenv.env['APPBASE_PHENOTYPE_FR_INDEX'];
      }
      searchWidget.setDataField([
        {
          'field':
              _searchController.text.isEmpty ? 'hpo_term.keyword' : 'hpo_term',
          'weight': 5
        },
        {'field': 'hpo_term.search', 'weight': 4},
        {'field': 'synonym', 'weight': 2},
        {'field': 'hpo_id', 'weight': 1}
      ]);
    } else if (widget.tabController.index == 2) {
      searchWidget.index = dotenv.env['APPBASE_GENE_INDEX'];
      searchWidget.setDataField([
        {
          'field': _searchController.text.isEmpty
              ? 'gene_name.keyword'
              : 'gene_name',
          'weight': 5
        },
        {'field': 'gene_synonym', 'weight': 2},
        {'field': 'symbol', 'weight': 1}
      ]);
    }
    searchWidget
        .setSortBy(_searchController.text.isEmpty ? SortType.asc : null);
    await searchWidget.triggerDefaultQuery();
    if (mounted) {
      setState(() {
        _isLoading = false;
        if (widget.tabController.index == 0) {
          _searchDiseaseResult = searchWidget.suggestions;
        } else if (widget.tabController.index == 1) {
          _searchSymptomResult = searchWidget.suggestions;
        } else if (widget.tabController.index == 2) {
          _searchGeneResult = searchWidget.suggestions;
        }
      });
    }
  }

  void initialFetchData(SearchController searchWidget) async {
    searchWidget.setValue(_searchController.text);
    // fetch Diseases
    if (currentLocale.languageCode == 'en') {
      searchWidget.index = dotenv.env['APPBASE_DISEASE_EN_INDEX'];
    } else if (currentLocale.languageCode == 'fr') {
      searchWidget.index = dotenv.env['APPBASE_DISEASE_FR_INDEX'];
    }
    searchWidget.setDataField([
      {'field': 'name.keyword', 'weight': 5},
      {'field': 'synonym', 'weight': 2},
      {'field': 'orpha_number', 'weight': 1}
    ]);
    await searchWidget.triggerDefaultQuery();
    setState(() {
      _searchDiseaseResult = searchWidget.suggestions;
    });

    // fetch Symptoms
    if (currentLocale.languageCode == 'en') {
      searchWidget.index = dotenv.env['APPBASE_PHENOTYPE_EN_INDEX'];
    } else if (currentLocale.languageCode == 'fr') {
      searchWidget.index = dotenv.env['APPBASE_PHENOTYPE_FR_INDEX'];
    }
    searchWidget.setDataField([
      {'field': 'hpo_term.keyword', 'weight': 5},
      {'field': 'hpo_term.search', 'weight': 4},
      {'field': 'synonym', 'weight': 2},
      {'field': 'hpo_id', 'weight': 1}
    ]);
    await searchWidget.triggerDefaultQuery();
    setState(() {
      _searchSymptomResult = searchWidget.suggestions;
    });

    // fetch Genes
    searchWidget.index = dotenv.env['APPBASE_GENE_INDEX'];
    searchWidget.setDataField([
      {'field': 'gene_name.keyword', 'weight': 5},
      {'field': 'gene_synonym', 'weight': 2},
      {'field': 'symbol', 'weight': 1}
    ]);
    await searchWidget.triggerDefaultQuery();
    setState(() {
      _searchGeneResult = searchWidget.suggestions;
    });
    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget buildSearchBar() {
    String searchIndex;
    List dataField = [];
    if (widget.tabController.index == 0) {
      if (currentLocale.languageCode == 'en') {
        searchIndex = dotenv.env['APPBASE_DISEASE_EN_INDEX'];
      } else if (currentLocale.languageCode == 'fr') {
        searchIndex = dotenv.env['APPBASE_DISEASE_FR_INDEX'];
      }
      dataField = [
        {'field': 'name.keyword', 'weight': 5},
        {'field': 'synonym', 'weight': 2},
        {'field': 'orpha_number', 'weight': 1}
      ];
    } else if (widget.tabController.index == 1) {
      if (currentLocale.languageCode == 'en') {
        searchIndex = dotenv.env['APPBASE_PHENOTYPE_EN_INDEX'];
      } else if (currentLocale.languageCode == 'fr') {
        searchIndex = dotenv.env['APPBASE_PHENOTYPE_FR_INDEX'];
      }
      dataField = [
        {'field': 'hpo_term.keyword', 'weight': 5},
        {'field': 'hpo_term.search', 'weight': 4},
        {'field': 'synonym', 'weight': 2},
        {'field': 'hpo_id', 'weight': 1}
      ];
    } else if (widget.tabController.index == 2) {
      searchIndex = dotenv.env['APPBASE_GENE_INDEX'];
      dataField = [
        {'field': 'gene_name.keyword', 'weight': 5},
        {'field': 'gene_synonym', 'weight': 2},
        {'field': 'symbol', 'weight': 1}
      ];
    }

    var searchBaseInstance = SearchBase(
      searchIndex,
      dotenv.env['APPBASE_HOST'],
      dotenv.env['APPBASE_CREDENTIAL'],
    );
    return SearchBaseProvider(
      searchbase: searchBaseInstance,
      child: SearchWidgetConnector(
        id: 'exploration-search',
        enablePopularSuggestions: false,
        triggerQueryOnInit: false,
        size: 50,
        subscribeTo: [],
        fuzziness: 'AUTO',
        showDistinctSuggestions: true,
        sortBy: SortType.asc,
        dataField: dataField,
        builder: (BuildContext context, SearchController searchWidget) =>
            SearchInputNoSuggestion(
          searchController: _searchController,
          initialLoad: () async {
            initialFetchData(searchWidget);
          },
          onChanged: () async {
            if (mounted) {
              setState(() {
                _isLoading = true;
              });
            }
            fetchData(searchWidget);
          },
          height: 50,
          padding: EdgeInsets.zero,
          contentPadding: const EdgeInsets.symmetric(vertical: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: const Color(0xFFFDFDFF),
          ),
          prefixIcon: const Icon(
            PhosphorIcons.magnifyingGlass,
            size: 20,
            color: Color(0xFF8F91AC),
          ),
          placeholderStyle: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color(0xFFADAFCA),
            height: 1.2,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();

    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: MediaQuery.of(context).padding.top,
            ),
            Expanded(
              child: TabBarView(
                controller: widget.tabController,
                children: [
                  _isLoading
                      ? Center(
                          child: CircularProgressIndicator(
                            color: Theme.of(context).primaryColor,
                          ),
                        )
                      : (_searchDiseaseResult.isEmpty
                          ? Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: const Color(0xFFECECF2),
                                    ),
                                    child: const Icon(
                                      PhosphorIcons.magnifyingGlassBold,
                                      size: 44,
                                      color: Color(0xFF64748B),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 55, vertical: 8),
                                    child: Text(
                                      FlutterI18n.translate(context,
                                          'common.text.no_result_found'),
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF1E293B),
                                          height: 1.5),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : ListView.builder(
                              padding: EdgeInsets.only(
                                  top: 32,
                                  bottom:
                                      MediaQuery.of(context).padding.bottom +
                                          90),
                              shrinkWrap: true,
                              itemCount: _searchDiseaseResult.length,
                              itemBuilder: (context, index) => GestureDetector(
                                behavior: HitTestBehavior.deferToChild,
                                onTap: () {
                                  Posthog().capture(
                                    eventName: 'search-resource',
                                    properties: {
                                      'eventType': 'click',
                                      'resourceType': 'disease',
                                      'resourceId': _searchDiseaseResult[index]
                                          .source['orpha_number']
                                          .toString(),
                                      'resourceName':
                                          _searchDiseaseResult[index]
                                              .source['name'],
                                      'searchInput': _searchController.text,
                                      '\$screen_name': 'Search'
                                    },
                                  );
                                  Navigator.of(context).pushNamed(
                                    DiseaseInfoScreen.routeName,
                                    arguments: {
                                      'orphaNumber': _searchDiseaseResult[index]
                                          .source['orpha_number']
                                          .toString(),
                                      'backBtn': FlutterI18n.translate(
                                          context, 'pages.search_tools'),
                                    },
                                  );
                                },
                                child: DiseaseExplorationCard(
                                    diseaseData: _searchDiseaseResult[index]),
                              ),
                            )),
                  _isLoading
                      ? Center(
                          child: CircularProgressIndicator(
                            color: Theme.of(context).primaryColor,
                          ),
                        )
                      : (_searchSymptomResult.isEmpty
                          ? Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: const Color(0xFFECECF2),
                                    ),
                                    child: const Icon(
                                      PhosphorIcons.magnifyingGlassBold,
                                      size: 44,
                                      color: Color(0xFF64748B),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 55, vertical: 8),
                                    child: Text(
                                      FlutterI18n.translate(context,
                                          'common.text.no_result_found'),
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF1E293B),
                                          height: 1.5),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : ListView.builder(
                              padding: EdgeInsets.only(
                                  top: 32,
                                  bottom:
                                      MediaQuery.of(context).padding.bottom +
                                          90),
                              shrinkWrap: true,
                              itemCount: _searchSymptomResult.length,
                              itemBuilder: (context, index) {
                                if (index == 0) {
                                  return Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      if (currentLocale.languageCode == 'fr')
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 15, right: 15, bottom: 8),
                                          child: Text.rich(
                                            TextSpan(children: [
                                              const WidgetSpan(
                                                alignment:
                                                    PlaceholderAlignment.middle,
                                                child: Icon(
                                                  PhosphorIcons.asteriskSimple,
                                                  size: 10,
                                                  color: Color(0xFF006394),
                                                ),
                                              ),
                                              TextSpan(
                                                  text:
                                                      '\t${FlutterI18n.translate(context, 'common.text.translated_by')} DeepL'),
                                            ]),
                                            textScaleFactor:
                                                MediaQuery.of(context)
                                                    .textScaleFactor,
                                            style: const TextStyle(
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xFF8487AC),
                                                height: 1.2),
                                          ),
                                        ),
                                      GestureDetector(
                                        behavior: HitTestBehavior.deferToChild,
                                        onTap: () {
                                          Posthog().capture(
                                            eventName: 'search-resource',
                                            properties: {
                                              'eventType': 'click',
                                              'resourceType': 'phenotype',
                                              'resourceId':
                                                  _searchSymptomResult[index]
                                                      .source['hpo_id'],
                                              'resourceName':
                                                  _searchSymptomResult[index]
                                                      .source['hpo_term'],
                                              'searchInput':
                                                  _searchController.text,
                                              '\$screen_name': 'Search'
                                            },
                                          );
                                          Navigator.of(context).pushNamed(
                                            PhenotypeInfoScreen.routeName,
                                            arguments:
                                                _searchSymptomResult[index]
                                                    .source['hpo_id'],
                                          );
                                        },
                                        child: PhenotypeExplorationCard(
                                            phenotypeData:
                                                _searchSymptomResult[index]),
                                      ),
                                    ],
                                  );
                                } else {
                                  return GestureDetector(
                                    behavior: HitTestBehavior.deferToChild,
                                    onTap: () {
                                      Posthog().capture(
                                        eventName: 'search-resource',
                                        properties: {
                                          'eventType': 'click',
                                          'resourceType': 'phenotype',
                                          'resourceId':
                                              _searchSymptomResult[index]
                                                  .source['hpo_id'],
                                          'resourceName':
                                              _searchSymptomResult[index]
                                                  .source['hpo_term'],
                                          'searchInput': _searchController.text,
                                          '\$screen_name': 'Search'
                                        },
                                      );
                                      Navigator.of(context).pushNamed(
                                        PhenotypeInfoScreen.routeName,
                                        arguments: _searchSymptomResult[index]
                                            .source['hpo_id'],
                                      );
                                    },
                                    child: PhenotypeExplorationCard(
                                        phenotypeData:
                                            _searchSymptomResult[index]),
                                  );
                                }
                              })),
                  _isLoading
                      ? Center(
                          child: CircularProgressIndicator(
                            color: Theme.of(context).primaryColor,
                          ),
                        )
                      : (_searchGeneResult.isEmpty
                          ? Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: const Color(0xFFECECF2),
                                    ),
                                    child: const Icon(
                                      PhosphorIcons.magnifyingGlassBold,
                                      size: 44,
                                      color: Color(0xFF64748B),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 55, vertical: 8),
                                    child: Text(
                                      FlutterI18n.translate(context,
                                          'common.text.no_result_found'),
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF1E293B),
                                          height: 1.5),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : ListView.builder(
                              padding: EdgeInsets.only(
                                  top: 32,
                                  bottom:
                                      MediaQuery.of(context).padding.bottom +
                                          90),
                              shrinkWrap: true,
                              itemCount: _searchGeneResult.length,
                              itemBuilder: (context, index) => GestureDetector(
                                behavior: HitTestBehavior.deferToChild,
                                onTap: () {
                                  Posthog().capture(
                                    eventName: 'search-resource',
                                    properties: {
                                      'eventType': 'click',
                                      'resourceType': 'gene',
                                      'resourceId': _searchGeneResult[index]
                                          .source['symbol'],
                                      'resourceName': _searchGeneResult[index]
                                          .source['gene_name'],
                                      'searchInput': _searchController.text,
                                      '\$screen_name': 'Search'
                                    },
                                  );
                                  Navigator.of(context).pushNamed(
                                    GeneInfoScreen.routeName,
                                    arguments: _searchGeneResult[index]
                                        .source['symbol'],
                                  );
                                },
                                child: GeneExplorationCard(
                                    geneData: _searchGeneResult[index]),
                              ),
                            )),
                ],
              ),
            ),
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
              child: Container(
                padding: const EdgeInsets.only(
                  top: 8,
                  left: 16,
                  right: 16,
                ),
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom),
                width: MediaQuery.of(context).size.width,
                color: const Color(0xFFECECF2).withOpacity(0.8),
                child: buildSearchBar(),
              ),
            ),
          ),
        )
      ],
    );
  }
}
