import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/gene.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/genes.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/gene/gene_disease_tab_view.dart';
import 'package:rdk_mobile/widgets/gene/gene_information_tab_view.dart';
import 'package:rdk_mobile/widgets/tab_bar_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class GeneInfoScreen extends StatefulWidget {
  static const routeName = '/gene-info';

  @override
  _GeneInfoScreenState createState() => _GeneInfoScreenState();
}

class _GeneInfoScreenState extends State<GeneInfoScreen>
    with TickerProviderStateMixin {
  var _isInit = true;
  var _isLoading = false;

  Gene _loadedGene;
  TabController _tabController;
  List<String> tabTitles = [
    'common.tabs.summary',
    'common.tabs.diseases',
  ];
  Locale currentLocale;

  @override
  void initState() {
    _tabController = TabController(length: tabTitles.length, vsync: this);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
      final dynamic symbol = ModalRoute.of(context).settings.arguments;
      Provider.of<Genes>(context)
          .getGeneBySymbol(context, symbol, currentLocale.languageCode)
          .then((data) {
        _loadedGene = data;
        Posthog().screen(
            screenName: 'Gene Info - ${_loadedGene.symbol}',
            properties: {
              'resourceType': 'gene',
              'resourceId': _loadedGene.symbol
            });
        setState(() {
          _isLoading = false;
        });
      }).catchError((error) async {
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.connection_error'
                      : 'common.errors.an_error_occured'),
              style: Theme.of(context).textTheme.headline5,
            ),
            content: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.unable_access_server'
                      : 'common.errors.something_went_wrong'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            actions: <Widget>[
              TextButton(
                child: Text(
                  FlutterI18n.translate(context, 'common.buttons.go_back'),
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              )
            ],
          ),
        );
        Navigator.of(context).pop();
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            if (connected) {
              return child;
            } else {
              return Scaffold(
                appBar: CustomAppBar(
                  key: ValueKey('no_connection_appbar'),
                  title: 'RDK',
                ),
                body: NoConnectionScreen(),
              );
            }
          },
          child: _isLoading
              ? Scaffold(
                  appBar: CustomAppBar(
                    key: const ValueKey('gene_info'),
                    title: FlutterI18n.translate(
                        context, 'pages.model_info', translationParams: {
                      'model':
                          FlutterI18n.plural(context, 'models.gene.title', 1)
                    }),
                  ),
                  body: Center(
                    child: CircularProgressIndicator(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                )
              : Listener(
                  onPointerDown: (_) =>
                      FocusManager.instance.primaryFocus.unfocus(),
                  child: Scaffold(
                    extendBodyBehindAppBar: true,
                    appBar: CustomAppBar(
                      key: const ValueKey('gene_info'),
                      title: _loadedGene.name,
                      tabBar: TabBarCard(
                        tabController: _tabController,
                        tabTitles: tabTitles,
                      ),
                    ),
                    body: NotificationListener<OverscrollIndicatorNotification>(
                      onNotification:
                          (OverscrollIndicatorNotification overScroll) {
                        overScroll.disallowGlow();
                        return false;
                      },
                      child: NestedScrollView(
                        headerSliverBuilder: (context, value) {
                          return [
                            SliverToBoxAdapter(
                              child: Container(
                                height: 32,
                              ),
                            ),
                          ];
                        },
                        body: TabBarView(
                          controller: _tabController,
                          children: [
                            GeneInformationTabView(loadedGene: _loadedGene),
                            GeneDiseaseTabView(
                                geneDiseases: _loadedGene.diseases),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
