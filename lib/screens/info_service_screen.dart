import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter_svg/svg.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class InfoServiceScreen extends StatelessWidget {
  static const routeName = '/information';

  const InfoServiceScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: 'Information',
    );
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('information_screen'),
              title: FlutterI18n.translate(context, 'pages.information')),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                bottom: 32,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 15,
                  ),
                  Card(
                    elevation: 0,
                    margin:
                        const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                    shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    clipBehavior: Clip.antiAlias,
                    child: Container(
                      padding: const EdgeInsets.all(16),
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Maladies Rares Info Service',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFF1E293B),
                            ),
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Text(
                            language == 'fr'
                                ? 'Maladies Rares Infos Services est le service d’information et de soutien de référence sur les maladies rares en France. Pour être écouté, s’informer, témoigner, échanger. Un service professionnel et sans but lucratif.'
                                : 'Maladies Rares Infos Services is the leading information and support service for rare diseases in France. To be listened to, to get information, to testify, to exchange ideas. A professional, non-profit service.',
                            style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Color(0xFF8F91AC),
                                height: 1.6),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 12),
                            padding: const EdgeInsets.all(15),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: const Color(0xFFF9F9FA),
                              borderRadius: BorderRadius.circular(24),
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                GestureDetector(
                                  child: SvgPicture.asset(
                                    'assets/icons/mr_info_services.svg',
                                  ),
                                  onTap: () => launch(
                                      'https://www.maladiesraresinfo.org/'),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                GestureDetector(
                                  child: SvgPicture.asset(
                                    'assets/icons/mr_info_service_number.svg',
                                    fit: BoxFit.fitWidth,
                                  ),
                                  onTap: () => launch('tel:0800404043'),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
