import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/compliances_screen.dart';
import 'package:rdk_mobile/screens/cookie_policy_screen.dart';
import 'package:rdk_mobile/screens/legal_notices_screen.dart';
import 'package:rdk_mobile/screens/privacy_policy_screen.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/html_content_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class LegalMenuScreen extends StatelessWidget {
  static const routeName = '/legal-menu';

  const LegalMenuScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: 'Legal Menu',
    );
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          appBar: CustomAppBar(
              key: const ValueKey('legal'),
              title: FlutterI18n.translate(context, 'pages.legal')),
          body: Column(
            children: [
              const SizedBox(
                height: 8,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                child: ListTile(
                  tileColor: const Color(0xFFFDFDFF),
                  horizontalTitleGap: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  leading: Icon(
                    PhosphorIcons.scroll,
                    size: 24,
                    color: Theme.of(context).primaryColor,
                  ),
                  title: Text(
                    FlutterI18n.translate(context, 'common.tabs.legal_notices'),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF1E293B),
                    ),
                  ),
                  trailing: const Icon(
                    PhosphorIcons.caretRightBold,
                    size: 14,
                    color: Color(0xFF1E293B),
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(LegalNoticesScreen.routeName);
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                child: ListTile(
                  tileColor: const Color(0xFFFDFDFF),
                  horizontalTitleGap: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  leading: Icon(
                    PhosphorIcons.detective,
                    size: 24,
                    color: Theme.of(context).primaryColor,
                  ),
                  title: Text(
                    FlutterI18n.translate(
                        context, 'common.tabs.privacy_policy'),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF1E293B),
                    ),
                  ),
                  trailing: const Icon(
                    PhosphorIcons.caretRightBold,
                    size: 14,
                    color: Color(0xFF1E293B),
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(PrivacyPolicyScreen.routeName);
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                child: ListTile(
                  tileColor: const Color(0xFFFDFDFF),
                  horizontalTitleGap: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  leading: Icon(
                    PhosphorIcons.cookie,
                    size: 24,
                    color: Theme.of(context).primaryColor,
                  ),
                  title: Text(
                    FlutterI18n.translate(context, 'common.tabs.cookie_policy'),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF1E293B),
                    ),
                  ),
                  trailing: const Icon(
                    PhosphorIcons.caretRightBold,
                    size: 14,
                    color: Color(0xFF1E293B),
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(CookiePolicyScreen.routeName);
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                child: ListTile(
                  tileColor: const Color(0xFFFDFDFF),
                  horizontalTitleGap: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  leading: Icon(
                    PhosphorIcons.listChecks,
                    size: 24,
                    color: Theme.of(context).primaryColor,
                  ),
                  title: Text(
                    FlutterI18n.translate(context, 'common.text.compliances'),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF1E293B),
                    ),
                  ),
                  trailing: const Icon(
                    PhosphorIcons.caretRightBold,
                    size: 14,
                    color: Color(0xFF1E293B),
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(CompliancesScreen.routeName);
                  },
                ),
              ),
            ],
          ),
          // body: NestedScrollView(
          //   headerSliverBuilder: (context, value) {
          //     return [
          //       SliverToBoxAdapter(
          //         child: Container(
          //           padding: const EdgeInsets.all(4),
          //           margin: EdgeInsets.only(
          //               left: 15,
          //               right: 15,
          //               top: MediaQuery.of(context).padding.top + 24),
          //           height: 40,
          //           decoration: BoxDecoration(
          //             color: const Color(0xFFEAEAF5),
          //             borderRadius: BorderRadius.circular(8),
          //           ),
          //           child: TabBar(
          //             controller: _tabController,
          //             indicator: BoxDecoration(
          //               borderRadius: BorderRadius.circular(8),
          //               color: Colors.white,
          //             ),
          //             labelColor: const Color(0xFF3E3F5E),
          //             unselectedLabelColor: const Color(0xFF8F91AC),
          //             tabs: [
          //               Tab(
          //                 text: FlutterI18n.translate(
          //                     context, 'common.tabs.legal_notices'),
          //               ),
          //               Tab(
          //                 text: FlutterI18n.translate(
          //                     context, 'common.tabs.privacy_policy'),
          //               ),
          //             ],
          //           ),
          //         ),
          //       ),
          //     ];
          //   },
          //   body: TabBarView(
          //     controller: _tabController,
          //     children: [
          //       SingleChildScrollView(
          //         child: HtmlContentCard(data: _legalNotices),
          //       ),
          //       SingleChildScrollView(
          //         child: HtmlContentCard(data: _privacyPolicy),
          //       ),
          //     ],
          //   ),
          // ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
