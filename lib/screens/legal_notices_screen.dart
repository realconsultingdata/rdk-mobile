import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class LegalNoticesScreen extends StatelessWidget {
  static const routeName = '/legal-notices';

  final String _legalNotices = """
  <div>
    <h1>Mentions Légales &amp; Conditions d’utilisation</h1>

    <h4>1. Editeur de l’Application et directeur de la publication</h4>
    <p>L’Application RDK (« l’Application ») est éditée par as we know – Société par actions simplifiée enregistrée au RCS
      de Nanterre sous le numéro 948 156 146 et dont le siège social se situe 11-19 rue de la Vanne SOPARQ Bâtiment C 5ème
      étage 92120 Montrouge, au capital social de 5 000 euros (« as we know »).</p>
    <p>Téléphone : +33 9 72 88 80 39</p>
    <p>Adresse email de contact : <a href="mailto:contact@asweknow.com">contact@asweknow.com</a></p>
    <p>Numéro de TVA intra-communautaire : FR13 948156146</p>
    <p>Directeur de la publication : Bruno Sarfati en sa qualité de gérant</p>

    <h4>2. Hébergement de l’Application</h4>
    <p>L’Application est hébergé par la société Google Inc., 1600 Amphitheatre Parkway Mountain View, CA 94043, États-Unis
      sur des serveurs situés dans un pays de l’Union Européenne (Belgique, France ou Allemagne).</p>
    <p>Téléphone : +1 650-253-0000</p>

    <h4>3. Accès à l’Application</h4>
    <p>L’utilisation du site par l’Utilisateur (« l’Utilisateur ») implique l’acceptation pleine et entière des présentes
      conditions générales d’utilisation ainsi que du guide d’utilisation accessibles via le lien <a
        href="https://rcd-media.com/apps/rdk/guides/fr.pdf">https://rcd-media.com/apps/rdk/guides/fr.pdf</a>.</p>
    <p>L’Application est en principe disponible 24h/24 et 7j/7 y compris les dimanches et jours fériés à l’exception, sans
      que cette liste ne soit limitative, des périodes de maintenances des infrastructures d’hébergement de l’Application,
      cas de force majeures, difficultés informatiques ou bugs inhérents à internet et les interruptions d’accès qui
      peuvent en résulter.</p>

    <h4>4. Comportement de l’Utilisateur</h4>
    <p>L’Utilisateur doit notamment s’abstenir de :</p>
    <ul>
      <li>télécharger vers l’Application, afficher, envoyer par courrier électronique ou transmettre par tout autre moyen
        tout élément contenant des virus logiciels, ou autres codes, fichiers ou programmes informatiques conçus pour
        interrompre, détruire ou limiter les fonctionnalités de tout logiciel ou matériel informatique ou de tout
        équipement de télécommunication;</li>
      <li>perturber ou interrompre le fonctionnement de l’Application, des serveurs ou réseaux connectés à l’Application
        ou enfreindre les exigences, procédures, règles ou réglementations s’y rapportant;</li>
      <li>tenter de porter atteinte au fonctionnement de l’Application, notamment en exposant l’Application à un virus, en
        causant une surcharge de consultation (bande passante), en surchargeant le serveur, en envoyant des "spams" ou en
        surchargeant le service de messagerie de l’Application;</li>
      <li>consulter des informations qui ne lui sont pas destinées ou accéder à un serveur ou un compte auquel
        l’Utilisateur n’est pas autorisé à accéder;</li>
      <li>chercher à évaluer, constater ou à tester la vulnérabilité de l’Application, ou enfreindre les mesures de
        sécurité ou d’authentification de l’Application sans l’accord écrit préalable de la Société;</li>
      <li>exercer une activité illégale ou toute autre activité susceptible de porter préjudice aux droits de la Société,
        de ses fournisseurs, prestataires, détaillants, annonceurs ou autres personnes, et s’abstenir d’y inciter des
        tiers;</li>
      <li>télécharger vers l’Application, transmettre, poster ou mettre par tout moyen à disposition des éléments
        publicitaires ou promotionnels non sollicités ou non autorisés, des "junk mails", des "spams", des "chaînes de
        lettres" ou toute autre forme de sollicitation;</li>
      <li>télécharger vers l’Application, afficher, envoyer par courrier électronique ou transmettre (notamment par le
        chatbot) un contenu illégal, préjudiciable, diffamatoire, offensant, raciste, vulgaire, obscène, menaçant,
        violent, contraire aux bonnes mœurs, portant atteinte à la vie privée d’une personne, désobligeant ou choquant
        notamment d’un point de vue racial ou ethnique, susceptible de porter atteinte à la vie privée, à l’honneur, à la
        sensibilité, à l’image de marque, à la notoriété de toute personne physique ou morale et notamment de la Société.
      </li>
    </ul>
    <p>Le cas échéant, la Société peut mettre fin à tout moment au droit d’accès à l’Application d’un utilisateur si
      celui-ci ne respecte pas les obligations qui lui incombent en vertu des Conditions Générales d’Utilisation et/ou de
      tout autre document, sans préjudice du droit de la Société de demander des dommages-intérêts.</p>

    <h4>5. Garanties et responsabilité</h4>
    <p>Les matériels et logiciels nécessaires pour l’accès à Internet et à l’Application relèvent de la responsabilité des
      Utilisateurs.</p>
    <p>La Société s’engage à permettre l’accès et l’utilisation de l’Application dans le cadre d’une obligation de moyens.
      Les informations accessibles sur l’Application sont fournies « EN L’ETAT » sans garantie d’aucune sorte, concernant
      notamment l’intégrité, l’exactitude, l’actualité, la disponibilité, la fiabilité ou l’exhaustivité des informations
      ou services apparaissant sur l’Application ou encore leur adéquation à l’utilisation que l’Utilisateur projette d’en
      faire.</p>
    <p>La Société se réserve le droit, à sa seule discrétion, de suspendre ou mettre fin à l’accès à tout ou partie de
      l’Application, de son contenu ou services proposés sur l’Application, sans notification préalable et sans que cela
      soit susceptible de donner lieu à une quelconque indemnisation au profit d’un utilisateur.</p>
    <p>Il est strictement interdit de modifier les éléments logiciels de l’Application en vue d'obtenir un accès non
      autorisé à l’Application.</p>
    <p>La Société permet l’accès à l’Application à titre gratuit et décline toute responsabilité :</p>
    <ul>
      <li>pour toute interruption de l’Application;</li>
      <li>en cas de survenance de bogues, de virus informatiques, d’erreurs d’affichage ou de téléchargement;</li>
      <li>pour toute inexactitude ou omission disponible sur l’Application;</li>
      <li>pour tous dommages résultant d'une intrusion frauduleuse d'un tiers;</li>
      <li>et plus généralement pour tous les dommages directs et indirects, quelles qu'en soient les causes ou
        conséquences, pouvant survenir à la suite de l'accès à l’Application et résultant d’une quelconque information
        provenant directement ou indirectement de l’Application.</li>
    </ul>

    <h4>6. Propriété intellectuelle et liens hypertextes</h4>
    <p>L’ensemble des éléments constituant l’Application, sa structure générale et son architecture (incluant son
      graphisme, les logiciels, textes, bases de données images, sons, savoir-faire, marques, logos, logiciels,
      photographies, illustrations, schémas, dessins et modèles…) ainsi que l’Application lui-même sont protégés par le
      droit de la propriété intellectuelle et sont la propriété exclusive de la Société ou de ses partenaires. Ainsi, sans
      que cette liste soit limitative, toute représentation, reproduction, modification, extraction, traduction de
      l’Application ou de son contenu, sans l’autorisation express de la Société est interdite et susceptible de
      constituer un acte de contrefaçon sanctionne notamment par le code de la propriété intellectuelle.</p>
    <p>Tout Utilisateur, propriétaire d’un site internet ou non qui souhaiterait utiliser un lien hypertexte de renvoi sur
      l’Application doit préalablement et nécessairement demander l’autorisation à la Société en utilisant l’adresse mail
      de contact mentionnée dans les présentes. Tout lien devra être retiré à la simple et unique demande de la Société.
    </p>
    <p>L’Application peut présenter des liens vers d’autres sites ou d'autres sources Internet. Dans la mesure où la
      Société ne peut contrôler ces sites et ces sources externes, elle ne saurait être tenue responsable du contenu,
      publicités, produits, services ou tout autre élément disponible sur ces sites ou sources externes. De plus, la
      Société ne pourra être tenue responsable de tous dommages ou pertes (avérés ou allégués) découlant directement ou
      indirectement de l'utilisation du contenu, des biens ou services disponibles sur ces sites ou sources externes.</p>

      <p>L’Utilisateur s’interdit expressément de développer, participer au développement ou utiliser un logiciel, outil, robot, script ou tout autre moyen (incluant des crawlers, plugin, API, browser ou tout autre technologie) permettant de ré-utiliser, extraire, copier et plus généralement aspirer (ou procéder au « scrapping ») de tout ou partie du Site dont les données et bases de données y figurant. L’Utilisateur s’interdit également de désactiver et tenter de désactiver toute mesure de protection et contrôle visant à limiter les accès non autorisés, les extractions ainsi que de décompiler, désassembler et procéder à du reverse engineering de tout ou partie du Site ou d’accéder au code source. </p>

    <h4>7. Loi applicable et juridiction compétente</h4>
    <p>Les Conditions Générales d’Utilisation, ainsi que la Politique de Confidentialité auxquelles il est fait référence
      sont régies par le droit français.</p>
    <p>Sauf disposition légale ou règlementaire contraire, tout différend concernant le Contenu et l’utilisation de
      l’Application relève de la compétence exclusive du tribunal compétent du ressort de la Cour d’Appel de Versailles, y
      compris en cas d’appel en garantie, de pluralité de défendeurs ou de procédure en référé ou sur requête.</p>
  </div>
  """;

  const LegalNoticesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: 'Legal Notices',
    );
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('legal_notices'),
              title:
                  FlutterI18n.translate(context, 'common.tabs.legal_notices')),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 15,
                right: 15,
                bottom: 30),
            child: Html(
              data: _legalNotices,
              onLinkTap: (link, ctx, attributes, element) {
                launchUrl(Uri.parse(link));
              },
              style: {
                "body": Style(
                  margin: EdgeInsets.zero,
                  padding: EdgeInsets.zero,
                  lineHeight: const LineHeight(1.5),
                ),
                "a": Style(color: Theme.of(context).primaryColor),
              },
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
