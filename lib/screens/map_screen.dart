import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/widgets/disease/disease_expert_center_list_bottom_sheet.dart';

import '../widgets/disease/custom_map_info_widget.dart';

class MapScreen extends StatefulWidget {
  static const routeName = '/expert-center-map';

  final String orphaNumber;
  final List<Establishment> establishments;
  final double zoom;
  final bool zoomGesturesEnabled;
  final bool scrollGesturesEnabled;
  final bool tiltGesturesEnabled;
  final bool rotateGesturesEnabled;
  final Function onPopupClick;
  final Function onPinClick;
  final bool enableMarkerPopup;
  final bool enableMoreInfoBtn;

  const MapScreen({
    Key key,
    this.orphaNumber,
    @required this.establishments,
    this.zoom,
    this.zoomGesturesEnabled,
    this.scrollGesturesEnabled,
    this.tiltGesturesEnabled,
    this.rotateGesturesEnabled,
    this.onPopupClick,
    this.onPinClick,
    this.enableMarkerPopup = false,
    this.enableMoreInfoBtn = false,
  }) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen>
    with AutomaticKeepAliveClientMixin {
  final Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  InfoWidgetRoute _infoWidgetRoute;
  StreamSubscription _mapIdleSubscription;
  bool locationServiceEnabled;
  // LocationPermission locationPermission;

  static CameraPosition _kGooglePlex;
  bool isCameraPositionChanged = false;

  BitmapDescriptor unselectedMarker;
  BitmapDescriptor selectedMarker;

  int selectedMarkerIdx;

  // Future<dynamic> _checkLocationPermission() async {
  //   // Test if location services are enabled.
  //   locationServiceEnabled = await Geolocator.isLocationServiceEnabled();
  //   if (!locationServiceEnabled) {
  //     return Future.error('Location services are disabled.');
  //   }
  //   locationPermission = await Geolocator.checkPermission();
  // }

  // Future<Position> _determinePosition() async {
  //   // Test if location services are enabled.
  //   _checkLocationPermission().then((_) => null);
  //   if (!locationServiceEnabled) {
  //     if (Platform.isIOS) {
  //       await showCupertinoDialog(
  //           context: context,
  //           builder: (ctx) => CupertinoAlertDialog(
  //                 title: Text(
  //                   FlutterI18n.translate(
  //                       context, 'common.errors.location_disabled'),
  //                 ),
  //                 content: Text(
  //                   FlutterI18n.translate(
  //                       context, 'common.errors.location_disabled_content'),
  //                 ),
  //                 actions: <Widget>[
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.settings')),
  //                     onPressed: () async {
  //                       await Geolocator.openLocationSettings();
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.cancel')),
  //                     onPressed: () {
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                 ],
  //               ));
  //     } else {
  //       await showDialog(
  //         context: context,
  //         builder: (ctx) => AlertDialog(
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           title: Text(
  //             FlutterI18n.translate(context, 'common.errors.location_disabled'),
  //             style: Theme.of(context).textTheme.headline5,
  //           ),
  //           content: Text(
  //             FlutterI18n.translate(
  //                 context, 'common.errors.location_disabled_content'),
  //             style: Theme.of(context).textTheme.bodyText2,
  //           ),
  //           actions: <Widget>[
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.cancel'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               },
  //             ),
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.settings'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () async {
  //                 await Geolocator.openLocationSettings();
  //                 Navigator.of(context).pop();
  //               },
  //             )
  //           ],
  //         ),
  //       );
  //     }
  //     return Future.error('Location services are disabled.');
  //   }
  //   if (locationPermission == LocationPermission.denied) {
  //     locationPermission = await Geolocator.requestPermission();
  //     if (locationPermission == LocationPermission.denied) {
  //       // Permissions are denied, next time you could try
  //       // requesting permissions again (this is also where
  //       // Android's shouldShowRequestPermissionRationale
  //       // returned true. According to Android guidelines
  //       // your App should show an explanatory UI now.

  //       return Future.error('Location permissions are denied');
  //     }
  //   }

  //   if (locationPermission == LocationPermission.deniedForever) {
  //     // Permissions are denied forever, handle appropriately.
  //     if (Platform.isIOS) {
  //       await showCupertinoDialog(
  //           context: context,
  //           builder: (ctx) => CupertinoAlertDialog(
  //                 title: Text(FlutterI18n.translate(
  //                     context, 'common.errors.location_permission_denied')),
  //                 content: Text(FlutterI18n.translate(context,
  //                     'common.errors.location_permission_denied_content')),
  //                 actions: <Widget>[
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.settings')),
  //                     onPressed: () async {
  //                       await Geolocator.openLocationSettings();
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.cancel')),
  //                     onPressed: () {
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                 ],
  //               ));
  //     } else {
  //       await showDialog(
  //         context: context,
  //         builder: (ctx) => AlertDialog(
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           title: Text(
  //             FlutterI18n.translate(
  //                 context, 'common.errors.location_permission_denied'),
  //             style: Theme.of(context).textTheme.headline5,
  //           ),
  //           content: Text(
  //             FlutterI18n.translate(
  //                 context, 'common.errors.location_permission_denied_content'),
  //             style: Theme.of(context).textTheme.bodyText2,
  //           ),
  //           actions: <Widget>[
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.cancel'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               },
  //             ),
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.settings'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () async {
  //                 await Geolocator.openAppSettings();
  //                 Navigator.of(context).pop();
  //               },
  //             )
  //           ],
  //         ),
  //       );
  //     }
  //     return Future.error(
  //         'Location permissions are permanently denied, we cannot request permissions.');
  //   }

  //   // When we reach here, permissions are granted and we can
  //   // continue accessing the position of the device.
  //   return await Geolocator.getCurrentPosition();
  // }

  @override
  void initState() {
    // _checkLocationPermission();

    // make sure to initialize before map loading
    BitmapDescriptor.fromAssetImage(
            const ImageConfiguration(devicePixelRatio: 1),
            'assets/markers/map-pin-unselected-small.png')
        .then((d) {
      setState(() {
        unselectedMarker = d;
      });
    });
    BitmapDescriptor.fromAssetImage(
            const ImageConfiguration(size: Size(24, 24)),
            'assets/markers/map-pin-selected-small.png')
        .then((d) {
      setState(() {
        selectedMarker = d;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _kGooglePlex = CameraPosition(
      target: const LatLng(46.616607119878715, 2.433185),
      zoom: widget.zoom ?? 5.25,
    );
    if (unselectedMarker != null && selectedMarker != null) {
      return Scaffold(
        body: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          child: Stack(
            children: [
              GoogleMap(
                mapType: MapType.normal,
                initialCameraPosition: _kGooglePlex,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                  _mapController = controller;
                },
                zoomControlsEnabled: false,
                zoomGesturesEnabled: widget.zoomGesturesEnabled ?? true,
                scrollGesturesEnabled: widget.scrollGesturesEnabled ?? true,
                tiltGesturesEnabled: widget.tiltGesturesEnabled ?? true,
                rotateGesturesEnabled: widget.rotateGesturesEnabled ?? true,
                myLocationEnabled: false,
                myLocationButtonEnabled: false,
                mapToolbarEnabled: false,
                gestureRecognizers: {}
                  ..add(Factory<PanGestureRecognizer>(
                      () => PanGestureRecognizer()))
                  ..add(Factory<ScaleGestureRecognizer>(
                      () => ScaleGestureRecognizer()))
                  ..add(Factory<TapGestureRecognizer>(
                      () => TapGestureRecognizer()))
                  ..add(Factory<VerticalDragGestureRecognizer>(
                      () => VerticalDragGestureRecognizer()))
                  ..add(Factory<HorizontalDragGestureRecognizer>(
                      () => HorizontalDragGestureRecognizer())),
                markers: widget.establishments.map((establishment) {
                  return Marker(
                      position: LatLng(
                          establishment.latitude, establishment.longitude),
                      markerId: MarkerId(establishment.id),
                      icon: selectedMarkerIdx ==
                              widget.establishments.indexOf(establishment)
                          ? selectedMarker
                          : unselectedMarker,
                      onTap: () {
                        setState(() {
                          selectedMarkerIdx =
                              widget.establishments.indexOf(establishment);
                        });
                        _onTap(
                            LatLng(establishment.latitude,
                                establishment.longitude),
                            establishment);
                      });
                }).toSet(),
                onCameraMove: (newPosition) {
                  _mapIdleSubscription?.cancel();
                  _mapIdleSubscription =
                      Future.delayed(const Duration(milliseconds: 150))
                          .asStream()
                          .listen((_) {
                    if (_infoWidgetRoute != null && widget.enableMarkerPopup) {
                      Navigator.of(context, rootNavigator: true)
                          .push(_infoWidgetRoute)
                          .then<void>(
                        (newValue) {
                          _infoWidgetRoute = null;
                        },
                      );
                    }
                  });
                  setState(() {
                    isCameraPositionChanged = true;
                  });
                },
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 40,
                  height: 40,
                  margin: EdgeInsets.only(
                      left: 16, top: MediaQuery.of(context).padding.top + 10),
                  child: FloatingActionButton(
                    heroTag: 'backBtn',
                    elevation: 0,
                    backgroundColor: const Color(0xFFF5F5F9),
                    child: const Icon(
                      PhosphorIcons.caretLeftBold,
                      size: 20,
                      color: Color(0xFF1E293B),
                    ),
                    onPressed: () {
                      selectedMarkerIdx = null;
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              if (isCameraPositionChanged)
                Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    width: 40,
                    height: 40,
                    margin: EdgeInsets.only(
                        right: 16,
                        bottom: MediaQuery.of(context).padding.bottom + 24),
                    child: FloatingActionButton(
                      heroTag: 'resetMapBtn',
                      elevation: 0,
                      backgroundColor: const Color(0xFFF5F5F9),
                      child: const Icon(
                        PhosphorIcons.arrowsOutCardinal,
                        size: 20,
                        color: Color(0xFF1E293B),
                      ),
                      onPressed: () async {
                        selectedMarkerIdx = null;
                        await _mapController.animateCamera(
                          CameraUpdate.newCameraPosition(_kGooglePlex),
                        );
                        setState(() {
                          isCameraPositionChanged = false;
                        });
                      },
                    ),
                  ),
                ),
              // Align(
              //   alignment: Alignment.bottomRight,
              //   child: Container(
              //     width: 40,
              //     height: 40,
              //     margin: const EdgeInsets.only(right: 8, bottom: 8),
              //     child: FloatingActionButton(
              //       heroTag: 'locationBtn',
              //       elevation: 0,
              //       backgroundColor: const Color(0xFFF5F5F9),
              //       child: const Icon(
              //         PhosphorIcons.crosshairFill,
              //         size: 20,
              //         color: Color(0xFF1E293B),
              //       ),
              //       onPressed: () async {
              //         _determinePosition().then((value) async {
              //           await _mapController.animateCamera(
              //             CameraUpdate.newCameraPosition(CameraPosition(
              //               target:
              //                   LatLng(value.latitude, value.longitude),
              //               zoom: 12,
              //             )),
              //           );
              //           setState(() {
              //             isCameraPositionChanged = true;
              //           });
              //         });
              //       },
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      );
    } else {
      return Center(
        child: CircularProgressIndicator(
          color: Theme.of(context).primaryColor,
        ),
      );
    }
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  _onTap(LatLng latLng, Establishment establishment) async {
    // final RenderBox renderBox = context.findRenderObject();
    // Rect _itemRect = renderBox.localToGlobal(Offset.zero) & renderBox.size;

    // _infoWidgetRoute = InfoWidgetRoute(
    //   height: widget.enableMoreInfoBtn ? 130 : 100,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     mainAxisSize: MainAxisSize.min,
    //     children: [
    //       Text(
    //         establishment.label,
    //         textAlign: TextAlign.center,
    //         style: const TextStyle(
    //           fontFamily: 'Inter',
    //           fontSize: 12,
    //           fontWeight: FontWeight.w700,
    //           color: Color(0xFF1E293B),
    //         ),
    //       ),
    //       const SizedBox(
    //         height: 4,
    //       ),
    //       RichText(
    //         textScaleFactor: MediaQuery.of(context).textScaleFactor,
    //         textAlign: TextAlign.center,
    //         text: TextSpan(
    //           style: const TextStyle(
    //             fontFamily: 'Inter',
    //           ),
    //           children: [
    //             const WidgetSpan(
    //                 alignment: PlaceholderAlignment.bottom,
    //                 child: Icon(
    //                   PhosphorIcons.mapPinBold,
    //                   size: 14,
    //                   color: Color(0xFF8F91AC),
    //                 )),
    //             TextSpan(
    //               text: ' ' + establishment.zipCode + ' ' + establishment.city,
    //               style: const TextStyle(
    //                   fontSize: 10,
    //                   fontWeight: FontWeight.w500,
    //                   color: Color(0xFF1E293B),
    //                   height: 1.2),
    //             )
    //           ],
    //         ),
    //       ),
    //       if (widget.enableMoreInfoBtn)
    //         TextButton(
    //           child: Text(
    //             FlutterI18n.translate(context, 'common.buttons.more_info'),
    //             style: TextStyle(
    //               fontSize: 12,
    //               fontWeight: FontWeight.w400,
    //               decoration: TextDecoration.underline,
    //               color: Theme.of(context).primaryColor,
    //               height: 1,
    //             ),
    //           ),
    //           onPressed: () => widget.onPopupClick(establishment.label),
    //           // onPressed: () {
    //           //   Posthog().capture(
    //           //     eventName: 'see-disease-establishment',
    //           //     properties: {
    //           //       'eventType': 'click',
    //           //       'resourceType': 'expert-center',
    //           //       'resourceId': establishment.id,
    //           //       'resourceName': establishment.label,
    //           //       '\$screen_name': 'Disease Info - ${widget.orphaNumber}'
    //           //     },
    //           //   );
    //           //   Navigator.of(context).pushNamed(
    //           //     ExpertCenterInfoScreen.routeName,
    //           //     arguments: establishment.id,
    //           //   );
    //           // },
    //         ),
    //     ],
    //   ),
    //   buildContext: context,
    //   textStyle: const TextStyle(
    //     fontSize: 14,
    //     color: Colors.black,
    //   ),
    //   mapsWidgetSize: _itemRect,
    // );

    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(24), topLeft: Radius.circular(24)),
      ),
      enableDrag: true,
      elevation: 100,
      barrierColor: Colors.transparent,
      isScrollControlled: true,
      backgroundColor: const Color(0xFFF5F5F9),
      builder: (context) {
        return DiseaseExpertCenterListBottomSheet(
            expertCenterList: establishment.institutions
                .map((inst) => inst.expertCenters)
                .expand((expCenters) => expCenters)
                .toList(),
            orphaNumber: widget.orphaNumber);
      },
    );

    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            latLng.latitude - 0.0001,
            latLng.longitude,
          ),
          zoom: 12,
        ),
      ),
    );
    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            latLng.latitude,
            latLng.longitude,
          ),
          zoom: 12,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
