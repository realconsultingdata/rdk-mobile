import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

class NoConnectionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            PhosphorIcons.wifiSlash,
            size: 100,
            color: Color(0xFF8F91AC),
          ),
          Text(
            FlutterI18n.translate(
                context, 'common.text.no_internet_connection'),
            style: Theme.of(context).textTheme.headline3,
          ),
          const SizedBox(
            height: 15,
          ),
          Text(
            FlutterI18n.translate(
                context, 'common.text.please_check_connection'),
            style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: Color(0xFF1E293B)),
          )
        ],
      ),
    );
  }
}
