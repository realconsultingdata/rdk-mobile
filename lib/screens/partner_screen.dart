import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/partner_sponsor/partner_sponsor_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class PartnerScreen extends StatefulWidget {
  static const routeName = '/partners';

  @override
  _PartnerScreenState createState() => _PartnerScreenState();
}

class _PartnerScreenState extends State<PartnerScreen> {
  @override
  void initState() {
    Posthog().screen(
      screenName: 'Partners',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('partner_screen'),
              title: FlutterI18n.translate(context, 'pages.partners')),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                bottom: 32,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: PartnerSponsorCard(
                      key: const ValueKey('rcd'),
                      title: 'Tekkare®',
                      description: language == 'fr'
                          ? "Partenaire technique. Entreprise française dont l'objet est de partager les connaissances médicales et scientifiques en développant des écosystèmes numériques pour valoriser les données de santé."
                          : 'Technical partner. French company whose purpose is to share medical and scientific knowledge by developing digital ecosystems to enhance health data.',
                      logoPath: 'assets/icons/tekkare.png',
                      logoPadding: 15,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: PartnerSponsorCard(
                      key: const ValueKey('orphanet'),
                      title: 'Orphanet',
                      description: language == 'fr'
                          ? "Partenaire scientifique. Créé par l’Inserm, afin de rassembler les connaissances disponibles sur les maladies rares pour améliorer le diagnostic, le soin et le traitement des patients."
                          : 'Scientific partner. Created by Inserm, to gather the available knowledge on rare diseases to improve diagnosis, care and treatment of patients.',
                      logoPath: 'assets/icons/orphanet.png',
                      logoPadding: 15,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
