import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/phenotype.dart';
import 'package:rdk_mobile/models/phenotype_disease.dart';
import 'package:rdk_mobile/models/phenotype_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/phenotypes.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/phenotype/phenotype_disease_tab_view.dart';
import 'package:rdk_mobile/widgets/phenotype/phenotype_hierarchy_tab_view.dart';
import 'package:rdk_mobile/widgets/phenotype/phenotype_information_tab_view.dart';
import 'package:rdk_mobile/widgets/tab_bar_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class PhenotypeInfoScreen extends StatefulWidget {
  static const routeName = '/phenotype-info';

  @override
  _PhenotypeInfoScreenState createState() => _PhenotypeInfoScreenState();
}

class _PhenotypeInfoScreenState extends State<PhenotypeInfoScreen>
    with TickerProviderStateMixin {
  var _isInit = true;
  var _isLoading = false;

  Phenotype _loadedPhenotype;
  PhenotypeTranslation _translation;

  Map<String, List<PhenotypeDisease>> _sortedDiseaseMap;
  // Map<String, int> _statisticsData;
  TabController _tabController;
  List<String> tabTitles = [
    'common.tabs.summary',
    'common.tabs.diseases',
    'common.tabs.classification'
  ];
  Locale currentLocale;

  @override
  void initState() {
    _tabController = TabController(length: tabTitles.length, vsync: this);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
      final dynamic hpoId = ModalRoute.of(context).settings.arguments;
      Provider.of<Phenotypes>(context)
          .getPhenotypeById(context, hpoId, currentLocale.languageCode)
          .then((data) {
        _loadedPhenotype = data;
        _translation = _loadedPhenotype.translations.firstWhere(
            (translation) => translation.language == currentLocale.languageCode,
            orElse: () => null);
        _sortedDiseaseMap = {};
        if (_loadedPhenotype.diseases.isNotEmpty) {
          _loadedPhenotype.diseases.sort((p1, p2) =>
              int.parse(p2.frequency
                  .split(' ')
                  .last
                  .replaceAll(RegExp(r'[(<>%)]'), '')
                  .split('-')[0]) -
              int.parse(p1.frequency
                  .split(' ')
                  .last
                  .replaceAll(RegExp(r'[(<>%)]'), '')
                  .split('-')[0]));
          for (PhenotypeDisease item in _loadedPhenotype.diseases) {
            _sortedDiseaseMap.putIfAbsent(
                item.frequency,
                () => _loadedPhenotype.diseases
                    .where((p) => p.frequency == item.frequency)
                    .toList());
          }
        }
        // _statisticsData = {
        //   "(100%)": 0,
        //   "(99-80%)": 0,
        //   "(79-30%)": 0,
        //   "(29-5%)": 0,
        //   "(<4-1%)": 0,
        //   "(0%)": 0
        // };
        // _sortedDiseaseMap.forEach((key, value) {
        //   _statisticsData.forEach((k, v) {
        //     if (key.contains(k)) {
        //       _statisticsData.update(k, (_) => value.length);
        //     }
        //   });
        // });
        Posthog().screen(
            screenName: 'Symptom Info - ${_loadedPhenotype.id}',
            properties: {
              'resourceType': 'phenotype',
              'resourceId': _loadedPhenotype.id
            });
        setState(() {
          _isLoading = false;
        });
      }).catchError((error) async {
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.connection_error'
                      : 'common.errors.an_error_occured'),
              style: Theme.of(context).textTheme.headline5,
            ),
            content: Text(
              FlutterI18n.translate(
                  context,
                  error == HttpStatus.gatewayTimeout
                      ? 'common.errors.unable_access_server'
                      : 'common.errors.something_went_wrong'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            actions: <Widget>[
              TextButton(
                child: Text(
                  FlutterI18n.translate(context, 'common.buttons.go_back'),
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              )
            ],
          ),
        );
        Navigator.of(context).pop();
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    return Stack(
      children: [
        OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            if (connected) {
              return child;
            } else {
              return Scaffold(
                appBar: CustomAppBar(
                  key: const ValueKey('no_connection_appbar'),
                  title: 'RDK',
                ),
                body: NoConnectionScreen(),
              );
            }
          },
          child: _isLoading
              ? Scaffold(
                  appBar: CustomAppBar(
                      key: const ValueKey('phenotype_info'),
                      title: FlutterI18n.translate(context, 'pages.model_info',
                          translationParams: {
                            'model': FlutterI18n.plural(
                                context, 'models.symptom.title', 1)
                          })),
                  body: Center(
                    child: CircularProgressIndicator(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                )
              : Listener(
                  onPointerDown: (_) =>
                      FocusManager.instance.primaryFocus.unfocus(),
                  child: Scaffold(
                    extendBodyBehindAppBar: true,
                    appBar: CustomAppBar(
                      key: const ValueKey('phenotype_info'),
                      title: currentLocale.languageCode == 'en'
                          ? _loadedPhenotype.hpoTerm
                          : _translation.hpoTerm,
                      tabBar: TabBarCard(
                        tabController: _tabController,
                        tabTitles: tabTitles,
                      ),
                    ),
                    body: NotificationListener<OverscrollIndicatorNotification>(
                      onNotification:
                          (OverscrollIndicatorNotification overScroll) {
                        overScroll.disallowGlow();
                        return false;
                      },
                      child: NestedScrollView(
                        headerSliverBuilder: (context, value) {
                          return [
                            SliverToBoxAdapter(
                              child: Container(
                                height: 32,
                              ),
                            ),
                          ];
                        },
                        body: TabBarView(
                          controller: _tabController,
                          children: [
                            PhenotypeInformationTabView(
                              loadedPhenotype: _loadedPhenotype,
                              // statisticsData: _statisticsData,
                            ),
                            PhenotypeDiseaseTabView(
                              sortedDiseaseList: _loadedPhenotype.diseases,
                            ),
                            PhenotypeHierarchyTabView(
                              hierarchies: _loadedPhenotype.hierarchies,
                              hpoId: _loadedPhenotype.id,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
