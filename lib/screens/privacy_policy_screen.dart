import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class PrivacyPolicyScreen extends StatelessWidget {
  static const routeName = '/privacy-policy';

  final String _privacyPolicy = """
    <h1>POLITIQUE CONFIDENTIALITE</h1>

    <p>Version de novembre 2023</p>
    <p>
        Cette politique de confidentialité vous informe de la façon dont as we know, Société par actions simplifiée enregistrée au RCS de Nanterre sous le numéro 948 156 146 et dont le siège social se situe 11-19 rue de la Vanne SOPARQ Bâtiment C 92120 Montrouge (la « Société ») traite vos données personnelles dans le cadre de l’utilisation du site / de l’application RDK. 
    </p>
    <p>Cette politique de confidentialité détaille également les droits dont vous disposez sur ces données et la façon de les exercer. </p>
    
    <h2>1. Qui est le responsable de traitement ?</h2>
    <p>
        Vous fournissez à la Société, dans le cadre de votre accès et utilisation de RDK, des informations qui permettent de vous identifier : ce sont des données personnelles. 
    </p>
    <p>
        La loi oblige la personne qui recueille des informations personnelles à informer les personnes concernées notamment sur les finalités et les durées de conservation des données.
    </p>
    <p>
        <i>
            Ces obligations sont prévues par le Règlement Européen (UE) 2016/679 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel (« RGPD ») et la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.
        </i>
    </p>
    <p>
        Une donnée personnelle est une information qui permet de vous identifier directement ou indirectement. <i>Exemples : Vos noms, prénoms, adresse email, adresse IP, numéro de téléphone</i>
    </p>
    <p>
        La Société agit en tant que <b>responsable des traitements</b> de vos données personnelles. 
    </p>
    <p>
        <i>
            Conformément à l’article 4 du RGPD, le responsable de traitement est l’entité qui, seule ou conjointement avec d'autres, détermine les finalités et les moyens du traitement. Conformément à l’article 5 du RGPD, nous sommes responsables des actions réalisées sur vos données en notre possession.
        </i>
    </p>
    <p>
        A ce titre, nous sommes responsables en cas de suppression, fuite ou modification de vos données personnelles.
    </p>
    <h2>2. Quelles catégories de données collectons-nous et pour quelles finalités ?</h2>
    <p>La Société collecte les catégories de données suivantes : </p>
    <ul>
        <li><b>Vos données de navigation et de suivi</b> Exemple : adresse IP, préférence de langue. </li>
        <li><b>Vos données d’identification et de contact</b> Exemple : nom, prénom et adresse email </li>
        <li><b>Vos données de localisation</b> Exemple : votre adresse </li>
    </ul>
    
    <p>Nous ne pouvons pas collecter vos données sans finalité ou but. Chacune de vos données en notre possession doit servir à une finalité. Voici la liste des finalités pour lesquelles nous utilisons vos données : </p>
    <ol>
        <li>Obtenir vos retours, suggestions et commentaires sur RDK (notamment des demandes d'ajout de fonctionnalités, signalements de disfonctionnements) et être informé de leur implémentation</li>
        <li>Obtenir des statistiques de mesure d'audience et performance du site</li>
        <li>Connaitre votre localisation pour vous proposer les centres expert le plus proches</li>
    </ol>
    
    <p><i>Conformément à l’article 5 du RGPD, les données sont collectées pour des finalités déterminées, explicites et légitimes. En cas de réutilisation des données ultérieurement, nous vous en informerons</i></p>
    
    
    <p>Nous devons, en outre, justifier que cette finalité repose sur l’un des fondements légaux prévus par la loi. Les voici :</p>
    
    
    <ul>
        <li>Obtenir vos retours, suggestions et commentaires sur RDK (notamment des demandes d'ajout de fonctionnalités, signalements de disfonctionnements) et être informé de leur implémentation</li>
    </ul>
    <h3>Intérêts légitimes et Consentement (pour l'addresse email)</h3>
    
    <p><i>Nous avons besoin d’avoir vos avis sur RDK afin de la faire évoluer pour qu’elle soit toujours compatible avec les besoins de nos utilisateurs. Si vous souhaitez suivre la prise en compte de vos commentaires et retours, vous pouvez renseigner votre email afin d’en être informé. Nous vous demanderons votre consentement dans cette hypothèse.</i></p>
    <ul>
        <li>Obtenir des statistiques de mesure d'audience et performance du site</li>
        <li>Connaitre votre localisation pour vous proposer les centres expert le plus proches</li>
    </ul>
    <h3>Consentement ou Intérêts légitimes</h3>
    
    <p><i>Les traceurs utilisés sur RDK ne nécessitent pas de consentement, la collecte de données qu’ils permettent reposera sur nos intérêts légitimes d’obtenir de telles informations afin de faire évoluer RDK. Nous vous informons de l’utilisation de cookies via la bannière cookies et la politique de cookies.</i></p>
    <p><i>Concernant les données de localisation, votre navigateur vous demandera expressément d’accepter ou bloquer la collecte de ces informations. Votre consentement est donc nécessaire.</i></p>
    
    <h2>3. Quels sont vos droits sur les données personnelles que nous collectons ?</h2>
    <p>La loi et le RGPD vous garantissent 7 droits sur les données que nous collectons. </p>
    <p>Les voici : </p>
    <ul>
        <li><b>Droit d’accès</b> vous permet d’obtenir une copie de vos données personnelles et des informations concernant
            leur traitement</li>
        <li><b>Droit à l’effacement</b> vous permet de demander la suppression de vos données dans certaines circonstances
        </li>
        <li><b>Droit à la rectification</b> vous permet de modifier vos données inexactes</li>
        <li><b>Directives post mortem</b> vous permet de décider de la manière dont vos données seront conservées, partagées
            ou supprimées après votre décès</li>
        <li><b>Droit d’opposition</b> vous permet de retirer votre consentement et/ou d’interrompre le traitement de vos
            données dans certaines circonstances</li>
        <li><b>Droit à la limitation</b> vous permet de demander à restreindre les données qui sont traitées dans certaines
            circonstances</li>
        <li><b>Droit à la portabilité</b> vous permet de recevoir les données que vous avez fournies afin de les transmettre
            à une autre entité</li>
    </ul>
    <p>Ces droits s’appliquent différemment selon la base légale sur laquelle nous fondons nos finalités. </p>
    <ul>
        <li>Obtenir vos retours, suggestions et commentaires sur RDK (notamment des demandes d'ajout de fonctionnalités, signalements de disfonctionnements) et être informé de leur implémentation</li>
        <li>Obtenir des statistiques de mesure d'audience et performance du site</li>
        <li>Connaitre votre localisation pour vous proposer les centres expert le plus proches</li>
    
    </ul>
    <h3>Intérêts légitimes ou Consentement</h3>
    <p>Vos droits :</p>
    <ul>
        <li><b>Droit d’accès</b> vous permet d’obtenir une copie de vos données personnelles et des informations concernant
            leur traitement</li>
        <li><b>Droit à l’effacement</b> vous permet de demander la suppression de vos données dans certaines circonstances
        </li>
        <li><b>Droit à la rectification</b> vous permet de modifier vos données inexactes</li>
        <li><b>Directives post mortem</b> vous permet de décider de la manière dont vos données seront conservées, partagées
            ou supprimées après votre décès</li>
        <li><b>Droit d’opposition</b> vous permet de retirer votre consentement et/ou d’interrompre le traitement de vos
            données dans certaines circonstances</li>
        <li><b>Droit à la limitation</b> vous permet de demander à restreindre les données qui sont traitées dans certaines
            circonstances</li>
    </ul>
    <p><i>Conformément aux articles 15 à 21 du RGDP, vous pouvez nous contacter pour exercer certains droits sur les données en notre possession. À cette occasion, nous pouvons exiger des informations complémentaires. </i></p>
    <p><i>Par la suite, vous pouvez également saisir la CNIL pour lui adresser une réclamation si vous estimez que nous n’avons pas respecté vos droits ou la réglementation relative à la protection des données. </i></p>
    <p>Nous avons nommé un délégué à la protection des données personnelles qui peut être contactée par email à : <a href="mailto:dataprivacy@asweknow.com">dataprivacy@asweknow.com</a>. </p>
    <p>Pour exercer ces droits (et de manière générale pour toute demande, vous pouvez nous contacter) aux coordonnées :</p>
    <ul>
        <li>Par courrier postal à : As we know, 11-19 rue de la Vanne, 92120 Montrouge</li>
        <li>Par email à : <a href="mailto:dataprivacy@asweknow.com">dataprivacy@asweknow.com</a>.</li>
    </ul>
    
    
    <h2>4. Pendant quelle durée conservons-nous vos données personnelles ?</h2>
    
    <ul>
        <li>Obtenir vos retours, suggestions et commentaires sur RDK (notamment des demandes d'ajout de fonctionnalités, signalements de disfonctionnements) et être informé de leur implémentation. Pendant 3 ans à compter du dernier échange avec vous</li>
        <li>Obtenir des statistiques de mesure d'audience et performance du site. La durée est précisée dans la politique cookies</li>
        <li>Connaitre votre localisation pour vous proposer les centres expert le plus proches. Pendant la durée de l’utilisation de RDK</li>
    </ul>
    
    
    <h2>5. Certains tiers ou prestataires ont-ils accès à vos données ?</h2>
    <p>Les données collectées seront communiquées aux destinataires suivants en interne: CTO, CEO, marketing.</p>
    
    <p>Nous allons devoir permettre à des tiers d’accéder à vos données. Nous nous assurons que ces tiers les utilisent en
        toute sécurité.</p>
    <p>En effet, afin de réaliser tout ou partie des finalités mentionnées ci-dessus, nous transmettrons et partagerons vos
        données avec les destinataires indiqués ci-après:</p>
    <ul>
        <li>Prestataire technique pour l’analyse des données de mesure d’audience (Posthog)</li>
        <li>Prestataire pour la collecte d’avis et retours (Typeform)</li>
        <li>Prestataire pour la notification d’incidents et la demande de fonctionnalités (Click up)</li>
        <li>Navigateur internet (pour la donnée de localisation)</li>
    </ul>
    <p>Si nous réalisons une opération sur notre société (cession, levée de fonds…), nous transférons également vos données
        au tiers impliqué et à ses conseils.</p>
    <p>Dans ce cas, vos données seront traitées par un nouveau responsable de traitement, à savoir l’entité qui a reçu vos
        données. Nous ne serons plus responsables de leur traitement. Il lui appartiendra de vous informer directement.</p>
    
    
    <h2>6. Vos données sont-elles transférées ou accessibles en dehors de l’Union Européenne ?</h2>
    <p>Si nous utilisons les services d’un prestataire qui se situe en dehors de l’Union Européenne, nous mettrons en place
        des garanties nécessaires et adaptées.</p>
    <p><i>Conformément aux articles 44 et suivants du RGPD, en cas de transfert des données en dehors de l’Union Européenne, il
        convient de recourir à des mesures prévues par le RGPD pour sécuriser ces transferts.</i></p>
    
    <h2>7. Informations et mise à jour de la politique de confidentialité</h2>
    <p>Si nous actualisons ce document, nous vous en informerons par tous moyens en cas de modification majeure. Nous vous invitons également à prendre connaissance régulièrement de ce document. </p>
    <p><i>Dans le cadre de notre obligation de transparence, nous actualiserons ce document en cas de modification des
        traitements de vos données (ex : nouvelles finalités, nouvelle typologie de sous-traitants…). Vous devez prendre
        connaissance de ces modifications afin d’être informé des différents traitements de vos données.</i></p>
    <p>Si vous avez des questions sur le contenu de ce document, nous sommes à votre disposition.</p>
    <p><i>Vous pouvez nous contacter </i>:</p>
    <ul>
        <li>Par courrier postal à : as we know, 11-19 rue de la Vanne, Soparq, 92120 Montrouge</li>
        <li>Par email à : <a href="mailto:dataprivacy@asweknow.com">dataprivacy@asweknow.com</a></li>
    </ul>
  """;

  const PrivacyPolicyScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: 'Privacy Policy',
    );
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('legal_privacy_policy'),
              title:
                  FlutterI18n.translate(context, 'common.tabs.privacy_policy')),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                left: 15,
                right: 15,
                bottom: 30),
            child: Html(
              data: _privacyPolicy,
              onLinkTap: (link, ctx, attributes, element) {
                launchUrl(Uri.parse(link));
              },
              style: {
                "body": Style(
                  margin: EdgeInsets.zero,
                  padding: EdgeInsets.zero,
                  lineHeight: const LineHeight(1.5),
                ),
                "a": Style(color: Theme.of(context).primaryColor),
              },
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
