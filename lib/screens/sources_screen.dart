import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/flavor_config.dart';
import 'package:rdk_mobile/models/rdk_source.dart';
import 'package:rdk_mobile/providers/rdk_sources.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/rdk_source/source_card.dart';
import 'package:collection/collection.dart';

import 'no_connection_screen.dart';

class SourcesScreen extends StatefulWidget {
  static const routeName = '/sources';

  @override
  _SourcesScreenState createState() => _SourcesScreenState();
}

class _SourcesScreenState extends State<SourcesScreen> {
  List<RdkSource> rdkSources;
  List<RdkSource> displayList;
  List<Map<String, Object>> countryList = [];

  @override
  void initState() {
    rdkSources = Provider.of<RdkSources>(context, listen: false).rdkSources;
    for (var src in rdkSources) {
      if (!countryList.any((element) => const DeepCollectionEquality()
          .equals(element, {'name': src.country, 'value': src.countryCode}))) {
        if (src.country != null && src.countryCode != null) {
          countryList.add({'name': src.country, 'value': src.countryCode});
        }
      }
    }
    displayList = [...rdkSources];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: const ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('sources_screen'),
              title: FlutterI18n.translate(context, 'pages.sources')),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                bottom: 32,
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 15,
                  ),
                  // SourceSearchCard(
                  //   countries: countryList,
                  //   handleSearch: filterSourceList,
                  // ),
                  ...displayList.map((source) => SourceCard(
                        source: source,
                      )),
                  SizedBox(
                    height: MediaQuery.of(context).padding.bottom,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }

  void filterSourceList(String countryCode, String queryString) {
    setState(() {
      displayList = rdkSources
          .where((src) =>
              (src.countryCode == null || src.countryCode == countryCode) &&
              (src.name.toLowerCase().contains(queryString.toLowerCase()) ||
                  src.abbr.toLowerCase().contains(queryString.toLowerCase())))
          .toList();
    });
  }
}
