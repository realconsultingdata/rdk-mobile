import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/partner_sponsor/partner_sponsor_card.dart';

import '../flavor_config.dart';
import 'no_connection_screen.dart';

class SponsorScreen extends StatefulWidget {
  static const routeName = '/sponsors';

  @override
  _SponsorScreenState createState() => _SponsorScreenState();
}

class _SponsorScreenState extends State<SponsorScreen> {
  @override
  void initState() {
    Posthog().screen(
      screenName: 'Sponsors',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Stack(children: [
      OfflineBuilder(
        connectivityBuilder: (BuildContext context,
            ConnectivityResult connectivity, Widget child) {
          final bool connected = connectivity != ConnectivityResult.none;
          if (connected) {
            return child;
          } else {
            return Scaffold(
              appBar: CustomAppBar(
                key: ValueKey('no_connection_appbar'),
                title: 'RDK',
              ),
              body: NoConnectionScreen(),
            );
          }
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: CustomAppBar(
              key: const ValueKey('sponsor_screen'),
              title: FlutterI18n.translate(context, 'pages.sponsors')),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 60,
                bottom: 32,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: PartnerSponsorCard(
                      key: const ValueKey('pfizer'),
                      title: 'Pfizer',
                      description: language == 'fr'
                          ? 'Acteur clef dans la recherche sur les maladies rares, Pfizer est un des premiers soutiens au projet : dans l’innovation thérapeutique, le développement d’outils d’accompagnement des patients, la lutte contre l’errance diagnostique, etc.'
                          : 'Pfizer, a key player in research on rare diseases, is one of the first supporters of the project: in therapeutic innovation, in the development of patient support tools, in the fight against misdiagnosis, and so on.',
                      logoPath: 'assets/icons/pfizer.png',
                      logoPadding: 15,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: PartnerSponsorCard(
                      key: const ValueKey('amylyx'),
                      title: 'Amylyx®',
                      description: language == 'fr'
                          ? 'Amylyx® engagé dans la recherche pour vaincre les maladies dégénératives. Amylyx® soutient les initiatives visant à améliorer le diagnostic et la prise en charge des personnes qui en sont atteintes.'
                          : 'Amylyx® is committed to research to overcome degenerative diseases and supports initiatives to improve diagnosis and care for those affected.',
                      logoPath: 'assets/icons/amylyx.png',
                      logoPadding: 25,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: PartnerSponsorCard(
                      key: const ValueKey('sobi'),
                      title: 'Sobi',
                      description: language == 'fr'
                          ? 'Sobi est une société biopharmaceutique internationale spécialisée dans les maladies rares. Nous nous engageons à fournir un accès à des traitements innovants qui transforment la vie des personnes atteintes de maladies rares.'
                          : 'Sobi is a specialised international biopharmaceutical company transforming the lives of people with rare and debilitating diseases.',
                      logoPath: 'assets/icons/sobi.png',
                      logoPadding: 20,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      FlavorConfig.isProduction()
          ? const SizedBox.shrink()
          : Align(
              alignment: Alignment.topLeft,
              child: Banner(
                message: FlavorConfig.instance.name,
                location: BannerLocation.topStart,
                color: FlavorConfig.instance.color,
              ),
            ),
    ]);
  }
}
