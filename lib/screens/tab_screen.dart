import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/flavor_config.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/bookmark_screen.dart';
import 'package:rdk_mobile/screens/diagnostic_screen.dart';
import 'package:rdk_mobile/screens/expert_center_search_screen.dart';
import 'package:rdk_mobile/screens/exploration_screen.dart';
import 'package:rdk_mobile/screens/no_connection_screen.dart';
import 'package:rdk_mobile/screens/sources_screen.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:rdk_mobile/widgets/disclaimer/disclaimer_dialog.dart';
import 'package:rdk_mobile/widgets/main_drawer.dart';
import 'package:rdk_mobile/widgets/tab_bar_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TabScreen extends StatefulWidget {
  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen>
    with AutomaticKeepAliveClientMixin<TabScreen>, TickerProviderStateMixin {
  final List<Map<String, Object>> _pages = [
    {
      'title': 'pages.screening_tools',
      'icon': const Icon(
        PhosphorIcons.crosshairSimple,
        size: 32,
      ),
      'name': 'Screening',
      'route': 'screening',
    },
    {
      'title': 'pages.search_tools',
      'icon': const Icon(
        PhosphorIcons.compass,
        size: 32,
      ),
      'name': 'Browsing',
      'route': 'browsing',
    },
    {
      'title': 'pages.centers',
      'icon': const Icon(
        PhosphorIcons.buildings,
        size: 32,
      ),
      'name': 'Browsing',
      'route': 'browsing',
    },
    {
      'title': 'pages.bookmarks',
      'icon': const Icon(
        PhosphorIcons.bookmarkSimple,
        size: 32,
      ),
      'name': 'Saved',
      'route': 'bookmarks',
    },
  ];

  TabController _explorationTabController;

  final List<String> _explorationTabTitles = [
    'common.tabs.diseases',
    'common.tabs.symptoms',
    'common.tabs.genes',
  ];

  final List<Widget> tabWidgets = [
    DiagnosticScreen(),
    const SizedBox(),
    const SizedBox(),
    const SizedBox(),
  ];

  int _selectedTabIndex = 0;

  void _selectTab(int index) {
    if (_selectedTabIndex != index) {
      switch (index) {
        case 0:
          Posthog().screen(
            screenName: 'Screening',
          );
          break;
        case 1:
          Posthog().screen(
            screenName: 'Search',
          );
          break;
        case 2:
          Posthog().screen(
            screenName: 'Expert Center Search',
          );
          break;
        case 3:
          Posthog().screen(
            screenName: 'Bookmarks',
          );
          break;
      }
      setState(() {
        if (tabWidgets[index] is SizedBox) {
          if (index == 1) {
            _explorationTabController = TabController(
                length: _explorationTabTitles.length, vsync: this);
            tabWidgets[index] = ExplorationScreen(
              tabController: _explorationTabController,
            );
          } else if (index == 2) {
            tabWidgets[index] = const ExpertCenterSearchScreen();
          } else {
            tabWidgets[index] = const BookmarkScreen();
          }
        }
        _selectedTabIndex = index;
      });
    }
  }

  final keyIsFirstLoaded = 'is_first_loaded';

  @override
  void initState() {
    if (Platform.isAndroid) {
      checkForUpdateAndroid();
    }
    _showDialog(context);
    Posthog().screen(
      screenName: 'Screening',
    );
    super.initState();
  }

  @override
  void dispose() {
    _explorationTabController.dispose();
    super.dispose();
  }

  _showDialog(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstLoaded = prefs.getBool(keyIsFirstLoaded);
    if (isFirstLoaded == null) {
      await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => DisclaimerDialog(
          agreeDisclaimerFunction: () {
            prefs.setBool(keyIsFirstLoaded, false);
            Navigator.of(context).pop();
          },
        ),
      );
    }
  }

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  AppUpdateInfo _updateInfo;

  Future<void> checkForUpdateAndroid() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
      if (info.updateAvailability == UpdateAvailability.updateAvailable) {
        InAppUpdate.performImmediateUpdate().catchError((e) {
          showSnack(e.toString());
          return AppUpdateResult.inAppUpdateFailed;
        });
      }
    }).catchError((e) {
      showSnack(e.toString());
    });
  }

  void showSnack(String text) {
    if (_scaffoldKey.currentContext != null) {
      ScaffoldMessenger.of(_scaffoldKey.currentContext)
          .showSnackBar(SnackBar(content: Text(text)));
    }
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CurrentLocale>(context).currentLocale();
    super.build(context);
    return Stack(
      children: [
        OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            if (connected) {
              return child;
            } else {
              return Scaffold(
                appBar: CustomAppBar(
                  key: const ValueKey('no_connection_appbar'),
                  title: 'RDK',
                ),
                body: NoConnectionScreen(),
              );
            }
          },
          child: Listener(
            onPointerDown: (_) => FocusManager.instance.primaryFocus.unfocus(),
            child: Scaffold(
              extendBody: true,
              extendBodyBehindAppBar: true,
              appBar: CustomAppBar(
                key: ValueKey(_pages[_selectedTabIndex]['title']),
                title: FlutterI18n.translate(
                    context, _pages[_selectedTabIndex]['title']),
                tabBar: _selectedTabIndex == 1
                    ? TabBarCard(
                        tabController: _explorationTabController,
                        tabTitles: _explorationTabTitles,
                      )
                    : null,
              ),
              endDrawer: const MainDrawer(),
              body: IndexedStack(
                index: _selectedTabIndex,
                children: tabWidgets,
              ),
              bottomNavigationBar: ClipRRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
                  child: BottomNavigationBar(
                    onTap: _selectTab,
                    selectedItemColor: Theme.of(context).primaryColor,
                    unselectedItemColor: const Color(0xFF8487AC),
                    selectedFontSize: 11,
                    unselectedFontSize: 11,
                    currentIndex: _selectedTabIndex,
                    backgroundColor: const Color(0xFFECECF2).withOpacity(0.80),
                    elevation: 0,
                    type: BottomNavigationBarType.fixed,
                    showSelectedLabels: true,
                    showUnselectedLabels: true,
                    items: _pages
                        .map(
                          (page) => BottomNavigationBarItem(
                            icon: page['icon'],
                            label:
                                FlutterI18n.translate(context, page['title']),
                            tooltip:
                                FlutterI18n.translate(context, page['title']),
                          ),
                        )
                        .toList(),
                  ),
                ),
              ),
            ),
          ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class MyKeys {
  static final first = GlobalKey(debugLabel: 'page1');
  static final second = GlobalKey(debugLabel: 'page2');
  static final third = GlobalKey(debugLabel: 'page3');

  static List<GlobalKey> getKeys() => [first, second, third];
}
