import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/custom_appbar.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../flavor_config.dart';

class UserGuideViewer extends StatelessWidget {
  static const routeName = '/user-guide';

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String languageCode = currentLocale.languageCode;
    Posthog().screen(
      screenName: 'User Guide',
    );
    return Stack(
      children: [
        Scaffold(
          appBar: CustomAppBar(
              key: const ValueKey('user_guide_screen'),
              title: FlutterI18n.translate(context, 'pages.user_guide')),
          body: Padding(
            padding: EdgeInsets.only(
                top: 15,
                left: 15,
                right: 15,
                bottom: MediaQuery.of(context).padding.bottom + 10),
            child: SfPdfViewer.network(
              'https://rcd-media.com/apps/rdk/guides/$languageCode.pdf',
            ),
          ),
        ),
        FlavorConfig.isProduction()
            ? const SizedBox.shrink()
            : Align(
                alignment: Alignment.topLeft,
                child: Banner(
                  message: FlavorConfig.instance.name,
                  location: BannerLocation.topStart,
                  color: FlavorConfig.instance.color,
                ),
              ),
      ],
    );
  }
}
