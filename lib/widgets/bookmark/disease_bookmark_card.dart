import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_synonym.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class DiseaseBookmarkCard extends StatefulWidget {
  const DiseaseBookmarkCard({
    Key key,
    @required Disease diseaseData,
  })  : _diseaseData = diseaseData,
        super(key: key);

  final Disease _diseaseData;

  @override
  _DiseaseExplorationCardState createState() => _DiseaseExplorationCardState();
}

class _DiseaseExplorationCardState extends State<DiseaseBookmarkCard> {
  Locale currentLocale;
  DiseaseTranslation translation;

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String languageCode = currentLocale.languageCode;
    List<DiseaseSynonym> synonyms = widget._diseaseData.synonyms
        .where((syn) => syn.language == languageCode).toList();
    if (languageCode != 'en') {
      translation = widget._diseaseData.translations.firstWhere(
          (trans) => trans.language == languageCode,
          orElse: () => null);
    }
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget._diseaseData.disorderGroup != 'Disorder') ...[
              Container(
                padding:
                    widget._diseaseData.disorderGroup == 'Group of disorders'
                        ? const EdgeInsets.symmetric(vertical: 4, horizontal: 8)
                        : const EdgeInsets.symmetric(vertical: 4),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  color:
                      widget._diseaseData.disorderGroup == 'Group of disorders'
                          ? const Color(0xFF6C5CE3).withOpacity(0.1)
                          : Colors.transparent,
                ),
                child: Text(
                  languageCode != 'en' && translation != null
                      ? translation.disorderGroup
                      : widget._diseaseData.disorderGroup,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: widget._diseaseData.disorderGroup ==
                              'Group of disorders'
                          ? const Color(0xFF4533C9)
                          : const Color(0xFFADAFCA),
                      height: 1.2),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
            ],
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    languageCode != 'en' && translation != null
                        ? translation.name
                        : widget._diseaseData.name,
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.headline3.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.headline3.fontWeight,
                        color: const Color(0xFF1E293B),
                        height: 1.2),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: const BoxConstraints(),
                  splashRadius: 20,
                  icon: Icon(
                    PhosphorIcons.bookmarkSimpleFill,
                    size: 24,
                    color: Theme.of(context).primaryColor,
                  ),
                  onPressed: () async {
                    await Provider.of<BookmarkDiseases>(context, listen: false)
                        .deleteDisease(
                            context, widget._diseaseData.orphaNumber);
                  },
                ),
              ],
            ),
            if (widget._diseaseData.contents != null &&
                widget._diseaseData.contents != '') ...[
              const SizedBox(
                height: 32,
              ),
              Html(
                data: languageCode != 'en' && translation != null
                    ? translation.contents
                    : widget._diseaseData.contents,
                style: {
                  "body": Style(
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero,
                    fontSize: const FontSize(14),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xFF1E293B),
                    lineHeight: const LineHeight(1.5),
                  )
                },
              ),
            ],
            if (synonyms != null && synonyms.isNotEmpty) ...[
              const SizedBox(
                height: 32,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: synonyms
                    .map<Widget>(
                      (item) => Chip(
                        key: ValueKey(item),
                        label: Text(item.synonym),
                        labelStyle: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.bodyText2.fontSize,
                          fontWeight:
                              Theme.of(context).textTheme.bodyText2.fontWeight,
                          color: const Color(0xFF1E293B),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                        backgroundColor:
                            const Color(0xFF8F91AC).withOpacity(0.1),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      ),
                    )
                    .toList(),
              ),
            ],
            const SizedBox(
              height: 24,
            ),
            Text(
              'ORPHA: ' + widget._diseaseData.orphaNumber,
              style: const TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF8487AC),
                  height: 1.2),
            ),
          ],
        ),
      ),
    );
  }
}
