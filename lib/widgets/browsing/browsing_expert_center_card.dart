import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class BrowsingExpertCenterCard extends StatelessWidget {
  final dynamic expertCenterData;

  const BrowsingExpertCenterCard({
    Key key,
    this.expertCenterData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List statusList = expertCenterData.source['expert_center_status'];

    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              expertCenterData.source['expert_center_name'],
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700,
                color: Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              '${expertCenterData.source['label']} - ${expertCenterData.source['zip_code']} ${expertCenterData.source['city']}',
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Color(0xFF64748B),
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            Wrap(
              spacing: 5,
              runSpacing: 5,
              children: [
                if (statusList
                    .where((element) => element['128'] != null)
                    .isNotEmpty)
                  Chip(
                    key: const ValueKey('128'),
                    label: Text(
                      FlutterI18n.translate(
                          context, 'models.status_flag.value.128'),
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                      ),
                    ),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                    backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(4),
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                if (statusList
                    .where((element) => element['64'] != null)
                    .isNotEmpty)
                  Chip(
                    key: const ValueKey('64'),
                    label: Text(
                      FlutterI18n.translate(
                          context, 'models.status_flag.value.64'),
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                      ),
                    ),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                    backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(4),
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              '${expertCenterData.source['orpha_number_count'].toString()} ${FlutterI18n.plural(context, 'models.disease.title', expertCenterData.source['orpha_number_count']).toLowerCase()} ${FlutterI18n.plural(context, FlutterI18n.translate(context, 'common.text.and_group_covered.value', translationParams: {
                        'value': expertCenterData.source['category_count'].toString()
                      }),  expertCenterData.source['category_count'].toInt())}',
              style: const TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Color(0xFF1E293B),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
