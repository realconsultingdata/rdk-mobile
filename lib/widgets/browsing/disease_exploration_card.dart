import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';

class DiseaseExplorationCard extends StatefulWidget {
  const DiseaseExplorationCard({
    Key key,
    @required diseaseData,
  })  : _diseaseData = diseaseData,
        super(key: key);

  final _diseaseData;

  @override
  _DiseaseExplorationCardState createState() => _DiseaseExplorationCardState();
}

class _DiseaseExplorationCardState extends State<DiseaseExplorationCard> {
  bool _isBookmarked = false;

  List<Disease> _diseases = [];

  @override
  Widget build(BuildContext context) {
    _diseases = Provider.of<BookmarkDiseases>(context).diseases;
    _isBookmarked = _diseases.firstWhere(
            (disease) =>
                disease.orphaNumber ==
                widget._diseaseData.source['orpha_number'].toString(),
            orElse: () => null) !=
        null;
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget._diseaseData.source['disorder_group'] != 'Pathologie' &&
                widget._diseaseData.source['disorder_group'] != 'Disorder') ...[
              Container(
                padding: widget._diseaseData.source['disorder_group'] ==
                            'Group of disorders' ||
                        widget._diseaseData.source['disorder_group'] ==
                            'Groupe de pathologies'
                    ? const EdgeInsets.symmetric(vertical: 4, horizontal: 8)
                    : const EdgeInsets.symmetric(vertical: 4),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  color: widget._diseaseData.source['disorder_group'] ==
                              'Group of disorders' ||
                          widget._diseaseData.source['disorder_group'] ==
                              'Groupe de pathologies'
                      ? const Color(0xFF6C5CE3).withOpacity(0.1)
                      : Colors.transparent,
                ),
                child: Text(
                  widget._diseaseData.source['disorder_group'],
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: widget._diseaseData.source['disorder_group'] ==
                                  'Group of disorders' ||
                              widget._diseaseData.source['disorder_group'] ==
                                  'Groupe de pathologies'
                          ? const Color(0xFF4533C9)
                          : const Color(0xFFADAFCA),
                      height: 1.2),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
            ],
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    widget._diseaseData.source['name'],
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.headline3.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.headline3.fontWeight,
                        color: const Color(0xFF1E293B),
                        height: 1.2),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: const BoxConstraints(),
                  splashRadius: 20,
                  icon: Icon(
                    _isBookmarked
                        ? PhosphorIcons.bookmarkSimpleFill
                        : PhosphorIcons.bookmarkSimple,
                    size: 24,
                    color: _isBookmarked
                        ? Theme.of(context).primaryColor
                        : const Color(0xFF64748B),
                  ),
                  onPressed: () async {
                    if (!_isBookmarked) {
                      Provider.of<BookmarkDiseases>(context, listen: false)
                          .addDisease(
                              context,
                              Disease(
                                  orphaNumber: widget
                                      ._diseaseData.source['orpha_number'],
                                  name: widget._diseaseData.source['name']))
                          .then((value) {
                        if (value != null) {
                          setState(() {
                            _isBookmarked = true;
                          });
                        }
                      });
                    } else {
                      Provider.of<BookmarkDiseases>(context, listen: false)
                          .deleteDisease(context,
                              widget._diseaseData.source['orpha_number'])
                          .then((value) {
                        if (value > 0) {
                          setState(() {
                            _isBookmarked = false;
                          });
                        }
                      });
                    }
                  },
                ),
              ],
            ),
            if (widget._diseaseData.source['contents'] != null &&
                widget._diseaseData.source['contents'] != '') ...[
              const SizedBox(
                height: 32,
              ),
              Html(
                data: widget._diseaseData.source['contents'],
                style: {
                  "body": Style(
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero,
                    fontSize: const FontSize(14),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xFF1E293B),
                    lineHeight: const LineHeight(1.5),
                  )
                },
              ),
            ],
            if (widget._diseaseData.source['synonym'] != null &&
                widget._diseaseData.source['synonym'].length > 0) ...[
              const SizedBox(
                height: 32,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: widget._diseaseData.source['synonym']
                    .map<Widget>(
                      (item) => Chip(
                        key: ValueKey(item),
                        label: Text(item),
                        labelStyle: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.bodyText2.fontSize,
                          fontWeight:
                              Theme.of(context).textTheme.bodyText2.fontWeight,
                          color: const Color(0xFF1E293B),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                        backgroundColor:
                            const Color(0xFF8F91AC).withOpacity(0.1),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      ),
                    )
                    .toList(),
              ),
            ],
            const SizedBox(
              height: 24,
            ),
            Text(
              'ORPHA: ' + widget._diseaseData.source['orpha_number'].toString(),
              style: const TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF8487AC),
                  height: 1.2),
            ),
          ],
        ),
      ),
    );
  }
}
