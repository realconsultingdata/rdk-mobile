import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class GeneExplorationCard extends StatelessWidget {
  const GeneExplorationCard({
    Key key,
    @required geneData,
  })  : _geneData = geneData,
        super(key: key);

  final _geneData;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _geneData.source['gene_name'],
              style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline3.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.2),
            ),
            RichText(
              textScaleFactor: MediaQuery.of(context).textScaleFactor,
              text: TextSpan(
                style: const TextStyle(
                  fontSize: 14,
                  height: 1.5,
                ),
                children: [
                  TextSpan(
                    text:
                        '${FlutterI18n.translate(context, 'models.gene.symbol')}: ',
                    style: const TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color(0xFFADAFCA),
                    ),
                  ),
                  TextSpan(
                    text: _geneData.source['symbol'],
                    style: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.italic,
                      color: Color(0xFFADAFCA),
                    ),
                  )
                ],
              ),
            ),
            // Text(
            //   '${FlutterI18n.translate(context, 'models.gene.symbol')}: ${_geneData.source['symbol']}',
            //   style: const TextStyle(
            //       fontSize: 12,
            //       fontWeight: FontWeight.w400,
            //       color: Color(0xFFADAFCA),
            //       height: 1.5),
            // ),
            if (_geneData.source['gene_synonym'] != null &&
                _geneData.source['gene_synonym'].length > 0) ...[
              const SizedBox(
                height: 32,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: _geneData.source['gene_synonym'].map<Widget>((item) {
                  if (item != null) {
                    return Chip(
                      key: ValueKey(item),
                      label: Text(item),
                      labelStyle: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.bodyText2.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.bodyText2.fontWeight,
                        color: const Color(0xFF1E293B),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(8),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    );
                  } else {
                    return const SizedBox.shrink();
                  }
                }).toList(),
              ),
            ]
          ],
        ),
      ),
    );
  }
}
