import 'package:flutter/material.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class PhenotypeExplorationCard extends StatelessWidget {
  const PhenotypeExplorationCard({
    Key key,
    @required phenotypeData,
  })  : _phenotypeData = phenotypeData,
        super(key: key);

  final _phenotypeData;

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (_phenotypeData.source['phenotype_category'] != null &&
                _phenotypeData.source['phenotype_category'] != '') ...[
              Text(
                _phenotypeData.source['phenotype_category']
                    .toString()
                    .replaceAll('|', '-'),
                style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFFADAFCA),
                    height: 1.2),
              ),
              const SizedBox(
                height: 8,
              ),
            ],
            Text.rich(
              TextSpan(children: [
                TextSpan(text: _phenotypeData.source['hpo_term']),
                if (language == 'fr' &&
                    _phenotypeData.source['hpo_term_translation_source'] ==
                        'DeepL')
                  const WidgetSpan(
                    alignment: PlaceholderAlignment.top,
                    child: Icon(
                      PhosphorIcons.asteriskSimple,
                      size: 10,
                      color: Color(0xFF006394),
                    ),
                  ),
              ]),
              textScaleFactor: MediaQuery.of(context).textScaleFactor,
              style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline3.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.2),
            ),
            if (_phenotypeData.source['contents'] != null &&
                _phenotypeData.source['contents'] != '') ...[
              const SizedBox(
                height: 32,
              ),
              Text.rich(
                TextSpan(children: [
                  TextSpan(text: _phenotypeData.source['contents']),
                  if (language == 'fr' &&
                      _phenotypeData.source['contents_translation_source'] ==
                          'DeepL')
                    const WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Icon(
                        PhosphorIcons.asteriskSimple,
                        size: 10,
                        color: Color(0xFF006394),
                      ),
                    ),
                ]),
                textScaleFactor: MediaQuery.of(context).textScaleFactor,
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                  fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.5,
                ),
              ),
            ],
            if (_phenotypeData.source['synonym'] != null &&
                _phenotypeData.source['synonym'].length > 0) ...[
              const SizedBox(
                height: 32,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: [
                  ..._phenotypeData.source['synonym']
                      .map<Widget>(
                        (item) => Chip(
                          key: ValueKey(item),
                          label: Text(item),
                          labelStyle: TextStyle(
                            fontSize:
                                Theme.of(context).textTheme.bodyText2.fontSize,
                            fontWeight: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .fontWeight,
                            color: const Color(0xFF1E293B),
                          ),
                          labelPadding:
                              const EdgeInsets.symmetric(horizontal: 4),
                          backgroundColor:
                              const Color(0xFF8F91AC).withOpacity(0.1),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                          ),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                        ),
                      )
                      .toList(),
                  if (language == 'fr' &&
                      _phenotypeData.source['synonym_translation_source'] ==
                          'DeepL')
                    const Icon(
                      PhosphorIcons.asteriskSimple,
                      size: 10,
                      color: Color(0xFF006394),
                    ),
                ],
              ),
            ]
          ],
        ),
      ),
    );
  }
}
