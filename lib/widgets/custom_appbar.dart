import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/language_menu.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar(
      {Key key,
      this.title,
      this.backBtnText,
      this.tabBar,
      this.isBookmarked,
      this.setBookmark})
      : super(key: key);

  final String title;
  final String backBtnText;
  final Widget tabBar;
  final bool isBookmarked;
  final Function setBookmark;

  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  @override
  Size get preferredSize =>
      Size.fromHeight((key.toString().contains('disease_info') ||
              key.toString().contains('gene_info') ||
              key.toString().contains('phenotype_info') ||
              key.toString().contains('expert_center_info') ||
              key.toString().contains('search_tools'))
          ? kToolbarHeight * 2
          : kToolbarHeight);
}

class _CustomAppBarState extends State<CustomAppBar> {
  Locale currentLocale;

  changeLanguage(BuildContext context, String value) {
    currentLocale = Locale(value);
    Provider.of<CurrentLocale>(context, listen: false).setLocale(currentLocale);
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    Color appBarColor;
    if (widget.key.toString().contains('disease_info') ||
        widget.key.toString().contains('gene_info') ||
        widget.key.toString().contains('phenotype_info') ||
        widget.key.toString().contains('expert_center_info') ||
        widget.key.toString().contains('sources') ||
        widget.key.toString().contains('request_feature') ||
        widget.key.toString().contains('report_bug') ||
        widget.key.toString().contains('evaluation') ||
        widget.key.toString().contains('information') ||
        widget.key.toString().contains('partner') ||
        widget.key.toString().contains('sponsor') ||
        widget.key.toString().contains('legal') ||
        widget.key.toString().contains('user_guide')) {
      appBarColor = const Color(0xFFECECF2);
    } else {
      appBarColor = const Color(0xFFF5F5F9);
    }

    return ClipRRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
        child: AppBar(
          leading: (!widget.key.toString().contains('screening_tool') &&
                  !widget.key.toString().contains('search_tool') &&
                  !widget.key.toString().contains('centers') &&
                  !widget.key.toString().contains('bookmarks') &&
                  !widget.key.toString().contains('disease_info') &&
                  !widget.key.toString().contains('no_connection_appbar'))
              ? Container(
                  height: 40,
                  padding: const EdgeInsets.only(left: 16),
                  alignment: Alignment.center,
                  child: IconButton(
                    splashRadius: 20,
                    icon: const Icon(
                      PhosphorIcons.caretLeft,
                      size: 24,
                      color: Color(0xFF1E293B),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                )
              : null,
          // Platform.isIOS || key.toString().contains('no_connection_appbar')
          //     ? const SizedBox.shrink()
          //     : const VerticalDivider(
          //         color: Color(0xFF6EA6FF),
          //         thickness: 1,
          //       ),
          // leadingWidth: (!key.toString().contains('screening_tools') &&
          //         !key.toString().contains('search_tools') &&
          //         !key.toString().contains('centers') &&
          //         !key.toString().contains('saved'))
          //     ? 65
          //     : 0,
          automaticallyImplyLeading: false,
          titleSpacing: 15,
          centerTitle: (!widget.key.toString().contains('screening_tools') &&
                  !widget.key.toString().contains('search_tools') &&
                  !widget.key.toString().contains('centers') &&
                  !widget.key.toString().contains('bookmarks') &&
                  !widget.key.toString().contains('disease_info'))
              ? true
              : false,
          title: widget.key.toString().contains('disease_info')
              ? TextButton(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(
                        PhosphorIcons.caretLeft,
                        size: 24,
                        color: Color(0xFF1E293B),
                      ),
                      Text(
                        widget.backBtnText,
                        style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B),
                            height: 1.2),
                      ),
                    ],
                  ),
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.only(right: 4),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    alignment: Alignment.centerLeft,
                    primary: ThemeData().splashColor,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              : Text(widget.title),
          titleTextStyle: (widget.key.toString().contains('disease_info') ||
                  widget.key.toString().contains('gene_info') ||
                  widget.key.toString().contains('phenotype_info') ||
                  widget.key.toString().contains('expert_center_info') ||
                  widget.key.toString().contains('sources') ||
                  widget.key.toString().contains('request_feature') ||
                  widget.key.toString().contains('report_bug') ||
                  widget.key.toString().contains('evaluation') ||
                  widget.key.toString().contains('information') ||
                  widget.key.toString().contains('partner') ||
                  widget.key.toString().contains('sponsor') ||
                  widget.key.toString().contains('legal') ||
                  widget.key.toString().contains('user_guide'))
              ? const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Inter',
                  color: Color(0xFF111112),
                  overflow: TextOverflow.ellipsis)
              : Theme.of(context).appBarTheme.titleTextStyle,
          actions: [
            if (widget.key.toString().contains('screening_tools') ||
                widget.key.toString().contains('search_tools') ||
                widget.key.toString().contains('centers') ||
                widget.key.toString().contains('bookmarks')) ...[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: IconButton(
                  padding: const EdgeInsets.all(0),
                  splashRadius: 25,
                  iconSize: 46,
                  icon: Stack(
                    children: [
                      Container(
                        width: 46,
                        padding: const EdgeInsets.all(0),
                        alignment: Alignment.center,
                        child: const Icon(
                          PhosphorIcons.globe,
                          size: 32,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: const EdgeInsets.only(top: 16, left: 17),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 3, vertical: 2),
                          color: const Color(0xFFF5F5F9),
                          child: Text(
                            currentLocale.languageCode.toUpperCase(),
                            style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF1E293B),
                                height: 1),
                          ),
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {
                    showModalBottomSheet(
                      context: context,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(24),
                            topLeft: Radius.circular(24)),
                      ),
                      enableDrag: true,
                      elevation: 100,
                      backgroundColor: const Color(0xFFF5F5F9),
                      builder: (context) {
                        return LanguageMenu();
                      },
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: IconButton(
                  splashRadius: 25,
                  icon: const Icon(
                    PhosphorIcons.list,
                    size: 32,
                    color: Color(0xFF1E293B),
                  ),
                  onPressed: () {
                    Scaffold.of(context).openEndDrawer();
                  },
                ),
              ),
            ] else if (widget.key.toString().contains('disease_info')) ...[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: IconButton(
                  splashRadius: 20,
                  icon: Icon(
                    widget.isBookmarked
                        ? PhosphorIcons.bookmarkSimpleFill
                        : PhosphorIcons.bookmarkSimple,
                    size: 24,
                    color: widget.isBookmarked
                        ? Theme.of(context).primaryColor
                        : const Color(0xFF1E293B),
                  ),
                  onPressed: () async {
                    widget.setBookmark();
                  },
                ),
              ),
            ],
          ],
          backgroundColor: appBarColor.withOpacity(0.8),
          elevation: 0,
          bottom: widget.tabBar ??
              PreferredSize(
                preferredSize: Size(MediaQuery.of(context).size.width, 0),
                child: Container(),
              ),
        ),
      ),
    );
  }
}
