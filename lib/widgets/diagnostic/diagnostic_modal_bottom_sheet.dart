import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_searchbox/flutter_searchbox.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/providers/diseases.dart';
import 'package:searchbase/searchbase.dart';

import '../search_input_no_suggestion.dart';

class DiagnosticModalBottomSheet extends StatefulWidget {
  final List selectedPhenotypeIds;
  final List selectedPhenotypeAndChildrenIds;
  final List selectedGeneSymbols;
  final Set<String> selectedAgeIds;

  final List<String> selectedPhenotypes;
  final List<String> selectedGenes;
  final List<String> selectedAgeLabels;

  final Function setLoadingScreen;

  const DiagnosticModalBottomSheet(
      {Key key,
      @required this.selectedPhenotypeIds,
      @required this.selectedPhenotypeAndChildrenIds,
      @required this.selectedGeneSymbols,
      @required this.selectedAgeIds,
      @required this.selectedPhenotypes,
      @required this.selectedGenes,
      @required this.selectedAgeLabels,
      this.setLoadingScreen})
      : super(key: key);

  @override
  _DiagnosticModalBottomSheetState createState() =>
      _DiagnosticModalBottomSheetState();
}

class _DiagnosticModalBottomSheetState
    extends State<DiagnosticModalBottomSheet> {
  bool _showFirstChild = true;
  bool _showSymptoms = false;
  bool _showGenes = false;
  bool _showAges = false;
  bool _isLoading = false;
  bool _isFetchLoading = false;
  Locale currentLocale;

  List<dynamic> _filterResultList = [];
  List<String> _ageFilterList = ['7', '4', '6', '3', '1', '2', '9'];

  bool timerActive = false;
  Timer timer;

  int dataCount;

  final TextEditingController _searchController = TextEditingController();

  final ScrollController _listScrollController = ScrollController();

  @override
  void initState() {
    _searchController.addListener(() {
      setState(() {});
    });
    // Provider.of<NhAgeOnsets>(context, listen: false)
    //     .getAllAges(context)
    //     .then((data) {
    //   _loadedAgeList = data;
    // });
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _listScrollController.dispose();
    super.dispose();
  }

  void _setTimer() {
    setState(() {
      if (!timerActive) {
        timerActive = true;
        // print('start timer');
        timer = Timer(const Duration(seconds: 4), () {
          // print('sent data to posthog');
          timerActive = false;
          Posthog().capture(
            eventName: 'screening-no-results',
            properties: {
              'eventType': 'no-results',
              'resourceType': 'disease',
              'phenotypeIds': widget.selectedPhenotypeIds
                  .expand((element) => element)
                  .toList(),
              'geneSymbols': widget.selectedGeneSymbols,
              'ageOnsetLabel': widget.selectedAgeLabels,
              '\$screen_name': 'Screening'
            },
          );
        });
      }
    });
  }

  void _cancelTimer() {
    setState(() {
      // print('cancel timer');
      timer.cancel();
      timerActive = false;
    });
  }

  void fetchDiagnosticData(
      List phenotypeIds, List geneSymbols, List ageOnsetIds) {
    if (timerActive) {
      _cancelTimer();
    }
    setState(() {
      _isFetchLoading = true;
    });
    Provider.of<Diseases>(context, listen: false)
        .getDiagnosticDiseases(
            context: context,
            phenotypeIds: phenotypeIds,
            geneSymbols: geneSymbols,
            ageOnsetIds: ageOnsetIds,
            operator: 'AND',
            language: currentLocale.languageCode,
            take: 10,
            skip: 0)
        .then((_) {
      List data =
          Provider.of<Diseases>(context, listen: false).diagnosticDiseases;
      setState(() {
        _isFetchLoading = false;
      });
      // Send no-results event to Posthog
      if (data.length == 1 && data.contains(null)) {
        _setTimer();
      }
    }).catchError((error) async {
      print(error);
      setState(() {
        _isFetchLoading = false;
      });
      // widget.setLoadingScreen(false);
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Text(
            FlutterI18n.translate(context, 'common.errors.an_error_occured'),
            style: Theme.of(context).textTheme.headline5,
          ),
          content: Text(
            FlutterI18n.translate(
                context, 'common.errors.something_went_wrong'),
            style: Theme.of(context).textTheme.bodyText2,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                FlutterI18n.translate(context, 'common.buttons.close'),
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            )
          ],
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    dataCount = Provider.of<Diseases>(context).dataCount;
    // if (data.length == 1 && data.contains(null)) {
    //   dataCount = 0;
    // } else {
    //   dataCount = data.length;
    // }
    var searchBaseInstance = SearchBase(
      dotenv.env['APPBASE_PHENOTYPE_SCREENING_EN_INDEX'],
      dotenv.env['APPBASE_HOST'],
      dotenv.env['APPBASE_CREDENTIAL'],
    );
    return Stack(
      children: [
        AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          child: AnimatedCrossFade(
            duration: const Duration(milliseconds: 300),
            crossFadeState: _showFirstChild
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            firstChild: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              width: double.infinity,
              height: widget.selectedPhenotypeIds.isNotEmpty ||
                      widget.selectedGeneSymbols.isNotEmpty ||
                      widget.selectedAgeIds.isNotEmpty
                  ? 490
                  : 340,
              child: Column(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      color: Color(0xFFCDCEDE),
                    ),
                    margin: const EdgeInsets.only(top: 8, bottom: 32),
                    height: 3,
                    width: MediaQuery.of(context).size.width / 5,
                  ),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(bottom: 32),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.text.what_do_you_want_to_select'),
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF1E293B),
                      ),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      splashColor:
                          Theme.of(context).primaryColor.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(30),
                      child: ListTile(
                        tileColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        dense: true,
                        horizontalTitleGap: 12,
                        contentPadding:
                            const EdgeInsets.only(left: 2, right: 12),
                        leading: Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color:
                                Theme.of(context).primaryColor.withOpacity(0.1),
                          ),
                          child: Icon(
                            PhosphorIcons.faceMask,
                            size: 24,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        title: Text(
                          FlutterI18n.plural(
                              context, 'models.symptom.title', 2),
                          textScaleFactor:
                              MediaQuery.of(context).textScaleFactor,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            height: 1.15,
                          ),
                        ),
                        // subtitle: Text(
                        //   FlutterI18n.plural(
                        //       context,
                        //       'common.placeholders.item_selected.value',
                        //       widget.selectedPhenotypeIds.length),
                        //   textScaleFactor:
                        //       MediaQuery.of(context).textScaleFactor,
                        //   style: const TextStyle(
                        //     fontSize: 14,
                        //     height: 1.15,
                        //   ),
                        // ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            if (widget.selectedPhenotypeIds.isNotEmpty)
                              Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: const Color(0xFFEF4444),
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                alignment: Alignment.center,
                                child: Text(
                                  widget.selectedPhenotypeIds.length.toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                              ),
                            const SizedBox(
                              width: 12,
                            ),
                            const Icon(
                              PhosphorIcons.caretRightBold,
                              size: 14,
                              color: Color(0xFF1E293B),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _showFirstChild = false;
                            _showSymptoms = true;
                            _isLoading = true;
                          });
                        }
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      splashColor:
                          Theme.of(context).primaryColor.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(30),
                      child: ListTile(
                        tileColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        dense: true,
                        horizontalTitleGap: 12,
                        contentPadding:
                            const EdgeInsets.only(left: 2, right: 12),
                        leading: Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color:
                                Theme.of(context).primaryColor.withOpacity(0.1),
                          ),
                          child: Icon(
                            PhosphorIcons.fingerprintSimple,
                            size: 24,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        title: Text(
                          FlutterI18n.plural(context, 'models.gene.title', 2),
                          textScaleFactor:
                              MediaQuery.of(context).textScaleFactor,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            height: 1.15,
                          ),
                        ),
                        // subtitle: Text(
                        //   FlutterI18n.plural(
                        //       context,
                        //       'common.placeholders.item_selected.value',
                        //       widget.selectedGeneSymbols.length),
                        //   textScaleFactor:
                        //       MediaQuery.of(context).textScaleFactor,
                        //   style: const TextStyle(
                        //     fontSize: 14,
                        //     height: 1.15,
                        //   ),
                        // ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            if (widget.selectedGeneSymbols.isNotEmpty)
                              Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: const Color(0xFFEF4444),
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                alignment: Alignment.center,
                                child: Text(
                                  widget.selectedGeneSymbols.length.toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                              ),
                            const SizedBox(
                              width: 12,
                            ),
                            const Icon(
                              PhosphorIcons.caretRightBold,
                              size: 14,
                              color: Color(0xFF1E293B),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _showFirstChild = false;
                            _showGenes = true;
                            _isLoading = true;
                          });
                        }
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      splashColor:
                          Theme.of(context).primaryColor.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(30),
                      child: ListTile(
                        tileColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        dense: true,
                        horizontalTitleGap: 12,
                        contentPadding:
                            const EdgeInsets.only(left: 2, right: 12),
                        leading: Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color:
                                Theme.of(context).primaryColor.withOpacity(0.1),
                          ),
                          child: Icon(
                            PhosphorIcons.calendarBlank,
                            size: 24,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        title: Text(
                          FlutterI18n.translate(
                              context, 'models.nh_age_apparition.title'),
                          textScaleFactor:
                              MediaQuery.of(context).textScaleFactor,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            height: 1.15,
                          ),
                        ),
                        // subtitle: Text(
                        //   FlutterI18n.plural(
                        //       context,
                        //       'common.placeholders.item_selected.value',
                        //       widget.selectedGeneSymbols.length),
                        //   textScaleFactor:
                        //       MediaQuery.of(context).textScaleFactor,
                        //   style: const TextStyle(
                        //     fontSize: 14,
                        //     height: 1.15,
                        //   ),
                        // ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            if (widget.selectedAgeIds.isNotEmpty)
                              Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  color: const Color(0xFFEF4444),
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                alignment: Alignment.center,
                                child: Text(
                                  widget.selectedAgeIds
                                      .where((id) => id != '5' && id != '8')
                                      .length
                                      .toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                              ),
                            const SizedBox(
                              width: 12,
                            ),
                            const Icon(
                              PhosphorIcons.caretRightBold,
                              size: 14,
                              color: Color(0xFF1E293B),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _showFirstChild = false;
                            _showAges = true;
                            // _isLoading = true;
                          });
                        }
                      },
                    ),
                  ),
                  if (widget.selectedPhenotypeIds.isNotEmpty ||
                      widget.selectedGeneSymbols.isNotEmpty ||
                      widget.selectedAgeIds.isNotEmpty) ...[
                    const Expanded(
                      child: SizedBox.shrink(),
                    ),
                    ElevatedButton(
                      child: _isFetchLoading
                          ? const SizedBox(
                              height: 20,
                              width: 20,
                              child: CircularProgressIndicator(
                                strokeWidth: 3,
                                color: Colors.white,
                              ),
                            )
                          : SizedBox(
                              width: double.infinity,
                              child: Text(
                                '${FlutterI18n.translate(context, 'common.buttons.see_results')} ($dataCount)',
                                style: const TextStyle(
                                    fontWeight: FontWeight.w700, fontSize: 16),
                                textAlign: TextAlign.center,
                              ),
                            ),
                      style: ElevatedButton.styleFrom(
                          elevation: 8,
                          primary: _isFetchLoading
                              ? Colors.grey
                              : Theme.of(context).primaryColor,
                          padding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 24),
                          shadowColor: const Color.fromRGBO(95, 178, 237, 0.2),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          )),
                      onPressed: _isFetchLoading
                          ? null
                          : () {
                              Navigator.of(context).pop();
                            },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Material(
                      type: MaterialType.transparency,
                      child: InkWell(
                        splashColor: const Color(0xFFEF4444).withOpacity(0.3),
                        borderRadius: BorderRadius.circular(50),
                        child: Container(
                          padding: const EdgeInsets.all(18),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: const Color(0xFFEF4444).withOpacity(0.1),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.buttons.clear_selection'),
                            style: const TextStyle(
                              color: Color(0xFFEF4444),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        onTap: () {
                          if (mounted) {
                            setState(() {
                              widget.selectedPhenotypeIds.clear();
                              widget.selectedGeneSymbols.clear();
                              widget.selectedPhenotypes.clear();
                              widget.selectedGenes.clear();
                              widget.selectedPhenotypeAndChildrenIds.clear();
                              widget.selectedAgeIds.clear();
                              fetchDiagnosticData(
                                  widget.selectedPhenotypeAndChildrenIds,
                                  widget.selectedGeneSymbols,
                                  widget.selectedAgeIds.toList());
                            });
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).padding.bottom + 10,
                    ),
                  ],
                ],
              ),
            ),
            secondChild: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.85,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24),
                  topRight: Radius.circular(24),
                ),
                color: Color(0xFFF5F5F9),
              ),
              child: Column(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      color: Color(0xFFCDCEDE),
                    ),
                    margin: const EdgeInsets.only(top: 8, bottom: 8),
                    height: 3,
                    width: MediaQuery.of(context).size.width / 6,
                  ),
                  Stack(children: [
                    if (_showSymptoms && !_showFirstChild)
                      SizedBox(
                        width: double.infinity,
                        height: 45.0,
                        child: Center(
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.text.symptom_filter'),
                            style: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xFF1E293B)),
                          ),
                        ),
                      )
                    else if (_showGenes && !_showFirstChild)
                      SizedBox(
                        width: double.infinity,
                        height: 45.0,
                        child: Center(
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.text.gene_filter'),
                            style: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xFF1E293B)),
                          ),
                        ),
                      )
                    else if (_showAges && !_showFirstChild)
                      SizedBox(
                        width: double.infinity,
                        height: 45.0,
                        child: Center(
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.text.age_filter'),
                            style: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xFF1E293B)),
                          ),
                        ),
                      )
                    else
                      const SizedBox(
                        width: double.infinity,
                        height: 45.0,
                      ),
                    Positioned(
                      left: 0.0,
                      top: 0.0,
                      child: Material(
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        child: IconButton(
                          splashRadius: 20,
                          icon: const Icon(
                            PhosphorIcons.caretLeft,
                            size: 24,
                            color: Color(0xFF1E293B),
                          ),
                          onPressed: () {
                            if (mounted) {
                              setState(() {
                                _showFirstChild = true;
                                _showSymptoms = false;
                                _showGenes = false;
                                _showAges = false;
                              });
                            }
                          },
                        ),
                      ),
                    ),
                  ]),
                  if (!_showFirstChild) ...[
                    if (!_showAges)
                      SearchBaseProvider(
                        searchbase: searchBaseInstance,
                        child: _showSymptoms && !_showGenes
                            ? SearchWidgetConnector(
                                id: 'screening-phenotype',
                                enablePopularSuggestions: false,
                                triggerQueryOnInit: false,
                                size: 50,
                                subscribeTo: [],
                                fuzziness: 'AUTO',
                                showDistinctSuggestions: true,
                                dataField: const [
                                  {'field': 'hpo_term', 'weight': 5},
                                  {'field': 'hpo_term.search', 'weight': 4},
                                  {'field': 'synonym', 'weight': 1}
                                ],
                                builder: (context, searchWidget) {
                                  if (currentLocale.languageCode == 'en') {
                                    searchWidget.index = dotenv.env[
                                        'APPBASE_PHENOTYPE_SCREENING_EN_INDEX'];
                                  } else if (currentLocale.languageCode ==
                                      'fr') {
                                    searchWidget.index = dotenv.env[
                                        'APPBASE_PHENOTYPE_SCREENING_FR_INDEX'];
                                  }
                                  // }
                                  return SearchInputNoSuggestion(
                                    searchController: _searchController,
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 4),
                                    padding: EdgeInsets.zero,
                                    initialLoad: () async {
                                      searchWidget
                                          .setValue(_searchController.text);
                                      if (currentLocale.languageCode == 'en') {
                                        searchWidget.index = dotenv.env[
                                            'APPBASE_PHENOTYPE_SCREENING_EN_INDEX'];
                                      } else if (currentLocale.languageCode ==
                                          'fr') {
                                        searchWidget.index = dotenv.env[
                                            'APPBASE_PHENOTYPE_SCREENING_FR_INDEX'];
                                      }
                                      await searchWidget.triggerDefaultQuery();
                                      List removeDuplicateList = [];
                                      for (var suggestion
                                          in searchWidget.suggestions) {
                                        if (removeDuplicateList.firstWhere(
                                                (element) =>
                                                    const ListEquality().equals(
                                                        element
                                                            .source['hpo_id'],
                                                        suggestion
                                                            .source['hpo_id']),
                                                orElse: () => null) ==
                                            null) {
                                          removeDuplicateList.add(suggestion);
                                        }
                                      }
                                      if (mounted) {
                                        setState(() {
                                          _isLoading = false;
                                          _filterResultList =
                                              removeDuplicateList;
                                        });
                                      }
                                    },
                                    onChanged: () async {
                                      setState(() {
                                        _isLoading = true;
                                      });
                                      searchWidget
                                          .setValue(_searchController.text);
                                      if (currentLocale.languageCode == 'en') {
                                        searchWidget.index = dotenv.env[
                                            'APPBASE_PHENOTYPE_SCREENING_EN_INDEX'];
                                      } else if (currentLocale.languageCode ==
                                          'fr') {
                                        searchWidget.index = dotenv.env[
                                            'APPBASE_PHENOTYPE_SCREENING_FR_INDEX'];
                                      }
                                      await searchWidget.triggerDefaultQuery();
                                      List removeDuplicateList = [];
                                      if (searchWidget.suggestions.isEmpty) {
                                        Posthog().capture(
                                          eventName:
                                              'screening-phenotype-no-results',
                                          properties: {
                                            'eventType': 'no-results',
                                            'resourceType': 'phenotype',
                                            'searchInput':
                                                _searchController.text,
                                            '\$screen_name': 'Screening'
                                          },
                                        );
                                      } else {
                                        for (var suggestion
                                            in searchWidget.suggestions) {
                                          if (removeDuplicateList.firstWhere(
                                                  (element) =>
                                                      const ListEquality()
                                                          .equals(
                                                              element.source[
                                                                  'hpo_id'],
                                                              suggestion.source[
                                                                  'hpo_id']),
                                                  orElse: () => null) ==
                                              null) {
                                            removeDuplicateList.add(suggestion);
                                          }
                                        }
                                      }
                                      if (mounted) {
                                        setState(() {
                                          _isLoading = false;
                                          _filterResultList =
                                              removeDuplicateList;
                                        });
                                      }
                                    },
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: const Color(0xFFECECF2),
                                    ),
                                  );
                                })
                            : SearchWidgetConnector(
                                id: 'screening-gene',
                                enablePopularSuggestions: false,
                                triggerQueryOnInit: false,
                                size: 50,
                                subscribeTo: [],
                                fuzziness: 'AUTO',
                                showDistinctSuggestions: true,
                                dataField: const [
                                  {'field': 'gene_name', 'weight': 5},
                                  {'field': 'gene_synonym', 'weight': 2},
                                  {'field': 'symbol', 'weight': 1},
                                ],
                                builder: (context, searchWidget) {
                                  searchWidget.index =
                                      dotenv.env['APPBASE_GENE_INDEX'];
                                  return SearchInputNoSuggestion(
                                    searchController: _searchController,
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 4),
                                    padding: EdgeInsets.zero,
                                    initialLoad: () async {
                                      searchWidget
                                          .setValue(_searchController.text);
                                      await searchWidget.triggerDefaultQuery();
                                      List removeDuplicateList = [];
                                      for (var suggestion
                                          in searchWidget.suggestions) {
                                        if (removeDuplicateList.firstWhere(
                                                (element) =>
                                                    element.source['symbol'] ==
                                                    suggestion.source['symbol'],
                                                orElse: () => null) ==
                                            null) {
                                          removeDuplicateList.add(suggestion);
                                        }
                                      }
                                      if (mounted) {
                                        setState(() {
                                          _isLoading = false;
                                          _filterResultList =
                                              removeDuplicateList;
                                        });
                                      }
                                    },
                                    onChanged: () async {
                                      setState(() {
                                        _isLoading = true;
                                      });
                                      searchWidget
                                          .setValue(_searchController.text);
                                      await searchWidget.triggerDefaultQuery();
                                      List removeDuplicateList = [];
                                      if (searchWidget.suggestions.isEmpty) {
                                        Posthog().capture(
                                          eventName:
                                              'screening-gene-no-results',
                                          properties: {
                                            'eventType': 'no-results',
                                            'resourceType': 'gene',
                                            'searchInput':
                                                _searchController.text,
                                            '\$screen_name': 'Screening'
                                          },
                                        );
                                      } else {
                                        for (var suggestion
                                            in searchWidget.suggestions) {
                                          if (removeDuplicateList.firstWhere(
                                                  (element) =>
                                                      element
                                                          .source['symbol'] ==
                                                      suggestion
                                                          .source['symbol'],
                                                  orElse: () => null) ==
                                              null) {
                                            removeDuplicateList.add(suggestion);
                                          }
                                        }
                                      }
                                      if (mounted) {
                                        setState(() {
                                          _isLoading = false;
                                          _filterResultList =
                                              removeDuplicateList;
                                        });
                                      }
                                    },
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: const Color(0xFFECECF2),
                                    ),
                                  );
                                }),
                      )
                    else ...[
                      Text(
                        FlutterI18n.translate(
                            context, 'common.text.age_filter_text'),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF8487AC),
                          height: 1.5,
                        ),
                      )
                    ],
                    const SizedBox(
                      height: 5,
                    ),
                    Expanded(
                      child: _isLoading
                          ? Center(
                              child: CircularProgressIndicator(
                                color: Theme.of(context).primaryColor,
                              ),
                            )
                          : ((!_showAges && _filterResultList.isEmpty) ||
                                  (_showAges && _ageFilterList.isEmpty)
                              ? Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(20),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: const Color(0xFFECECF2),
                                        ),
                                        child: const Icon(
                                          PhosphorIcons.magnifyingGlassBold,
                                          size: 44,
                                          color: Color(0xFF64748B),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 55, vertical: 8),
                                        child: Text(
                                          FlutterI18n.translate(context,
                                              'common.text.no_result_found'),
                                          style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                              color: Color(0xFF1E293B),
                                              height: 1.5),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : ListView(
                                  controller: _listScrollController,
                                  children: [
                                    if (widget.selectedPhenotypes.isNotEmpty &&
                                        !_showFirstChild &&
                                        _showSymptoms) ...[
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        FlutterI18n.translate(context,
                                            'common.text.my_selection'),
                                        style: TextStyle(
                                          fontSize: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontSize,
                                          fontWeight: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontWeight,
                                          height: 1.2,
                                          color: const Color(0xFF8487AC),
                                        ),
                                      ),
                                      const Divider(
                                        color: Color(0xFFECECF2),
                                        thickness: 1,
                                      ),
                                      ListView.builder(
                                          shrinkWrap: true,
                                          padding:
                                              const EdgeInsets.only(bottom: 12),
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount:
                                              widget.selectedPhenotypes.length,
                                          itemBuilder: (ctx, i) {
                                            return Material(
                                              type: MaterialType.transparency,
                                              child: InkWell(
                                                splashColor: Theme.of(context)
                                                    .primaryColor
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                child: ListTile(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  title: Text(
                                                    widget
                                                        .selectedPhenotypes[i],
                                                    style: const TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF1E293B),
                                                      height: 1.15,
                                                    ),
                                                  ),
                                                  trailing: const Icon(
                                                    PhosphorIcons
                                                        .plusCircleFill,
                                                    size: 24,
                                                    color: Color(0xFF32D175),
                                                  ),
                                                ),
                                                onTap: () async {
                                                  setState(() {
                                                    widget.selectedPhenotypeIds
                                                        .removeAt(i);
                                                    widget.selectedPhenotypes
                                                        .removeAt(i);
                                                    widget
                                                        .selectedPhenotypeAndChildrenIds
                                                        .removeAt(i);
                                                  });
                                                  fetchDiagnosticData(
                                                      widget
                                                          .selectedPhenotypeAndChildrenIds,
                                                      widget
                                                          .selectedGeneSymbols,
                                                      widget.selectedAgeIds
                                                          .toList());
                                                },
                                              ),
                                            );
                                          }),
                                      Text(
                                        FlutterI18n.translate(
                                            context, 'common.text.suggestion'),
                                        style: TextStyle(
                                          fontSize: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontSize,
                                          fontWeight: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontWeight,
                                          height: 1.2,
                                          color: const Color(0xFF8487AC),
                                        ),
                                      ),
                                      const Divider(
                                        color: Color(0xFFECECF2),
                                        thickness: 1,
                                      ),
                                    ] else if (widget
                                            .selectedGenes.isNotEmpty &&
                                        !_showFirstChild &&
                                        _showGenes) ...[
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        FlutterI18n.translate(context,
                                            'common.text.my_selection'),
                                        style: TextStyle(
                                          fontSize: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontSize,
                                          fontWeight: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontWeight,
                                          height: 1.2,
                                          color: const Color(0xFF8487AC),
                                        ),
                                      ),
                                      const Divider(
                                        color: Color(0xFFECECF2),
                                        thickness: 1,
                                      ),
                                      ListView.builder(
                                          shrinkWrap: true,
                                          padding:
                                              const EdgeInsets.only(bottom: 12),
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount:
                                              widget.selectedGenes.length,
                                          itemBuilder: (ctx, i) {
                                            return Material(
                                              type: MaterialType.transparency,
                                              child: InkWell(
                                                splashColor: Theme.of(context)
                                                    .primaryColor
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                child: ListTile(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  title: Text(
                                                    widget.selectedGenes[i],
                                                    style: const TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF1E293B),
                                                      height: 1.15,
                                                    ),
                                                  ),
                                                  trailing: const Icon(
                                                    PhosphorIcons
                                                        .plusCircleFill,
                                                    size: 24,
                                                    color: Color(0xFF32D175),
                                                  ),
                                                ),
                                                onTap: () async {
                                                  setState(() {
                                                    widget.selectedGeneSymbols
                                                        .removeAt(i);
                                                    widget.selectedGenes
                                                        .removeAt(i);
                                                  });
                                                  fetchDiagnosticData(
                                                      widget
                                                          .selectedPhenotypeAndChildrenIds,
                                                      widget
                                                          .selectedGeneSymbols,
                                                      widget.selectedAgeIds
                                                          .toList());
                                                },
                                              ),
                                            );
                                          }),
                                      Text(
                                        FlutterI18n.translate(
                                            context, 'common.text.suggestion'),
                                        style: TextStyle(
                                          fontSize: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontSize,
                                          fontWeight: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontWeight,
                                          height: 1.2,
                                          color: const Color(0xFF8487AC),
                                        ),
                                      ),
                                      const Divider(
                                        color: Color(0xFFECECF2),
                                        thickness: 1,
                                      ),
                                    ] else if (widget
                                            .selectedAgeIds.isNotEmpty &&
                                        !_showFirstChild &&
                                        _showAges) ...[
                                      const SizedBox(
                                        height: 16,
                                      ),
                                      Text(
                                        FlutterI18n.translate(context,
                                            'common.text.my_selection'),
                                        style: TextStyle(
                                          fontSize: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontSize,
                                          fontWeight: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontWeight,
                                          height: 1.2,
                                          color: const Color(0xFF8487AC),
                                        ),
                                      ),
                                      const Divider(
                                        color: Color(0xFFECECF2),
                                        thickness: 1,
                                      ),
                                      ListView.builder(
                                          shrinkWrap: true,
                                          padding:
                                              const EdgeInsets.only(bottom: 12),
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount:
                                              widget.selectedAgeIds.length,
                                          itemBuilder: (ctx, i) {
                                            if (widget.selectedAgeIds
                                                        .elementAt(i) ==
                                                    '5' ||
                                                widget.selectedAgeIds
                                                        .elementAt(i) ==
                                                    '8') {
                                              return const SizedBox.shrink();
                                            }
                                            return Material(
                                              type: MaterialType.transparency,
                                              child: InkWell(
                                                splashColor: Theme.of(context)
                                                    .primaryColor
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                child: ListTile(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  dense: true,
                                                  title: Text(
                                                    FlutterI18n.translate(
                                                        context,
                                                        'models.nh_age_apparition.value.${widget.selectedAgeIds.elementAt(i)}.title'),
                                                    style: const TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF1E293B),
                                                      height: 1.15,
                                                    ),
                                                  ),
                                                  subtitle: Text(
                                                    FlutterI18n.translate(
                                                        context,
                                                        'models.nh_age_apparition.value.${widget.selectedAgeIds.elementAt(i)}.definition'),
                                                    style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF8487AC),
                                                      height: 1.5,
                                                    ),
                                                  ),
                                                  trailing: const Icon(
                                                    PhosphorIcons
                                                        .plusCircleFill,
                                                    size: 24,
                                                    color: Color(0xFF32D175),
                                                  ),
                                                ),
                                                onTap: () async {
                                                  setState(() {
                                                    if (widget.selectedAgeIds
                                                            .where((id) =>
                                                                id != '5' &&
                                                                id != '8')
                                                            .length ==
                                                        1) {
                                                      widget.selectedAgeIds
                                                          .clear();
                                                    } else {
                                                      widget.selectedAgeIds
                                                          .remove(widget
                                                              .selectedAgeIds
                                                              .elementAt(i));
                                                    }
                                                    // widget.selectedGenes
                                                    //     .removeAt(i);
                                                  });
                                                  fetchDiagnosticData(
                                                      widget
                                                          .selectedPhenotypeAndChildrenIds,
                                                      widget
                                                          .selectedGeneSymbols,
                                                      widget.selectedAgeIds
                                                          .toList());
                                                },
                                              ),
                                            );
                                          }),
                                      Text(
                                        FlutterI18n.translate(
                                            context, 'common.text.suggestion'),
                                        style: TextStyle(
                                          fontSize: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontSize,
                                          fontWeight: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .fontWeight,
                                          height: 1.2,
                                          color: const Color(0xFF8487AC),
                                        ),
                                      ),
                                      const Divider(
                                        color: Color(0xFFECECF2),
                                        thickness: 1,
                                      ),
                                    ],
                                    if (!_showAges)
                                      ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          padding: EdgeInsets.only(
                                              bottom: MediaQuery.of(context)
                                                      .padding
                                                      .bottom +
                                                  60),
                                          itemCount: _filterResultList.length,
                                          itemBuilder: (ctx, i) {
                                            // bool isSelected = false;
                                            if (_showSymptoms) {
                                              bool isSelected = widget
                                                  .selectedPhenotypeIds
                                                  .any((hpoIds) =>
                                                      const ListEquality()
                                                          .equals(
                                                              hpoIds,
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'hpo_id']));
                                              if (isSelected) {
                                                return const SizedBox.shrink();
                                              }
                                              return Material(
                                                type: MaterialType.transparency,
                                                child: InkWell(
                                                  splashColor: Theme.of(context)
                                                      .primaryColor
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  child: ListTile(
                                                    contentPadding:
                                                        EdgeInsets.zero,
                                                    title: Text(
                                                      _filterResultList[i]
                                                          .source['hpo_term'],
                                                      style: const TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color:
                                                            Color(0xFF1E293B),
                                                        height: 1.15,
                                                      ),
                                                    ),
                                                    trailing: Icon(
                                                      PhosphorIcons
                                                          .plusCircleFill,
                                                      size: 24,
                                                      color: isSelected
                                                          ? const Color(
                                                              0xFF32D175)
                                                          : const Color(
                                                              0xFFCDCEDE),
                                                    ),
                                                  ),
                                                  onTap: () async {
                                                    setState(() {
                                                      isSelected = true;
                                                    });
                                                    // Phenotype phenotype =
                                                    //     await Provider.of<
                                                    //                 Phenotypes>(
                                                    //             context,
                                                    //             listen: false)
                                                    //         .getPhenotypeForDiagnostic(
                                                    //             context,
                                                    //             _filterResultList[
                                                    //                         i]
                                                    //                     .source[
                                                    //                 'hpo_id'][0]);
                                                    setState(() {
                                                      widget
                                                          .selectedPhenotypeIds
                                                          .add(
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'hpo_id']);
                                                      widget.selectedPhenotypes
                                                          .add(
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'hpo_term']);
                                                      widget
                                                          .selectedPhenotypeAndChildrenIds
                                                          .add([
                                                        _filterResultList[i]
                                                            .source['hpo_id'],
                                                        _filterResultList[i]
                                                                .source[
                                                            'hpo_children']
                                                      ]
                                                              .expand(
                                                                  (element) =>
                                                                      element)
                                                              .toList());
                                                      Posthog().capture(
                                                        eventName:
                                                            'screening-select-item',
                                                        properties: {
                                                          'eventType': 'click',
                                                          'resourceType':
                                                              'phenotype',
                                                          'resourceId':
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'hpo_id'],
                                                          'resourceName':
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'hpo_term'],
                                                          'phenotypeIds': widget
                                                              .selectedPhenotypeIds
                                                              .expand(
                                                                  (element) =>
                                                                      element)
                                                              .toList(),
                                                          'geneSymbols': widget
                                                              .selectedGeneSymbols,
                                                          'ageOnsetLabel': widget
                                                              .selectedAgeLabels,
                                                          'searchInput':
                                                              _searchController
                                                                  .text,
                                                          '\$screen_name':
                                                              'Screening'
                                                        },
                                                      );
                                                      // isSelected = true;
                                                    });
                                                    fetchDiagnosticData(
                                                        widget
                                                            .selectedPhenotypeAndChildrenIds,
                                                        widget
                                                            .selectedGeneSymbols,
                                                        widget.selectedAgeIds
                                                            .toList());
                                                  },
                                                ),
                                              );
                                            } else {
                                              bool isSelected = widget
                                                  .selectedGeneSymbols
                                                  .contains(_filterResultList[i]
                                                      .source['symbol']);
                                              if (isSelected) {
                                                return const SizedBox.shrink();
                                              }
                                              return Material(
                                                type: MaterialType.transparency,
                                                child: InkWell(
                                                  splashColor: Theme.of(context)
                                                      .primaryColor
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  child: ListTile(
                                                    contentPadding:
                                                        EdgeInsets.zero,
                                                    title: Text(
                                                      _filterResultList[i]
                                                          .source['gene_name'],
                                                      style: const TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color:
                                                            Color(0xFF1E293B),
                                                        height: 1.15,
                                                      ),
                                                    ),
                                                    trailing: Icon(
                                                      PhosphorIcons
                                                          .plusCircleFill,
                                                      size: 24,
                                                      color: isSelected
                                                          ? const Color(
                                                              0xFF32D175)
                                                          : const Color(
                                                              0xFFCDCEDE),
                                                    ),
                                                  ),
                                                  onTap: () async {
                                                    setState(() {
                                                      isSelected = true;
                                                    });
                                                    // Gene gene = await Provider.of<
                                                    //             Genes>(context,
                                                    //         listen: false)
                                                    //     .getGeneForDiagnostic(
                                                    //         context,
                                                    //         _filterResultList[i]
                                                    //                 .source[
                                                    //             'symbol']);
                                                    setState(() {
                                                      widget.selectedGeneSymbols
                                                          .add(
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'symbol']);
                                                      widget.selectedGenes.add(
                                                          _filterResultList[i]
                                                                  .source[
                                                              'gene_name']);
                                                      Posthog().capture(
                                                        eventName:
                                                            'screening-select-item',
                                                        properties: {
                                                          'eventType': 'click',
                                                          'resourceType':
                                                              'gene',
                                                          'resourceId':
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'symbol'],
                                                          'resourceName':
                                                              _filterResultList[
                                                                          i]
                                                                      .source[
                                                                  'gene_name'],
                                                          'phenotypeIds': widget
                                                              .selectedPhenotypeIds
                                                              .expand(
                                                                  (element) =>
                                                                      element)
                                                              .toList(),
                                                          'geneSymbols': widget
                                                              .selectedGeneSymbols,
                                                          'ageOnsetLabel': widget
                                                              .selectedAgeLabels,
                                                          'searchInput':
                                                              _searchController
                                                                  .text,
                                                          '\$screen_name':
                                                              'Screening'
                                                        },
                                                      );
                                                      // isSelected = true;
                                                    });
                                                    fetchDiagnosticData(
                                                        widget
                                                            .selectedPhenotypeAndChildrenIds,
                                                        widget
                                                            .selectedGeneSymbols,
                                                        widget.selectedAgeIds
                                                            .toList());
                                                  },
                                                ),
                                              );
                                            }
                                          })
                                    else
                                      ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          padding: EdgeInsets.only(
                                              bottom: MediaQuery.of(context)
                                                      .padding
                                                      .bottom +
                                                  60),
                                          itemCount: _ageFilterList.length,
                                          itemBuilder: (ctx, i) {
                                            bool isSelected = widget
                                                .selectedAgeIds
                                                .contains(_ageFilterList[i]);
                                            if (isSelected ||
                                                _ageFilterList[i] == '5' ||
                                                _ageFilterList[i] == '8') {
                                              return const SizedBox.shrink();
                                            }
                                            return Material(
                                              type: MaterialType.transparency,
                                              child: InkWell(
                                                splashColor: Theme.of(context)
                                                    .primaryColor
                                                    .withOpacity(0.3),
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                child: ListTile(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  dense: true,
                                                  title: Text(
                                                    FlutterI18n.translate(
                                                        context,
                                                        'models.nh_age_apparition.value.${_ageFilterList[i]}.title'),
                                                    style: const TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF1E293B),
                                                      height: 1.15,
                                                    ),
                                                  ),
                                                  subtitle: Text(
                                                    FlutterI18n.translate(
                                                        context,
                                                        'models.nh_age_apparition.value.${_ageFilterList[i]}.definition'),
                                                    style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF8487AC),
                                                      height: 1.5,
                                                    ),
                                                  ),
                                                  trailing: Icon(
                                                    PhosphorIcons
                                                        .plusCircleFill,
                                                    size: 24,
                                                    color: isSelected
                                                        ? const Color(
                                                            0xFF32D175)
                                                        : const Color(
                                                            0xFFCDCEDE),
                                                  ),
                                                ),
                                                onTap: () async {
                                                  setState(() {
                                                    if (_ageFilterList[i] ==
                                                        '7') {
                                                      widget.selectedAgeIds
                                                          .addAll({'7', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Antenatal');
                                                    } else if (_ageFilterList[
                                                            i] ==
                                                        '6') {
                                                      widget.selectedAgeIds
                                                          .addAll(
                                                              {'6', '8', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Infancy');
                                                    } else if (_ageFilterList[
                                                            i] ==
                                                        '4') {
                                                      widget.selectedAgeIds
                                                          .addAll(
                                                              {'4', '8', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Neonatal');
                                                    } else if (_ageFilterList[
                                                            i] ==
                                                        '3') {
                                                      widget.selectedAgeIds
                                                          .addAll(
                                                              {'3', '8', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Childhood');
                                                    } else if (_ageFilterList[
                                                            i] ==
                                                        '2') {
                                                      widget.selectedAgeIds
                                                          .addAll(
                                                              {'2', '8', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Adult');
                                                    } else if (_ageFilterList[
                                                            i] ==
                                                        '1') {
                                                      widget.selectedAgeIds
                                                          .addAll(
                                                              {'1', '8', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Adolescent');
                                                    } else if (_ageFilterList[
                                                            i] ==
                                                        '9') {
                                                      widget.selectedAgeIds
                                                          .addAll(
                                                              {'9', '8', '5'});
                                                      widget.selectedAgeLabels
                                                          .add('Elderly');
                                                    } else {
                                                      widget.selectedAgeIds.add(
                                                          _ageFilterList[i]);
                                                    }
                                                    Posthog().capture(
                                                      eventName:
                                                          'screening-select-item',
                                                      properties: {
                                                        'eventType': 'click',
                                                        'resourceType':
                                                            'ageOnset',
                                                        'resourceId':
                                                            _ageFilterList[i],
                                                        'resourceName': widget
                                                            .selectedAgeLabels
                                                            .last,
                                                        'phenotypeIds': widget
                                                            .selectedPhenotypeIds
                                                            .expand((element) =>
                                                                element)
                                                            .toList(),
                                                        'geneSymbols': widget
                                                            .selectedGeneSymbols,
                                                        'ageOnsetLabel': widget
                                                            .selectedAgeLabels,
                                                        'searchInput': null,
                                                        '\$screen_name':
                                                            'Screening'
                                                      },
                                                    );
                                                    // isSelected = true;
                                                  });
                                                  fetchDiagnosticData(
                                                      widget
                                                          .selectedPhenotypeAndChildrenIds,
                                                      widget
                                                          .selectedGeneSymbols,
                                                      widget.selectedAgeIds
                                                          .toList());
                                                },
                                              ),
                                            );
                                          }),
                                  ],
                                )),
                    ),
                  ],
                ],
              ),
            ),
          ),
        ),
        if ((widget.selectedPhenotypeIds.isNotEmpty ||
                widget.selectedGeneSymbols.isNotEmpty ||
                widget.selectedAgeIds.isNotEmpty) &&
            (_showSymptoms || _showGenes || _showAges))
          Positioned.fill(
            bottom: MediaQuery.of(context).padding.bottom + 30,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: ElevatedButton(
                child: _isFetchLoading
                    ? const SizedBox(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator(
                          strokeWidth: 3,
                          color: Colors.white,
                        ),
                      )
                    : Text(
                        '${FlutterI18n.translate(context, 'common.buttons.see_results')} ($dataCount)',
                        style: const TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                      ),
                style: ElevatedButton.styleFrom(
                    elevation: 8,
                    primary: _isFetchLoading
                        ? Colors.grey
                        : Theme.of(context).primaryColor,
                    padding: const EdgeInsets.symmetric(
                        vertical: 18, horizontal: 24),
                    shadowColor: const Color.fromRGBO(95, 178, 237, 0.2),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    )),
                onPressed: _isFetchLoading
                    ? null
                    : () {
                        // showModalBottomSheet(
                        //   context: context,
                        //   shape: const RoundedRectangleBorder(
                        //     borderRadius: BorderRadius.only(
                        //         topRight: Radius.circular(24),
                        //         topLeft: Radius.circular(24)),
                        //   ),
                        //   enableDrag: false,
                        //   elevation: 100,
                        //   isScrollControlled: true,
                        //   backgroundColor: const Color(0xFFF5F5F9),
                        //   builder: (context) {
                        //     return StatefulBuilder(builder: (context, setState) {
                        //       return DiagnosticModalBottomSheet();
                        //     });
                        //   },
                        // );
                        Navigator.of(context).pop();
                      },
              ),
            ),
          ),
      ],
    );
  }
}
