import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:rdk_mobile/utils/device_type.dart';

class ScoringInfoDialog extends StatelessWidget {
  const ScoringInfoDialog({
    Key key,
    @required this.agreeDisclaimerFunction,
  }) : super(key: key);

  final Function agreeDisclaimerFunction;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      contentPadding: const EdgeInsets.all(0),
      backgroundColor: const Color(0xFFF9F9FA),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      title: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(12), topLeft: Radius.circular(12)),
        ),
        child: Text(
          FlutterI18n.translate(context, 'common.text.relevance'),
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.headline4.fontSize,
            fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
            color: const Color(0xFF1E293B),
          ),
        ),
      ),
      content: Container(
        padding:
            const EdgeInsets.only(top: 20, right: 20, bottom: 10, left: 20),
        decoration: const BoxDecoration(
          color: Color(0xFFF9F9FA),
          border: Border(
            top: BorderSide(
              width: 1,
              color: Color(0xFFEAEAF5),
            ),
          ),
        ),
        child: Text(
          FlutterI18n.translate(context, 'common.text.relevance_info'),
          style: TextStyle(
            fontSize: getDeviceType() == DeviceType.Phone ? 15 : 17,
            fontWeight: FontWeight.w600,
            color: const Color(0xFF1E293B),
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actionsPadding: const EdgeInsets.only(left: 12, right: 12, bottom: 10),
      actions: <Widget>[
        SizedBox(
          width: double.infinity,
          height: 50,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: const Color(0xFF1E293B),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
            child: Text(
              FlutterI18n.translate(context, 'common.buttons.understand'),
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () => agreeDisclaimerFunction(),
          ),
        )
      ],
    );
  }
}

DeviceType getDeviceType() {
  final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
  return data.size.shortestSide < 550 ? DeviceType.Phone : DeviceType.Tablet;
}
