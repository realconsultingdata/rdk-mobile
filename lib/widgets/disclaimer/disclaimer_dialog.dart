import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:rdk_mobile/screens/legal_menu_screen.dart';
import 'package:rdk_mobile/utils/device_type.dart';

class DisclaimerDialog extends StatelessWidget {
  const DisclaimerDialog({
    Key key,
    @required this.agreeDisclaimerFunction,
  }) : super(key: key);

  final Function agreeDisclaimerFunction;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: const EdgeInsets.all(0),
      contentPadding: const EdgeInsets.all(0),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      title: Container(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(12), topLeft: Radius.circular(12)),
        ),
        child: Row(
          children: [
            Container(
              height: 40,
              width: 40,
              margin: const EdgeInsets.only(right: 12),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor.withOpacity(0.1),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Icon(
                PhosphorIcons.cookie,
                size: 24,
                color: Theme.of(context).primaryColor,
              ),
            ),
            Text(
              FlutterI18n.translate(context, 'common.text.welcome'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ],
        ),
      ),
      content: Container(
        padding: const EdgeInsets.only(top: 0, right: 24, bottom: 16, left: 24),
        // height: MediaQuery.of(context).size.height / 2.5,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text.rich(
              TextSpan(children: [
                TextSpan(
                  text: FlutterI18n.translate(
                      context, 'common.text.cookie_dialog_1'),
                ),
                TextSpan(
                  text: ' ' +
                      FlutterI18n.translate(
                          context, 'common.text.cookie_dialog_2'),
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const TextSpan(text: '.'),
              ]),
              style: TextStyle(
                fontSize: getDeviceType() == DeviceType.Phone ? 14 : 17,
                fontWeight: FontWeight.w300,
                color: const Color(0xFF1E293B),
                height: 1.5,
              ),
            ),
            const SizedBox(
              height: 14,
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: FlutterI18n.translate(
                      context, 'common.text.cookie_dialog_3'),
                  style: TextStyle(
                    fontSize: getDeviceType() == DeviceType.Phone ? 14 : 17,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.italic,
                    color: const Color(0xFF1E293B),
                    height: 1.5,
                  ),
                ),
                WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: GestureDetector(
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.text.cookie_policy'),
                        style: TextStyle(
                          fontSize:
                              getDeviceType() == DeviceType.Phone ? 14 : 17,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.italic,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(LegalMenuScreen.routeName);
                      },
                    )),
                const TextSpan(text: '.'),
              ]),
              textScaleFactor: MediaQuery.of(context).textScaleFactor,
            ),
          ],
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actionsPadding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      actions: <Widget>[
        SizedBox(
          width: double.infinity,
          height: 50,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12))),
            child: Text(
              FlutterI18n.translate(context, 'common.buttons.understand'),
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () => agreeDisclaimerFunction(),
          ),
        )
      ],
    );
  }
}

DeviceType getDeviceType() {
  final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
  return data.size.shortestSide < 550 ? DeviceType.Phone : DeviceType.Tablet;
}
