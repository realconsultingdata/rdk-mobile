import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class DiseaseExpertCenterCard extends StatelessWidget {
  // final Map<Institution, List<StatusFlag>> institutionData;
  final NewExpertCenter expertCenter;
  final String orphaNumber;
  final double distance;
  final bool isLocationEnabled;

  const DiseaseExpertCenterCard({
    Key key,
    @required this.expertCenter,
    this.orphaNumber = '',
    this.distance = 0.0,
    this.isLocationEnabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    // String address;
    // if (diseaseExpCenter.expertCenter.streetLabel != null &&
    //     diseaseExpCenter.expertCenter.streetType != null) {
    //   address =
    //       '${diseaseExpCenter.expertCenter.streetNumber == null ? '' : '${diseaseExpCenter.expertCenter.streetNumber} '}${diseaseExpCenter.expertCenter.streetIndex == null ? '' : '${diseaseExpCenter.expertCenter.streetIndex} '}${diseaseExpCenter.expertCenter.streetType} ${diseaseExpCenter.expertCenter.streetLabel}';
    // }
    // sort Status list to put ERN at first
    // institutionData.values.first.sort((a, b) {
    //   if (a.id == '2048') {
    //     return 0;
    //   } else {
    //     return 1;
    //   }
    // });
    expertCenter.statusFlags.sort((a, b) {
      if (a.id == '2048') {
        return 0;
      } else {
        return 1;
      }
    });
    // List<String> levels = expertCenter.level.split(', ');
    // levels.sort((a, b) {
    //   if (a == 'N') {
    //     return 0;
    //   } else {
    //     return 1;
    //   }
    // });
    if (expertCenter.institutions.first.establishment != null) {
      return Card(
        elevation: 0,
        margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
        shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Container(
          padding: const EdgeInsets.all(24),
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (isLocationEnabled) ...[
                Text(
                  (distance / 1000).toStringAsFixed(1) + ' km',
                  style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF8487AC),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
              ],
              Text(
                language == 'fr' ? expertCenter.name : expertCenter.nameEn,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                '${expertCenter.institutions.first.establishment.label} - ${expertCenter.institutions.first.establishment.zipCode} ${expertCenter.institutions.first.establishment.city}',
                style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF64748B),
                    height: 1.5),
              ),
              const SizedBox(
                height: 24,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: [
                  if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '128') >
                      -1)
                    Chip(
                      key: const ValueKey('128'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.128'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '64') >
                      -1)
                    Chip(
                      key: const ValueKey('64'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.64'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                ],
              ),
            ],
          ),
        ),
      );
    } else {
      return const SizedBox.shrink();
    }
  }
}
