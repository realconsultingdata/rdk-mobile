import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_specialty.dart';
import 'package:rdk_mobile/models/disease_specialty_translation.dart';
import 'package:rdk_mobile/models/status_flag.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class DiseaseExpertCenterFilter extends StatefulWidget {
  final Map<StatusFlag, bool> consultationTypes;
  final Map<DiseaseSpecialty, bool> specialties;
  final Function applyFilterFunction;

  const DiseaseExpertCenterFilter(
      {Key key,
      @required this.consultationTypes,
      @required this.specialties,
      @required this.applyFilterFunction})
      : super(key: key);

  @override
  _DiseaseExpertCenterFilterState createState() =>
      _DiseaseExpertCenterFilterState();
}

class _DiseaseExpertCenterFilterState extends State<DiseaseExpertCenterFilter> {
  Locale currentLocale;
  Map<StatusFlag, bool> _consultationTypes;
  Map<DiseaseSpecialty, bool> _specialties;
  final ScrollController _listScrollController = ScrollController();

  @override
  void initState() {
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    _consultationTypes = Map.from(widget.consultationTypes);
    _specialties = Map.from(widget.specialties);
    // _specialties.entries.toList().sort((a, b) {
    //   if (currentLocale.languageCode == 'en') {
    //     return a.key.specialtyLabel.compareTo(b.key.specialtyLabel);
    //   } else {
    //     DiseaseSpecialtyTranslation aTranslation = a.key.translations
    //         .firstWhere(
    //             (trans) =>
    //                 trans.language == currentLocale.languageCode.toUpperCase(),
    //             orElse: () => null);
    //     DiseaseSpecialtyTranslation bTranslation = b.key.translations
    //         .firstWhere(
    //             (trans) =>
    //                 trans.language == currentLocale.languageCode.toUpperCase(),
    //             orElse: () => null);
    //     if (aTranslation != null && bTranslation != null) {
    //       return aTranslation.label.compareTo(bTranslation.label);
    //     } else {
    //       return a.key.specialtyLabel.compareTo(b.key.specialtyLabel);
    //     }
    //   }
    // });
    super.initState();
  }

  @override
  void dispose() {
    _listScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.8,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Color(0xFFCDCEDE),
            ),
            margin: const EdgeInsets.only(top: 16, bottom: 16),
            height: 3,
            width: MediaQuery.of(context).size.width / 5,
          ),
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(bottom: 32),
            alignment: Alignment.topCenter,
            child: Text(
              FlutterI18n.translate(
                  context, 'common.text.expert_center_filter'),
              style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Color(0xFF1E293B),
                  height: 1.2),
              textAlign: TextAlign.start,
            ),
          ),
          Expanded(
            child: ListView(
              controller: _listScrollController,
              children: [
                Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(bottom: 16),
                  child: Text(
                    FlutterI18n.translate(
                        context, 'models.expert_center.consultation_type'),
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Color(0xFF1E293B),
                        height: 1.2),
                    textAlign: TextAlign.start,
                  ),
                ),
                ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: _consultationTypes.length,
                  itemBuilder: (context, i) => InkWell(
                    splashColor:
                        Theme.of(context).primaryColor.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(5),
                    onTap: () {
                      setState(() {
                        _consultationTypes[
                                _consultationTypes.keys.elementAt(i)] =
                            !_consultationTypes.entries.elementAt(i).value;
                      });
                    },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(
                          vertical: 2, horizontal: 0),
                      margin: const EdgeInsets.symmetric(vertical: 4),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Checkbox(
                            key: ValueKey(
                                _consultationTypes.keys.elementAt(i).id),
                            value: _consultationTypes.values.elementAt(i),
                            activeColor: Theme.of(context).primaryColor,
                            side: const BorderSide(
                                color: Color(0xFFD4D5E3), width: 2),
                            visualDensity: VisualDensity.compact,
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            onChanged: (value) {
                              setState(() {
                                _consultationTypes[_consultationTypes.keys
                                    .elementAt(i)] = value;
                              });
                            },
                          ),
                          const SizedBox(
                            width: 2,
                          ),
                          Flexible(
                            child: Text(
                              FlutterI18n.translate(context,
                                  'models.status_flag.value.${_consultationTypes.keys.elementAt(i).id}'),
                              style: const TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color: Color(0xFF1E293B),
                                  overflow: TextOverflow.visible,
                                  height: 1.2),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                if (_specialties.isNotEmpty) ...[
                  const SizedBox(
                    height: 32,
                  ),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(bottom: 16),
                    child: Text(
                      FlutterI18n.translate(context, 'common.text.specialties'),
                      style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFF1E293B),
                          height: 1.2),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _specialties.length,
                    itemBuilder: (context, i) {
                      DiseaseSpecialtyTranslation specialtyTranslation =
                          _specialties.keys
                              .elementAt(i)
                              .translations
                              .firstWhere(
                                  (trans) =>
                                      trans.language ==
                                      currentLocale.languageCode.toUpperCase(),
                                  orElse: () => null);
                      return InkWell(
                        splashColor:
                            Theme.of(context).primaryColor.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(5),
                        onTap: () {
                          setState(() {
                            _specialties[_specialties.keys.elementAt(i)] =
                                !_specialties.entries.elementAt(i).value;
                          });
                        },
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(
                              vertical: 2, horizontal: 0),
                          margin: const EdgeInsets.symmetric(vertical: 4),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Checkbox(
                                key:
                                    ValueKey(_specialties.keys.elementAt(i).id),
                                value: _specialties.values.elementAt(i),
                                activeColor: Theme.of(context).primaryColor,
                                side: const BorderSide(
                                    color: Color(0xFFD4D5E3), width: 2),
                                visualDensity: VisualDensity.compact,
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                onChanged: (value) {
                                  setState(() {
                                    _specialties[
                                        _specialties.keys.elementAt(i)] = value;
                                  });
                                },
                              ),
                              const SizedBox(
                                width: 2,
                              ),
                              Flexible(
                                child: Text(
                                  currentLocale.languageCode != 'en' &&
                                          specialtyTranslation != null
                                      ? specialtyTranslation.label
                                      : _specialties.keys
                                          .elementAt(i)
                                          .specialtyLabel,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                      color: Color(0xFF1E293B),
                                      overflow: TextOverflow.visible,
                                      height: 1.2),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ],
            ),
          ),
          const SizedBox(
            height: 32,
          ),
          ElevatedButton(
            child: SizedBox(
              width: double.infinity,
              child: Text(
                FlutterI18n.translate(context, 'common.buttons.apply_filters'),
                style:
                    const TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ),
            style: ElevatedButton.styleFrom(
                elevation: 8,
                primary: Theme.of(context).primaryColor,
                padding:
                    const EdgeInsets.symmetric(vertical: 18, horizontal: 24),
                shadowColor: const Color.fromRGBO(95, 178, 237, 0.2),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                )),
            onPressed: () {
              widget.consultationTypes.clear();
              widget.consultationTypes.addAll(_consultationTypes);
              widget.specialties.clear();
              widget.specialties.addAll(_specialties);
              widget.applyFilterFunction();
              Navigator.of(context).pop();
            },
          ),
          const SizedBox(
            height: 8,
          ),
          ElevatedButton(
            child: SizedBox(
              width: double.infinity,
              child: Text(
                FlutterI18n.translate(context, 'common.buttons.cancel'),
                style: const TextStyle(
                  color: Color(0xFF1E293B),
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            style: ElevatedButton.styleFrom(
                elevation: 8,
                primary: const Color(0xFFECECF2),
                padding:
                    const EdgeInsets.symmetric(vertical: 18, horizontal: 24),
                shadowColor: const Color.fromRGBO(95, 178, 237, 0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                )),
            onPressed: () {
              Navigator.of(context).pop();
              _consultationTypes = Map.from(widget.consultationTypes);
              _specialties = Map.from(widget.specialties);
            },
          ),
          SizedBox(
            height: MediaQuery.of(context).padding.bottom + 36,
          ),
        ],
      ),
    );
  }
}
