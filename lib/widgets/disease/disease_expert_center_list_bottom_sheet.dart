import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/expert_center_info_screen.dart';
import 'package:rdk_mobile/widgets/disease/disease_expert_center_card.dart';

class DiseaseExpertCenterListBottomSheet extends StatefulWidget {
  final List<NewExpertCenter> expertCenterList;
  final String orphaNumber;

  const DiseaseExpertCenterListBottomSheet(
      {Key key, this.expertCenterList, this.orphaNumber})
      : super(key: key);

  @override
  _DiseaseExpertCenterListBottomSheetState createState() =>
      _DiseaseExpertCenterListBottomSheetState();
}

class _DiseaseExpertCenterListBottomSheetState
    extends State<DiseaseExpertCenterListBottomSheet> {
  Locale currentLocale;
  final ScrollController _listScrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _listScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    return FractionallySizedBox(
      heightFactor: 0.4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Color(0xFFCDCEDE),
              ),
              margin: const EdgeInsets.only(top: 16, bottom: 16),
              height: 3,
              width: MediaQuery.of(context).size.width / 5,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.expert_centers'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          const SizedBox(
            height: 32,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 9),
              child: ListView.builder(
                // physics: const NeverScrollableScrollPhysics(),
                controller: _listScrollController,
                shrinkWrap: true,
                itemCount: widget.expertCenterList.length,
                itemBuilder: (context, i) => GestureDetector(
                  behavior: HitTestBehavior.deferToChild,
                  onTap: () {
                    Posthog().capture(
                      eventName: 'see-disease-establishment',
                      properties: {
                        'eventType': 'click',
                        'resourceType': 'expert-center',
                        'resourceId': widget.expertCenterList[i].id,
                        'resourceName': widget.expertCenterList[i].name,
                        '\$screen_name': 'Disease Info - ${widget.orphaNumber}'
                      },
                    );
                    Navigator.of(context).pushNamed(
                      ExpertCenterInfoScreen.routeName,
                      arguments: widget.expertCenterList[i].id,
                    );
                  },
                  child: DiseaseExpertCenterCard(
                      expertCenter: widget.expertCenterList[i]),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
