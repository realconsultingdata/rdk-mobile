import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_new_expert_center.dart';
import 'package:rdk_mobile/models/disease_specialty.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/institution.dart';
import 'package:rdk_mobile/models/status_flag.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/expert_center_info_screen.dart';
import 'package:rdk_mobile/screens/map_screen.dart';
import 'package:rdk_mobile/widgets/disease/disease_expert_center_filter.dart';

import '../map_widget.dart';
import 'disease_expert_center_card.dart';

class DiseaseExpertCenterTabView extends StatefulWidget {
  final List<DiseaseNewExpertCenter> expertCenters;
  final String diseaseOrphaNumber;
  final TextEditingController searchController;
  final bool filterAdultCentre;
  final bool filterChildrenCentre;

  const DiseaseExpertCenterTabView({
    Key key,
    @required this.expertCenters,
    @required this.diseaseOrphaNumber,
    @required this.searchController,
    this.filterAdultCentre,
    this.filterChildrenCentre,
  }) : super(key: key);

  @override
  _DiseaseExpertCenterTabViewState createState() =>
      _DiseaseExpertCenterTabViewState();
}

class _DiseaseExpertCenterTabViewState extends State<DiseaseExpertCenterTabView>
    with TickerProviderStateMixin {
  // TabController _tabController;

  final List<Establishment> _establishmentList = [];
  final List<Institution> _institutionList = [];
  Map<DiseaseNewExpertCenter, double> _expCenterWithDistanceList = {};
  List<DiseaseNewExpertCenter> _searchResultList = [];
  Map<StatusFlag, bool> _consultationTypes = {};
  Map<DiseaseSpecialty, bool> _specialties = {};
  // bool _mapOpened = false;
  Locale currentLocale;
  // Position currentLocation;

  @override
  void initState() {
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();

    for (DiseaseNewExpertCenter center in widget.expertCenters) {
      for (Institution institution in center.expertCenter.institutions) {
        Institution existedInstitution = _institutionList.firstWhere(
            (inst) => inst.id == institution.id,
            orElse: () => null);
        if (existedInstitution != null) {
          existedInstitution.expertCenters.add(center.expertCenter);
        } else {
          institution.expertCenters.add(center.expertCenter);
          _institutionList.add(institution);
        }
        if (institution.establishment != null) {
          Establishment existedEst = _establishmentList.firstWhere(
              (est) => est.id == institution.establishment.id,
              orElse: () => null);
          if (existedEst == null) {
            institution.establishment.institutions.add(institution);
            _establishmentList.add(institution.establishment);
          } else {
            Institution existedInstitutionInEst = existedEst.institutions
                .firstWhere((inst) => inst.id == institution.id,
                    orElse: () => null);
            if (existedInstitutionInEst == null) {
              existedEst.institutions.add(institution);
            }
          }
        }
      }
    }

    // widget.expertCenters.sort((a, b) => a.level.compareTo(b.level));
    // List<DiseaseNewExpertCenter> centersWithEst = List.from(widget.expertCenters
    //     .where((element) =>
    //         element.expertCenter.institutions.first.establishment != null));
    List<DiseaseNewExpertCenter> nLevelCenterList =
        widget.expertCenters.where((center) => center.level == 'N').toList();
    List<DiseaseNewExpertCenter> prefParentCenterList = widget.expertCenters
        .where((center) => center.isPrefParent && center.level != 'N')
        .toList();
    List<DiseaseNewExpertCenter> notNLevelCenterList = widget.expertCenters
        .where((center) => center.level != 'N' && !center.isPrefParent)
        .toList();
    prefParentCenterList.sort((a, b) => a.level.compareTo(b.level));
    notNLevelCenterList.sort((a, b) => a.level.compareTo(b.level));
    // _expCenterWithDistanceList = {for (var e in centersWithEst) e: 0.0};
    widget.expertCenters.clear();
    widget.expertCenters.addAll(
        [...nLevelCenterList, ...prefParentCenterList, ...notNLevelCenterList]);
    _searchResultList = List.from(widget.expertCenters);
    _consultationTypes = {
      StatusFlag(id: '128', name: 'Adult Clinic'):
          widget.filterAdultCentre && !widget.filterChildrenCentre
              ? true
              : false,
      StatusFlag(id: '64', name: 'Child Clinic'):
          widget.filterChildrenCentre && !widget.filterAdultCentre
              ? true
              : false,
    };
    for (DiseaseNewExpertCenter expCenter in _searchResultList) {
      for (DiseaseSpecialty spe in expCenter.expertCenter.specialties) {
        if (_specialties.isEmpty) {
          _specialties[spe] = false;
        } else {
          DiseaseSpecialty foundSpecialty = _specialties.keys.firstWhere(
              (specialty) => specialty.id == spe.id,
              orElse: () => null);
          if (foundSpecialty == null) {
            _specialties[spe] = false;
          }
        }
      }
    }
    // _searchResultList = List.from(widget.expertCenters.where((element) =>
    //     element.expertCenter.institutions.first.establishment != null));
    // _tabController = TabController(length: 2, vsync: this);
    // _tabController.addListener(() {
    //   if (_tabController.index == 1) {
    //     switchToMap(true);
    //   } else {
    //     switchToMap(false);
    //   }
    // });
    // _determinePosition().then((value) {
    //   setState(() {
    //     currentLocation = value;
    //     if (currentLocation != null) {
    //       for (MapEntry<DiseaseNewExpertCenter, double> item
    //           in _expCenterWithDistanceList.entries) {
    //         _expCenterWithDistanceList.update(
    //             item.key,
    //             (value) => Geolocator.distanceBetween(
    //                 currentLocation.latitude,
    //                 currentLocation.longitude,
    //                 item.key.expertCenter.institutions.first.establishment
    //                     .latitude,
    //                 item.key.expertCenter.institutions.first.establishment
    //                     .longitude));
    //       }
    //       _searchResultList.clear();
    //       _expCenterWithDistanceList = Map.fromEntries(
    //           _expCenterWithDistanceList.entries.toList()
    //             ..sort((e1, e2) => e1.value.compareTo(e2.value)));
    //       _searchResultList = Map.from(_expCenterWithDistanceList);
    //     }
    //   });
    // }).whenComplete(() => updateExpertCenterList());
    super.initState();
  }

  @override
  void dispose() {
    // _tabController.dispose();
    super.dispose();
  }

  // Future<Position> _determinePosition() async {
  //   bool serviceEnabled;
  //   LocationPermission permission;

  //   // Test if location services are enabled.
  //   serviceEnabled = await Geolocator.isLocationServiceEnabled();
  //   if (!serviceEnabled) {
  //     // Location services are not enabled don't continue
  //     // accessing the position and request users of the
  //     // App to enable the location services.
  //     if (Platform.isIOS) {
  //       await showCupertinoDialog(
  //           context: context,
  //           builder: (ctx) => CupertinoAlertDialog(
  //                 title: Text(
  //                   FlutterI18n.translate(
  //                       context, 'common.errors.location_disabled'),
  //                 ),
  //                 content: Text(
  //                   FlutterI18n.translate(
  //                       context, 'common.errors.location_disabled_content'),
  //                 ),
  //                 actions: <Widget>[
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.settings')),
  //                     onPressed: () async {
  //                       await Geolocator.openLocationSettings();
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.cancel')),
  //                     onPressed: () {
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                 ],
  //               ));
  //     } else {
  //       await showDialog(
  //         context: context,
  //         builder: (ctx) => AlertDialog(
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           title: Text(
  //             FlutterI18n.translate(context, 'common.errors.location_disabled'),
  //             style: Theme.of(context).textTheme.headline5,
  //           ),
  //           content: Text(
  //             FlutterI18n.translate(
  //                 context, 'common.errors.location_disabled_content'),
  //             style: Theme.of(context).textTheme.bodyText2,
  //           ),
  //           actions: <Widget>[
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.cancel'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               },
  //             ),
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.settings'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () async {
  //                 await Geolocator.openLocationSettings();
  //                 Navigator.of(context).pop();
  //               },
  //             )
  //           ],
  //         ),
  //       );
  //     }
  //     return Future.error('Location service are disabled.');
  //   }

  //   permission = await Geolocator.checkPermission();
  //   if (permission == LocationPermission.denied) {
  //     permission = await Geolocator.requestPermission();
  //     if (permission == LocationPermission.denied) {
  //       // Permissions are denied, next time you could try
  //       // requesting permissions again (this is also where
  //       // Android's shouldShowRequestPermissionRationale
  //       // returned true. According to Android guidelines
  //       // your App should show an explanatory UI now.
  //       return Future.error('Location permissions are denied');
  //     }
  //   }

  //   if (permission == LocationPermission.deniedForever) {
  //     // Permissions are denied forever, handle appropriately.
  //     if (Platform.isIOS) {
  //       await showCupertinoDialog(
  //           context: context,
  //           builder: (ctx) => CupertinoAlertDialog(
  //                 title: Text(FlutterI18n.translate(
  //                     context, 'common.errors.location_permission_denied')),
  //                 content: Text(FlutterI18n.translate(context,
  //                     'common.errors.location_permission_denied_content')),
  //                 actions: <Widget>[
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.settings')),
  //                     onPressed: () async {
  //                       await Geolocator.openLocationSettings();
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                   CupertinoDialogAction(
  //                     child: Text(FlutterI18n.translate(
  //                         context, 'common.buttons.cancel')),
  //                     onPressed: () {
  //                       Navigator.of(context).pop();
  //                     },
  //                   ),
  //                 ],
  //               ));
  //     } else {
  //       await showDialog(
  //         context: context,
  //         builder: (ctx) => AlertDialog(
  //           shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           title: Text(
  //             FlutterI18n.translate(
  //                 context, 'common.errors.location_permission_denied'),
  //             style: Theme.of(context).textTheme.headline5,
  //           ),
  //           content: Text(
  //             FlutterI18n.translate(
  //                 context, 'common.errors.location_permission_denied_content'),
  //             style: Theme.of(context).textTheme.bodyText2,
  //           ),
  //           actions: <Widget>[
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.cancel'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               },
  //             ),
  //             TextButton(
  //               child: Text(
  //                 FlutterI18n.translate(context, 'common.buttons.settings'),
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //               onPressed: () async {
  //                 await Geolocator.openAppSettings();
  //                 Navigator.of(context).pop();
  //               },
  //             )
  //           ],
  //         ),
  //       );
  //     }
  //     return Future.error('Location permission are permanently denied.');
  //   }

  //   // When we reach here, permissions are granted and we can
  //   // continue accessing the position of the device.
  //   return await Geolocator.getCurrentPosition();
  // }

  void updateExpertCenterList() {
    setState(() {
      _searchResultList.clear();
      _establishmentList.clear();
      // if (currentLocation == null) {
      _searchResultList = widget.expertCenters
          .where((diseaseExpCenter) =>
              ((currentLocale.languageCode == 'fr' && removeDiacritics(diseaseExpCenter.expertCenter.name).toLowerCase().contains(removeDiacritics(widget.searchController.text.toLowerCase()))) ||
                  (currentLocale.languageCode == 'en' &&
                      diseaseExpCenter.expertCenter.nameEn.toLowerCase().contains(
                          widget.searchController.text.toLowerCase())) ||
                  (diseaseExpCenter.expertCenter.institutions.first.establishment != null &&
                      (diseaseExpCenter.expertCenter.institutions.first.establishment.label.toLowerCase().contains(widget.searchController.text.toLowerCase()) ||
                          diseaseExpCenter.expertCenter.institutions.first.establishment.city
                              .toLowerCase()
                              .contains(
                                  widget.searchController.text.toLowerCase()) ||
                          diseaseExpCenter.expertCenter.institutions.first.establishment.zipCode
                              .toLowerCase()
                              .contains(
                                  widget.searchController.text.toLowerCase()) ||
                          diseaseExpCenter.expertCenter.institutions.first.establishment.departmentName
                              .toLowerCase()
                              .contains(removeDiacritics(widget.searchController.text.toLowerCase())) ||
                          diseaseExpCenter.expertCenter.institutions.first.establishment.region.toLowerCase().contains(removeDiacritics(widget.searchController.text.toLowerCase()))))) &&
              (_consultationTypes.values.where((value) => value == true).isEmpty ? true : diseaseExpCenter.expertCenter.statusFlags.any((status) => _consultationTypes.entries.where((entry) => entry.value == true).any((entry) => entry.key.id == status.id))) &&
              (_specialties.values.where((value) => value == true).isEmpty ? true : diseaseExpCenter.expertCenter.specialties.any((specialty) => _specialties.entries.where((entry) => entry.value == true).any((entry) => entry.key.id == specialty.id))))
          .toList();
      // _searchResultList = {for (var e in searchResults) e: 0.0};
      // } else {
      //   _searchResultList = Map.fromEntries(_expCenterWithDistanceList.entries.where((element) =>
      //       ((currentLocale.languageCode == 'fr' && removeDiacritics(element.key.expertCenter.name).toLowerCase().contains(removeDiacritics(widget.searchController.text.toLowerCase()))) ||
      //           (currentLocale.languageCode == 'en' &&
      //               element.key.expertCenter.nameEn.toLowerCase().contains(
      //                   widget.searchController.text.toLowerCase())) ||
      //           element.key.expertCenter.institutions.first.establishment != null &&
      //               (element.key.expertCenter.institutions.first.establishment.label.toLowerCase().contains(widget.searchController.text.toLowerCase()) ||
      //                   element.key.expertCenter.institutions.first.establishment.city.toLowerCase().contains(
      //                       widget.searchController.text.toLowerCase()) ||
      //                   element.key.expertCenter.institutions.first.establishment.zipCode
      //                       .toLowerCase()
      //                       .contains(
      //                           widget.searchController.text.toLowerCase()) ||
      //                   element.key.expertCenter.institutions.first.establishment.departmentName
      //                       .toLowerCase()
      //                       .contains(removeDiacritics(widget.searchController.text.toLowerCase())) ||
      //                   element.key.expertCenter.institutions.first.establishment.region.toLowerCase().contains(removeDiacritics(widget.searchController.text.toLowerCase())))) &&
      //       (_consultationTypes.values.where((value) => value == true).isEmpty ? true : element.key.expertCenter.statusFlags.any((status) => _consultationTypes.entries.where((entry) => entry.value == true).any((entry) => entry.key.id == status.id)))));
      // }
      for (DiseaseNewExpertCenter center in _searchResultList) {
        for (Institution institution in center.expertCenter.institutions) {
          if (institution.establishment != null) {
            _establishmentList.add(institution.establishment);
          }
        }
      }
    });
  }

  // void switchToMap(bool _isMapOpened) {
  //   setState(() {
  //     _mapOpened = _isMapOpened;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    if (widget.expertCenters.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(
                    context, 'common.text.no_expert_center_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.expert_centers'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 15, left: 15, right: 15),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    height: 42,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color(0xFFFFFFFF),
                    ),
                    child: TextField(
                      controller: widget.searchController,
                      onChanged: (_) => updateExpertCenterList(),
                      cursorHeight: 12,
                      decoration: InputDecoration(
                        contentPadding:
                            const EdgeInsets.only(top: 15, bottom: 15),
                        focusedBorder: InputBorder.none,
                        prefixIcon: const Icon(
                          PhosphorIcons.magnifyingGlass,
                          size: 24,
                          color: Color(0xFFADAFCA),
                        ),
                        hintText: FlutterI18n.translate(context,
                            'common.placeholders.search_expert_center'),
                        hintStyle: const TextStyle(
                            color: Color(0xFFADAFCA),
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            height: 1.2),
                        border: InputBorder.none,
                        suffixIcon: widget.searchController.text.isNotEmpty
                            ? IconButton(
                                onPressed: () {
                                  widget.searchController.clear();
                                  updateExpertCenterList();
                                },
                                padding: const EdgeInsets.only(left: 15),
                                icon: const Icon(
                                  PhosphorIcons.x,
                                  size: 24,
                                  color: Color(0xFF1E293B),
                                ))
                            : const SizedBox.shrink(),
                        // : IconButton(
                        //     onPressed: () {
                        //       print('Micro pressed');
                        //     },
                        //     padding: const EdgeInsets.only(left: 15),
                        //     icon: const Icon(
                        //       PhosphorIcons.microphone,
                        //       size: 24,
                        //       color: Color(0xFF1E293B),
                        //     ),
                        //   ),
                      ),
                    ),
                  ),
                ),
                Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Container(
                      width: 42,
                      height: 42,
                      margin: const EdgeInsets.symmetric(horizontal: 8),
                      child: FloatingActionButton(
                        heroTag: 'filterBtn',
                        elevation: 0,
                        backgroundColor: Theme.of(context).primaryColor,
                        child: const Icon(
                          PhosphorIcons.funnel,
                          size: 20,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          showModalBottomSheet(
                            context: context,
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(24),
                                  topLeft: Radius.circular(24)),
                            ),
                            enableDrag: true,
                            elevation: 100,
                            isScrollControlled: true,
                            backgroundColor: const Color(0xFFF5F5F9),
                            builder: (context) {
                              return DiseaseExpertCenterFilter(
                                  consultationTypes: _consultationTypes,
                                  specialties: _specialties,
                                  applyFilterFunction: updateExpertCenterList);
                            },
                          );
                        },
                      ),
                    ),
                    if (_consultationTypes.values
                            .where((val) => val == true)
                            .isNotEmpty ||
                        _specialties.values
                            .where((val) => val == true)
                            .isNotEmpty)
                      Container(
                        width: 15,
                        height: 15,
                        margin: const EdgeInsets.only(right: 3),
                        decoration: BoxDecoration(
                          color: const Color(0xFFEF4444),
                          borderRadius: BorderRadius.circular(100),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          (_consultationTypes.values
                                      .where((val) => val == true)
                                      .length +
                                  _specialties.values
                                      .where((val) => val == true)
                                      .length)
                              .toString(),
                          style: const TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                  ],
                ),
                // SizedBox(
                //   width: 42,
                //   height: 42,
                //   child: FloatingActionButton(
                //     heroTag: 'locationExpCenterBtn',
                //     elevation: 0,
                //     backgroundColor: Theme.of(context).primaryColor,
                //     child: const Icon(
                //       PhosphorIcons.crosshairFill,
                //       size: 20,
                //       color: Colors.white,
                //     ),
                //     onPressed: () async {
                //       _determinePosition().then((value) {
                //         currentLocation = value;
                //         if (currentLocation != null) {
                //           setState(() {
                //             for (MapEntry<DiseaseNewExpertCenter, double> item
                //                 in _expCenterWithDistanceList.entries) {
                //               _expCenterWithDistanceList.update(
                //                   item.key,
                //                   (value) => Geolocator.distanceBetween(
                //                       currentLocation.latitude,
                //                       currentLocation.longitude,
                //                       item.key.expertCenter.institutions.first
                //                           .establishment.latitude,
                //                       item.key.expertCenter.institutions.first
                //                           .establishment.longitude));
                //             }
                //             _searchResultList.clear();
                //             _expCenterWithDistanceList = Map.fromEntries(
                //                 _expCenterWithDistanceList.entries.toList()
                //                   ..sort((e1, e2) =>
                //                       e1.value.compareTo(e2.value)));
                //             _searchResultList =
                //                 Map.from(_expCenterWithDistanceList);
                //           });
                //         }
                //       });
                //     },
                //   ),
                // ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 24,
              left: 15,
              right: 15,
              bottom: 15,
            ),
            child: InkWell(
              splashColor: Theme.of(context).primaryColor.withOpacity(0.3),
              borderRadius: BorderRadius.circular(12),
              child: ListTile(
                tileColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                dense: true,
                minLeadingWidth: 30,
                horizontalTitleGap: 0,
                contentPadding: const EdgeInsets.symmetric(horizontal: 24),
                leading: const Icon(
                  PhosphorIcons.mapTrifold,
                  size: 20,
                  color: Color(0xFF1E293B),
                ),
                title: Text(
                  FlutterI18n.translate(context, 'common.buttons.show_map'),
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    height: 1.15,
                  ),
                ),
                trailing: const Icon(
                  PhosphorIcons.caretRight,
                  size: 20,
                  color: Color(0xFF1E293B),
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MapScreen(
                      orphaNumber: widget.diseaseOrphaNumber,
                      establishments: _establishmentList
                          .where((element) =>
                              element.latitude != null &&
                              element.longitude != null)
                          .toList(),
                      enableMarkerPopup: true,
                    ),
                  ),
                );
              },
            ),
          ),
          // if (!_mapOpened) ...[
          //   Padding(
          //     padding: const EdgeInsets.only(left: 15, right: 15, bottom: 4),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.start,
          //       crossAxisAlignment: CrossAxisAlignment.start,
          //       children: [
          //         const Text(
          //           'N  ',
          //           style: TextStyle(
          //             fontSize: 12,
          //             fontWeight: FontWeight.w600,
          //             color: Color(0xFFAFB0CA),
          //             height: 1.2,
          //           ),
          //         ),
          //         Flexible(
          //           child: Text(
          //             FlutterI18n.translate(
          //                 context, 'models.expert_center.n_explanation'),
          //             style: const TextStyle(
          //               fontSize: 12,
          //               fontWeight: FontWeight.w400,
          //               color: Color(0xFFAFB0CA),
          //               height: 1.2,
          //             ),
          //           ),
          //         )
          //       ],
          //     ),
          //   ),
          //   Padding(
          //     padding: const EdgeInsets.only(left: 15, right: 15, bottom: 4),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.start,
          //       crossAxisAlignment: CrossAxisAlignment.start,
          //       children: [
          //         const Text(
          //           'N+1  ',
          //           style: TextStyle(
          //             fontSize: 12,
          //             fontWeight: FontWeight.w600,
          //             color: Color(0xFFAFB0CA),
          //             height: 1.2,
          //           ),
          //         ),
          //         Flexible(
          //           child: Text(
          //             FlutterI18n.translate(
          //                 context, 'models.expert_center.n_plus_1_explanation'),
          //             style: const TextStyle(
          //               fontSize: 12,
          //               fontWeight: FontWeight.w400,
          //               color: Color(0xFFAFB0CA),
          //               height: 1.2,
          //             ),
          //           ),
          //         )
          //       ],
          //     ),
          //   ),
          //   Padding(
          //     padding: const EdgeInsets.only(left: 15, right: 15, bottom: 12),
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.start,
          //       crossAxisAlignment: CrossAxisAlignment.start,
          //       children: [
          //         const Text(
          //           'N-1  ',
          //           style: TextStyle(
          //             fontSize: 12,
          //             fontWeight: FontWeight.w600,
          //             color: Color(0xFFAFB0CA),
          //             height: 1.2,
          //           ),
          //         ),
          //         Flexible(
          //           child: Text(
          //             FlutterI18n.translate(context,
          //                 'models.expert_center.n_minus_1_explanation'),
          //             style: const TextStyle(
          //               fontSize: 12,
          //               fontWeight: FontWeight.w400,
          //               color: Color(0xFFAFB0CA),
          //               height: 1.2,
          //             ),
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ],
          _searchResultList.isEmpty
              ? Container(
                  padding: const EdgeInsets.symmetric(vertical: 50),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: const Color(0xFFECECF2),
                        ),
                        child: const Icon(
                          PhosphorIcons.magnifyingGlass,
                          size: 44,
                          color: Color(0xFF8487AC),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 55, vertical: 8),
                        child: Text(
                          FlutterI18n.translate(
                              context, 'common.text.no_expert_center_found'),
                          style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF1E293B),
                              height: 1.5),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              : ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: _searchResultList.length,
                  itemBuilder: (ctx, i) {
                    return GestureDetector(
                      behavior: HitTestBehavior.deferToChild,
                      onTap: () {
                        Posthog().capture(
                          eventName: 'see-disease-establishment',
                          properties: {
                            'eventType': 'click',
                            'resourceType': 'expert-center',
                            'resourceId': _searchResultList[i].expertCenter.id,
                            'resourceName':
                                _searchResultList[i].expertCenter.name,
                            '\$screen_name':
                                'Disease Info - ${widget.diseaseOrphaNumber}'
                          },
                        );
                        Navigator.of(context).pushNamed(
                          ExpertCenterInfoScreen.routeName,
                          arguments: _searchResultList[i].expertCenter.id,
                        );
                      },
                      child: DiseaseExpertCenterCard(
                        key: ValueKey(_searchResultList[i].expertCenter.id),
                        expertCenter: _searchResultList[i].expertCenter,
                        orphaNumber: widget.diseaseOrphaNumber,
                        isLocationEnabled: false,
                        level: _searchResultList[i].level,
                        isPrefParent: _searchResultList[i].isPrefParent,
                      ),
                    );
                  },
                ),
        ],
      );
    }
  }
}
