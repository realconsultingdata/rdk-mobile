import 'package:flutter/material.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:rdk_mobile/models/gene_disease.dart';

class DiseaseGeneCard extends StatelessWidget {
  final GeneDisease geneDisease;

  const DiseaseGeneCard({
    Key key,
    @required this.geneDisease,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    // String language = currentLocale.languageCode;
    // PhenotypeTranslation translation = phenotype.translations.firstWhere(
    //     (translation) => translation.language == language,
    //     orElse: () => null);
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          geneDisease.gene.name,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF1E293B),
                            height: 1.5,
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Text(
                          geneDisease.gene.symbol,
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.italic,
                            color: Color(0xFF8487AC),
                            height: 1.15,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const Icon(
                  PhosphorIcons.caretRightBold,
                  size: 18,
                  color: Color(0xFFAFB0CA),
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            const Divider(
              color: Color(0xFFECECF2),
              thickness: 1,
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Relation',
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF8487AC),
                  ),
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      geneDisease.associationType,
                      style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.bodyText2.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.bodyText2.fontWeight,
                        color: const Color(0xFF1E293B),
                      ),
                      textAlign: TextAlign.end,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
