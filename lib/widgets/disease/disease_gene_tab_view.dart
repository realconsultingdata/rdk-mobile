import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/models/gene_disease.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/gene_info_screen.dart';
import 'package:rdk_mobile/widgets/disease/disease_gene_card.dart';

class DiseaseGeneTabView extends StatefulWidget {
  final List<GeneDisease> geneDiseases;

  const DiseaseGeneTabView({Key key, @required this.geneDiseases})
      : super(key: key);

  @override
  _DiseaseGeneTabViewState createState() => _DiseaseGeneTabViewState();
}

class _DiseaseGeneTabViewState extends State<DiseaseGeneTabView> {
  final TextEditingController _searchController = TextEditingController();
  Map<String, List<GeneDisease>> _geneMap;
  List<MapEntry<String, List<GeneDisease>>> _searchList;
  Locale currentLocale;

  @override
  void initState() {
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    _geneMap = groupBy(widget.geneDiseases, (GeneDisease gd) {
      if (gd.childDisease != null) {
        if (currentLocale.languageCode == 'en') {
          return gd.childDisease.name;
        } else {
          DiseaseTranslation translation = gd.childDisease.translations
              .firstWhere(
                  (element) => element.language == currentLocale.languageCode,
                  orElse: () => null);
          if (translation != null) {
            return translation.name;
          }
          return gd.childDisease.name;
        }
      }
      return null;
    });
    for (String k in _geneMap.keys) {
      _geneMap[k].sort((a, b) =>
          a.gene.name.toLowerCase().compareTo(b.gene.name.toLowerCase()));
    }
    _searchList = _geneMap.entries.toList();
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void updateGeneList() {
    setState(() {
      _searchList.clear();
      // Map<String, List<Gene>> newMap = Map.from(_geneMap);
      for (MapEntry<String, List<GeneDisease>> entry in _geneMap.entries) {
        List<GeneDisease> newEntryValue = entry.value
            .where((GeneDisease gd) =>
                gd.gene.name
                    .toLowerCase()
                    .contains(_searchController.text.toLowerCase()) ||
                gd.gene.symbol
                    .toLowerCase()
                    .contains(_searchController.text.toLowerCase()))
            .toList();
        if (newEntryValue.isNotEmpty) {
          _searchList.add(MapEntry(entry.key, newEntryValue));
        }
      }
      for (MapEntry<String, List<GeneDisease>> entry in _searchList) {
        entry.value.sort((a, b) =>
            a.gene.name.toLowerCase().compareTo(b.gene.name.toLowerCase()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.geneDiseases.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_gene_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.genes'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          Container(
            height: 40,
            width: double.infinity,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: const Color(0xFFFFFFFF),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (_) => updateGeneList(),
              cursorHeight: 12,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFF8F91AC),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_here'),
                hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1),
                border: InputBorder.none,
                suffixIcon: _searchController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _searchController.clear();
                          updateGeneList();
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
          if (_searchList.isEmpty)
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color(0xFFECECF2),
                    ),
                    child: const Icon(
                      PhosphorIcons.magnifyingGlass,
                      size: 44,
                      color: Color(0xFF8487AC),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.text.no_gene_found'),
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.5),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            )
          else
            ..._searchList.map((item) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 15, right: 15, top: 24),
                    child: Text(
                      item.key != null
                          ? '${FlutterI18n.translate(context, 'common.text.genes_linked_to')} ${item.key}'
                          : FlutterI18n.translate(
                              context, 'common.text.direct_linked_genes'),
                      style: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.bodyText1.fontSize,
                          fontWeight:
                              Theme.of(context).textTheme.bodyText1.fontWeight,
                          color: const Color(0xFF1E293B),
                          height: 1.15),
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: item.value.length,
                    itemBuilder: (ctx, i) {
                      return GestureDetector(
                        behavior: HitTestBehavior.deferToChild,
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            GeneInfoScreen.routeName,
                            arguments: item.value[i].gene.symbol,
                          );
                        },
                        child: DiseaseGeneCard(
                          key: ValueKey(item.value[i].gene.symbol),
                          geneDisease: item.value[i],
                        ),
                      );
                    },
                    shrinkWrap: true,
                  )
                ],
              );
            })
        ],
      );
    }
  }
}
