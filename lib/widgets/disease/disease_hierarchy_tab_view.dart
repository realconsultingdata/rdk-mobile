import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_treeview/flutter_treeview.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_hierarchy.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/hierarchy_card.dart';

class DiseaseHierarchyTabView extends StatelessWidget {
  final List<DiseaseHierarchy> hierarchies;
  final String orphaNumber;
  Locale currentLocale;

  DiseaseHierarchyTabView(
      {Key key, @required this.hierarchies, @required this.orphaNumber})
      : super(key: key);

  // Build Hierarchy tree from a list
  List<Node> buildHierarchyTree(BuildContext context, DiseaseHierarchy data,
      int dataIndex, int currentLevel, int maxNodeLevel) {
    List<Node<DiseaseHierarchyNode>> resultTree = [];
    String language = currentLocale.languageCode;

    if (currentLevel < maxNodeLevel) {
      DiseaseHierarchyNode currentNode =
          data.hierarchy.firstWhere((node) => node.nodeLevel == currentLevel);
      DiseaseTranslation translation = currentNode.disease.translations
          .firstWhere((translation) => translation.language == language,
              orElse: () => null);
      resultTree.add(Node<DiseaseHierarchyNode>(
        key: currentNode.disease.orphaNumber,
        label:
            (language == 'en' ? currentNode.disease.name : translation.name) +
                ' - ${currentNode.disease.orphaNumber}',
        data: currentNode,
        expanded: true,
        children: [
          ...buildHierarchyTree(
              context, data, dataIndex, currentLevel + 1, maxNodeLevel)
        ],
      ));
    } else {
      List<DiseaseHierarchyNode> currentNodes = data.hierarchy
          .where((node) => node.nodeLevel == currentLevel)
          .toList();
      for (var node in currentNodes) {
        if (resultTree.indexWhere(
                (anyNode) => anyNode.key == node.disease.orphaNumber) >
            -1) {
          continue;
        }
        DiseaseTranslation translation = node.disease.translations.firstWhere(
            (translation) => translation.language == language,
            orElse: () => null);
        resultTree.add(Node<DiseaseHierarchyNode>(
          key: node.disease.orphaNumber,
          label: (language == 'en' ? node.disease.name : translation.name) +
              ' - ${node.disease.orphaNumber}',
          data: node,
          expanded: true,
        ));
      }
    }
    return resultTree;
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    if (hierarchies.isEmpty) {
      return Center(
        child: Text(
          FlutterI18n.translate(context, 'common.text.no_model_available',
              translationParams: {
                'model': FlutterI18n.plural(
                        context, 'models.classification.title', 0)
                    .toLowerCase()
              }),
          style: Theme.of(context).textTheme.headline5,
        ),
      );
    } else {
      List<Node> hierarchyTree = [];
      for (int i = 0; i < hierarchies.length; i++) {
        var data = hierarchies[i];
        int maxNodeLevel = data.hierarchy
            .reduce((a, b) => a.nodeLevel > b.nodeLevel ? a : b)
            .nodeLevel;
        hierarchyTree
            .addAll(buildHierarchyTree(context, data, i, 0, maxNodeLevel));
      }
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.classification'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          HierarchyCard(
            key: const ValueKey('disease_hierarchy'),
            hierarchyData: hierarchyTree,
            currentInfoId: orphaNumber,
          )
        ],
      );
    }
  }
}
