import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_pnds.dart';
import 'package:rdk_mobile/models/disease_relation.dart';
import 'package:rdk_mobile/models/disease_synonym.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/disease/expansion_tile_resume_card.dart';
import 'package:url_launcher/url_launcher.dart';

import '../general_card.dart';
import 'external_content_card.dart';

class DiseaseInformationTabView extends StatelessWidget {
  final Disease loadedDisease;
  final List<DiseaseSynonym> synonyms;
  final TabController tabController;
  final TextEditingController expCenterSearchController;

  DiseaseInformationTabView(
      {@required this.loadedDisease,
      @required this.synonyms,
      @required this.tabController,
      @required this.expCenterSearchController});

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    List<DiseaseSynonym> filteredSynonyms =
        synonyms.where((syn) => syn.language == language).toList();
    Map<String, List<String>> _diseaseRelations = {};
    _diseaseRelations.putIfAbsent(
        'Code ORPHA', () => [loadedDisease.orphaNumber]);
    if (loadedDisease.relations.isNotEmpty) {
      if (loadedDisease.relations
          .any((element) => element.source == 'ICD-10')) {
        _diseaseRelations.putIfAbsent(
            'ICD-10',
            () => [
                  ...loadedDisease.relations
                      .where((element) => element.source == 'ICD-10')
                      .map((e) => e.reference)
                      .toList()
                ]);
      }
      if (loadedDisease.relations
          .any((element) => element.source == 'ICD-11')) {
        _diseaseRelations.putIfAbsent(
            'ICD-11',
            () => [
                  ...loadedDisease.relations
                      .where((element) => element.source == 'ICD-11')
                      .map((e) => e.reference)
                      .toList()
                ]);
      }
    }
    // loadedDisease.relations.sort((e1, e2) {
    //   if (e1.source == 'ICD-10' || e1.source == 'ICD-11') {
    //     return 0;
    //   } else {
    //     return 1;
    //   }
    // });
    DiseaseTranslation translation = loadedDisease.translations.firstWhere(
        (element) => element.language == language,
        orElse: () => null);
    for (DiseaseRelation relation in loadedDisease.relations.where((relation) =>
        relation.source != null &&
        relation.source != 'ICD-10' &&
        relation.source != 'ICD-11')) {
      if (relation.source.isNotEmpty) {
        _diseaseRelations.putIfAbsent(relation.source, () {
          List<String> relationValues = [];
          for (DiseaseRelation item in loadedDisease.relations) {
            if (item.source == relation.source) {
              relationValues.add(item.reference);
            }
          }
          return relationValues;
        });
      }
    }
    DiseasePnds diseasePnds;
    if (loadedDisease.diseasePnds.isNotEmpty) {
      loadedDisease.diseasePnds.sort((e1, e2) {
        DateTime e1Date = DateTime.parse(e1.minValidationDate);
        DateTime e2Date = DateTime.parse(e2.minValidationDate);
        if (e1Date.isAfter(e2Date)) {
          return 0;
        } else {
          return 1;
        }
      });
      diseasePnds = loadedDisease.diseasePnds.first;
    }

    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            language != 'en' && translation != null
                ? translation.name
                : loadedDisease.name,
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.headline4.fontSize,
              fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
              color: const Color(0xFF1E293B),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, right: 15, bottom: 30, left: 15),
          child: Text(
            'ORPHA:${loadedDisease.orphaNumber}',
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Color(0xFF64748B),
            ),
          ),
        ),
        GeneralCard(
          key: const ValueKey('description_disease'),
          title: FlutterI18n.translate(context, 'common.text.description'),
          descriptionContents: language != 'en' && translation != null
              ? translation.contents
              : loadedDisease.contents,
          synonyms: filteredSynonyms,
          histories: loadedDisease.histories,
          parents: loadedDisease.preferentialParents,
        ),
        const SizedBox(
          height: 24,
        ),
        // ..._diseaseRelations.entries
        //     .map((relation) => ExpansionTileRelationCard(
        //           key: ObjectKey(relation.key),
        //           mapRelation: relation,
        //         ))
        //     .toList(),
        // GeneralCard(
        //   key: const ValueKey('apparition_disease'),
        //   title: FlutterI18n.translate(context, 'common.text.apparition'),
        //   histories: loadedDisease.histories,
        // ),
        // const SizedBox(
        //   height: 24,
        // ),
        if (loadedDisease.textSections.isNotEmpty) ...[
          ExpansionTileResumeCard(
            textSectionList: loadedDisease.textSections,
            authorList: loadedDisease.authors,
          ),
          const SizedBox(
            height: 24,
          )
        ],
        if (loadedDisease.externalContents.isNotEmpty ||
            diseasePnds != null) ...[
          // GeneralCard(
          //   key: const ValueKey('pnds'),
          //   title: 'PNDS',
          //   diseasePnds: diseasePnds,
          // ),
          ExternalContentCard(
              key: const ValueKey('pnds'),
              diseaseName: language == 'en'
                  ? loadedDisease.name
                  : loadedDisease.translations
                      .firstWhere((element) => element.language == language)
                      .name,
              externalContents: loadedDisease.externalContents,
              diseasePnds: diseasePnds),
          const SizedBox(
            height: 24,
          ),
        ],
        if (loadedDisease.newExpertCenters.isNotEmpty) ...[
          GeneralCard(
            key: const ValueKey('exp_center_map'),
            title: FlutterI18n.plural(context, 'models.expert_center.title', 2),
            establishments: loadedDisease.newExpertCenters
                .map((element) =>
                    element.expertCenter.institutions.first.establishment)
                .toList(),
            diseaseRelations: _diseaseRelations,
            tabController: tabController,
            expCenterSearchController: expCenterSearchController,
          ),
          const SizedBox(
            height: 24,
          ),
        ],
        GeneralCard(
          key: const ValueKey('relation_disease'),
          title: FlutterI18n.plural(context, 'models.terminology.title', 2),
          diseaseRelations: _diseaseRelations,
        ),
        const SizedBox(
          height: 24,
        ),
        Card(
          elevation: 0,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          child: Container(
            padding: const EdgeInsets.all(24),
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                GestureDetector(
                  child: SvgPicture.asset(
                    'assets/icons/mr_info_services.svg',
                  ),
                  onTap: () => launch('https://www.maladiesraresinfo.org/'),
                ),
                const SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  child: SvgPicture.asset(
                    'assets/icons/mr_info_service_number.svg',
                    fit: BoxFit.fitWidth,
                  ),
                  onTap: () => launch('tel:0800404043'),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 40,
        ),
      ],
    );
  }
}
