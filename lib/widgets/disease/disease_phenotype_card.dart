import 'package:flutter/material.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/phenotype.dart';
import 'package:rdk_mobile/models/phenotype_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class DiseasePhenotypeCard extends StatelessWidget {
  final Phenotype phenotype;
  final String diagnostic;

  const DiseasePhenotypeCard({
    Key key,
    @required this.phenotype,
    this.diagnostic,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    PhenotypeTranslation translation = phenotype.translations.firstWhere(
        (translation) => translation.language == language,
        orElse: () => null);
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(right: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (phenotype.category != null &&
                        phenotype.category != '') ...[
                      Text(
                        language != 'en' &&
                                translation != null &&
                                translation.category != null
                            ? translation.category.replaceAll('|', '-')
                            : phenotype.category.replaceAll('|', '-'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF8F91AC),
                          height: 1.15,
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                    ],
                    RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        children: [
                          if (diagnostic == 'Diagnostic criterion')
                            WidgetSpan(
                              child: Container(
                                margin: const EdgeInsets.only(right: 4),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 4, vertical: 2),
                                decoration: BoxDecoration(
                                  color:
                                      const Color(0xFFF55A8D).withOpacity(0.1),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(4)),
                                ),
                                child: const Text(
                                  'DC',
                                  style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFF55A8D),
                                    height: 1.3,
                                  ),
                                ),
                              ),
                            )
                          else if (diagnostic == 'Pathognomonic sign')
                            WidgetSpan(
                              child: Container(
                                margin: const EdgeInsets.only(right: 4),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 4, vertical: 2),
                                decoration: BoxDecoration(
                                  color:
                                      const Color(0xFFA17DEF).withOpacity(0.1),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(4)),
                                ),
                                child: const Text(
                                  'PS',
                                  style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFA17DEF),
                                    height: 1.3,
                                  ),
                                ),
                              ),
                            ),
                          TextSpan(
                            text: language != 'en' && translation != null
                                ? translation.hpoTerm
                                : phenotype.hpoTerm,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF1E293B),
                              height: 1.5,
                            ),
                          ),
                          if (language != 'en' &&
                              translation != null &&
                              translation.hpoTermTranslationSource == 'DeepL')
                            const WidgetSpan(
                              alignment: PlaceholderAlignment.top,
                              child: Icon(
                                PhosphorIcons.asteriskSimple,
                                size: 10,
                                color: Color(0xFF006394),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const Icon(
              PhosphorIcons.caretRightBold,
              size: 18,
              color: Color(0xFFAFB0CA),
            ),
          ],
        ),
      ),
    );
  }
}
