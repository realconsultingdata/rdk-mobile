import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:group_button/group_button.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/phenotype_disease.dart';
import 'package:rdk_mobile/models/phenotype_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/phenotype_info_screen.dart';

import 'disease_phenotype_card.dart';

class DiseasePhenotypeTabView extends StatefulWidget {
  final List<PhenotypeDisease> phenotypeDiseases;

  const DiseasePhenotypeTabView({
    @required this.phenotypeDiseases,
  });

  @override
  _DiseasePhenotypeTabViewState createState() =>
      _DiseasePhenotypeTabViewState();
}

class _DiseasePhenotypeTabViewState extends State<DiseasePhenotypeTabView> {
  final GroupButtonController groupBtnController = GroupButtonController();

  List<PhenotypeDisease> _resultList;

  int _selectedFreqIndex = 0;
  final List<Map> _frequencies = [
    {
      'title': 'common.text.all',
      'freqValue': 'all',
    },
    {
      'title': 'models.symptom.hpo_frequency.value.100%',
      'freqValue': '(100%)',
    },
    {
      'title': 'models.symptom.hpo_frequency.value.99-80%',
      'freqValue': '(99-80%)',
    },
    {
      'title': 'models.symptom.hpo_frequency.value.79-30%',
      'freqValue': '(79-30%)',
    },
    {
      'title': 'models.symptom.hpo_frequency.value.29-5%',
      'freqValue': '(29-5%)',
    },
    {
      'title': 'models.symptom.hpo_frequency.value.4-1%',
      'freqValue': '(<4-1%)',
    },
    {
      'title': 'models.symptom.hpo_frequency.value.0%',
      'freqValue': '(0%)',
    },
  ];
  final List<Map> _freqListFromData = [
    {
      'title': 'common.text.all',
      'freqValue': 'all',
    }
  ];

  @override
  void initState() {
    groupBtnController.selectIndex(0);
    _resultList = List.from(widget.phenotypeDiseases);
    _freqListFromData.addAll(_frequencies
        .where((freq) => widget.phenotypeDiseases
            .any((pd) => pd.frequency.contains(freq['freqValue'])))
        .toList());
    super.initState();
  }

  void filterPhenotypeList() {
    setState(() {
      _resultList.clear();
      if (_selectedFreqIndex == 0) {
        _resultList.addAll(widget.phenotypeDiseases);
      } else {
        _resultList.addAll(widget.phenotypeDiseases.where((pd) => pd.frequency
            .contains(_freqListFromData[_selectedFreqIndex]['freqValue'])));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    if (widget.phenotypeDiseases.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_symptom_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      Map<String, List<PhenotypeDisease>> sortedPhenotypeMap = {
        '100%': [],
        '99-80%': [],
        '79-30%': [],
        '29-5%': [],
        '4-1%': [],
        '0%': [],
      };
      _resultList.sort((p1, p2) =>
          int.parse(p2.frequency
              .split(' ')
              .last
              .replaceAll(RegExp(r'[(<>%)]'), '')
              .split('-')[0]) -
          int.parse(p1.frequency
              .split(' ')
              .last
              .replaceAll(RegExp(r'[(<>%)]'), '')
              .split('-')[0]));
      for (PhenotypeDisease item in _resultList) {
        sortedPhenotypeMap[item.frequency
                .split(' ')
                .last
                .replaceAll(RegExp(r'[(<>)]'), '')]
            .add(item);
      }
      for (String k in sortedPhenotypeMap.keys) {
        sortedPhenotypeMap[k].sort((a, b) {
          if (currentLocale.languageCode == 'en') {
            return a.phenotype.hpoTerm.compareTo(b.phenotype.hpoTerm);
          } else {
            PhenotypeTranslation aTranslation = a.phenotype.translations
                .firstWhere((ea) => ea.language == currentLocale.languageCode,
                    orElse: () => null);
            PhenotypeTranslation bTranslation = b.phenotype.translations
                .firstWhere((eb) => eb.language == currentLocale.languageCode,
                    orElse: () => null);
            if (aTranslation != null && bTranslation != null) {
              return aTranslation.hpoTerm.compareTo(bTranslation.hpoTerm);
            }
            return a.phenotype.hpoTerm.compareTo(b.phenotype.hpoTerm);
          }
        });
      }
      return ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.symptoms'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            height: 60,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                GroupButton(
                  controller: groupBtnController,
                  onSelected: (label, index, isSelected) {
                    setState(() {
                      _selectedFreqIndex = index;
                      filterPhenotypeList();
                      // fetchData(_offset, _limit);
                      // _selectedPage = 1;
                    });
                  },
                  isRadio: true,
                  buttons: _freqListFromData
                      .map((freq) =>
                          FlutterI18n.translate(context, freq['title']))
                      .toList(),
                  options: GroupButtonOptions(
                    groupingType: GroupingType.row,
                    mainGroupAlignment: MainGroupAlignment.start,
                    selectedBorderColor: Theme.of(context).primaryColor,
                    unselectedBorderColor: Colors.transparent,
                    selectedColor: Colors.white,
                    unselectedColor: Colors.white,
                    selectedTextStyle: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFF1E293B),
                    ),
                    unselectedTextStyle: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFF8F91AC),
                    ),
                    selectedShadow: [
                      const BoxShadow(
                        color: Color.fromRGBO(94, 92, 154, 0.06),
                        offset: Offset.zero,
                        blurRadius: 20,
                      )
                    ],
                    unselectedShadow: [
                      const BoxShadow(
                        color: Colors.transparent,
                      )
                    ],
                    elevation: 0,
                    borderRadius: BorderRadius.circular(48),
                    spacing: 4,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Container(
                          margin: const EdgeInsets.only(right: 4),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 2),
                          decoration: BoxDecoration(
                            color: const Color(0xFFA17DEF).withOpacity(0.1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(4)),
                          ),
                          child: const Text(
                            'PS',
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFFA17DEF),
                              height: 1.3,
                            ),
                          ),
                        ),
                      ),
                      TextSpan(
                        text: FlutterI18n.translate(
                            context, 'common.text.pathognomonic_sign'),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.2,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 24,
                ),
                RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Container(
                          margin: const EdgeInsets.only(right: 4),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 2),
                          decoration: BoxDecoration(
                            color: const Color(0xFFF55A8D).withOpacity(0.1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(4)),
                          ),
                          child: const Text(
                            'DC',
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFFF55A8D),
                              height: 1.3,
                            ),
                          ),
                        ),
                      ),
                      TextSpan(
                        text: FlutterI18n.translate(
                            context, 'common.text.diagnostic_criterion'),
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.2,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          if (currentLocale.languageCode != 'en')
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 8),
              child: RichText(
                textScaleFactor: MediaQuery.of(context).textScaleFactor,
                text: TextSpan(
                  style: const TextStyle(fontFamily: 'Inter'),
                  children: [
                    const WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Icon(
                        PhosphorIcons.asteriskSimple,
                        size: 10,
                        color: Color(0xFF006394),
                      ),
                    ),
                    WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Text(
                        FlutterI18n.translate(
                                context, 'common.text.translated_by') +
                            ' DeepL',
                        style: const TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.italic,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          if (_resultList.isEmpty)
            Center(
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_model_available',
                    translationParams: {
                      'model':
                          FlutterI18n.plural(context, 'models.symptom.title', 0)
                              .toLowerCase()
                    }),
                style: Theme.of(context).textTheme.headline5,
              ),
            )
          else
            ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: const EdgeInsets.only(top: 0),
                itemCount: sortedPhenotypeMap.entries.length,
                itemBuilder: (context, index) {
                  if (sortedPhenotypeMap.entries
                      .toList()[index]
                      .value
                      .isNotEmpty) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15, right: 15, top: 24),
                          child: Text(
                            FlutterI18n.translate(context,
                                    'models.symptom.hpo_frequency.value.${sortedPhenotypeMap.entries.toList()[index].key}') +
                                ' (${sortedPhenotypeMap.entries.toList()[index].key})',
                            style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.15),
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: sortedPhenotypeMap.entries
                              .toList()[index]
                              .value
                              .length,
                          itemBuilder: (ctx, i) {
                            return GestureDetector(
                              behavior: HitTestBehavior.deferToChild,
                              onTap: () {
                                Navigator.of(context).pushNamed(
                                  PhenotypeInfoScreen.routeName,
                                  arguments: sortedPhenotypeMap.entries
                                      .toList()[index]
                                      .value[i]
                                      .phenotype
                                      .id,
                                );
                              },
                              child: DiseasePhenotypeCard(
                                key: ValueKey(sortedPhenotypeMap.entries
                                    .toList()[index]
                                    .value[i]
                                    .phenotype
                                    .id),
                                phenotype: sortedPhenotypeMap.entries
                                    .toList()[index]
                                    .value[i]
                                    .phenotype,
                                diagnostic: sortedPhenotypeMap.entries
                                    .toList()[index]
                                    .value[i]
                                    .diagnostic,
                              ),
                            );
                          },
                          shrinkWrap: true,
                        )
                      ],
                    );
                  } else {
                    return const SizedBox.shrink();
                  }
                }),
          const SizedBox(
            height: 7,
          ),
        ],
      );
    }
  }
}
