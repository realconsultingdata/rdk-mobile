import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/disease_info_screen.dart';
import 'package:rdk_mobile/widgets/fiche/disease_card.dart';

class DiseaseRareDiseaseTabView extends StatefulWidget {
  final List<Disease> diseases;

  const DiseaseRareDiseaseTabView({@required this.diseases});

  @override
  _DiseaseRareDiseaseTabViewState createState() =>
      _DiseaseRareDiseaseTabViewState();
}

class _DiseaseRareDiseaseTabViewState extends State<DiseaseRareDiseaseTabView> {
  final TextEditingController _searchController = TextEditingController();
  List<Disease> _searchList;

  Locale currentLocale;

  @override
  void initState() {
    super.initState();
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    _searchController.addListener(() {
      setState(() {});
    });
    _searchList = List.from(widget.diseases);
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void updateDiseaseList() {
    setState(() {
      _searchList.clear();
      if (_searchController.text.isNotEmpty) {
        _searchList = widget.diseases.where((Disease disease) {
          if (currentLocale.languageCode == 'en') {
            return removeDiacritics(disease.name).toLowerCase().contains(
                    removeDiacritics(_searchController.text).toLowerCase()) ||
                disease.synonyms.any((syn) =>
                    syn.language == currentLocale.languageCode &&
                    removeDiacritics(syn.synonym).toLowerCase().contains(
                        removeDiacritics(_searchController.text)
                            .toLowerCase()));
          } else {
            return disease.translations.any((trans) =>
                    trans.language == currentLocale.languageCode &&
                    removeDiacritics(trans.name).toLowerCase().contains(
                        removeDiacritics(_searchController.text)
                            .toLowerCase())) ||
                disease.synonyms.any((syn) =>
                    syn.language == currentLocale.languageCode &&
                    removeDiacritics(syn.synonym).toLowerCase().contains(
                        removeDiacritics(_searchController.text)
                            .toLowerCase()));
          }
        }).toList();
      } else {
        _searchList = List.from(widget.diseases);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.diseases.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_disease_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.diseases'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          Container(
            height: 40,
            width: double.infinity,
            margin: const EdgeInsets.only(bottom: 15, left: 15, right: 15),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: const Color(0xFFFFFFFF),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (_) => updateDiseaseList(),
              cursorHeight: 12,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFF8F91AC),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_here'),
                hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1),
                border: InputBorder.none,
                suffixIcon: _searchController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _searchController.clear();
                          updateDiseaseList();
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
          if (_searchList.isEmpty)
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color(0xFFECECF2),
                    ),
                    child: const Icon(
                      PhosphorIcons.magnifyingGlass,
                      size: 44,
                      color: Color(0xFF8487AC),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.text.no_disease_found'),
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.5),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            )
          else
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _searchList.length,
              itemBuilder: (context, index) => GestureDetector(
                behavior: HitTestBehavior.deferToChild,
                child: FicheDiseaseCard(disease: _searchList[index]),
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(DiseaseInfoScreen.routeName, arguments: {
                    'orphaNumber': _searchList[index].orphaNumber,
                    'backBtn': FlutterI18n.translate(
                        context, 'common.buttons.go_back'),
                  });
                },
              ),
            ),
          const SizedBox(
            height: 7,
          ),
        ],
      );
    }
  }
}
