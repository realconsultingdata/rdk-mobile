import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/author.dart';
import 'package:rdk_mobile/models/text_section.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class ExpansionTileResumeCard extends StatefulWidget {
  final List<TextSection> textSectionList;
  final List<Author> authorList;

  const ExpansionTileResumeCard(
      {Key key, @required this.textSectionList, @required this.authorList})
      : super(key: key);

  @override
  _ExpansionTileResumeCardState createState() =>
      _ExpansionTileResumeCardState();
}

class _ExpansionTileResumeCardState extends State<ExpansionTileResumeCard> {
  bool isPanelExpanded = true;

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    List<String> authorNames = widget.authorList
        .map((author) =>
            author.firstName.substring(0, 1) + '. ' + author.lastName)
        .toList();
    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: ExpansionTile(
            backgroundColor: const Color(0xFFFFFFFF),
            collapsedBackgroundColor: const Color(0xFFFFFFFF),
            initiallyExpanded: isPanelExpanded,
            tilePadding:
                const EdgeInsets.symmetric(horizontal: 24, vertical: 4),
            trailing: Text(
              FlutterI18n.translate(
                  context,
                  isPanelExpanded
                      ? 'common.buttons.hide'
                      : 'common.buttons.show'),
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                decoration: TextDecoration.underline,
                color: Theme.of(context).primaryColor,
                height: 1.5,
              ),
            ),
            collapsedIconColor: Theme.of(context).primaryColor,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  FlutterI18n.translate(context, 'common.text.summary'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.headline4.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.headline4.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
                // if (isPanelExpanded)
                //   Text(
                //     '${FlutterI18n.translate(context, 'common.text.updated')} ${
                //       DateFormat('d MMMM yyyy', currentLocale.languageCode)
                //           .format(DateTime.parse(
                //               widget.textSectionList[0].validationDate))
                //     }',
                //     style: const TextStyle(
                //       fontSize: 10,
                //       fontWeight: FontWeight.w400,
                //       fontStyle: FontStyle.italic,
                //       color: Color(0xFF1E293B),
                //       height: 1.5,
                //     ),
                //   ),
              ],
            ),
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    style: const TextStyle(
                      fontFamily: 'Inter',
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.italic,
                      color: Color(0xFF1E293B),
                      height: 1.5,
                    ),
                    children: [
                      TextSpan(
                        text:
                            '${FlutterI18n.translate(context, 'common.text.updated')} ${DateFormat('d MMMM yyyy', currentLocale.languageCode).format(DateTime.parse(widget.textSectionList[0].validationDate))}',
                      ),
                      if (widget.authorList.isNotEmpty)
                        TextSpan(
                          text:
                              '\t - \t${FlutterI18n.translate(context, 'common.text.reviewed_by')} ${authorNames.join(', ')}',
                        ),
                      if (widget.authorList.isNotEmpty &&
                          widget.authorList
                              .any((author) => author.network != null)) ...[
                        const TextSpan(
                          text: '\t - \t',
                        ),
                        WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: Container(
                            height: 16,
                            width: 16,
                            margin: const EdgeInsets.only(right: 4),
                            child: SvgPicture.asset(
                              'assets/icons/ern_logo.svg',
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        const TextSpan(
                          text: 'ERN',
                        ),
                      ]
                    ],
                  ),
                ),
              ),
              Container(
                color: Colors.white,
                padding: const EdgeInsets.symmetric(horizontal: 24),
                margin: const EdgeInsets.only(bottom: 24),
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.textSectionList.length,
                  itemBuilder: (ctx, i) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 32,
                        ),
                        Text(
                          widget.textSectionList[i].type,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF1E293B),
                            height: 1.2,
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Html(
                          data: widget.textSectionList[i].contents,
                          style: {
                            "body": Style(
                              margin: EdgeInsets.zero,
                              padding: EdgeInsets.zero,
                              fontSize: const FontSize(14),
                              fontWeight: FontWeight.w400,
                              color: const Color(0xFF1E293B),
                              lineHeight: const LineHeight(1.5),
                            )
                          },
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
            expandedCrossAxisAlignment: CrossAxisAlignment.start,
            onExpansionChanged: (bool isExpanded) {
              setState(() => isPanelExpanded = isExpanded);
            },
          ),
        ),
      ),
    );
  }
}
