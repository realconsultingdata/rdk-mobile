import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

class ExpertCenterSearchCard extends StatefulWidget {
  final TextEditingController controller;
  final Function handleSearch;
  final Function handleTab;

  const ExpertCenterSearchCard(
      {Key key, @required this.controller, this.handleSearch, this.handleTab})
      : super(key: key);

  @override
  _ExpertCenterSearchCardState createState() => _ExpertCenterSearchCardState();
}

class _ExpertCenterSearchCardState extends State<ExpertCenterSearchCard> {
  bool _isMapOpened = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        width: double.infinity,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: ElevatedButton.icon(
                    style: ButtonStyle(
                      splashFactory: NoSplash.splashFactory,
                      backgroundColor: _isMapOpened
                          ? MaterialStateProperty.all<Color>(
                              const Color(0xFFADAFCA))
                          : MaterialStateProperty.all<Color>(
                              Theme.of(context).primaryColor),
                      elevation: MaterialStateProperty.all(0),
                      fixedSize: MaterialStateProperty.all<Size>(
                        const Size(double.infinity, 45),
                      ),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        _isMapOpened = false;
                        widget.handleTab(_isMapOpened);
                      });
                    },
                    icon: const Icon(
                      PhosphorIcons.list,
                      size: 25,
                    ),
                    label: Text(
                      FlutterI18n.translate(context, 'common.buttons.list'),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: ElevatedButton.icon(
                    style: ButtonStyle(
                      splashFactory: NoSplash.splashFactory,
                      backgroundColor: _isMapOpened
                          ? MaterialStateProperty.all<Color>(
                              Theme.of(context).primaryColor)
                          : MaterialStateProperty.all<Color>(
                              const Color(0xFFADAFCA)),
                      elevation: MaterialStateProperty.all(0),
                      fixedSize: MaterialStateProperty.all<Size>(
                        const Size(double.infinity, 45),
                      ),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        _isMapOpened = true;
                        widget.handleTab(_isMapOpened);
                      });
                    },
                    icon: const Icon(
                      PhosphorIcons.mapTrifold,
                      size: 25,
                    ),
                    label: Text(
                      FlutterI18n.translate(context, 'common.buttons.map'),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 14,
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: const Color(0xFFDEDFE9),
                  style: BorderStyle.solid,
                  width: 1,
                ),
              ),
              child: TextField(
                controller: widget.controller,
                onChanged: (_) => widget.handleSearch(),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                  focusedBorder: InputBorder.none,
                  prefixIcon: const Icon(
                    PhosphorIcons.magnifyingGlass,
                    size: 24,
                    color: Color(0xFFCDCEDE),
                  ),
                  hintText: FlutterI18n.translate(
                      context, 'common.placeholders.search_here'),
                  hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                  border: InputBorder.none,
                  suffixIcon: widget.controller.text.isNotEmpty
                      ? IconButton(
                          onPressed: () {
                            widget.controller.clear();
                            widget.handleSearch();
                          },
                          padding: const EdgeInsets.only(left: 15),
                          icon: const Icon(
                            PhosphorIcons.x,
                            size: 24,
                            color: Color(0xFF1E293B),
                          ))
                      : const SizedBox.shrink(),
                  // : IconButton(
                  //     onPressed: () {
                  //       print('Micro pressed');
                  //     },
                  //     padding: const EdgeInsets.only(left: 15),
                  //     icon: const Icon(
                  //       PhosphorIcons.microphone,
                  //       size: 24,
                  //       color: Color(0xFF1E293B),
                  //     ),
                  //   ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
