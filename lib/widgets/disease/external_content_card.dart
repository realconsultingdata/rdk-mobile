import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_pnds.dart';
import 'package:rdk_mobile/models/external_content.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:url_launcher/url_launcher.dart';

class ExternalContentCard extends StatelessWidget {
  final String diseaseName;
  final List<ExternalContent> externalContents;
  final DiseasePnds diseasePnds;

  const ExternalContentCard(
      {Key key, this.diseaseName, this.externalContents, this.diseasePnds})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    ExternalContent emergencyFile = externalContents
        .firstWhere((content) => content.lang == language, orElse: () => null);
    emergencyFile ??= externalContents
        .firstWhere((content) => content.lang == 'en', orElse: () => null);
    emergencyFile ??= externalContents
        .firstWhere((content) => content.lang == 'fr', orElse: () => null);
    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (emergencyFile != null) ...[
              Text(
                FlutterI18n.translate(context, 'common.text.emergency_sheet'),
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline4.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                  color: const Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                '${FlutterI18n.translate(context, 'common.text.updated')} ${DateFormat("d MMMM yyyy", language).format(DateTime.parse(emergencyFile.validationDate))}',
                style: const TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.italic,
                  color: Color(0xFF1E293B),
                  height: 1.5,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: ListTile(
                  tileColor: const Color(0xFFF9F9FA),
                  shape: RoundedRectangleBorder(
                    side:
                        const BorderSide(color: Color(0xFFEAEAF5), width: 1.5),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  horizontalTitleGap: 0,
                  // contentPadding: const EdgeInsets.all(0),
                  leading: const Icon(
                    PhosphorIcons.filePdfBold,
                    color: Color(0xFFD93221),
                    size: 24,
                  ),
                  title: Text(
                    '${FlutterI18n.translate(context, 'common.text.emergency_sheet')} - $diseaseName',
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      height: 1.15,
                      color: Color(0xFF1E293B),
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  onTap: () {
                    // Navigator.of(context).pushNamed(
                    //   DiseasePndsViewer.routeName,
                    //   arguments: document,
                    // );
                    launch(emergencyFile.url);
                  },
                ),
              ),
            ],
            if (emergencyFile != null && diseasePnds != null)
              const SizedBox(
                height: 32,
              ),
            if (diseasePnds != null) ...[
              Text(
                'PNDS',
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline4.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                  color: const Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                '${FlutterI18n.translate(context, 'common.text.updated')} ${DateFormat("d MMMM yyyy", language).format(DateTime.parse(diseasePnds.minValidationDate))}',
                style: const TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.italic,
                  color: Color(0xFF1E293B),
                  height: 1.5,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                FlutterI18n.translate(context, 'common.text.pnds_description'),
                style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    height: 1.5,
                    color: const Color(0xFF1E293B)),
              ),
              ...diseasePnds.documents.map(
                (document) => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: ListTile(
                    tileColor: const Color(0xFFF9F9FA),
                    shape: RoundedRectangleBorder(
                      side: const BorderSide(
                          color: Color(0xFFEAEAF5), width: 1.5),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    horizontalTitleGap: 0,
                    // contentPadding: const EdgeInsets.all(0),
                    leading: const Icon(
                      PhosphorIcons.filePdfBold,
                      color: Color(0xFFD93221),
                      size: 24,
                    ),
                    title: Text(
                      document.name.split(' - ').last,
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        height: 1.15,
                        color: Color(0xFF1E293B),
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    onTap: () {
                      // Navigator.of(context).pushNamed(
                      //   DiseasePndsViewer.routeName,
                      //   arguments: document,
                      // );
                      launch(document.hyperLink);
                    },
                  ),
                ),
              )
            ]
          ],
        ),
      ),
    );
  }
}
