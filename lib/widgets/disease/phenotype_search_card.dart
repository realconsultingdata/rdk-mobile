import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class PhenotypeSearchCard extends StatefulWidget {
  final TextEditingController searchController;
  final Map allFreqCheck;
  final Map allCategoryCheck;
  final List<Map> frequencies;
  final List<Map> categories;
  final Function handleSearch;
  final Function handleFilter;
  final Function handleCategoryCount;
  final Function handleFrequencyCount;

  const PhenotypeSearchCard({
    Key key,
    @required this.searchController,
    @required this.allFreqCheck,
    @required this.allCategoryCheck,
    @required this.frequencies,
    @required this.categories,
    @required this.handleSearch,
    @required this.handleFilter,
    @required this.handleCategoryCount,
    @required this.handleFrequencyCount,
  }) : super(key: key);

  @override
  _PhenotypeSearchCardState createState() => _PhenotypeSearchCardState();
}

class _PhenotypeSearchCardState extends State<PhenotypeSearchCard> {
  bool _isFreqPanelExpanded = true;
  bool _isCategoryPanelExpanded = true;
  int _resultCount = 0;
  List<Map> _categorySearches = [];
  TextEditingController _categorySearchController = TextEditingController();

  @override
  void initState() {
    _resultCount = widget.allFreqCheck['count'];
    _categorySearches = widget.categories;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 5,
        shadowColor: const Color.fromRGBO(37, 99, 235, 0.1),
        margin: const EdgeInsets.only(bottom: 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          padding: const EdgeInsets.all(15),
          width: double.infinity,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: 50,
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: const Color(0xFFDEDFE9),
                      style: BorderStyle.solid,
                      width: 1,
                    ),
                  ),
                  child: TextField(
                    controller: widget.searchController,
                    onChanged: (_) => widget.handleSearch(),
                    decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.only(top: 15, bottom: 15),
                      focusedBorder: InputBorder.none,
                      prefixIcon: const Icon(
                        PhosphorIcons.magnifyingGlass,
                        size: 24,
                        color: Color(0xFFCDCEDE),
                      ),
                      hintText: FlutterI18n.translate(
                          context, 'common.placeholders.search_here'),
                      hintStyle: const TextStyle(
                        color: Color(0xFFADAFCA),
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                      ),
                      border: InputBorder.none,
                      suffixIcon: widget.searchController.text.isNotEmpty
                          ? IconButton(
                              onPressed: () {
                                widget.searchController.clear();
                                widget.handleSearch();
                              },
                              padding: const EdgeInsets.only(left: 15),
                              icon: const Icon(
                                PhosphorIcons.x,
                                size: 24,
                                color: Color(0xFF1E293B),
                              ))
                          : const SizedBox.shrink(),
                      // : IconButton(
                      //     onPressed: () {
                      //       print('Micro pressed');
                      //     },
                      //     padding: const EdgeInsets.only(left: 15),
                      //     icon: const Icon(
                      //       PhosphorIcons.microphone,
                      //       size: 24,
                      //       color: Color(0xFF1E293B),
                      //     ),
                      //   ),
                    ),
                  ),
                ),
              ),
              IconButton(
                onPressed: () {
                  showBottomSheet(
                      context: context,
                      elevation: 100,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            topLeft: Radius.circular(10)),
                      ),
                      backgroundColor: Colors.white,
                      builder: (BuildContext ctx) {
                        return buildBottomSheet(ctx);
                      });
                },
                padding: const EdgeInsets.only(left: 8),
                icon: const Icon(PhosphorIcons.fadersHorizontal),
                iconSize: 31,
              )
            ],
          ),
        ));
  }

  Widget buildBottomSheet(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;

    return SafeArea(
      child: StatefulBuilder(
        builder: (BuildContext ctx, StateSetter setState) => Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Color(0xFFADAFCA),
              ),
              margin: const EdgeInsets.only(top: 14, bottom: 20),
              height: 3,
              width: MediaQuery.of(context).size.width / 5,
            ),
            Stack(children: [
              SizedBox(
                width: double.infinity,
                height: 50.0,
                child: Center(
                  child: Text(
                    FlutterI18n.translate(context, 'common.text.filters'),
                    style: TextStyle(
                        fontWeight:
                            Theme.of(context).textTheme.headline3.fontWeight,
                        fontSize:
                            Theme.of(context).textTheme.headline3.fontSize,
                        color: const Color(0xFF1E293B)),
                  ),
                ),
              ),
              Positioned(
                  left: 15.0,
                  top: 0.0,
                  child: IconButton(
                      icon: const Icon(
                        PhosphorIcons.x,
                        size: 25,
                        color: Color(0xFF071E31),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }))
            ]),
            Container(
              margin: const EdgeInsets.only(top: 24, left: 24, right: 24),
              color: const Color(0xFFEAEAF5),
              height: 1,
              width: double.infinity,
            ),
            Expanded(
              child: ListView(
                children: [
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: Column(
                      children: [
                        ExpansionTile(
                          backgroundColor: Colors.transparent,
                          collapsedBackgroundColor: Colors.transparent,
                          initiallyExpanded: _isFreqPanelExpanded,
                          iconColor: const Color(0xFF1E293B),
                          title: Text(
                            FlutterI18n.translate(
                                context, 'common.text.symptom_frequency'),
                            style: TextStyle(
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .fontWeight,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .fontSize,
                                color: const Color(0xFF1E293B)),
                          ),
                          tilePadding:
                              const EdgeInsets.symmetric(horizontal: 24),
                          onExpansionChanged: (bool isExpanded) {
                            setState(() => _isFreqPanelExpanded = isExpanded);
                          },
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  widget.allFreqCheck['value'] =
                                      !widget.allFreqCheck['value'];
                                  for (var freq in widget.frequencies) {
                                    freq['value'] =
                                        widget.allFreqCheck['value'];
                                  }
                                  widget.handleCategoryCount();
                                  if (widget.categories.every(
                                      (category) => !category['value'])) {
                                    _resultCount = 0;
                                  } else {
                                    _resultCount = widget.frequencies
                                        .where((freq) => freq['value'])
                                        .fold(
                                            0,
                                            (preValue, curr) =>
                                                preValue + curr['count']);
                                  }
                                });
                              },
                              child: Container(
                                height: 30,
                                width: double.infinity,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 15),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Checkbox(
                                      key: ValueKey(
                                          widget.allFreqCheck['title']),
                                      value: widget.allFreqCheck['value'],
                                      activeColor:
                                          Theme.of(context).primaryColor,
                                      onChanged: (value) {
                                        setState(() {
                                          widget.allFreqCheck['value'] = value;
                                          for (var freq in widget.frequencies) {
                                            freq['value'] = value;
                                          }
                                          widget.handleCategoryCount();
                                          if (widget.categories.every(
                                              (category) =>
                                                  !category['value'])) {
                                            _resultCount = 0;
                                          } else {
                                            _resultCount = widget.frequencies
                                                .where((freq) => freq['value'])
                                                .fold(
                                                    0,
                                                    (preValue, curr) =>
                                                        preValue +
                                                        curr['count']);
                                          }
                                        });
                                      },
                                    ),
                                    RichText(
                                      textScaleFactor: MediaQuery.of(context)
                                          .textScaleFactor,
                                      text: TextSpan(
                                          style: const TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'Inter',
                                          ),
                                          children: [
                                            TextSpan(
                                                text: FlutterI18n.translate(
                                                    context,
                                                    widget
                                                        .allFreqCheck['title']),
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xFF1E293B),
                                                )),
                                            TextSpan(
                                                text:
                                                    '  ${widget.allFreqCheck['count']}'
                                                        .toString(),
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w700,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                ))
                                          ]),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            ...widget.frequencies
                                .map((freq) => GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          freq['value'] = !freq['value'];
                                          widget.allFreqCheck['value'] = widget
                                              .frequencies
                                              .every((freq) => freq['value']);
                                          widget.handleCategoryCount();
                                          if (widget.categories.every(
                                              (category) =>
                                                  !category['value'])) {
                                            _resultCount = 0;
                                          } else {
                                            _resultCount = widget.frequencies
                                                .where((freq) => freq['value'])
                                                .fold(
                                                    0,
                                                    (preValue, curr) =>
                                                        preValue +
                                                        curr['count']);
                                          }
                                        });
                                      },
                                      child: Container(
                                        height: 30,
                                        width: double.infinity,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 15),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Checkbox(
                                              key: ValueKey(freq['title']),
                                              value: freq['value'],
                                              activeColor: Theme.of(context)
                                                  .primaryColor,
                                              onChanged: (value) {
                                                setState(() {
                                                  freq['value'] = value;
                                                  widget.allFreqCheck['value'] =
                                                      widget.frequencies.every(
                                                          (freq) =>
                                                              freq['value']);
                                                  widget.handleCategoryCount();
                                                  if (widget.categories.every(
                                                      (category) =>
                                                          !category['value'])) {
                                                    _resultCount = 0;
                                                  } else {
                                                    _resultCount = widget
                                                        .frequencies
                                                        .where((freq) =>
                                                            freq['value'])
                                                        .fold(
                                                            0,
                                                            (preValue, curr) =>
                                                                preValue +
                                                                curr['count']);
                                                  }
                                                });
                                              },
                                            ),
                                            RichText(
                                              textScaleFactor:
                                                  MediaQuery.of(context)
                                                      .textScaleFactor,
                                              text: TextSpan(
                                                  style: const TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'Inter',
                                                  ),
                                                  children: [
                                                    TextSpan(
                                                        text: FlutterI18n
                                                                .translate(
                                                                    context,
                                                                    freq[
                                                                        'title']) +
                                                            ' ${freq['freqValue']}',
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color:
                                                              Color(0xFF1E293B),
                                                        )),
                                                    TextSpan(
                                                        text:
                                                            '  ${freq['count']}'
                                                                .toString(),
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          color:
                                                              Theme.of(context)
                                                                  .primaryColor,
                                                        ))
                                                  ]),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList(),
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 24),
                          color: const Color(0xFFEAEAF5),
                          height: 1,
                          width: double.infinity,
                        ),
                        ExpansionTile(
                          backgroundColor: Colors.transparent,
                          collapsedBackgroundColor: Colors.transparent,
                          initiallyExpanded: _isCategoryPanelExpanded,
                          iconColor: const Color(0xFF1E293B),
                          title: Text(
                            FlutterI18n.translate(
                                context, 'common.text.symptom_category'),
                            style: TextStyle(
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .fontWeight,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .fontSize,
                                color: const Color(0xFF1E293B)),
                          ),
                          tilePadding:
                              const EdgeInsets.symmetric(horizontal: 24),
                          onExpansionChanged: (bool isExpanded) {
                            setState(
                                () => _isCategoryPanelExpanded = isExpanded);
                          },
                          children: [
                            Container(
                              height: 50,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 24, vertical: 5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: const Color(0xFFDEDFE9),
                                  style: BorderStyle.solid,
                                  width: 1,
                                ),
                              ),
                              child: TextField(
                                controller: _categorySearchController,
                                onChanged: (value) {
                                  setState(() {
                                    _categorySearchController
                                      ..text = value
                                      ..selection = TextSelection.collapsed(
                                          offset: _categorySearchController
                                              .text.length);
                                    if (language == 'en') {
                                      _categorySearches = widget.categories
                                          .where((category) => (category[
                                                          'title'] !=
                                                      null &&
                                                  category['title'] != '')
                                              ? category['title']
                                                  .toLowerCase()
                                                  .contains(
                                                      _categorySearchController
                                                          .text
                                                          .toLowerCase())
                                              : false)
                                          .toList();
                                    } else {
                                      _categorySearches = widget.categories
                                          .where((category) => (category[
                                                          'title'] !=
                                                      null &&
                                                  category['title'] != '')
                                              ? removeDiacritics(
                                                      category['translation'])
                                                  .toLowerCase()
                                                  .contains(removeDiacritics(
                                                          _categorySearchController
                                                              .text)
                                                      .toLowerCase())
                                              : false)
                                          .toList();
                                    }
                                  });
                                },
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.only(
                                      top: 15, bottom: 15),
                                  focusedBorder: InputBorder.none,
                                  prefixIcon: const Icon(
                                    PhosphorIcons.magnifyingGlass,
                                    size: 24,
                                    color: Color(0xFFCDCEDE),
                                  ),
                                  hintText: FlutterI18n.translate(context,
                                      'common.placeholders.search_here'),
                                  hintStyle: const TextStyle(
                                    color: Color(0xFFADAFCA),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  border: InputBorder.none,
                                  suffixIcon: _categorySearchController
                                          .text.isNotEmpty
                                      ? IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _categorySearchController.clear();
                                              _categorySearches =
                                                  widget.categories;
                                            });
                                            // widget.handleSearch();
                                          },
                                          padding:
                                              const EdgeInsets.only(left: 15),
                                          icon: const Icon(
                                            PhosphorIcons.x,
                                            size: 24,
                                            color: Color(0xFF1E293B),
                                          ))
                                      : const SizedBox.shrink(),
                                  // : IconButton(
                                  //     onPressed: () {
                                  //       print('Micro pressed');
                                  //     },
                                  //     padding:
                                  //         const EdgeInsets.only(left: 15),
                                  //     icon: const Icon(
                                  //       PhosphorIcons.microphone,
                                  //       size: 24,
                                  //       color: Color(0xFF1E293B),
                                  //     ),
                                  //   ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  widget.allCategoryCheck['value'] =
                                      !widget.allCategoryCheck['value'];
                                  for (var category in widget.categories) {
                                    category['value'] =
                                        widget.allCategoryCheck['value'];
                                  }
                                  widget.handleFrequencyCount();
                                  if (widget.frequencies
                                      .every((freq) => !freq['value'])) {
                                    _resultCount = 0;
                                  } else {
                                    _resultCount = widget.categories
                                        .where((category) => category['value'])
                                        .fold(
                                            0,
                                            (preValue, curr) =>
                                                preValue + curr['count']);
                                  }
                                });
                              },
                              child: Container(
                                height: 30,
                                width: double.infinity,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 15),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Checkbox(
                                      key: ValueKey(
                                          widget.allCategoryCheck['title']),
                                      value: widget.allCategoryCheck['value'],
                                      activeColor:
                                          Theme.of(context).primaryColor,
                                      onChanged: (value) {
                                        setState(() {
                                          widget.allCategoryCheck['value'] =
                                              value;
                                          for (var category
                                              in widget.categories) {
                                            category['value'] = value;
                                          }
                                          widget.handleFrequencyCount();
                                          if (widget.frequencies.every(
                                              (freq) => !freq['value'])) {
                                            _resultCount = 0;
                                          } else {
                                            _resultCount = widget.categories
                                                .where((category) =>
                                                    category['value'])
                                                .fold(
                                                    0,
                                                    (preValue, curr) =>
                                                        preValue +
                                                        curr['count']);
                                          }
                                        });
                                      },
                                    ),
                                    RichText(
                                      textScaleFactor: MediaQuery.of(context)
                                          .textScaleFactor,
                                      text: TextSpan(
                                          style: const TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'Inter',
                                          ),
                                          children: [
                                            TextSpan(
                                                text: FlutterI18n.translate(
                                                    context,
                                                    widget.allCategoryCheck[
                                                        'title']),
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xFF1E293B),
                                                )),
                                            TextSpan(
                                                text:
                                                    '  ${widget.allCategoryCheck['count']}'
                                                        .toString(),
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w700,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                ))
                                          ]),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            ..._categorySearches
                                .map((category) => GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          category['value'] =
                                              !category['value'];
                                          widget.allCategoryCheck['value'] =
                                              widget.categories.every(
                                                  (category) =>
                                                      category['value']);
                                          widget.handleFrequencyCount();
                                          if (widget.frequencies.every(
                                              (freq) => !freq['value'])) {
                                            _resultCount = 0;
                                          } else {
                                            _resultCount = widget.categories
                                                .where((category) =>
                                                    category['value'])
                                                .fold(
                                                    0,
                                                    (preValue, curr) =>
                                                        preValue +
                                                        curr['count']);
                                          }
                                        });
                                      },
                                      child: Container(
                                        height: 30,
                                        width: double.infinity,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 15),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Checkbox(
                                              key: ValueKey(category['title']),
                                              value: category['value'],
                                              activeColor: Theme.of(context)
                                                  .primaryColor,
                                              onChanged: (value) {
                                                setState(() {
                                                  category['value'] = value;
                                                  widget.allCategoryCheck[
                                                          'value'] =
                                                      widget.categories.every(
                                                          (category) =>
                                                              category[
                                                                  'value']);
                                                  widget.handleFrequencyCount();
                                                  if (widget.frequencies.every(
                                                      (freq) =>
                                                          !freq['value'])) {
                                                    _resultCount = 0;
                                                  } else {
                                                    _resultCount = widget
                                                        .categories
                                                        .where((category) =>
                                                            category['value'])
                                                        .fold(
                                                            0,
                                                            (preValue, curr) =>
                                                                preValue +
                                                                curr['count']);
                                                  }
                                                });
                                              },
                                            ),
                                            RichText(
                                              textScaleFactor:
                                                  MediaQuery.of(context)
                                                      .textScaleFactor,
                                              text: TextSpan(
                                                  style: const TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'Inter',
                                                  ),
                                                  children: [
                                                    TextSpan(
                                                        text: (category['title'] !=
                                                                    null &&
                                                                category[
                                                                        'title'] !=
                                                                    '')
                                                            ? (language == 'en'
                                                                ? category[
                                                                    'title']
                                                                : category[
                                                                    'translation'])
                                                            : FlutterI18n.translate(
                                                                context,
                                                                'common.text.uncategorized'),
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color:
                                                              Color(0xFF1E293B),
                                                        )),
                                                    TextSpan(
                                                        text:
                                                            '  ${category['count']}'
                                                                .toString(),
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          color:
                                                              Theme.of(context)
                                                                  .primaryColor,
                                                        ))
                                                  ]),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList(),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 24, right: 24),
              color: const Color(0xFFEAEAF5),
              height: 1,
              width: double.infinity,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
              height: 70,
              margin: const EdgeInsets.only(bottom: 25),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Expanded(
                    flex: 2,
                    child: TextButton(
                      style: ButtonStyle(
                        overlayColor:
                            MaterialStateProperty.all(const Color(0xFFFEF5F6)),
                      ),
                      onPressed: () {
                        setState(() {
                          widget.allFreqCheck['value'] = true;
                          widget.allCategoryCheck['value'] = true;
                          for (var freq in widget.frequencies) {
                            freq['value'] = true;
                          }
                          for (var category in widget.categories) {
                            category['value'] = true;
                          }
                          widget.handleCategoryCount();
                          widget.handleFrequencyCount();
                          _resultCount = widget.allFreqCheck['count'];
                        });
                      },
                      child: Text(
                        FlutterI18n.translate(
                            context, 'common.buttons.reset_all'),
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFFFB4654),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10))),
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) {
                            if (states.contains(MaterialState.disabled)) {
                              return const Color(0xFFCDCEDE);
                            } else {
                              return Theme.of(context).primaryColor;
                            }
                          },
                        ),
                      ),
                      onPressed:
                          widget.frequencies.every((freq) => !freq['value']) ||
                                  widget.categories
                                      .every((category) => !category['value'])
                              ? null
                              : () {
                                  widget.handleFilter();
                                  Navigator.pop(context);
                                },
                      child: Text(
                        FlutterI18n.translate(context, 'common.buttons.apply') +
                            ' ($_resultCount)',
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              alignment: Alignment.center,
            ),
          ],
        ),
      ),
    );
  }
}
