import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:rdk_mobile/models/prevalence.dart';

class PrevalenceCard extends StatelessWidget {
  const PrevalenceCard({
    Key key,
    @required this.prevalence,
  }) : super(key: key);

  final MapEntry<String, List<Prevalence>> prevalence;

  @override
  Widget build(BuildContext context) {
    List<Prevalence> prevalenceOrIncidenceList = prevalence.value
        .where((prev) =>
            prev.prevalenceType != 'Cases/families' &&
            prev.prevalenceQualification != 'Class only')
        .toList();
    List<Prevalence> caseFamilyList = prevalence.value
        .where((prev) => prev.prevalenceType == 'Cases/families')
        .toList();
    List<Prevalence> classOnlyList = prevalence.value
        .where((prev) =>
            prev.prevalenceQualification == 'Class only' &&
            prev.prevalenceClass != 'Not yet documented' &&
            prev.prevalenceClass != 'Unknown')
        .toList();
    prevalenceOrIncidenceList
        .sort((e1, e2) => e1.prevalenceType == 'Annual Incidence' ? 0 : 1);
    if (prevalenceOrIncidenceList.isNotEmpty ||
        caseFamilyList.isNotEmpty ||
        classOnlyList.isNotEmpty) {
      return Card(
        elevation: 0,
        margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Container(
          padding: const EdgeInsets.all(24),
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                prevalence.key,
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline4.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                  color: const Color(0xFF1E293B),
                ),
              ),
              if (prevalenceOrIncidenceList.isNotEmpty) ...[
                const SizedBox(
                  height: 32,
                ),
                Table(
                  columnWidths: const {
                    0: FlexColumnWidth(2),
                    1: FlexColumnWidth(1),
                  },
                  children: [
                    TableRow(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text(
                            'Type',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.2,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: Text(
                            '${FlutterI18n.translate(context, 'common.text.prevalence_type_avg')}/100 000',
                            textAlign: TextAlign.end,
                            style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.2,
                            ),
                          ),
                        ),
                      ],
                    ),
                    ...prevalenceOrIncidenceList.map(
                      (prev) => TableRow(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              FlutterI18n.translate(
                                  context,
                                  'models.epidemiology_prevalence_type.' +
                                      prev.prevalenceType
                                          .replaceAll(' ', '_')
                                          .toLowerCase() +
                                      '.class_title'),
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.5,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              prev.valMoy.toString(),
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.5,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
              if (classOnlyList.isNotEmpty) ...[
                const SizedBox(
                  height: 32,
                ),
                Table(
                  columnWidths: const {
                    0: FlexColumnWidth(2),
                    1: FlexColumnWidth(1),
                  },
                  children: [
                    TableRow(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text(
                            'Type',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.2,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.text.prevalence_class'),
                            textAlign: TextAlign.end,
                            style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.2,
                            ),
                          ),
                        ),
                      ],
                    ),
                    ...classOnlyList.map(
                      (prev) => TableRow(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              FlutterI18n.translate(
                                  context,
                                  'models.epidemiology_prevalence_type.' +
                                      prev.prevalenceType
                                          .replaceAll(' ', '_')
                                          .toLowerCase() +
                                      '.class_title'),
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.5,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              prev.prevalenceClass,
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.5,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
              if (caseFamilyList.isNotEmpty) ...[
                const SizedBox(
                  height: 32,
                ),
                Table(
                  columnWidths: const {
                    0: FlexColumnWidth(2),
                    1: FlexColumnWidth(1),
                  },
                  children: [
                    TableRow(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: Text(
                            FlutterI18n.translate(context,
                                'models.epidemiology_prevalence_type.cases/families.class_title'),
                            style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.2,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: Text(
                            FlutterI18n.translate(
                                context, 'common.text.published_number'),
                            textAlign: TextAlign.end,
                            style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF8F91AC),
                              height: 1.2,
                            ),
                          ),
                        ),
                      ],
                    ),
                    ...caseFamilyList.map(
                      (prev) => TableRow(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              FlutterI18n.translate(
                                  context,
                                  'models.epidemiology_prevalence_type.cases/families.' +
                                      prev.prevalenceQualification
                                          .replaceAll(' ', '_')
                                          .toLowerCase()),
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.5,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              prev.valMoy.toString(),
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .fontWeight,
                                color: const Color(0xFF1E293B),
                                height: 1.5,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ]
            ],
          ),
        ),
      );
    }
    return const SizedBox.shrink();
  }
}
