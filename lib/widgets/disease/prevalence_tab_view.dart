import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:rdk_mobile/models/prevalence.dart';
import 'package:rdk_mobile/widgets/disease/prevalence_card.dart';
import 'package:collection/collection.dart';

class PrevalenceTabView extends StatefulWidget {
  const PrevalenceTabView({
    @required this.prevalenceList,
  });

  final List<Prevalence> prevalenceList;

  @override
  _PrevalenceTabViewState createState() => _PrevalenceTabViewState();
}

class _PrevalenceTabViewState extends State<PrevalenceTabView> {
  final TextEditingController _searchController = TextEditingController();
  Map<String, List<Prevalence>> _resultList;
  List<MapEntry<String, List<Prevalence>>> _searchList;

  @override
  void initState() {
    _resultList = groupBy(
        widget.prevalenceList
            .where((prev) =>
                prev.validationStatus == 'Validated' &&
                prev.prevalenceClass != 'Unknown')
            .toList(),
        (obj) => obj.prevalenceGeographic);
    _searchList = _resultList.entries.toList();
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void updatePrevalenceList() {
    setState(() {
      _searchList.clear();
      _searchList = _resultList.entries
          .where((item) => item.key
              .toLowerCase()
              .contains(_searchController.text.toLowerCase()))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    _searchList.sort((e1, e2) {
      if (e1.key == 'France' || e1.key == 'Europe') {
        return 0;
      }
      return 1;
    });
    if (_resultList.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_epidemiology_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.epidemiology'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          Container(
            height: 40,
            width: double.infinity,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: const Color(0xFFFFFFFF),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (_) => updatePrevalenceList(),
              cursorHeight: 12,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFF8F91AC),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_for_country'),
                hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1),
                border: InputBorder.none,
                suffixIcon: _searchController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _searchController.clear();
                          updatePrevalenceList();
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
          _searchList.isEmpty
              ? Container(
                  padding: const EdgeInsets.symmetric(vertical: 50),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: const Color(0xFFECECF2),
                        ),
                        child: const Icon(
                          PhosphorIcons.magnifyingGlass,
                          size: 44,
                          color: Color(0xFF8487AC),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 55, vertical: 8),
                        child: Text(
                          FlutterI18n.translate(
                              context, 'common.text.no_epidemiology_found'),
                          style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF1E293B),
                              height: 1.5),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              : const SizedBox(
                  height: 20,
                ),
          ..._searchList
              .map((prevalence) => PrevalenceCard(prevalence: prevalence))
        ],
      );
    }
  }
}
