import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/publication.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

import 'dart:ui' as ui;

import 'package:url_launcher/url_launcher.dart';

class PublicationCard extends StatefulWidget {
  PublicationCard({
    Key key,
    @required this.publication,
  }) : super(key: key);

  final Publication publication;
  bool isReadMore = false;

  @override
  _PublicationCardState createState() => _PublicationCardState();
}

class _PublicationCardState extends State<PublicationCard>
    with AutomaticKeepAliveClientMixin {
  String _abstractFirstHalf;
  String _abstractSecondHalf;

  @override
  void initState() {
    super.initState();
    if (widget.publication.abstract.length > 100) {
      _abstractFirstHalf = widget.publication.abstract.substring(0, 150);
      _abstractSecondHalf = widget.publication.abstract
          .substring(150, widget.publication.abstract.length);
    } else {
      _abstractFirstHalf = widget.publication.abstract;
      _abstractSecondHalf = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                if (widget.publication.types.contains('Review')) ...[
                  Chip(
                    label: Text(
                      FlutterI18n.translate(
                          context, 'models.publication.type.review'),
                      style: const TextStyle(
                        fontFamily: 'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                      ),
                    ),
                    labelPadding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 0),
                    backgroundColor: const Color(0x1A8F91AC),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                ],
                if (widget.publication.types.contains('Case Reports'))
                  Chip(
                    label: Text(
                      FlutterI18n.translate(
                          context, 'models.publication.type.case_reports'),
                      style: const TextStyle(
                        fontFamily: 'Inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                      ),
                    ),
                    labelPadding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 0),
                    backgroundColor: const Color(0x1A8F91AC),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8),
                      ),
                    ),
                  ),
                const Spacer(),
                Directionality(
                  textDirection: ui.TextDirection.rtl,
                  child: TextButton.icon(
                    label: Text(
                      FlutterI18n.translate(
                          context, 'common.buttons.see_article'),
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    icon: Icon(
                      PhosphorIcons.arrowSquareOutBold,
                      size: 20,
                      color: Theme.of(context).primaryColor,
                    ),
                    onPressed: () {
                      launch(widget.publication.link);
                    },
                  ),
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.publication.title,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      color: Color(0xFF1E293B),
                      height: 1.15,
                    ),
                  ),
                  Text(
                    '${widget.publication.source} - ${widget.publication.publishedDate}',
                    style: const TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF8F91AC),
                      height: 1.5,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.publication.authorList.join(", "),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      color: Color(0xFF1E293B),
                      height: 1.15,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  if (_abstractSecondHalf.isEmpty)
                    Text(
                      widget.publication.abstract,
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                        height: 1.5,
                      ),
                    )
                  else ...[
                    Text(
                      widget.isReadMore
                          ? (_abstractFirstHalf + _abstractSecondHalf)
                          : (_abstractFirstHalf + '...'),
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                        height: 1.5,
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    InkWell(
                      child: Text(
                        widget.isReadMore
                            ? FlutterI18n.translate(
                                context, 'common.buttons.show_less')
                            : FlutterI18n.translate(
                                context, 'common.buttons.read_more'),
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Theme.of(context).primaryColor,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          widget.isReadMore = !widget.isReadMore;
                        });
                      },
                    )
                  ]
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
