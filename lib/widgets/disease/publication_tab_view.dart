import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:group_button/group_button.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:rdk_mobile/models/publication.dart';
import 'package:rdk_mobile/providers/publications.dart';
import 'package:rdk_mobile/widgets/disease/publication_card.dart';
import 'package:rdk_mobile/widgets/pagination.dart';

class PublicationTabView extends StatefulWidget {
  final String diseaseName;
  final List<String> synonyms;

  const PublicationTabView(
      {Key key, @required this.diseaseName, @required this.synonyms})
      : super(key: key);

  @override
  _PublicationTabViewState createState() => _PublicationTabViewState();
}

class _PublicationTabViewState extends State<PublicationTabView> {
  // final TextEditingController _searchController = TextEditingController();
  final GroupButtonController groupBtnController = GroupButtonController();
  List<Publication> _resultList;
  bool _isLoading = false;
  bool _isInit = true;
  int _offset;
  int _limit;
  int _pageCount = 1;
  int _resultCount = 0;
  int _selectedPage = 1;
  int _selectedPublicationType = 0;
  final List<String> _publicationTypes = ['all', 'review', 'case_reports'];
  final int _pageVisible = 5;

  @override
  void initState() {
    groupBtnController.selectIndex(0);
    _offset = 0;
    _limit = 10;
    fetchData(_offset, _limit);
    super.initState();
  }

  @override
  void dispose() {
    groupBtnController.dispose();
    super.dispose();
  }

  // void updatePrevalenceList() {
  //   setState(() {
  //     _resultList = widget.prevalenceList
  //         .where((prevalence) => prevalence.prevalenceGeographic
  //             .toLowerCase()
  //             .contains(_searchController.text.toLowerCase()))
  //         .toList();
  //   });
  // }

  void fetchData(int offset, int limit) {
    setState(() {
      _isLoading = true;
    });
    String searchTerm = '"${widget.diseaseName}"[Title/Abstract:~0]';
    if (widget.synonyms.isNotEmpty) {
      for (String syn in widget.synonyms) {
        searchTerm += ' OR "$syn"[Title/Abstract:~0]';
      }
    }
    Publications.fetchPublications(
            searchTerm: searchTerm,
            type: _publicationTypes[_selectedPublicationType],
            offset: offset,
            limit: limit)
        .then((res) {
      _resultList = res['data'];
      _resultCount = int.parse(res['count']);
      _pageCount = (_resultCount ~/ limit +
          (int.parse(res['count']) % limit > 0 ? 1 : 0));
      setState(() {
        _isLoading = false;
        _isInit = false;
      });
    }).catchError((error) async {
      print(error);
      _resultList = [];
      setState(() {
        _isLoading = false;
      });
      // await showDialog(
      //   context: context,
      //   builder: (ctx) => AlertDialog(
      //     shape: RoundedRectangleBorder(
      //       borderRadius: BorderRadius.circular(10),
      //     ),
      //     title: Text(
      //       FlutterI18n.translate(
      //           context,
      //           error == HttpStatus.gatewayTimeout
      //               ? 'common.errors.connection_error'
      //               : 'common.errors.an_error_occured'),
      //       style: Theme.of(context).textTheme.headline5,
      //     ),
      //     content: Text(
      //       FlutterI18n.translate(
      //           context,
      //           error == HttpStatus.gatewayTimeout
      //               ? 'common.errors.unable_access_server'
      //               : 'common.errors.something_went_wrong'),
      //       style: Theme.of(context).textTheme.bodyText2,
      //     ),
      //     actions: <Widget>[
      //       TextButton(
      //         child: Text(
      //           FlutterI18n.translate(context, 'common.buttons.go_back'),
      //           style: TextStyle(color: Theme.of(context).primaryColor),
      //         ),
      //         onPressed: () {
      //           Navigator.of(ctx).pop();
      //         },
      //       )
      //     ],
      //   ),
      // );
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isInit && _isLoading) {
      return Center(
        child: CircularProgressIndicator(
          color: Theme.of(context).primaryColor,
        ),
      );
    }
    if (!_isLoading && _resultList.isEmpty && _selectedPublicationType == 0) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(
                    context, 'common.text.publication_no_results'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    }
    return Stack(
      children: [
        if (_resultList.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(bottom: 65),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(right: 15, bottom: 30, left: 15),
                  child: Text(
                    FlutterI18n.translate(context, 'common.tabs.publications'),
                    style: TextStyle(
                      fontSize: Theme.of(context).textTheme.headline3.fontSize,
                      fontWeight:
                          Theme.of(context).textTheme.headline3.fontWeight,
                      color: const Color(0xFF1E293B),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 15),
                  height: 60,
                  child: GroupButton(
                    controller: groupBtnController,
                    onSelected: (label, index, isSelected) {
                      setState(() {
                        _selectedPublicationType = index;
                        fetchData(_offset, _limit);
                        _selectedPage = 1;
                      });
                    },
                    isRadio: true,
                    buttons: [
                      FlutterI18n.translate(context, 'common.text.all'),
                      FlutterI18n.translate(
                          context, 'models.publication.type.review'),
                      FlutterI18n.translate(
                          context, 'models.publication.type.case_reports')
                    ],
                    options: GroupButtonOptions(
                      groupingType: GroupingType.row,
                      mainGroupAlignment: MainGroupAlignment.start,
                      selectedBorderColor: Theme.of(context).primaryColor,
                      unselectedBorderColor: Colors.transparent,
                      selectedColor: Colors.white,
                      unselectedColor: Colors.white,
                      selectedTextStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF1E293B),
                      ),
                      unselectedTextStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF8F91AC),
                      ),
                      selectedShadow: [
                        const BoxShadow(
                          color: Color.fromRGBO(94, 92, 154, 0.06),
                          offset: Offset.zero,
                          blurRadius: 20,
                        )
                      ],
                      unselectedShadow: [
                        const BoxShadow(
                          color: Color.fromRGBO(94, 92, 154, 0.06),
                          offset: Offset.zero,
                          blurRadius: 20,
                        )
                      ],
                      elevation: 0,
                      borderRadius: BorderRadius.circular(48),
                      spacing: 4,
                    ),
                  ),
                ),
                if (!_isLoading && _resultList.isNotEmpty)
                  ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _resultList.length,
                    itemBuilder: (ctx, i) {
                      return PublicationCard(
                          key: ValueKey(_resultList[i].pmId),
                          publication: _resultList[i]);
                    },
                  ),
              ],
            ),
          ),
        if (_isLoading)
          Align(
            alignment: Alignment.center,
            child: Center(
              child: CircularProgressIndicator(
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
        if (_resultList.isEmpty) ...[
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.only(
                  left: 15, right: 15, top: MediaQuery.of(context).padding.top),
              padding: const EdgeInsets.symmetric(vertical: 10),
              height: 60,
              child: GroupButton(
                controller: groupBtnController,
                onSelected: (label, index, isSelected) {
                  setState(() {
                    _selectedPublicationType = index;
                    fetchData(_offset, _limit);
                    _selectedPage = 1;
                  });
                },
                isRadio: true,
                buttons: [
                  FlutterI18n.translate(context, 'common.text.all'),
                  FlutterI18n.translate(
                      context, 'models.publication.type.review'),
                  FlutterI18n.translate(
                      context, 'models.publication.type.case_reports')
                ],
                options: GroupButtonOptions(
                  groupingType: GroupingType.row,
                  mainGroupAlignment: MainGroupAlignment.start,
                  selectedBorderColor: Theme.of(context).primaryColor,
                  unselectedBorderColor: Colors.transparent,
                  selectedColor: Colors.white,
                  unselectedColor: Colors.white,
                  selectedTextStyle: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Color(0xFF1E293B),
                  ),
                  unselectedTextStyle: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Color(0xFF8F91AC),
                  ),
                  selectedShadow: [
                    const BoxShadow(
                      color: Color.fromRGBO(94, 92, 154, 0.06),
                      offset: Offset.zero,
                      blurRadius: 20,
                    )
                  ],
                  unselectedShadow: [
                    const BoxShadow(
                      color: Color.fromRGBO(94, 92, 154, 0.06),
                      offset: Offset.zero,
                      blurRadius: 20,
                    )
                  ],
                  elevation: 0,
                  borderRadius: BorderRadius.circular(48),
                  spacing: 4,
                ),
              ),
            ),
          ),
          if (!_isLoading)
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color(0xFFECECF2),
                    ),
                    child: const Icon(
                      PhosphorIcons.magnifyingGlass,
                      size: 44,
                      color: Color(0xFF8487AC),
                    ),
                  ),
                  if (_selectedPublicationType == 1)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 55, vertical: 8),
                      child: Text(
                        FlutterI18n.translate(context,
                            'common.text.publication_no_review_results'),
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B),
                            height: 1.5),
                        textAlign: TextAlign.center,
                      ),
                    )
                  else if (_selectedPublicationType == 2)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 55, vertical: 8),
                      child: Text(
                        FlutterI18n.translate(context,
                            'common.text.publication_no_case_report_results'),
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1E293B),
                            height: 1.5),
                        textAlign: TextAlign.center,
                      ),
                    )
                ],
              ),
            ),
        ],
        if (!_isInit && _resultList.isNotEmpty)
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xFFF9F9FA),
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(94, 92, 154, 0.2),
                    offset: Offset.zero,
                    blurRadius: 10,
                  )
                ],
              ),
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom != 0
                      ? MediaQuery.of(context).padding.bottom + 10
                      : 20,
                  left: 15,
                  right: 15),
              child: Pagination(
                numOfPages: _pageCount,
                selectedPage: _selectedPage,
                pagesVisible: _pageVisible,
                activeBtnStyle: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Theme.of(context).primaryColor),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(48),
                    ),
                  ),
                ),
                inactiveBtnStyle: ButtonStyle(
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(48),
                  )),
                ),
                activeTextStyle: const TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  height: 1,
                ),
                inactiveTextStyle: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  height: 1,
                ),
                previousIcon: Icon(
                  PhosphorIcons.caretLeftBold,
                  size: 16,
                  color: _selectedPage == 1
                      ? const Color(0x803E3F5E)
                      : const Color(0xFF1E293B),
                ),
                nextIcon: Icon(
                  PhosphorIcons.caretRightBold,
                  size: 16,
                  color: _selectedPage == _pageCount
                      ? const Color(0x803E3F5E)
                      : const Color(0xFF1E293B),
                ),
                startIcon: Icon(
                  PhosphorIcons.caretDoubleLeftBold,
                  size: 16,
                  color: _selectedPage == 1
                      ? const Color(0x803E3F5E)
                      : const Color(0xFF1E293B),
                ),
                endIcon: Icon(
                  PhosphorIcons.caretDoubleRightBold,
                  size: 16,
                  color: _selectedPage == _pageCount
                      ? const Color(0x803E3F5E)
                      : const Color(0xFF1E293B),
                ),
                onPageChanged: (page) {
                  setState(() {
                    _selectedPage = page;
                  });
                  fetchData((page - 1) * _limit, _limit);
                },
              ),
            ),
          )
      ],
    );
  }
}
