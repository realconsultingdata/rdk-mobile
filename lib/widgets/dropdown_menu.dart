import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

class DropdownMenu extends StatefulWidget {
  final String title;
  final List<Map<String, Object>> items;
  final Function onSelectHandler;

  const DropdownMenu({Key key, this.title, this.items, this.onSelectHandler})
      : super(key: key);

  @override
  _DropdownMenuState createState() => _DropdownMenuState();
}

class _DropdownMenuState extends State<DropdownMenu> {
  String _selectedValue;

  @override
  void initState() {
    _selectedValue = widget.items[0]['value'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 50,
          width: MediaQuery.of(context).size.width - 30,
          alignment: Alignment.center,
          margin: const EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: const Color(0xFFDEDFE9),
              style: BorderStyle.solid,
              width: 1,
            ),
          ),
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              alignedDropdown: true,
              padding: const EdgeInsetsDirectional.only(end: 10),
              child: DropdownButton(
                value: _selectedValue,
                onChanged: (newVal) {
                  setState(() {
                    _selectedValue = newVal;
                  });
                  widget.onSelectHandler(_selectedValue);
                },
                icon: Container(
                  padding: const EdgeInsets.only(right: 10),
                  child: const Icon(
                    PhosphorIcons.caretDown,
                    color: Color(0xFF1E293B),
                    size: 22,
                  ),
                ),
                isExpanded: true,
                dropdownColor: Colors.white,
                borderRadius: BorderRadius.circular(10),
                items: widget.items.map<DropdownMenuItem>((item) {
                  return DropdownMenuItem(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Text(
                        getItemLabel(item['name']),
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    value: item['value'],
                  );
                }).toList(),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            margin: const EdgeInsets.only(left: 20),
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 3),
            color: Colors.white,
            child: Text(
              widget.title,
              style: const TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: Color(0xFFADAFCA),
              ),
            ),
          ),
        )
      ],
    );
  }

  String getItemLabel(String itemName) {
    if (widget.key.toString().contains('country')) {
      return itemName;
    } else {
      return FlutterI18n.plural(context, itemName, 2);
    }
  }
}
