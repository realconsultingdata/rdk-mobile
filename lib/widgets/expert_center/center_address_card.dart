import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_specialty_translation.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:url_launcher/url_launcher.dart';

import 'center_map_widget.dart';

class CenterAddressCard extends StatelessWidget {
  final NewExpertCenter expertCenter;

  const CenterAddressCard({
    this.expertCenter,
  });

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;

    String address;
    Establishment establishment = expertCenter.institutions.first.establishment;
    if (establishment != null &&
        establishment.streetLabel != null &&
        establishment.streetType != null) {
      address =
          '${establishment.streetNumber == null ? '' : '${establishment.streetNumber} '}${establishment.streetIndex == null ? '' : '${establishment.streetIndex} '}${establishment.streetType} ${establishment.streetLabel}';
    }
    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (establishment != null &&
                establishment.latitude != null &&
                establishment.longitude != null) ...[
              CenterMapWidget(
                coordinates:
                    LatLng(establishment.latitude, establishment.longitude),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
            if (establishment != null) ...[
              Text(
                FlutterI18n.translate(context, 'models.expert_center.address'),
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                establishment.label.toUpperCase(),
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                    height: 1.5),
              ),
              if (address != null)
                Text(
                  address.toUpperCase(),
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                      fontWeight:
                          Theme.of(context).textTheme.bodyText2.fontWeight,
                      color: const Color(0xFF1E293B),
                      height: 1.5),
                ),
              Text(
                '${establishment.zipCode} ${establishment.city.toUpperCase()}, FRANCE',
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                    height: 1.5),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
            Text(
              FlutterI18n.translate(context, 'common.text.service'),
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w700,
                color: Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              expertCenter.institutions.first.departmentName,
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                  fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.5),
            ),
            const SizedBox(
              height: 24,
            ),
            if (expertCenter.specialties.isNotEmpty) ...[
              Text(
                FlutterI18n.translate(context, 'common.text.specialties'),
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: expertCenter.specialties.map((specialty) {
                  DiseaseSpecialtyTranslation specialtyTranslation =
                      specialty.translations.firstWhere(
                          (trans) => trans.language == language.toUpperCase(),
                          orElse: () => null);
                  return Chip(
                    key: ValueKey(specialty.id),
                    label: Text(
                      language != 'en' && specialtyTranslation != null
                          ? specialtyTranslation.label
                          : specialty.specialtyLabel,
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                      ),
                    ),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                    backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(4),
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  );
                }).toList(),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
            if (expertCenter.statusFlags
                .where((status) => status.id != '1' && status.id != '2048')
                .isNotEmpty) ...[
              Text(
                FlutterI18n.translate(
                    context, 'models.expert_center.consultation_type'),
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: [
                  if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '4096') >
                      -1) ...[
                    Chip(
                      key: const ValueKey('4096'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.4096'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ],
                  if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '128') >
                      -1) ...[
                    Chip(
                      key: const ValueKey('128'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.128'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ],
                  if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '64') >
                      -1) ...[
                    Chip(
                      key: const ValueKey('64'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.64'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ],
                  if (expertCenter.statusFlags
                              .indexWhere((status) => status.id == '32') >
                          -1 &&
                      language == 'en') ...[
                    Chip(
                      key: const ValueKey('32'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.32'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ],
                  if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '8192') >
                      -1) ...[
                    Chip(
                      key: const ValueKey('8192'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.8192'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ] else if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '16') >
                      -1) ...[
                    Chip(
                      key: const ValueKey('16'),
                      label: Text(
                        FlutterI18n.translate(
                            context, 'models.status_flag.value.16'),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                        ),
                      ),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                      backgroundColor: const Color(0xFF8F91AC).withOpacity(0.1),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                  ],
                ],
              ),
              const SizedBox(
                height: 24,
              ),
            ],
            if (expertCenter.institutions.first.contact1 != null ||
                expertCenter.institutions.first.contact2 != null) ...[
              Text(
                FlutterI18n.translate(context, 'common.text.telephone'),
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              if (expertCenter.institutions.first.contact1 != null)
                InkWell(
                  onTap: () => launchUrl(Uri.parse(
                      'tel:${expertCenter.institutions.first.contact1.replaceAll(' ', '')}')),
                  child: Text(
                    expertCenter.institutions.first.contact1,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.bodyText2.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.bodyText2.fontWeight,
                        color: const Color(0xFF1E293B),
                        height: 1.5),
                  ),
                ),
              if (expertCenter.institutions.first.contact2 != null)
                InkWell(
                  onTap: () => launchUrl(Uri.parse(
                      'tel:${expertCenter.institutions.first.contact2.replaceAll(' ', '')}')),
                  child: Text(
                    expertCenter.institutions.first.contact2,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.bodyText2.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.bodyText2.fontWeight,
                        color: const Color(0xFF1E293B),
                        height: 1.5),
                  ),
                ),
              const SizedBox(
                height: 24,
              ),
            ],
            if (expertCenter.institutions.first.email != null) ...[
              Text(
                FlutterI18n.translate(context, 'common.buttons.email'),
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              InkWell(
                onTap: () => launchUrl(Uri.parse(
                    'mailto:${expertCenter.institutions.first.email}')),
                child: Text(
                  expertCenter.institutions.first.email,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                      fontWeight:
                          Theme.of(context).textTheme.bodyText2.fontWeight,
                      color: const Color(0xFF1E293B),
                      height: 1.5),
                ),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
            if (expertCenter.url != null) ...[
              Text(
                FlutterI18n.translate(context, 'common.text.website'),
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              InkWell(
                onTap: () => launchUrl(Uri.parse(expertCenter.url),
                    mode: LaunchMode.externalApplication),
                child: Text(
                  expertCenter.url,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: Theme.of(context).primaryColor,
                    height: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
            ],
          ],
        ),
      ),
    );
  }
}
