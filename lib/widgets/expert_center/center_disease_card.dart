import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_synonym.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/models/institution.dart';
import 'package:rdk_mobile/providers/bookmark_diseases.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class CenterDiseaseCard extends StatefulWidget {
  final Disease disease;

  const CenterDiseaseCard({Key key, @required this.disease}) : super(key: key);

  @override
  _CenterDiseaseCardState createState() => _CenterDiseaseCardState();
}

class _CenterDiseaseCardState extends State<CenterDiseaseCard> {
  Locale currentLocale;
  DiseaseTranslation translation;
  bool _isBookmarked = false;
  List<Disease> _diseases = [];
  List<DiseaseSynonym> synonyms = [];

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;

    synonyms = widget.disease.synonyms
        .where((syn) => syn.language == language)
        .toList();
    translation = widget.disease.translations.firstWhere(
        (translation) => translation.language == language,
        orElse: () => null);
    _diseases = Provider.of<BookmarkDiseases>(context).diseases;
    _isBookmarked = _diseases.firstWhere(
            (disease) => disease.orphaNumber == widget.disease.orphaNumber,
            orElse: () => null) !=
        null;

    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.disease.disorderGroup != 'Disorder') ...[
              Container(
                padding: widget.disease.disorderGroup == 'Group of disorders'
                    ? const EdgeInsets.symmetric(vertical: 4, horizontal: 8)
                    : const EdgeInsets.symmetric(vertical: 4),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  color: widget.disease.disorderGroup == 'Group of disorders'
                      ? const Color(0xFF6C5CE3).withOpacity(0.1)
                      : Colors.transparent,
                ),
                child: Text(
                  language != 'en' && translation != null
                      ? translation.disorderGroup
                      : widget.disease.disorderGroup,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color:
                          widget.disease.disorderGroup == 'Group of disorders'
                              ? const Color(0xFF4533C9)
                              : const Color(0xFFADAFCA),
                      height: 1.2),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
            ],
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    language != 'en' && translation != null
                        ? translation.name
                        : widget.disease.name,
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.headline3.fontSize,
                        fontWeight:
                            Theme.of(context).textTheme.headline3.fontWeight,
                        color: const Color(0xFF1E293B),
                        height: 1.2),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: const BoxConstraints(),
                  splashRadius: 20,
                  icon: Icon(
                    _isBookmarked
                        ? PhosphorIcons.bookmarkSimpleFill
                        : PhosphorIcons.bookmarkSimple,
                    size: 24,
                    color: _isBookmarked
                        ? Theme.of(context).primaryColor
                        : const Color(0xFF64748B),
                  ),
                  onPressed: () async {
                    if (!_isBookmarked) {
                      Provider.of<BookmarkDiseases>(context, listen: false)
                          .addDisease(
                              context,
                              Disease(
                                  orphaNumber: widget.disease.orphaNumber,
                                  name: widget.disease.name))
                          .then((value) {
                        if (value != null) {
                          setState(() {
                            _isBookmarked = true;
                          });
                        }
                      });
                    } else {
                      Provider.of<BookmarkDiseases>(context, listen: false)
                          .deleteDisease(context, widget.disease.orphaNumber)
                          .then((value) {
                        if (value > 0) {
                          setState(() {
                            _isBookmarked = false;
                          });
                        }
                      });
                    }
                  },
                ),
              ],
            ),
            if (widget.disease.contents != null &&
                widget.disease.contents != '') ...[
              const SizedBox(
                height: 32,
              ),
              Html(
                data: language != 'en' && translation != null
                    ? translation.contents
                    : widget.disease.contents,
                style: {
                  "body": Style(
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero,
                    fontSize: const FontSize(14),
                    fontWeight: FontWeight.w400,
                    color: const Color(0xFF1E293B),
                    lineHeight: const LineHeight(1.5),
                  )
                },
              ),
            ],
            if (synonyms != null && synonyms.isNotEmpty) ...[
              const SizedBox(
                height: 32,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: synonyms
                    .map<Widget>(
                      (syn) => Chip(
                        key: ValueKey(syn),
                        label: Text(syn.synonym),
                        labelStyle: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.bodyText2.fontSize,
                          fontWeight:
                              Theme.of(context).textTheme.bodyText2.fontWeight,
                          color: const Color(0xFF1E293B),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                        backgroundColor:
                            const Color(0xFF8F91AC).withOpacity(0.1),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      ),
                    )
                    .toList(),
              ),
            ],
            const SizedBox(
              height: 24,
            ),
            Text(
              'ORPHA: ' + widget.disease.orphaNumber,
              style: const TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF8487AC),
                  height: 1.2),
            ),
          ],
        ),
      ),
    );
  }
}
