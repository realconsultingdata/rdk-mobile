import 'dart:collection';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_new_expert_center.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/models/institution.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/disease_info_screen.dart';
import 'package:rdk_mobile/widgets/expert_center/center_disease_card.dart';

class CenterDiseaseTabView extends StatefulWidget {
  final List<Disease> diseaseList;

  const CenterDiseaseTabView({Key key, @required this.diseaseList})
      : super(key: key);

  @override
  _CenterDiseaseTabViewState createState() => _CenterDiseaseTabViewState();
}

class _CenterDiseaseTabViewState extends State<CenterDiseaseTabView> {
  Locale currentLocale;
  final TextEditingController _searchController = TextEditingController();
  // final Map<Disease, List<Institution>> _diseaseList = {};
  List<Disease> _resultList;

  @override
  void initState() {
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    // for (Institution institution in widget.institutionList) {
    //   for (NewExpertCenter center in institution.expertCenters) {
    //     for (DiseaseNewExpertCenter diseaseExpCenter in center.diseases) {
    //       if (_diseaseList.isNotEmpty) {
    //         Disease existedDisease = _diseaseList.keys.firstWhere((d) {
    //           return d.orphaNumber == diseaseExpCenter.disease.orphaNumber;
    //         }, orElse: () => null);
    //         if (existedDisease == null) {
    //           _diseaseList[diseaseExpCenter.disease] = [institution];
    //         } else {
    //           _diseaseList[existedDisease].add(institution);
    //         }
    //       } else {
    //         _diseaseList[diseaseExpCenter.disease] = [institution];
    //       }
    //     }
    //   }
    // }
    // _resultList = SplayTreeMap<Disease, List<Institution>>((a, b) {
    //   if (currentLocale.languageCode == 'en') {
    //     return a.name.compareTo(b.name);
    //   } else {
    //     DiseaseTranslation aTranslation = a.translations.firstWhere(
    //         (ea) => ea.language == currentLocale.languageCode,
    //         orElse: () => null);
    //     DiseaseTranslation bTranslation = b.translations.firstWhere(
    //         (eb) => eb.language == currentLocale.languageCode,
    //         orElse: () => null);
    //     if (aTranslation != null && bTranslation != null) {
    //       return aTranslation.name.compareTo(bTranslation.name);
    //     }
    //     return a.name.compareTo(b.name);
    //   }
    // });
    // _resultList.addAll(_diseaseList);
    _resultList = List.from(widget.diseaseList);
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void updateDiseaseList() {
    setState(() {
      _resultList.clear();
      if (currentLocale.languageCode == 'en') {
        _resultList.addAll(widget.diseaseList.where((disease) => disease
            .name
            .toLowerCase()
            .contains(_searchController.text.toLowerCase())));
      } else {
        _resultList.addAll(widget.diseaseList.where((disease) {
          DiseaseTranslation translation = disease.translations.firstWhere(
              (trans) => trans.language == currentLocale.languageCode,
              orElse: () => null);
          return removeDiacritics(translation.name)
              .toLowerCase()
              .contains(removeDiacritics(_searchController.text.toLowerCase()));
        }));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.diseaseList.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_disease_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Container(
            height: 40,
            width: double.infinity,
            margin: const EdgeInsets.only(bottom: 15, left: 15, right: 15),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: const Color(0xFFFFFFFF),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (_) => updateDiseaseList(),
              cursorHeight: 12,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFF8F91AC),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_here'),
                hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1),
                border: InputBorder.none,
                suffixIcon: _searchController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _searchController.clear();
                          updateDiseaseList();
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
          if (_resultList.isEmpty)
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color(0xFFECECF2),
                    ),
                    child: const Icon(
                      PhosphorIcons.magnifyingGlass,
                      size: 44,
                      color: Color(0xFF8487AC),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.text.no_disease_found'),
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.5),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            )
          else
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _resultList.length,
              itemBuilder: (context, i) => GestureDetector(
                behavior: HitTestBehavior.deferToChild,
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(DiseaseInfoScreen.routeName, arguments: {
                    'orphaNumber':
                        _resultList[i].orphaNumber,
                    'backBtn': FlutterI18n.translate(context, 'pages.centers'),
                  });
                },
                child: CenterDiseaseCard(
                  disease: _resultList[i],
                ),
              ),
            ),
          const SizedBox(
            height: 7,
          ),
        ],
      );
    }
  }
}
