import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/institution.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:url_launcher/url_launcher.dart';

class CenterExpertCenterCard extends StatelessWidget {
  final NewExpertCenter expertCenter;
  final Institution institution;
  final Establishment establishment;

  const CenterExpertCenterCard(
      {Key key,
      @required this.expertCenter,
      @required this.institution,
      @required this.establishment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              language == 'fr' ? expertCenter.name : expertCenter.nameEn,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            Wrap(
              spacing: 5,
              runSpacing: 5,
              children: [
                expertCenter.statusFlags.firstWhere(
                            (status) => status.id == '2048',
                            orElse: () => null) !=
                        null
                    ? Chip(
                        key: const ValueKey('2048'),
                        label: RichText(
                          textScaleFactor:
                              MediaQuery.of(context).textScaleFactor,
                          text: TextSpan(
                            children: [
                              WidgetSpan(
                                alignment: PlaceholderAlignment.middle,
                                child: Container(
                                  height: 22,
                                  width: 22,
                                  margin: const EdgeInsets.only(right: 4),
                                  child: SvgPicture.asset(
                                    'assets/icons/ern_logo.svg',
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              ),
                              TextSpan(
                                text: 'ERN',
                                style: TextStyle(
                                  fontSize: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .fontSize,
                                  fontWeight: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .fontWeight,
                                  color: const Color(0xFF024D8F),
                                ),
                              ),
                            ],
                          ),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                        backgroundColor:
                            const Color(0xFF024D8F).withOpacity(0.1),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      )
                    : const SizedBox.shrink(),
              ],
            ),
            if (expertCenter.statusFlags
                .where((status) => status.id != '1' && status.id != '2048')
                .isNotEmpty) ...[
              const SizedBox(
                height: 24,
              ),
              Text(
                FlutterI18n.translate(
                    context, 'models.expert_center.consultation_type'),
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
                  fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
                  color: const Color(0xFF1E293B),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              if (expertCenter.statusFlags
                      .indexWhere((status) => status.id == '4096') >
                  -1) ...[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  FlutterI18n.translate(
                      context, 'models.status_flag.value.4096'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ],
              if (expertCenter.statusFlags
                      .indexWhere((status) => status.id == '128') >
                  -1) ...[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  FlutterI18n.translate(
                      context, 'models.status_flag.value.128'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ],
              if (expertCenter.statusFlags
                      .indexWhere((status) => status.id == '64') >
                  -1) ...[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  FlutterI18n.translate(context, 'models.status_flag.value.64'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ],
              if (expertCenter.statusFlags
                          .indexWhere((status) => status.id == '32') >
                      -1 &&
                  language == 'en') ...[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  FlutterI18n.translate(context, 'models.status_flag.value.32'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ],
              if (expertCenter.statusFlags
                      .indexWhere((status) => status.id == '8192') >
                  -1) ...[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  FlutterI18n.translate(
                      context, 'models.status_flag.value.8192'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ] else if (expertCenter.statusFlags
                      .indexWhere((status) => status.id == '16') >
                  -1) ...[
                const SizedBox(
                  height: 4,
                ),
                Text(
                  FlutterI18n.translate(context, 'models.status_flag.value.16'),
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ],
            ],
            const SizedBox(
              height: 24,
            ),
            Text(
              institution.departmentName,
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
                fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              establishment.zipCode + ' ' + establishment.city + ', France',
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            Row(
              mainAxisAlignment: MediaQuery.of(context).size.width < 768
                  ? MainAxisAlignment.spaceBetween
                  : MainAxisAlignment.center,
              children: [
                InkWell(
                  splashColor: Theme.of(context).primaryColor.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(12),
                  onTap: institution.email != null
                      ? () {
                          if (institution.email != null) {
                            Posthog().capture(
                              eventName: 'mail-to-expert-center',
                              properties: {
                                'eventType': 'click',
                                'resourceType': 'expert-center',
                                'resourceId': expertCenter.id,
                                'resourceName': expertCenter.name,
                                'establishmentId': establishment.id,
                                'establishmentName': establishment.label,
                                '\$screen_name':
                                    'Expert Center Info - $establishment.id'
                              },
                            );
                            launch('mailto:${institution.email}');
                          }
                        }
                      : null,
                  child: Container(
                    height: 60,
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: institution.email != null
                          ? Theme.of(context).primaryColor.withOpacity(0.1)
                          : Colors.grey.withOpacity(0.1),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          PhosphorIcons.envelopeSimple,
                          color: institution.email != null
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                          size: 24,
                        ),
                        Text(
                          FlutterI18n.translate(
                              context, 'common.buttons.email'),
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: institution.email != null
                                ? Theme.of(context).primaryColor
                                : Colors.grey,
                            height: 1.2,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width < 768 ? 0 : 16,
                ),
                InkWell(
                  splashColor: Theme.of(context).primaryColor.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(12),
                  onTap: institution.contact1 != null ||
                          institution.contact2 != null
                      ? () {
                          Posthog().capture(
                            eventName: 'call-to-expert-center',
                            properties: {
                              'eventType': 'click',
                              'resourceType': 'expert-center',
                              'resourceId': expertCenter.id,
                              'resourceName': expertCenter.name,
                              'establishmentId': establishment.id,
                              'establishmentName': establishment.label,
                              '\$screen_name':
                                  'Expert Center Info - $establishment.id'
                            },
                          );
                          if (institution.contact1 != null) {
                            launch(
                                'tel:${institution.contact1.replaceAll(' ', '')}');
                          } else {
                            launch(
                                'tel:${institution.contact2.replaceAll(' ', '')}');
                          }
                        }
                      : null,
                  child: Container(
                    height: 60,
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: institution.contact1 != null ||
                              institution.contact2 != null
                          ? Theme.of(context).primaryColor.withOpacity(0.1)
                          : Colors.grey.withOpacity(0.1),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          PhosphorIcons.phone,
                          color: institution.contact1 != null ||
                                  institution.contact2 != null
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                          size: 24,
                        ),
                        Text(
                          FlutterI18n.translate(context, 'common.buttons.call'),
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: institution.contact1 != null ||
                                    institution.contact2 != null
                                ? Theme.of(context).primaryColor
                                : Colors.grey,
                            height: 1.2,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width < 768 ? 0 : 16,
                ),
                InkWell(
                  splashColor: Theme.of(context).primaryColor.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(12),
                  onTap: establishment.latitude != null &&
                          establishment.longitude != null
                      ? () {
                          Posthog().capture(
                            eventName: 'navigate-to-expert-center',
                            properties: {
                              'eventType': 'click',
                              'resourceType': 'expert-center',
                              'resourceId': expertCenter.id,
                              'resourceName': expertCenter.name,
                              'establishmentId': establishment.id,
                              'establishmentName': establishment.label,
                              '\$screen_name':
                                  'Expert Center Info - $establishment.id'
                            },
                          );
                          Platform.isIOS
                              ? launch(
                                  'https://maps.apple.com/?q=${establishment.latitude.toString()},${establishment.longitude.toString()}')
                              : launch(
                                  'https://www.google.com/maps/search/?api=1&query=${establishment.latitude.toString()},${establishment.longitude.toString()}');
                        }
                      : null,
                  child: Container(
                    height: 60,
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Theme.of(context).primaryColor.withOpacity(0.1),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          PhosphorIcons.mapPin,
                          color: Theme.of(context).primaryColor,
                          size: 24,
                        ),
                        Text(
                          FlutterI18n.translate(context, 'common.buttons.map'),
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: Theme.of(context).primaryColor,
                            height: 1.2,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
