import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/institution.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/expert_center/center_expert_center_card.dart';

class CenterExpertCenterTabView extends StatefulWidget {
  final List<Institution> institutionList;
  final Establishment establishment;

  const CenterExpertCenterTabView(
      {Key key, @required this.institutionList, @required this.establishment})
      : super(key: key);

  @override
  _CenterExpertCenterTabViewState createState() =>
      _CenterExpertCenterTabViewState();
}

class _CenterExpertCenterTabViewState extends State<CenterExpertCenterTabView> {
  Locale currentLocale;
  final TextEditingController _searchController = TextEditingController();
  final Map<NewExpertCenter, Institution> _centerList = {};
  Map<NewExpertCenter, Institution> _resultList = {};

  @override
  void initState() {
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    for (Institution institution in widget.institutionList) {
      for (NewExpertCenter center in institution.expertCenters) {
        _centerList[center] = institution;
      }
    }
    _resultList.addAll(_centerList);
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void updateDiseaseList() {
    setState(() {
      _resultList.clear();
      if (currentLocale.languageCode == 'fr') {
        _resultList.addEntries(_centerList.entries.where((entry) =>
            removeDiacritics(entry.key.name).toLowerCase().contains(
                removeDiacritics(_searchController.text.toLowerCase()))));
      } else {
        _resultList.addEntries(_centerList.entries.where((entry) => entry
            .key.nameEn
            .toLowerCase()
            .contains(_searchController.text.toLowerCase())));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.institutionList.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(
                    context, 'common.text.no_expert_center_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Container(
            height: 40,
            width: double.infinity,
            margin: const EdgeInsets.only(bottom: 15, left: 15, right: 15),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: const Color(0xFFFFFFFF),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (_) => updateDiseaseList(),
              cursorHeight: 12,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFF8F91AC),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_here'),
                hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1),
                border: InputBorder.none,
                suffixIcon: _searchController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _searchController.clear();
                          updateDiseaseList();
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
          if (_resultList.isEmpty)
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color(0xFFECECF2),
                    ),
                    child: const Icon(
                      PhosphorIcons.magnifyingGlass,
                      size: 44,
                      color: Color(0xFF8487AC),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.text.no_expert_center_found'),
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.5),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            )
          else
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _resultList.length,
              itemBuilder: (context, i) => CenterExpertCenterCard(
                institution: _resultList.entries.toList()[i].value,
                expertCenter: _resultList.entries.toList()[i].key,
                establishment: widget.establishment,
              ),
            ),
          const SizedBox(
            height: 7,
          ),
        ],
      );
    }
  }
}
