import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/network.dart';
import 'package:rdk_mobile/models/new_expert_center.dart';
import 'package:rdk_mobile/models/status_flag.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/expert_center/center_address_card.dart';
import 'package:url_launcher/url_launcher.dart';

class CenterInformationTabView extends StatelessWidget {
  final NewExpertCenter loadedExpertCenter;

  const CenterInformationTabView({@required this.loadedExpertCenter});

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;

    List<Network> ernList =
        loadedExpertCenter.networks.where((network) => network.isErn).toList();
    List<Network> networkList =
        loadedExpertCenter.networks.where((network) => !network.isErn).toList();

    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            language == 'fr'
                ? loadedExpertCenter.name
                : loadedExpertCenter.nameEn,
            style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
                height: 1.5),
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MediaQuery.of(context).size.width < 768
                ? MainAxisAlignment.spaceAround
                : MainAxisAlignment.start,
            children: [
              Expanded(
                child: Material(
                  child: Ink(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: loadedExpertCenter.institutions.first.contact1 !=
                                  null ||
                              loadedExpertCenter.institutions.first.contact2 !=
                                  null
                          ? Theme.of(context).primaryColor
                          : Colors.grey.withOpacity(0.1),
                    ),
                    child: InkWell(
                      splashColor: Colors.white.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(8),
                      onTap: loadedExpertCenter.institutions.first.contact1 !=
                                  null ||
                              loadedExpertCenter.institutions.first.contact2 !=
                                  null
                          ? () {
                              Posthog().capture(
                                eventName: 'call-to-expert-center',
                                properties: {
                                  'eventType': 'click',
                                  'resourceType': 'expert-center',
                                  'resourceId': loadedExpertCenter.id,
                                  'resourceName': loadedExpertCenter.name,
                                  'establishmentId': loadedExpertCenter
                                      .institutions.first.establishment.id,
                                  'establishmentName': loadedExpertCenter
                                      .institutions.first.establishment.label,
                                  '\$screen_name':
                                      'Expert Center Info - ${loadedExpertCenter.id}'
                                },
                              );
                              if (loadedExpertCenter
                                      .institutions.first.contact1 !=
                                  null) {
                                launchUrl(Uri.parse(
                                    'tel:${loadedExpertCenter.institutions.first.contact1.replaceAll(' ', '')}'));
                              } else {
                                launchUrl(Uri.parse(
                                    'tel:${loadedExpertCenter.institutions.first.contact2.replaceAll(' ', '')}'));
                              }
                            }
                          : null,
                      child: Container(
                        height: 40,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              PhosphorIcons.phone,
                              color: loadedExpertCenter
                                              .institutions.first.contact1 !=
                                          null ||
                                      loadedExpertCenter
                                              .institutions.first.contact2 !=
                                          null
                                  ? Colors.white
                                  : Colors.grey,
                              size: 20,
                            ),
                            Text(
                              ' ${FlutterI18n.translate(
                                  context, 'common.buttons.call')}',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: loadedExpertCenter
                                                .institutions.first.contact1 !=
                                            null ||
                                        loadedExpertCenter
                                                .institutions.first.contact2 !=
                                            null
                                    ? Colors.white
                                    : Colors.grey,
                                height: 1.2,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                child: Material(
                  child: Ink(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: loadedExpertCenter.institutions.first.email != null
                          ? Theme.of(context).primaryColor
                          : Colors.grey.withOpacity(0.1),
                    ),
                    child: InkWell(
                      splashColor: Colors.white.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(8),
                      onTap: loadedExpertCenter.institutions.first.email != null
                          ? () {
                              if (loadedExpertCenter.institutions.first.email !=
                                  null) {
                                Posthog().capture(
                                  eventName: 'mail-to-expert-center',
                                  properties: {
                                    'eventType': 'click',
                                    'resourceType': 'expert-center',
                                    'resourceId': loadedExpertCenter.id,
                                    'resourceName': loadedExpertCenter.name,
                                    'establishmentId': loadedExpertCenter
                                        .institutions.first.establishment.id,
                                    'establishmentName': loadedExpertCenter
                                        .institutions.first.establishment.label,
                                    '\$screen_name':
                                        'Expert Center Info - ${loadedExpertCenter.id}'
                                  },
                                );
                                launchUrl(Uri.parse(
                                    'mailto:${loadedExpertCenter.institutions.first.email}'));
                              }
                            }
                          : null,
                      child: Container(
                        height: 40,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              PhosphorIcons.envelopeSimple,
                              color:
                                  loadedExpertCenter.institutions.first.email !=
                                          null
                                      ? Colors.white
                                      : Colors.grey,
                              size: 20,
                            ),
                            Text(
                              ' ${FlutterI18n.translate(
                                  context, 'common.buttons.email')}',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: loadedExpertCenter
                                            .institutions.first.email !=
                                        null
                                    ? Colors.white
                                    : Colors.grey,
                                height: 1.2,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              // InkWell(
              //   splashColor: Theme.of(context).primaryColor.withOpacity(0.2),
              //   borderRadius: BorderRadius.circular(12),
              //   onTap: expertCenter.expertCenter.institutions.first.establishment
              //                   .latitude !=
              //               null &&
              //           expertCenter.expertCenter.institutions.first.establishment
              //                   .longitude !=
              //               null
              //       ? () {
              //           Posthog().capture(
              //             eventName: 'navigate-to-expert-center',
              //             properties: {
              //               'eventType': 'click',
              //               'resourceType': 'expert-center',
              //               'resourceId': expertCenter.expertCenter.id,
              //               'resourceName': expertCenter.expertCenter.name,
              //               'establishmentId': expertCenter
              //                   .expertCenter.institutions.first.establishment.id,
              //               'establishmentName': expertCenter.expertCenter
              //                   .institutions.first.establishment.label,
              //               '\$screen_name': 'Disease Info - $orphaNumber'
              //             },
              //           );
              //           Platform.isIOS
              //               ? launch(
              //                   'https://maps.apple.com/?q=${expertCenter.expertCenter.institutions.first.establishment.latitude.toString()},${expertCenter.expertCenter.institutions.first.establishment.longitude.toString()}')
              //               : launch(
              //                   'https://www.google.com/maps/search/?api=1&query=${expertCenter.expertCenter.institutions.first.establishment.latitude.toString()},${expertCenter.expertCenter.institutions.first.establishment.longitude.toString()}');
              //         }
              //       : null,
              //   child: Container(
              //     height: 60,
              //     padding: const EdgeInsets.symmetric(horizontal: 32),
              //     decoration: BoxDecoration(
              //       borderRadius: BorderRadius.circular(12),
              //       color: Theme.of(context).primaryColor.withOpacity(0.1),
              //     ),
              //     child: Column(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: [
              //         Icon(
              //           PhosphorIcons.mapPin,
              //           color: Theme.of(context).primaryColor,
              //           size: 24,
              //         ),
              //         Text(
              //           FlutterI18n.translate(context, 'common.buttons.map'),
              //           style: TextStyle(
              //             fontSize: 11,
              //             fontWeight: FontWeight.w400,
              //             color: Theme.of(context).primaryColor,
              //             height: 1.2,
              //           ),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        CenterAddressCard(
          expertCenter: loadedExpertCenter,
        ),
        const SizedBox(
          height: 16,
        ),
        if (networkList.isNotEmpty) ...[
          Card(
            elevation: 0,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // SizedBox(
                  //   height: 50,
                  //   child: SvgPicture.asset(
                  //     'assets/icons/ern.svg',
                  //     fit: BoxFit.fitHeight,
                  //   ),
                  // ),
                  // const SizedBox(
                  //   height: 16,
                  // ),
                  for (Network network in networkList) ...[
                    RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        style: const TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 14,
                          color: Color(0xFF1E293B),
                        ),
                        children: [
                          TextSpan(
                            text: FlutterI18n.translate(
                                context, 'common.text.center_is_part_of_network'),
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' ${network.filiereAcronym} - ${language == 'fr' ? network.name : network.nameEn}',
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          )
                        ],
                      ),
                    ),
                    if (network.url != null) ...[
                      const SizedBox(
                        height: 16,
                      ),
                      InkWell(
                        onTap: () => launchUrl(Uri.parse(network.url),
                            mode: LaunchMode.externalApplication),
                        child: Text(
                          network.url,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize:
                                Theme.of(context).textTheme.bodyText2.fontSize,
                            fontWeight: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .fontWeight,
                            color: Theme.of(context).primaryColor,
                            height: 1.5,
                          ),
                        ),
                      ),
                    ],
                    if (networkList.indexOf(network) != networkList.length - 1)
                      const SizedBox(
                        height: 24,
                      ),
                  ],
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 16,
          ),
        ],
        if (ernList.isNotEmpty)
          Card(
            elevation: 0,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50,
                    child: SvgPicture.asset(
                      'assets/icons/ern.svg',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  for (Network ern in ernList) ...[
                    RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        style: const TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 14,
                          color: Color(0xFF1E293B),
                        ),
                        children: [
                          TextSpan(
                            text: FlutterI18n.translate(
                                context, 'common.text.center_is_part_of_ern'),
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' ${ern.filiereAcronym} - ${language == 'fr' ? ern.name : ern.nameEn}',
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          )
                        ],
                      ),
                    ),
                    if (ern.url != null) ...[
                      const SizedBox(
                        height: 16,
                      ),
                      InkWell(
                        onTap: () => launchUrl(Uri.parse(ern.url),
                            mode: LaunchMode.externalApplication),
                        child: Text(
                          ern.url,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize:
                                Theme.of(context).textTheme.bodyText2.fontSize,
                            fontWeight: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .fontWeight,
                            color: Theme.of(context).primaryColor,
                            height: 1.5,
                          ),
                        ),
                      ),
                    ],
                    if (ernList.indexOf(ern) != ernList.length - 1)
                      const SizedBox(
                        height: 24,
                      ),
                  ],
                ],
              ),
            ),
          ),
      ],
    );
  }
}
