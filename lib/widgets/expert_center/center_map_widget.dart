import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CenterMapWidget extends StatefulWidget {
  final LatLng coordinates;

  const CenterMapWidget({Key key, @required this.coordinates})
      : super(key: key);

  @override
  _CenterMapWidgetState createState() => _CenterMapWidgetState();
}

class _CenterMapWidgetState extends State<CenterMapWidget> {
  CameraPosition _kGooglePlex;

  @override
  Widget build(BuildContext context) {
    _kGooglePlex = CameraPosition(target: widget.coordinates, zoom: 14);
    bool _isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return SizedBox(
      width: double.infinity,
      height: _isPortrait
          ? (MediaQuery.of(context).size.width -
              MediaQuery.of(context).padding.horizontal) / 2.2
          : (MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.vertical) /
              1.5,
      child: FutureBuilder(
        future: getBytesFromAsset('assets/markers/map-pin-unselected.png', 114),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              child: AbsorbPointer(
                child: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: _kGooglePlex,
                  zoomControlsEnabled: false,
                  myLocationEnabled: false,
                  myLocationButtonEnabled: false,
                  markers: {
                    Marker(
                      position: widget.coordinates,
                      markerId: MarkerId(widget.coordinates.toString()),
                      icon: BitmapDescriptor.fromBytes(snapshot.data),
                    )
                  },
                ),
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(
                color: Theme.of(context).primaryColor,
              ),
            );
          }
        },
      ),
    );
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png))
        .buffer
        .asUint8List();
  }
}
