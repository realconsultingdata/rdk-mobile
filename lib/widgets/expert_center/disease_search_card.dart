import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

class DiseaseSearchCard extends StatefulWidget {
  final TextEditingController controller;
  final Function handleSearch;

  const DiseaseSearchCard(
      {Key key, @required this.controller, @required this.handleSearch})
      : super(key: key);

  @override
  _DiseaseSearchCardState createState() => _DiseaseSearchCardState();
}

class _DiseaseSearchCardState extends State<DiseaseSearchCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: const Color.fromRGBO(37, 99, 235, 0.1),
      margin: const EdgeInsets.only(bottom: 1),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        width: double.infinity,
        child: Container(
          height: 50,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: const Color(0xFFDEDFE9),
              style: BorderStyle.solid,
              width: 1,
            ),
          ),
          child: TextField(
            controller: widget.controller,
            onChanged: (_) => widget.handleSearch(),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
              focusedBorder: InputBorder.none,
              prefixIcon: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 24,
                color: Color(0xFFCDCEDE),
              ),
              hintText: FlutterI18n.translate(
                  context, 'common.placeholders.search_here'),
              hintStyle: const TextStyle(
                color: Color(0xFFADAFCA),
                fontSize: 15,
                fontWeight: FontWeight.w600,
              ),
              border: InputBorder.none,
              suffixIcon: widget.controller.text.isNotEmpty
                  ? IconButton(
                      onPressed: () {
                        widget.controller.clear();
                        widget.handleSearch();
                      },
                      padding: const EdgeInsets.only(left: 15),
                      icon: const Icon(
                        PhosphorIcons.x,
                        size: 24,
                        color: Color(0xFF1E293B),
                      ))
                  : const SizedBox.shrink(),
              // : IconButton(
              //     onPressed: () {
              //       print('Micro pressed');
              //     },
              //     padding: const EdgeInsets.only(left: 15),
              //     icon: const Icon(
              //       PhosphorIcons.microphone,
              //       size: 24,
              //       color: Color(0xFF1E293B),
              //     ),
              //   ),
            ),
          ),
        ),
      ),
    );
  }
}
