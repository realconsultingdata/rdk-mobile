import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_synonym.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class FicheDiseaseCard extends StatelessWidget {
  const FicheDiseaseCard({
    Key key,
    @required Disease disease,
  })  : _disease = disease,
        super(key: key);

  final Disease _disease;

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;

    DiseaseTranslation _translation = _disease.translations.firstWhere(
        (translation) => translation.language == language,
        orElse: () => null);

    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (_disease.disorderGroup != 'Disorder') ...[
              Container(
                padding: _disease.disorderGroup == 'Group of disorders'
                    ? const EdgeInsets.symmetric(vertical: 4, horizontal: 8)
                    : const EdgeInsets.symmetric(vertical: 4),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  color: _disease.disorderGroup == 'Group of disorders'
                      ? const Color(0xFF6C5CE3).withOpacity(0.1)
                      : Colors.transparent,
                ),
                child: Text(
                  language != 'en' && _translation != null
                      ? _translation.disorderGroup
                      : _disease.disorderGroup,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: _disease.disorderGroup == 'Group of disorders'
                          ? const Color(0xFF4533C9)
                          : const Color(0xFFADAFCA),
                      height: 1.2),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
            ],
            Text(
              language != 'en' && _translation != null
                  ? _translation.name
                  : _disease.name,
              style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline3.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.2),
            ),
            if ((_disease.contents != null && _disease.contents != '')) ...[
              const SizedBox(
                height: 32,
              ),
              Text(
                language != 'en' &&
                        _translation != null &&
                        _translation.contents != null &&
                        _translation.contents != ''
                    ? _translation.contents
                    : _disease.contents,
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                  fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.5,
                ),
              ),
            ],
            if (_disease.synonyms.isNotEmpty) ...[
              const SizedBox(
                height: 32,
              ),
              Wrap(
                spacing: 5,
                runSpacing: -10,
                children: _disease.synonyms
                    .where((DiseaseSynonym synonym) =>
                        synonym.language == language)
                    .map<Widget>(
                      (item) => Chip(
                        key: ValueKey(item),
                        label: Text(item.synonym),
                        labelStyle: TextStyle(
                          fontSize:
                              Theme.of(context).textTheme.bodyText2.fontSize,
                          fontWeight:
                              Theme.of(context).textTheme.bodyText2.fontWeight,
                          color: const Color(0xFF1E293B),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                        backgroundColor:
                            const Color(0xFF8F91AC).withOpacity(0.1),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ],
            const SizedBox(
              height: 24,
            ),
            Text(
              'ORPHA: ' + _disease.orphaNumber,
              style: const TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF8487AC),
                  height: 1.2),
            ),
          ],
        ),
      ),
    );
  }
}
