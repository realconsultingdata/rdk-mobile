import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class DiseaseCard extends StatelessWidget {
  final Disease disease;
  final num scoring;
  final String parentWidget;

  const DiseaseCard(
      {Key key, @required this.disease, this.scoring, this.parentWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    DiseaseTranslation translation = disease.translations.firstWhere(
        (translation) => translation.language == language,
        orElse: () => null);
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              language == 'fr' && translation != null
                  ? translation.name
                  : disease.name,
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
            if (parentWidget == 'gene_disease_tab_view') ...[
              const SizedBox(
                height: 16,
              ),
              RichText(
                text: TextSpan(
                    style: Theme.of(context).textTheme.headline5,
                    children: [
                      TextSpan(
                          text:
                              '${FlutterI18n.translate(context, 'models.disease.orpha_code')}: ',
                          style: const TextStyle(
                            color: Color(0xFF8F91AC),
                            height: 1.5,
                          )),
                      TextSpan(
                          text: disease.orphaNumber,
                          style: const TextStyle(
                            color: Color(0xFF1E293B),
                            height: 1.5,
                          ))
                    ]),
              ),
            ],
            if (parentWidget == 'diagnostic_screen' && scoring > 0) ...[
              const SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                    decoration: BoxDecoration(
                      color: const Color(0xFF8F91AC).withOpacity(0.1),
                      borderRadius: const BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Text(
                      '${scoring.toString()}%',
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1E293B),
                        height: 1.2,
                      ),
                    ),
                  ),
                  if (disease.phenotypes
                      .any((p) => p.diagnostic == 'Pathognomonic sign'))
                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 2),
                      decoration: BoxDecoration(
                        color: const Color(0xFFA17DEF).withOpacity(0.1),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(4)),
                      ),
                      child: Text(
                        language == 'fr' ? 'SP' : 'PS',
                        style: const TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFFA17DEF),
                          height: 1.3,
                        ),
                      ),
                    ),
                  if (disease.phenotypes
                      .any((p) => p.diagnostic == 'Diagnostic criterion'))
                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 2),
                      decoration: BoxDecoration(
                        color: const Color(0xFFF55A8D).withOpacity(0.1),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(4)),
                      ),
                      child: Text(
                        language == 'fr' ? 'CD' : 'DC',
                        style: const TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFFF55A8D),
                          height: 1.3,
                        ),
                      ),
                    ),
                ],
              ),
            ],
          ],
        ),
      ),
    );
  }
}
