import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:rdk_mobile/models/gene.dart';

import '../general_card.dart';

class GeneInformationTabView extends StatelessWidget {
  final Gene loadedGene;

  GeneInformationTabView({@required this.loadedGene});

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            loadedGene.name,
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.headline4.fontSize,
              fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
              color: const Color(0xFF1E293B),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, right: 15, bottom: 30, left: 15),
          child: Text(
            loadedGene.symbol,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.italic,
              color: Color(0xFF64748B),
            ),
          ),
        ),
        GeneralCard(
          key: const ValueKey('description_gene'),
          title: FlutterI18n.translate(context, 'common.text.description'),
          synonyms: loadedGene.synonyms,
          // geneLocus: loadedGene.locus,
        ),
      ],
    );
  }
}
