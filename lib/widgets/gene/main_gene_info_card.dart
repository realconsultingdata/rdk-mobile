import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:rdk_mobile/models/gene.dart';

class MainGeneInfoCard extends StatefulWidget {
  final Gene loadedGene;

  MainGeneInfoCard(this.loadedGene, {Key key}) : super(key: key);

  @override
  _MainGeneInfoCardState createState() => _MainGeneInfoCardState();
}

class _MainGeneInfoCardState extends State<MainGeneInfoCard> {
  // var _isBookmark = false;
  // var _isPdfDownload = false;
  // var _isMessage = false;
  // var _isShare = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 5,
              children: [
                Text(
                  widget.loadedGene.name,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: Color(0xFF1E293B),
                  ),
                ),
                Chip(
                  label: Text(
                    widget.loadedGene.symbol,
                    style: const TextStyle(
                      fontFamily: 'Inter',
                      fontSize: 14,
                      fontStyle: FontStyle.italic,
                      color: Color(0xFF1E293B),
                    ),
                  ),
                  labelPadding:
                      const EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                  padding:
                      const EdgeInsets.symmetric(vertical: 0, horizontal: 4),
                  backgroundColor: const Color(0xFFF5F5F9),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                ),
              ],
            ),
            // Text(
            //   widget.loadedGene.name,
            //   style: const TextStyle(
            //     fontSize: 20,
            //     fontWeight: FontWeight.w700,
            //     color: Color(0xFF1E293B),
            //   ),
            // ),
            Text(
              '${widget.loadedGene.diseaseCount.toString()} ${FlutterI18n.plural(context, 'models.disease.title', widget.loadedGene.diseaseCount).toLowerCase()}',
              style: const TextStyle(
                fontFamily: 'Inter',
                fontSize: 12,
                color: Color(0xFF8F91AC),
                height: 1.5,
              ),
            ),
            // const SizedBox(
            //   height: 20,
            // ),
            // Text(
            //   FlutterI18n.translate(context, 'models.gene.symbol'),
            //   style: TextStyle(
            //     fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
            //     fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
            //     color: const Color(0xFF1E293B),
            //     height: 1.2,
            //   ),
            // ),
            // Container(
            //   child: Text(
            //     widget.loadedGene.symbol,
            //     style: const TextStyle(
            //       fontFamily: 'Inter',
            //       fontSize: 16,
            //       fontWeight: FontWeight.w500,
            //       fontStyle: FontStyle.italic,
            //       color: Color(0xFF1E293B),
            //     ),
            //   ),
            //   padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 4),
            //   margin: const EdgeInsets.only(top: 8),
            //   decoration: const BoxDecoration(
            //     borderRadius: BorderRadius.all(
            //       Radius.circular(4),
            //     ),
            //     color: Color(0xFFF5F5F9),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(top: 20),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: [
            //       IconButton(
            //           onPressed: () {
            //             setState(() {
            //               _isBookmark = !_isBookmark;
            //             });
            //           },
            //           icon: _isBookmark
            //               ? Icon(
            //                   PhosphorIcons.bookmarkSimpleFill,
            //                   size: 30,
            //                   color: Theme.of(context).primaryColor,
            //                 )
            //               : const Icon(
            //                   PhosphorIcons.bookmarkSimple,
            //                   size: 30,
            //                   color: Color(0xFFCDCEDE),
            //                 )),
            //       IconButton(
            //           onPressed: () {
            //             setState(() {
            //               _isPdfDownload = !_isPdfDownload;
            //             });
            //           },
            //           icon: _isPdfDownload
            //               ? Icon(
            //                   PhosphorIcons.filePdfFill,
            //                   size: 30,
            //                   color: Theme.of(context).primaryColor,
            //                 )
            //               : const Icon(
            //                   PhosphorIcons.filePdf,
            //                   size: 30,
            //                   color: Color(0xFFCDCEDE),
            //                 )),
            //       IconButton(
            //           onPressed: () {
            //             setState(() {
            //               _isMessage = !_isMessage;
            //             });
            //           },
            //           icon: _isMessage
            //               ? Icon(
            //                   PhosphorIcons.chatCircleTextFill,
            //                   size: 30,
            //                   color: Theme.of(context).primaryColor,
            //                 )
            //               : const Icon(
            //                   PhosphorIcons.chatCircleText,
            //                   size: 30,
            //                   color: Color(0xFFCDCEDE),
            //                 )),
            //       IconButton(
            //         onPressed: () {
            //           setState(() {
            //             _isShare = !_isShare;
            //           });
            //         },
            //         icon: _isShare
            //             ? Icon(
            //                 PhosphorIcons.shareNetworkFill,
            //                 size: 30,
            //                 color: Theme.of(context).primaryColor,
            //               )
            //             : const Icon(
            //                 PhosphorIcons.shareNetwork,
            //                 size: 30,
            //                 color: Color(0xFFCDCEDE),
            //               ),
            //       ),
            //     ],
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
