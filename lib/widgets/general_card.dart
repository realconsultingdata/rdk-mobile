import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease.dart';
import 'package:rdk_mobile/models/establishment.dart';
import 'package:rdk_mobile/models/natural_history.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/map_widget.dart';

class GeneralCard extends StatelessWidget {
  final String descriptionContents;
  final String title;
  final String id;
  final List<dynamic> synonyms;
  final Map<String, List<String>> diseaseRelations;
  final List<NaturalHistory> histories;
  final List<Disease> parents;
  final String hpoTermTranslationSource;
  final String contentsTranslationSource;
  final String synonymTranslationSource;
  final TabController tabController;
  final TextEditingController expCenterSearchController;

  // final String geneLocus;

  // final DiseasePnds diseasePnds;
  final List<Establishment> establishments;

  const GeneralCard({
    Key key,
    @required this.title,
    this.id,
    this.descriptionContents,
    this.synonyms,
    this.diseaseRelations,
    this.histories,
    this.parents,
    // this.geneLocus,
    // this.diseasePnds,
    this.establishments,
    this.hpoTermTranslationSource,
    this.contentsTranslationSource,
    this.synonymTranslationSource,
    this.tabController,
    this.expCenterSearchController,
  }) : super(key: key);

  List<Widget> buildCardTitle(BuildContext context, String language) {
    return [
      language != 'en' && key.toString().contains('description_phenotype')
          ? RichText(
              textScaleFactor: MediaQuery.of(context).textScaleFactor,
              text: TextSpan(
                style: TextStyle(
                    fontFamily: 'Inter',
                    fontWeight:
                        Theme.of(context).textTheme.headline4.fontWeight,
                    color: const Color(0xFF1E293B)),
                children: [
                  TextSpan(
                    text: title,
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.headline4.fontSize),
                  ),
                  if (language == 'fr' && hpoTermTranslationSource == 'DeepL')
                    const WidgetSpan(
                      alignment: PlaceholderAlignment.top,
                      child: Icon(
                        PhosphorIcons.asteriskSimple,
                        size: 10,
                        color: Color(0xFF006394),
                      ),
                    ),
                ],
              ),
            )
          : Text(
              title,
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
      // if (key.toString().contains('description_phenotype') || key.toString().contains('description_gene')) ...[
      //   const SizedBox(
      //     height: 4,
      //   ),
      //   Text(
      //     id ?? '',
      //     style: TextStyle(
      //       fontStyle: key.toString().contains('description_gene')
      //           ? FontStyle.italic
      //           : FontStyle.normal,
      //       fontSize: 12,
      //       fontWeight: FontWeight.w400,
      //       color: const Color(0xFF8487AC),
      //     ),
      //   )
      // ]
    ];
  }

  Widget buildDescriptionContent(BuildContext context, String language) {
    if (descriptionContents == null || descriptionContents == '') {
      return I18nText(
        'common.text.no_model_available',
        translationParams: {
          'model': FlutterI18n.translate(context, 'common.text.description')
              .toLowerCase()
        },
        child: const Text(
          '',
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: Color(0xFF1E293B),
            height: 2,
          ),
        ),
      );
    } else {
      if (language != 'en' &&
          key.toString().contains('description_phenotype')) {
        return Text.rich(
          TextSpan(children: [
            TextSpan(
              text: descriptionContents,
            ),
            if (language == 'fr' && contentsTranslationSource == 'DeepL')
              const WidgetSpan(
                alignment: PlaceholderAlignment.middle,
                child: Icon(
                  PhosphorIcons.asteriskSimple,
                  size: 10,
                  color: Color(0xFF006394),
                ),
              ),
          ]),
          textScaleFactor: MediaQuery.of(context).textScaleFactor,
          style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color(0xFF1E293B),
            height: 1.5,
          ),
        );
      }
      return Html(
        data: descriptionContents,
        style: {
          "body": Style(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            fontSize: const FontSize(14),
            fontWeight: FontWeight.w400,
            color: const Color(0xFF1E293B),
            lineHeight: const LineHeight(1.5),
          )
        },
      );
    }
  }

  List<Widget> buildSynonymContent(BuildContext context, String language) {
    return [
      if (!key.toString().contains('description_gene'))
        const SizedBox(
          height: 32,
        ),
      Text(
        FlutterI18n.plural(context, 'models.synonym.title', 2),
        style: TextStyle(
          fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
          color: const Color(0xFF1E293B),
          height: 1.15,
        ),
      ),
      const SizedBox(
        height: 8,
      ),
      if (synonyms
          .where((item) => item.synonym != null && item.synonym != '')
          .isEmpty)
        I18nText(
          'common.text.no_model_available',
          translationParams: {
            'model': FlutterI18n.plural(context, 'models.synonym.title', 0)
                .toLowerCase()
          },
          child: const Text(
            '',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Color(0xFF1E293B),
              height: 2,
            ),
          ),
        )
      else
        Wrap(spacing: 5, runSpacing: 5, children: [
          ...synonyms
              .map((item) => Chip(
                    key: ValueKey(item),
                    label: Text(item.synonym),
                    labelStyle: TextStyle(
                      fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                      fontWeight:
                          Theme.of(context).textTheme.bodyText2.fontWeight,
                      color: const Color(0xFF1E293B),
                    ),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                    backgroundColor: const Color(0xFFF5F5F9),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8),
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ))
              .toList(),
          if (language == 'fr' &&
              key.toString().contains('description_phenotype') &&
              synonymTranslationSource == 'DeepL')
            const Icon(
              PhosphorIcons.asteriskSimple,
              size: 10,
              color: Color(0xFF006394),
            ),
        ])
    ];
  }

  // List<Widget> buildLocusContent(BuildContext context) {
  //   return [
  //     Text(
  //       FlutterI18n.translate(context, 'common.text.gene_location'),
  //       style: TextStyle(
  //         fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
  //         fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
  //         color: const Color(0xFF1E293B),
  //         height: 1.15,
  //       ),
  //     ),
  //     if (geneLocus.isEmpty)
  //       I18nText(
  //         'common.text.no_model_available',
  //         translationParams: {
  //           'model': FlutterI18n.translate(context, 'common.text.gene_location')
  //               .toLowerCase()
  //         },
  //         child: const Text(
  //           '',
  //           style: TextStyle(
  //             fontSize: 12,
  //             fontWeight: FontWeight.w400,
  //             color: Color(0xFF1E293B),
  //             height: 2,
  //           ),
  //         ),
  //       )
  //     else
  //       Chip(
  //         key: ValueKey(geneLocus),
  //         label: Text(geneLocus),
  //         labelStyle: TextStyle(
  //           fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
  //           fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
  //           color: const Color(0xFF1E293B),
  //         ),
  //         labelPadding: const EdgeInsets.symmetric(horizontal: 4),
  //         backgroundColor: const Color(0xFFF5F5F9),
  //         shape: const RoundedRectangleBorder(
  //           borderRadius: BorderRadius.all(
  //             Radius.circular(8),
  //           ),
  //         ),
  //       ),
  //   ];
  // }

  List<Widget> buildDescriptionCard(BuildContext context, String language) {
    return [
      if (!key.toString().contains('gene')) ...[
        Text(
          FlutterI18n.translate(context, 'common.text.definition'),
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
            fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
            color: const Color(0xFF1E293B),
            height: 1.15,
          ),
        ),
        const SizedBox(
          height: 4,
        ),
        buildDescriptionContent(context, language),
      ],
      ...buildSynonymContent(context, language),
      // if (key.toString().contains('gene')) ...[
      //   const SizedBox(
      //     height: 32,
      //   ),
      //   ...buildLocusContent(context),
      // ]
    ];
  }

  List<Widget> buildRelationCard(BuildContext context) {
    return [
      Text(
        FlutterI18n.translate(context, 'common.text.terminology_state'),
        style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Color(0xFF1E293B)),
      ),
      const SizedBox(
        height: 24,
      ),
      ...diseaseRelations.entries
          .map((relation) {
            List<Widget> contentWidgets = [];
            contentWidgets.addAll([
              Text(
                relation.key,
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                  fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
                  color: const Color(0xFF1E293B),
                  height: 1.15,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Wrap(
                spacing: 5,
                runSpacing: 5,
                children: relation.value
                    .toSet()
                    .toList()
                    .map((item) => Chip(
                          key: ValueKey(item),
                          label: Text(item),
                          labelStyle: TextStyle(
                            fontSize:
                                Theme.of(context).textTheme.bodyText2.fontSize,
                            fontWeight: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .fontWeight,
                            color: const Color(0xFF1E293B),
                          ),
                          labelPadding:
                              const EdgeInsets.symmetric(horizontal: 4),
                          backgroundColor: const Color(0xFFF5F5F9),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                          ),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                        ))
                    .toList(),
              ),
              if (relation.key == 'Code ORPHA' &&
                  diseaseRelations.keys
                      .where((key) => key != 'Code ORPHA')
                      .isNotEmpty) ...[
                const SizedBox(
                  height: 24,
                ),
                Text(
                  FlutterI18n.translate(
                      context, 'models.terminology.other_terminologies'),
                  style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      color: Color(0xFF1E293B)),
                ),
                const SizedBox(
                  height: 4,
                ),
              ],
              if (diseaseRelations.entries.last.key != relation.key)
                const SizedBox(
                  height: 16,
                )
            ]);
            return contentWidgets;
          })
          .expand((element) => element)
          .toList()
    ];
  }

  List<Widget> buildApparitionCard(BuildContext context) {
    Set uniqueInheritanceList = <String>{};
    Set uniqueAgeList = <String>{};
    for (NaturalHistory history in histories) {
      if (history.inheritanceType != null) {
        uniqueInheritanceList.add(history.inheritanceType);
      }
      if (history.avgAgeOnset != null) {
        uniqueAgeList.add(history.avgAgeOnset);
      }
    }
    return [
      if (uniqueInheritanceList.isNotEmpty) ...[
        const SizedBox(
          height: 32,
        ),
        Text(
          FlutterI18n.translate(context, 'models.nh_heredity.title'),
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
            fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
            color: const Color(0xFF1E293B),
            height: 1.15,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: uniqueInheritanceList
              .map((item) => Chip(
                    key: ValueKey(item),
                    label: Text(FlutterI18n.translate(
                        context,
                        'models.nh_heredity.value.' +
                            item.replaceAll(' ', '_').toLowerCase())),
                    labelStyle: TextStyle(
                      fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                      fontWeight:
                          Theme.of(context).textTheme.bodyText2.fontWeight,
                      color: const Color(0xFF1E293B),
                    ),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                    backgroundColor: const Color(0xFFF5F5F9),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8),
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ))
              .toList(),
        ),
      ],
      if (uniqueAgeList.isNotEmpty) ...[
        const SizedBox(
          height: 32,
        ),
        Text(
          FlutterI18n.translate(context, 'models.nh_age_apparition.title'),
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
            fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
            color: const Color(0xFF1E293B),
            height: 1.15,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Wrap(
          spacing: 5,
          runSpacing: 5,
          children: uniqueAgeList
              .map((item) => Chip(
                    key: ValueKey(item),
                    label: Text(FlutterI18n.translate(
                        context,
                        'models.nh_age_apparition.value.' +
                            item.replaceAll(' ', '_').toLowerCase())),
                    labelStyle: TextStyle(
                      fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                      fontWeight:
                          Theme.of(context).textTheme.bodyText2.fontWeight,
                      color: const Color(0xFF1E293B),
                    ),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                    backgroundColor: const Color(0xFFF5F5F9),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8),
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ))
              .toList(),
        ),
      ]
    ];
  }

  List<Widget> buildPreferentialParentContent(
      BuildContext context, String language) {
    return [
      Text(
        FlutterI18n.plural(
            context, 'models.disease.primary_categories.title', parents.length),
        style: TextStyle(
          fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
          color: const Color(0xFF1E293B),
          height: 1.15,
        ),
      ),
      const SizedBox(
        height: 8,
      ),
      // if (parents.isEmpty)
      //   I18nText(
      //     'common.text.no_model_available',
      //     translationParams: {
      //       'model': FlutterI18n.plural(
      //               context, 'models.disease.primary_categories.title', 0)
      //           .toLowerCase()
      //     },
      //     child: const Text(
      //       '',
      //       style: TextStyle(
      //         fontSize: 12,
      //         fontWeight: FontWeight.w400,
      //         color: Color(0xFF1E293B),
      //         height: 2,
      //       ),
      //     ),
      //   )
      // else
      Wrap(
        spacing: 5,
        runSpacing: 5,
        children: parents
            .map((item) => Chip(
                  key: ValueKey(item),
                  label: Text(language == 'en'
                      ? item.name
                      : item.translations
                          .firstWhere((e) => e.language == language)
                          .name),
                  labelStyle: TextStyle(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.bodyText2.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                  labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                  backgroundColor: const Color(0xFFF5F5F9),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ))
            .toList(),
      )
    ];
  }

  // List<Widget> buildPndsCard(BuildContext context, String language) {
  //   return [
  //     Text(
  //       '${FlutterI18n.translate(context, 'common.text.updated')} - ${DateFormat("d MMMM yyyy", language).format(DateTime.parse(diseasePnds.minValidationDate))}',
  //       style: const TextStyle(
  //         fontSize: 10,
  //         fontWeight: FontWeight.w400,
  //         fontStyle: FontStyle.italic,
  //         color: Color(0xFF1E293B),
  //         height: 1.5,
  //       ),
  //     ),
  //     const SizedBox(
  //       height: 16,
  //     ),
  //     Text(
  //       FlutterI18n.translate(context, 'common.text.pnds_description'),
  //       style: TextStyle(
  //           fontSize: Theme.of(context).textTheme.bodyText2.fontSize,
  //           fontWeight: Theme.of(context).textTheme.bodyText2.fontWeight,
  //           height: 1.5,
  //           color: const Color(0xFF1E293B)),
  //     ),
  //     ...diseasePnds.documents.map(
  //       (document) => Padding(
  //         padding: const EdgeInsets.symmetric(vertical: 8),
  //         child: ListTile(
  //           tileColor: const Color(0xFFF9F9FA),
  //           shape: RoundedRectangleBorder(
  //             side: const BorderSide(color: Color(0xFFEAEAF5), width: 1.5),
  //             borderRadius: BorderRadius.circular(10),
  //           ),
  //           horizontalTitleGap: 0,
  //           // contentPadding: const EdgeInsets.all(0),
  //           leading: const Icon(
  //             PhosphorIcons.filePdfBold,
  //             color: Color(0xFFD93221),
  //             size: 24,
  //           ),
  //           title: Text(
  //             document.name.split(' - ').last,
  //             style: const TextStyle(
  //               fontSize: 14,
  //               fontWeight: FontWeight.w500,
  //               height: 1.15,
  //               color: Color(0xFF1E293B),
  //             ),
  //             overflow: TextOverflow.ellipsis,
  //           ),
  //           onTap: () {
  //             // Navigator.of(context).pushNamed(
  //             //   DiseasePndsViewer.routeName,
  //             //   arguments: document,
  //             // );
  //             launch(document.hyperLink);
  //           },
  //         ),
  //       ),
  //     ),
  //   ];
  // }

  Widget buildExpertCenterMapCard(BuildContext context) {
    return MapWidget(
      orphaNumber: diseaseRelations['Code ORPHA'].first,
      establishments: establishments
          .where((element) =>
              element != null &&
              element.latitude != null &&
              element.longitude != null)
          .toList(),
      padding: const EdgeInsets.all(0),
      height: MediaQuery.of(context).size.width * 0.8,
      zoom: 5,
      zoomGesturesEnabled: true,
      scrollGesturesEnabled: true,
      tiltGesturesEnabled: false,
      rotateGesturesEnabled: false,
      enableMarkerPopup: true,
      enableMoreInfoBtn: true,
      onPopupClick: (String expCenterName) {
        Navigator.of(context).pop();
        tabController.animateTo(3);
        expCenterSearchController.text = expCenterName;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ...buildCardTitle(context, language),
            const SizedBox(
              height: 32,
            ),
            if (key.toString().contains('description')) ...[
              ...buildDescriptionCard(context, language),
              if (key.toString().contains('description_disease')) ...[
                if (histories.isNotEmpty) ...[
                  ...buildApparitionCard(context),
                ],
                if (parents.isNotEmpty) ...[
                  const SizedBox(
                    height: 32,
                  ),
                  ...buildPreferentialParentContent(context, language)
                ]
              ]
            ]
            // else if (key.toString().contains('relation'))
            //   ...[
            //     Text(
            //       title,
            //       style: TextStyle(
            //         fontSize: Theme.of(context).textTheme.headline4.fontSize,
            //         fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
            //         color: const Color(0xFF1E293B),
            //       ),
            //     )
            //   ]
            // else
            // ...diseaseRelations.entries
            //     .map((relation) => buildRelationCard(context, relation))
            //     .toList(),
            else if (key.toString().contains('relation_disease'))
              ...buildRelationCard(context)
            // else if (key.toString().contains('apparition_disease'))
            //   ...buildApparitionCard(context)
            // else if (key.toString().contains('pnds'))
            //   ...buildPndsCard(context, language)
            else if (key.toString().contains('exp_center_map'))
              buildExpertCenterMapCard(context)
          ],
        ),
      ),
    );
  }
}
