import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_treeview/flutter_treeview.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_hierarchy.dart';
import 'package:rdk_mobile/models/phenotype_hierarchy.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/disease_info_screen.dart';
import 'package:rdk_mobile/screens/phenotype_info_screen.dart';

class HierarchyCard extends StatefulWidget {
  final List<Node> hierarchyData;
  final String currentInfoId;

  const HierarchyCard(
      {Key key, @required this.hierarchyData, @required this.currentInfoId})
      : super(key: key);

  @override
  _HierarchyCardState createState() => _HierarchyCardState();
}

class _HierarchyCardState extends State<HierarchyCard> {
  TreeViewTheme _treeViewTheme;
  List<TreeViewController> _treeViewControllers;
  Locale currentLocale;
  List<Map> _expansionState = [];

  void _expandNode(int controllerIndex, String key, bool expanded) {
    // Store expansion state of each node
    Map expansionStateExist = _expansionState.firstWhere(
        (element) =>
            element['clickedControllerIndex'] == controllerIndex &&
            element['clickedNodeKey'] == key,
        orElse: () => null);
    if (expansionStateExist != null) {
      int index = _expansionState.indexOf(expansionStateExist);
      expansionStateExist['clickedNodeExpansion'] = expanded;
      _expansionState[index] = expansionStateExist;
    } else {
      _expansionState.add({
        'clickedControllerIndex': controllerIndex,
        'clickedNodeKey': key,
        'clickedNodeExpansion': expanded
      });
    }
    Node node = _treeViewControllers[controllerIndex].getNode(key);
    if (node != null) {
      List<Node> updated;
      updated = _treeViewControllers[controllerIndex]
          .updateNode(key, node.copyWith(expanded: expanded));
      setState(() {
        _treeViewControllers[controllerIndex] =
            _treeViewControllers[controllerIndex].copyWith(children: updated);
      });
    }
  }

  void _mergeTree(List<Node> hierarchyTree) {
    var toBeRemoved = [];
    for (var tree1 in hierarchyTree) {
      if (toBeRemoved.firstWhere((rmElement) => identical(tree1, rmElement),
              orElse: () => null) ==
          null) {
        // var currTree1Index = hierarchyTree.indexOf(tree1);
        for (var tree2 in hierarchyTree) {
          if (toBeRemoved.firstWhere((rmElement) => identical(tree2, rmElement),
                  orElse: () => null) ==
              null) {
            // var currTree2Index = hierarchyTree.indexOf(tree2);
            if (identical(tree1, tree2)) {
              continue;
            } else {
              if (tree1.key == tree2.key) {
                // List<Node> tree2children = List.of(tree2.children);
                tree1.children.addAll(tree2.children);
                toBeRemoved.add(tree2);
                // hierarchyTree.remove(tree2);
              }
            }
          }
        }
        if (tree1.children.length > 1) {
          _mergeTree(tree1.children);
        }
      }
    }
    for (var rmElement in toBeRemoved) {
      hierarchyTree.removeWhere((element) => identical(element, rmElement));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.hierarchyData.length > 1) {
      _mergeTree(widget.hierarchyData);
    }
    if (currentLocale != Provider.of<CurrentLocale>(context).currentLocale()) {
      currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
      _treeViewControllers = [];
      _treeViewControllers.addAll(widget.hierarchyData
          .map((data) => TreeViewController(children: [data]))
          .toList());
      // Set node expansion state after translation
      if (_expansionState.isNotEmpty) {
        for (Map state in _expansionState) {
          _expandNode(state['clickedControllerIndex'], state['clickedNodeKey'],
              state['clickedNodeExpansion']);
        }
      }
    }
    _treeViewTheme = const TreeViewTheme(
      expanderTheme: ExpanderThemeData(
        animated: true,
        type: ExpanderType.chevron,
        modifier: ExpanderModifier.none,
        position: ExpanderPosition.start,
        size: 20,
        color: Color(0xFFCDCEDE),
      ),
      iconTheme: IconThemeData(
        size: 20,
      ),
    );

    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              FlutterI18n.plural(context, 'models.classification.title', 1),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _treeViewControllers.length,
              itemBuilder: (ctx, i) {
                return TreeView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  controller: _treeViewControllers[i],
                  allowParentSelect: false,
                  supportParentDoubleTap: false,
                  onExpansionChanged: (key, expanded) =>
                      _expandNode(i, key, expanded),
                  theme: _treeViewTheme,
                  nodeBuilder: (context, node) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        node.key == widget.currentInfoId
                            ? Container(
                                margin: const EdgeInsets.only(right: 5),
                                child: Icon(
                                  PhosphorIcons.circleFill,
                                  color: Theme.of(context).primaryColor,
                                  size: 5,
                                ),
                              )
                            : const SizedBox.shrink(),
                        Expanded(
                          child: RichText(
                            textScaleFactor:
                                MediaQuery.of(context).textScaleFactor,
                            text: TextSpan(
                              style: const TextStyle(
                                fontFamily: 'Inter',
                              ),
                              children: [
                                TextSpan(
                                  style: TextStyle(
                                    fontSize:
                                        node.data.nodeLevel == 0 ? 16 : 14,
                                    fontWeight: (node.data.nodeLevel == 0 ||
                                            node.key == widget.currentInfoId)
                                        ? FontWeight.w700
                                        : FontWeight.w400,
                                    color: const Color(0xFF1E293B),
                                  ),
                                  text: node.label.split('- ').first,
                                ),
                                // TextSpan(
                                //   style: const TextStyle(
                                //     fontWeight: FontWeight.w700,
                                //     color: Color(0xFFCDCEDE),
                                //   ),
                                //   text: node.label.split(' - ').last,
                                // ),
                              ],
                            ),
                          ),
                        ),
                        node.key != widget.currentInfoId
                            ? IconButton(
                                onPressed: () {
                                  if (node.key != widget.currentInfoId) {
                                    if (_treeViewControllers[i]
                                        .getNode(node.key)
                                        .data is DiseaseHierarchyNode) {
                                      Navigator.of(context).pushNamed(
                                          DiseaseInfoScreen.routeName,
                                          arguments: {
                                            'orphaNumber':
                                                _treeViewControllers[i]
                                                    .getNode(node.key)
                                                    .data
                                                    .disease
                                                    .orphaNumber,
                                            'backBtn': FlutterI18n.translate(
                                                context,
                                                'common.buttons.go_back'),
                                          });
                                    } else if (_treeViewControllers[i]
                                        .getNode(node.key)
                                        .data is PhenotypeHierarchyNode) {
                                      Navigator.of(context).pushNamed(
                                          PhenotypeInfoScreen.routeName,
                                          arguments: _treeViewControllers[i]
                                              .getNode(node.key)
                                              .data
                                              .phenotype
                                              .id);
                                    }
                                  }
                                },
                                icon: Icon(
                                  PhosphorIcons.arrowSquareOutBold,
                                  size: 16,
                                  color: Theme.of(context).primaryColor,
                                ),
                              )
                            : const SizedBox.shrink()
                      ],
                    );
                  },
                );
              },
              separatorBuilder: (ctx, i) => const Divider(
                color: Color(0xFFEAEAF5),
                thickness: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
