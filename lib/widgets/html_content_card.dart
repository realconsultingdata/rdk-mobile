import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class HtmlContentCard extends StatelessWidget {
  const HtmlContentCard({
    Key key,
    @required this.data,
  }) : super(key: key);

  final String data;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.only(
          top: 15,
          left: 15,
          right: 15,
          bottom: MediaQuery.of(context).padding.bottom + 10),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
          padding: const EdgeInsets.all(24),
          width: double.infinity,
          child: Html(
            data: data,
            onLinkTap: (link, ctx, attributes, element) {
              launch(link);
            },
            style: {
              "body": Style(
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                lineHeight: LineHeight(1.5)
              ),
              "a": Style(
                color: Theme.of(context).primaryColor
              ),
            },
          )),
    );
  }
}
