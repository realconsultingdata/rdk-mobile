import 'package:upgrader/upgrader.dart';

class IOSUpdateNotificationMessage extends UpgraderMessages {
  /// Override the message function to provide custom language localization.
  @override
  String message(UpgraderMessage messageKey) {
    if (languageCode == 'en') {
      switch (messageKey) {
        case UpgraderMessage.body:
          return 'A new version of {{appName}} is available! Version {{currentAppStoreVersion}} is now available, you have version {{currentInstalledVersion}}.';
        case UpgraderMessage.buttonTitleIgnore:
          return 'Ignore';
        case UpgraderMessage.buttonTitleLater:
          return 'Later';
        case UpgraderMessage.buttonTitleUpdate:
          return 'Update now';
        case UpgraderMessage.prompt:
          return 'Please update the application to get the latest features.';
        case UpgraderMessage.releaseNotes:
          return 'Release Notes';
        case UpgraderMessage.title:
          return 'Update available';
      }
    } else if (languageCode == 'fr') {
      switch (messageKey) {
        case UpgraderMessage.body:
          return 'Une nouvelle version de {{appName}} est disponible ! La version {{currentAppStoreVersion}} est maintenant disponible, vous avez la version {{currentInstalledVersion}}.';
        case UpgraderMessage.buttonTitleIgnore:
          return 'Ignorer';
        case UpgraderMessage.buttonTitleLater:
          return 'Plus tard';
        case UpgraderMessage.buttonTitleUpdate:
          return 'Installer maintenant';
        case UpgraderMessage.prompt:
          return 'Veuillez mettre à jour l\'application pour obtenir les dernières fonctionnalités.';
        case UpgraderMessage.releaseNotes:
          return 'Notes de version';
        case UpgraderMessage.title:
          return 'Mise à jour disponible';
      }
    }
    // Messages that are not provided above can still use the default values.
    return super.message(messageKey);
  }
}
