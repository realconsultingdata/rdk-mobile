import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';

class LanguageMenu extends StatefulWidget {
  @override
  _LanguageMenuState createState() => _LanguageMenuState();
}

class _LanguageMenuState extends State<LanguageMenu> {
  Locale currentLocale;

  changeLanguage(BuildContext context, String value) {
    currentLocale = Locale(value);
    Provider.of<CurrentLocale>(context, listen: false).setLocale(currentLocale);
  }

  @override
  Widget build(BuildContext context) {
    currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      width: double.infinity,
      height: 290,
      child: Column(
        children: [
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Color(0xFFCDCEDE),
            ),
            margin: const EdgeInsets.only(top: 16, bottom: 32),
            height: 3,
            width: MediaQuery.of(context).size.width / 5,
          ),
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(bottom: 32),
            alignment: Alignment.centerLeft,
            child: Text(
              FlutterI18n.translate(context, 'common.text.select_language'),
              style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF1E293B),
                  height: 1.2),
              textAlign: TextAlign.start,
            ),
          ),
          Container(
              width: double.infinity,
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Material(
                    type: MaterialType.transparency,
                    child: RadioListTile(
                      title: Text(
                        'English',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: language == 'en'
                              ? FontWeight.w500
                              : FontWeight.w400,
                          color: const Color(0xFF1E293B),
                          height: 1.15,
                        ),
                      ),
                      value: 'en',
                      groupValue: language,
                      activeColor: Theme.of(context).primaryColor,
                      tileColor: language == 'en'
                          ? Theme.of(context).primaryColor.withOpacity(0.05)
                          : Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onChanged: (value) {
                        setState(() {
                          language = value;
                          changeLanguage(context, value);
                        });
                      },
                    ),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: RadioListTile(
                      title: Text(
                        'Français',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: language == 'fr'
                              ? FontWeight.w500
                              : FontWeight.w400,
                          color: const Color(0xFF1E293B),
                          height: 1.15,
                        ),
                      ),
                      value: 'fr',
                      groupValue: language,
                      activeColor: Theme.of(context).primaryColor,
                      tileColor: language == 'fr'
                          ? Theme.of(context).primaryColor.withOpacity(0.05)
                          : Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onChanged: (value) {
                        setState(() {
                          language = value;
                          changeLanguage(context, value);
                        });
                      },
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
