import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/svg.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/embedded_webview_screen.dart';
import 'package:rdk_mobile/screens/info_service_screen.dart';
import 'package:rdk_mobile/screens/partner_screen.dart';
import 'package:rdk_mobile/screens/legal_menu_screen.dart';
import 'package:rdk_mobile/screens/sources_screen.dart';
import 'package:rdk_mobile/screens/sponsor_screen.dart';
import 'package:rdk_mobile/screens/user_guide_viewer.dart';
import 'package:url_launcher/url_launcher.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key key}) : super(key: key);

  Widget buildListTile(BuildContext context, String title, IconData icon,
      Widget trailing, String routeName) {
    return ListTile(
      horizontalTitleGap: 0,
      contentPadding: const EdgeInsets.symmetric(horizontal: 24),
      dense: true,
      leading: Icon(
        icon,
        size: 24,
        color: Theme.of(context).primaryColor,
      ),
      title: I18nText(
        title,
        child: Text(
          '',
          textScaleFactor: MediaQuery.of(context).textScaleFactor,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            height: 1.2,
          ),
        ),
      ),
      trailing: trailing,
      onTap: () async {
        // PageController pageController =
        //     Provider.of<BasePageController>(context, listen: false)
        //         .pageController();
        // Navigator.of(context).pop();
        // await Future.delayed(const Duration(milliseconds: 200));
        // if (ModalRoute.of(context).settings.name != BasePageView.routeName) {
        //   pageController.jumpToPage(pageIndex);
        //   Navigator.of(context).popUntil(
        //       (route) => route.settings.name == BasePageView.routeName);
        // } else {
        //   // pageController.animateToPage(pageIndex,
        //   //     curve: Curves.easeInToLinear,
        //   //     duration: const Duration(milliseconds: 300));
        //   pageController.jumpToPage(pageIndex);
        // }
        Navigator.of(context).pushNamed(routeName);
      },
    );
  }

  showErrorDialog(BuildContext context) async {
    if (Platform.isIOS) {
      await showCupertinoDialog(
          context: context,
          builder: (ctx) => CupertinoAlertDialog(
                title: Text(
                  FlutterI18n.translate(
                      context, 'common.errors.an_error_occured'),
                ),
                content: Text(
                  FlutterI18n.translate(
                      context, 'common.errors.something_went_wrong'),
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text(
                        FlutterI18n.translate(context, 'common.buttons.close')),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ));
    } else {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Text(
            FlutterI18n.translate(context, 'common.errors.an_error_occured'),
            style: Theme.of(context).textTheme.headline5,
          ),
          content: Text(
            FlutterI18n.translate(
                context, 'common.errors.something_went_wrong'),
            style: Theme.of(context).textTheme.bodyText2,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                FlutterI18n.translate(context, 'common.buttons.close'),
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String languageCode = currentLocale.languageCode;

    return ClipRRect(
      borderRadius: const BorderRadius.horizontal(
        left: Radius.circular(12),
      ),
      child: Drawer(
        backgroundColor: const Color(0xFFFFFFFF),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              height: 115,
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 24),
              alignment: Alignment.centerLeft,
              color: const Color(0xFFFFFFFF),
              child: Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).padding.top, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset(
                      'assets/icons/rdk-logo-rectangle.svg',
                      fit: BoxFit.fitHeight,
                    ),
                    FutureBuilder(
                      future: Future.wait([PackageInfo.fromPlatform()]),
                      builder:
                          (BuildContext context, AsyncSnapshot<List> snapshot) {
                        if (snapshot.hasData) {
                          var packageInfo = snapshot.data[0];
                          return Text(
                            'v${packageInfo.version}',
                            style: const TextStyle(
                                fontFamily: 'Inter',
                                fontSize: 12,
                                color: Color(0xFF8F91AC)),
                          );
                        } else {
                          return const SizedBox.shrink();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            // Container(
            //   padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
            //   width: double.infinity,
            //   child: FutureBuilder(
            //     future: Future.wait([PackageInfo.fromPlatform()]),
            //     builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            //       if (snapshot.hasData) {
            //         var packageInfo = snapshot.data[0];
            //         return Text(
            //           'v${packageInfo.version}',
            //           textAlign: TextAlign.start,
            //           style: const TextStyle(
            //               fontFamily: 'Inter',
            //               fontSize: 12,
            //               color: Color(0xFF8F91AC)),
            //         );
            //       } else {
            //         return const SizedBox.shrink();
            //       }
            //     },
            //   ),
            // ),
            buildListTile(context, 'pages.sources', PhosphorIcons.book,
                const SizedBox.shrink(), SourcesScreen.routeName),
            buildListTile(context, 'pages.information', PhosphorIcons.info,
                const SizedBox.shrink(), InfoServiceScreen.routeName),
            buildListTile(context, 'pages.user_guide', PhosphorIcons.question,
                const SizedBox.shrink(), UserGuideViewer.routeName),
            const Divider(
              thickness: 1,
              color: Color(0xFFECECF2),
              indent: 16,
              endIndent: 16,
            ),
            buildListTile(
                context,
                'pages.partners',
                PhosphorIcons.circleWavyCheck,
                const SizedBox.shrink(),
                PartnerScreen.routeName),
            buildListTile(context, 'pages.sponsors', PhosphorIcons.briefcase,
                const SizedBox.shrink(), SponsorScreen.routeName),
            const Divider(
              thickness: 1,
              color: Color(0xFFECECF2),
              indent: 16,
              endIndent: 16,
            ),
            // buildListTile(context, 'pages.sponsors', PhosphorIcons.handshake,
            //     SponsorScreen.routeName),
            // ListTile(
            //   horizontalTitleGap: 0,
            //   contentPadding: const EdgeInsets.symmetric(horizontal: 24),
            //   leading: const Icon(
            //     PhosphorIcons.thumbsUpBold,
            //     size: 24,
            //     color: Color(0xFF8F91AC),
            //   ),
            //   title: I18nText(
            //     'common.text.send_your_feedback',
            //     child: Text(
            //       '',
            //       textScaleFactor: MediaQuery.of(context).textScaleFactor,
            //       style: TextStyle(
            //         fontSize: Theme.of(context).textTheme.headline5.fontSize,
            //         fontWeight:
            //             Theme.of(context).textTheme.headline5.fontWeight,
            //         height: 1.15,
            //       ),
            //     ),
            //   ),
            //   onTap: () async {
            //     launch(
            //         'https://docs.google.com/forms/d/e/1FAIpQLScL89DoUHT2Px0x0pgWowgAtdX3kRA19ooyT2KSBTAWNuCuVw/viewform');
            //     Navigator.of(context).pop();
            //   },
            // ),
            buildListTile(
                context,
                'pages.improve_rdk',
                PhosphorIcons.chats,
                Icon(
                  PhosphorIcons.arrowUpRightBold,
                  size: 20,
                  color: Theme.of(context).primaryColor,
                ),
                languageCode == 'fr'
                    ? EmbeddedWebviewScreen.reportBugRouteNameFr
                    : EmbeddedWebviewScreen.reportBugRouteNameEn),
            // buildListTile(
            //     context,
            //     'common.text.request_feature',
            //     PhosphorIcons.circleWavyWarning,
            //     Icon(
            //       PhosphorIcons.arrowUpRightBold,
            //       size: 20,
            //       color: Theme.of(context).primaryColor,
            //     ),
            //     languageCode == 'en'
            //         ? EmbeddedWebviewScreen.requestFeatureRouteNameEn
            //         : EmbeddedWebviewScreen.requestFeatureRouteNameFr),
            buildListTile(
                context,
                'common.text.evaluation',
                PhosphorIcons.star,
                Icon(
                  PhosphorIcons.arrowUpRightBold,
                  size: 20,
                  color: Theme.of(context).primaryColor,
                ),
                EmbeddedWebviewScreen.evaluation),
            const Divider(
              thickness: 1,
              color: Color(0xFFECECF2),
              indent: 16,
              endIndent: 16,
            ),
            buildListTile(context, 'pages.legal', PhosphorIcons.scroll,
                const SizedBox.shrink(), LegalMenuScreen.routeName),
            // buildListTile(context, 'pages.privacy_policy',
            //     PhosphorIcons.briefcaseBold, PrivacyPolicyScreen.routeName),
            // buildListTile(
            //     context, 'pages.contact_numbers', PhosphorIcons.phoneBold, 2),
            const Expanded(child: SizedBox.shrink()),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 24),
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom + 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    FlutterI18n.translate(context, 'common.footer.follow_us'),
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF64748B),
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        icon: Icon(
                          PhosphorIcons.linkedinLogo,
                          size: 32,
                          color: Theme.of(context).primaryColor,
                        ),
                        splashRadius: 25,
                        onPressed: () async {
                          if (!await launchUrl(
                              Uri.parse(
                                  'https://www.linkedin.com/company/asweknow/'),
                              mode: LaunchMode.externalNonBrowserApplication)) {
                            if (!await launchUrl(
                                Uri.parse(
                                    'https://www.linkedin.com/company/asweknow/'),
                                mode: LaunchMode.externalApplication)) {
                              showErrorDialog(context);
                            }
                          }
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          PhosphorIcons.facebookLogo,
                          size: 32,
                          color: Theme.of(context).primaryColor,
                        ),
                        splashRadius: 25,
                        onPressed: () async {
                          if (!await launchUrl(
                              Uri.parse(
                                  'https://www.facebook.com/profile.php?id=61553497340777'),
                              mode: LaunchMode.externalNonBrowserApplication)) {
                            if (!await launchUrl(
                                Uri.parse(
                                    'https://www.facebook.com/profile.php?id=61553497340777'),
                                mode: LaunchMode.externalApplication)) {
                              showErrorDialog(context);
                            }
                          }
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          PhosphorIcons.instagramLogo,
                          size: 32,
                          color: Theme.of(context).primaryColor,
                        ),
                        splashRadius: 25,
                        onPressed: () async {
                          if (!await launchUrl(
                              Uri.parse(
                                  'https://www.instagram.com/rarediseaseknowledge'),
                              mode: LaunchMode.externalNonBrowserApplication)) {
                            if (!await launchUrl(
                                Uri.parse(
                                    'https://www.instagram.com/rarediseaseknowledge'),
                                mode: LaunchMode.externalApplication)) {
                              showErrorDialog(context);
                            }
                          }
                        },
                      ),
                    ],
                  )
                ],
              ),
            )
            // Container(
            //   width: double.infinity,
            //   margin: EdgeInsets.only(
            //       bottom: MediaQuery.of(context).padding.bottom),
            //   padding: const EdgeInsets.symmetric(horizontal: 16),
            //   alignment: Alignment.centerLeft,
            //   child: TextButton(
            //     child: Text(
            //       '${FlutterI18n.translate(context, 'common.tabs.legal_notices')} & ${FlutterI18n.translate(context, 'common.tabs.privacy_policy')}',
            //       style: TextStyle(
            //         fontSize: 14,
            //         fontWeight: FontWeight.w400,
            //         decoration: TextDecoration.underline,
            //         color: Theme.of(context).primaryColor,
            //         height: 1.5,
            //       ),
            //     ),
            //     onPressed: () {
            //       Navigator.of(context)
            //           .pushNamed(PrivacyPolicyScreen.routeName);
            //     },
            //   ),
            // )
            // Container(
            //   width: double.infinity,
            //   height: MediaQuery.of(context).padding.bottom + 60,
            //   color: const Color(0xFFFFFFFF),
            //   alignment: Alignment.topCenter,
            //   child: ListTile(
            //     horizontalTitleGap: 0,
            //     contentPadding: const EdgeInsets.symmetric(horizontal: 24),
            //     leading: const Icon(
            //       PhosphorIcons.rocketLaunchBold,
            //       size: 24,
            //       color: Color(0xFF918F92),
            //     ),
            //     title: RichText(
            //       text: TextSpan(
            //         style: const TextStyle(
            //           fontFamily: 'Inter',
            //           fontSize: 12,
            //           fontWeight: FontWeight.w700,
            //           color: Color(0xFF918F92),
            //         ),
            //         children: [
            //           TextSpan(
            //             text: FlutterI18n.translate(
            //                 context, 'common.footer.powered_by'),
            //             style: const TextStyle(
            //               fontWeight: FontWeight.w500,
            //             ),
            //           ),
            //           const TextSpan(
            //             text: ' as we know',
            //             style: TextStyle(
            //               fontWeight: FontWeight.w700,
            //             ),
            //           ),
            //         ],
            //       ),
            //       textScaleFactor: MediaQuery.of(context).textScaleFactor,
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
