import 'package:flutter/material.dart';

class PartnerSponsorCard extends StatelessWidget {
  final String title;
  final String description;
  final String logoPath;
  final double logoPadding;

  const PartnerSponsorCard({
    Key key,
    @required this.title,
    @required this.description,
    @required this.logoPath,
    @required this.logoPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      clipBehavior: Clip.antiAlias,
      child: Container(
        padding: const EdgeInsets.all(16),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            Text(
              description,
              style: const TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF8F91AC),
                  height: 1.6),
            ),
            Container(
              margin: const EdgeInsets.only(top: 12),
              padding: EdgeInsets.symmetric(vertical: logoPadding),
              height: 100,
              width: double.infinity,
              decoration: BoxDecoration(
                color: const Color(0xFFF9F9FA),
                borderRadius: BorderRadius.circular(24),
              ),
              child: Image.asset(
                logoPath,
                fit: BoxFit.fitHeight,
              ),
            )
          ],
        ),
      ),
    );
  }
}
