import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/disease_translation.dart';
import 'package:rdk_mobile/models/phenotype_disease.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/screens/disease_info_screen.dart';
import 'package:rdk_mobile/widgets/fiche/disease_card.dart';

class PhenotypeDiseaseTabView extends StatefulWidget {
  final List<PhenotypeDisease> sortedDiseaseList;

  const PhenotypeDiseaseTabView({
    @required this.sortedDiseaseList,
  });

  @override
  _PhenotypeDiseaseTabViewState createState() =>
      _PhenotypeDiseaseTabViewState();
}

class _PhenotypeDiseaseTabViewState extends State<PhenotypeDiseaseTabView> {
  final TextEditingController _searchController = TextEditingController();
  List<PhenotypeDisease> _searchList;

  Locale currentLocale;

  @override
  void initState() {
    super.initState();
    currentLocale =
        Provider.of<CurrentLocale>(context, listen: false).currentLocale();
    _searchController.addListener(() {
      setState(() {});
    });
    _searchList = List.from(widget.sortedDiseaseList);
    _searchList.sort((a, b) {
      if (currentLocale.languageCode == 'en') {
        return a.disease.name.compareTo(b.disease.name);
      } else {
        DiseaseTranslation aTranslation = a.disease.translations.firstWhere(
            (ea) => ea.language == currentLocale.languageCode,
            orElse: () => null);
        DiseaseTranslation bTranslation = b.disease.translations.firstWhere(
            (eb) => eb.language == currentLocale.languageCode,
            orElse: () => null);
        if (aTranslation != null && bTranslation != null) {
          return aTranslation.name.compareTo(bTranslation.name);
        }
        return a.disease.name.compareTo(b.disease.name);
      }
    });
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void updateDiseaseList() {
    setState(() {
      _searchList.clear();
      if (_searchController.text.isNotEmpty) {
        _searchList = widget.sortedDiseaseList.where((PhenotypeDisease pd) {
          if (currentLocale.languageCode == 'en') {
            return removeDiacritics(pd.disease.name).toLowerCase().contains(
                    removeDiacritics(_searchController.text).toLowerCase()) ||
                pd.disease.synonyms.any((syn) =>
                    syn.language == currentLocale.languageCode &&
                    removeDiacritics(syn.synonym).toLowerCase().contains(
                        removeDiacritics(_searchController.text)
                            .toLowerCase()));
          } else {
            return pd.disease.translations.any((trans) =>
                    trans.language == currentLocale.languageCode &&
                    removeDiacritics(trans.name).toLowerCase().contains(
                        removeDiacritics(_searchController.text)
                            .toLowerCase())) ||
                pd.disease.synonyms.any((syn) =>
                    syn.language == currentLocale.languageCode &&
                    removeDiacritics(syn.synonym).toLowerCase().contains(
                        removeDiacritics(_searchController.text)
                            .toLowerCase()));
          }
        }).toList();
      } else {
        _searchList = List.from(widget.sortedDiseaseList);
      }
      _searchList.sort((a, b) {
        if (currentLocale.languageCode == 'en') {
          return a.disease.name.compareTo(b.disease.name);
        } else {
          DiseaseTranslation aTranslation = a.disease.translations.firstWhere(
              (ea) => ea.language == currentLocale.languageCode,
              orElse: () => null);
          DiseaseTranslation bTranslation = b.disease.translations.firstWhere(
              (eb) => eb.language == currentLocale.languageCode,
              orElse: () => null);
          if (aTranslation != null && bTranslation != null) {
            return aTranslation.name.compareTo(bTranslation.name);
          }
          return a.disease.name.compareTo(b.disease.name);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.sortedDiseaseList.isEmpty) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: const Color(0xFFECECF2),
              ),
              child: const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 44,
                color: Color(0xFF8487AC),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
              child: Text(
                FlutterI18n.translate(context, 'common.text.no_disease_found'),
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1E293B),
                    height: 1.5),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 30, left: 15),
            child: Text(
              FlutterI18n.translate(context, 'common.tabs.diseases'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline3.fontSize,
                fontWeight: Theme.of(context).textTheme.headline3.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
          ),
          Container(
            height: 40,
            width: double.infinity,
            margin: const EdgeInsets.only(bottom: 15, left: 15, right: 15),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: const Color(0xFFFFFFFF),
            ),
            child: TextField(
              controller: _searchController,
              onChanged: (_) => updateDiseaseList(),
              cursorHeight: 12,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFF8F91AC),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_here'),
                hintStyle: const TextStyle(
                    color: Color(0xFFADAFCA),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1),
                border: InputBorder.none,
                suffixIcon: _searchController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _searchController.clear();
                          updateDiseaseList();
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
          if (_searchList.isEmpty)
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color(0xFFECECF2),
                    ),
                    child: const Icon(
                      PhosphorIcons.magnifyingGlass,
                      size: 44,
                      color: Color(0xFF8487AC),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 55, vertical: 8),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'common.text.no_disease_found'),
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF1E293B),
                          height: 1.5),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            )
          else
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _searchList.length,
              itemBuilder: (context, index) => GestureDetector(
                behavior: HitTestBehavior.deferToChild,
                child: FicheDiseaseCard(disease: _searchList[index].disease),
                onTap: () {
                  Navigator.of(context).pushNamed(
                    DiseaseInfoScreen.routeName,
                    arguments: {
                      'orphaNumber': _searchList[index].disease.orphaNumber,
                      'backBtn': FlutterI18n.translate(
                          context, 'pages.model_info', translationParams: {
                        'model': FlutterI18n.plural(
                            context, 'models.symptom.title', 1)
                      }),
                    },
                  );
                },
              ),
            ),
          const SizedBox(
            height: 7,
          ),
        ],
      );
    }
  }
}
