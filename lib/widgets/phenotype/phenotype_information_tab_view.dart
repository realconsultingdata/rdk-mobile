import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/phenotype.dart';
import 'package:rdk_mobile/models/phenotype_synonym.dart';
import 'package:rdk_mobile/models/phenotype_translation.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/phenotype/statistic_card.dart';

import '../general_card.dart';

class PhenotypeInformationTabView extends StatelessWidget {
  final Phenotype loadedPhenotype;
  // final Map<String, int> statisticsData;

  PhenotypeInformationTabView(
      {@required this.loadedPhenotype});

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    List<PhenotypeSynonym> filteredSynonym = loadedPhenotype.synonyms
        .where((syn) => syn.language == language)
        .toList();
    PhenotypeTranslation translation = loadedPhenotype.translations.firstWhere(
        (translation) => translation.language == language,
        orElse: () => null);
    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            language != 'en' && translation != null
              ? translation.hpoTerm
              : loadedPhenotype.hpoTerm,
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.headline4.fontSize,
              fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
              color: const Color(0xFF1E293B),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, right: 15, bottom: 30, left: 15),
          child: Text(
            loadedPhenotype.id,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Color(0xFF64748B),
            ),
          ),
        ),
        GeneralCard(
          key: const ValueKey('description_phenotype'),
          title: FlutterI18n.translate(context, 'common.text.description'),
          descriptionContents: language != 'en' && translation != null
              ? translation.contents
              : loadedPhenotype.contents,
          synonyms: filteredSynonym,
          hpoTermTranslationSource: translation?.hpoTermTranslationSource,
          contentsTranslationSource: translation?.contentsTranslationSource,
          synonymTranslationSource:
              filteredSynonym.any((syn) => syn.translationSource == 'DeepL')
                  ? 'DeepL'
                  : 'HPO',
        ),
        // const SizedBox(
        //   height: 24,
        // ),
        // StatisticCard(statisticsData),
        if (currentLocale.languageCode != 'en')
          Container(
            width: double.infinity,
            margin: const EdgeInsets.symmetric(vertical: 8),
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: RichText(
              textScaleFactor: MediaQuery.of(context).textScaleFactor,
              text: TextSpan(
                style: const TextStyle(fontFamily: 'Inter'),
                children: [
                  const WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: Icon(
                      PhosphorIcons.asteriskSimple,
                      size: 10,
                      color: Color(0xFF006394),
                    ),
                  ),
                  WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: Text(
                      FlutterI18n.translate(
                              context, 'common.text.translated_by') +
                          ' DeepL',
                      style: const TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.italic,
                        color: Color(0xFF1E293B),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
