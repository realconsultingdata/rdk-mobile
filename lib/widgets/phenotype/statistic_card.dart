import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class StatisticCard extends StatelessWidget {
  final Map<String, int> data;

  StatisticCard(this.data);

  List<charts.Series<StatisticsData, String>> seriesList;

  @override
  Widget build(BuildContext context) {
    final List<StatisticsData> statisticsData = [];
    data.forEach((key, value) => statisticsData
        .add(StatisticsData(frequency: key, diseaseCount: value)));

    seriesList = [
      charts.Series<StatisticsData, String>(
        id: 'Diseases by HPO Frequency',
        domainFn: (StatisticsData data, _) => FlutterI18n.translate(
            context,
            'models.symptom.hpo_frequency.value.' +
                data.frequency.replaceAll(RegExp(r'[(<>)]'), '')),
        measureFn: (StatisticsData data, _) => data.diseaseCount,
        colorFn: (_, __) =>
            charts.ColorUtil.fromDartColor(Theme.of(context).primaryColor),
        data: statisticsData,
        labelAccessorFn: (StatisticsData data, _) => '${data.diseaseCount}',
        outsideLabelStyleAccessorFn: (StatisticsData data, _) {
          return const charts.TextStyleSpec(fontSize: 12, fontWeight: '400');
        },
      )
    ];

    return Card(
      elevation: 0,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: const EdgeInsets.all(24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              FlutterI18n.translate(context, 'common.text.statistics'),
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              height: 323,
              child: charts.BarChart(
                seriesList,
                defaultRenderer: charts.BarRendererConfig(
                    cornerStrategy: const charts.ConstCornerStrategy(6),
                    barRendererDecorator: charts.BarLabelDecorator<String>(
                      labelPosition: charts.BarLabelPosition.auto,
                    ),
                    maxBarWidthPx: 25),
                animate: true,
                vertical: false,
                primaryMeasureAxis: const charts.NumericAxisSpec(
                  renderSpec: charts.NoneRenderSpec(),
                ),
                domainAxis: charts.OrdinalAxisSpec(
                  renderSpec: charts.SmallTickRendererSpec(
                    axisLineStyle: charts.LineStyleSpec(
                      dashPattern: const [44, 4],
                      thickness: 1,
                      color: charts.Color.fromHex(code: '#CBD5E1'),
                    ),
                    labelOffsetFromAxisPx: 10,
                    labelStyle: charts.TextStyleSpec(
                        fontSize: 12,
                        fontWeight: '600',
                        color: charts.Color.fromHex(code: '#64748B')),
                    tickLengthPx: 0,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
          ],
        ),
      ),
    );
  }
}

class StatisticsData {
  final String frequency;
  final int diseaseCount;

  StatisticsData({this.frequency, this.diseaseCount});
}
