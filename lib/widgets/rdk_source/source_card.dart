import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/models/rdk_source.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:url_launcher/url_launcher.dart';

class SourceCard extends StatelessWidget {
  final RdkSource source;

  const SourceCard({Key key, this.source}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Locale currentLocale = Provider.of<CurrentLocale>(context).currentLocale();
    String language = currentLocale.languageCode;
    return Card(
      elevation: 0,
      margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      shadowColor: const Color.fromRGBO(94, 92, 154, 0.12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      clipBehavior: Clip.antiAlias,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              source.name,
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline4.fontSize,
                fontWeight: Theme.of(context).textTheme.headline4.fontWeight,
                color: const Color(0xFF1E293B),
              ),
            ),
            Text(
              source.abbr,
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.headline5.fontSize,
                fontWeight: Theme.of(context).textTheme.headline5.fontWeight,
                color: Theme.of(context).primaryColor,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            if (source.abbr != 'DeepL')
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // if (source.countryCode != null)
                  //   Container(
                  //     margin: const EdgeInsets.only(top: 3, right: 5),
                  //     child: ClipRRect(
                  //       borderRadius: BorderRadius.circular(10),
                  //       child: SvgPicture.asset(
                  //         'assets/icons/' + source.countryCode + '.svg',
                  //         height: 10,
                  //         width: 10,
                  //         fit: BoxFit.fitWidth,
                  //       ),
                  //     ),
                  //   ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${FlutterI18n.translate(context, 'common.text.source_type')}: ${language == 'en' ? source.sourceTypeEn : source.sourceTypeFr}',
                        style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFF8F91AC),
                        ),
                      ),
                      if (source.updatedAt != null)
                        Text(
                          '${FlutterI18n.translate(context, 'common.text.last_updated')}: ${DateFormat('MM/yyyy', language).format(source.updatedAt)}',
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF8F91AC),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            if (source.abbr != 'DeepL')
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 10),
                child: Text(
                  '${FlutterI18n.translate(context, 'common.text.about_the_source')}:',
                  style: TextStyle(
                    fontSize: Theme.of(context).textTheme.headline5.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.headline5.fontWeight,
                    color: const Color(0xFF1E293B),
                  ),
                ),
              ),
            if (source.abbr != 'DeepL')
              Text(
                language == 'en' ? source.aboutEn : source.aboutFr,
                style: Theme.of(context).textTheme.bodyText2,
                textAlign: TextAlign.justify,
              ),
            Container(
              margin: const EdgeInsets.only(top: 20, bottom: 10),
              child: Text(
                '${FlutterI18n.translate(context, 'common.text.what_it_is_used_for')}:',
                style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline5.fontSize,
                  fontWeight: Theme.of(context).textTheme.headline5.fontWeight,
                  color: const Color(0xFF1E293B),
                ),
              ),
            ),
            Wrap(
              spacing: 5,
              runSpacing: 5,
              children: source.usedCategories
                  .map((item) => Chip(
                        key: ValueKey(item),
                        label: Text(FlutterI18n.translate(context, item)),
                        labelStyle: const TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 12,
                          color: Color(0xFF1E293B),
                        ),
                        labelPadding: const EdgeInsets.symmetric(horizontal: 4),
                        backgroundColor: const Color(0xFFF5F5F9),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(4),
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      ))
                  .toList(),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20, bottom: 10),
              child: Text(
                '${source.abbr} website:',
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Color(0xFF1E293B),
                ),
              ),
            ),
            GestureDetector(
              child: Text(
                source.link,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).primaryColor),
              ),
              onTap: () {
                launch(source.link);
              },
            )
          ],
        ),
      ),
    );
  }
}
