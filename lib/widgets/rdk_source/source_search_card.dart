import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rdk_mobile/providers/current_locale.dart';
import 'package:rdk_mobile/widgets/dropdown_menu.dart';

class SourceSearchCard extends StatefulWidget {
  final List<Map<String, Object>> countries;
  final Function handleSearch;

  const SourceSearchCard({Key key, this.handleSearch, this.countries})
      : super(key: key);

  @override
  _SourceSearchCardState createState() => _SourceSearchCardState();
}

class _SourceSearchCardState extends State<SourceSearchCard> {
  final TextEditingController _textEditingController = TextEditingController();

  String _selectedItem;

  void onItemSelected(String item) {
    setState(() {
      _selectedItem = item;
      _textEditingController.clear();
      widget.handleSearch(item, '');
    });
  }

  @override
  void initState() {
    _selectedItem = widget.countries[0]['value'];
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CurrentLocale>(context).currentLocale();
    return Container(
      height: 160,
      width: double.infinity,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
      margin: const EdgeInsets.only(bottom: 15),
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Color.fromARGB(6, 37, 99, 235),
              offset: Offset(0, 4),
              blurRadius: 8)
        ],
      ),
      child: Column(
        children: [
          DropdownMenu(
            key: const ValueKey('country'),
            title: FlutterI18n.plural(context, 'models.country.title', 1),
            items: widget.countries,
            onSelectHandler: onItemSelected,
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.only(top: 10),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: const Color(0xFFDEDFE9),
                style: BorderStyle.solid,
                width: 1,
              ),
            ),
            child: TextField(
              controller: _textEditingController,
              onChanged: (_) => widget.handleSearch(
                  _selectedItem, _textEditingController.text),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 15, bottom: 15),
                focusedBorder: InputBorder.none,
                prefixIcon: const Icon(
                  PhosphorIcons.magnifyingGlass,
                  size: 24,
                  color: Color(0xFFCDCEDE),
                ),
                hintText: FlutterI18n.translate(
                    context, 'common.placeholders.search_here'),
                hintStyle: const TextStyle(
                  color: Color(0xFFADAFCA),
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
                border: InputBorder.none,
                suffixIcon: _textEditingController.text.isNotEmpty
                    ? IconButton(
                        onPressed: () {
                          _textEditingController.clear();
                          widget.handleSearch(_selectedItem, '');
                        },
                        padding: const EdgeInsets.only(left: 15),
                        icon: const Icon(
                          PhosphorIcons.x,
                          size: 24,
                          color: Color(0xFF1E293B),
                        ))
                    : const SizedBox.shrink(),
                // : IconButton(
                //     onPressed: () {
                //       print('Micro pressed');
                //     },
                //     padding: const EdgeInsets.only(left: 15),
                //     icon: const Icon(
                //       PhosphorIcons.microphone,
                //       size: 24,
                //       color: Color(0xFF1E293B),
                //     ),
                //   ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
