import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

class SearchInputNoSuggestion extends StatefulWidget {
  final TextEditingController _searchController;
  final Function onChanged;
  final Function initialLoad;
  final BoxDecoration decoration;
  final Icon prefixIcon;
  final String placeholder;
  final TextStyle placeholderStyle;
  final bool hideSearchBar;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry contentPadding;
  final double height;

  const SearchInputNoSuggestion(
      {Key key,
      @required TextEditingController searchController,
      @required this.onChanged,
      this.initialLoad,
      this.decoration,
      this.prefixIcon,
      this.placeholder,
      this.placeholderStyle,
      this.hideSearchBar = false,
      this.margin,
      this.padding,
      this.contentPadding,
      this.height})
      : _searchController = searchController,
        super(key: key);

  @override
  _SearchInputNoSuggestionState createState() =>
      _SearchInputNoSuggestionState();
}

class _SearchInputNoSuggestionState extends State<SearchInputNoSuggestion> {
  var _isInit = true;


  @override
  void didChangeDependencies() {
    if (_isInit && widget.initialLoad != null) {
      widget.initialLoad();
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.hideSearchBar) {
      return const SizedBox.shrink();
    }
    return Container(
      height: widget.height ?? 50,
      width: double.infinity,
      margin: widget.margin ?? const EdgeInsets.symmetric(vertical: 10),
      padding: widget.padding ?? const EdgeInsets.symmetric(horizontal: 10),
      decoration: widget.decoration ??
          BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: const Color(0xFFDEDFE9),
              style: BorderStyle.solid,
              width: 1,
            ),
          ),
      alignment: Alignment.center,
      child: TextField(
        controller: widget._searchController,
        onChanged: (_) => widget.onChanged(),
        textAlignVertical: TextAlignVertical.center,
        cursorHeight: 12,
        decoration: InputDecoration(
          contentPadding: widget.contentPadding ??
              const EdgeInsets.only(top: 15, bottom: 15),
          focusedBorder: InputBorder.none,
          prefixIcon: widget.prefixIcon ??
              const Icon(
                PhosphorIcons.magnifyingGlass,
                size: 24,
                color: Color(0xFFCDCEDE),
              ),
          hintText: widget.placeholder ??
              FlutterI18n.translate(context, 'common.placeholders.search_here'),
          hintStyle: widget.placeholderStyle ??
              const TextStyle(
                color: Color(0xFFADAFCA),
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
          border: InputBorder.none,
          suffixIcon: widget._searchController.text.isNotEmpty
              ? IconButton(
                  onPressed: () {
                    widget._searchController.clear();
                    widget.onChanged();
                  },
                  padding: const EdgeInsets.only(left: 15),
                  icon: const Icon(
                    PhosphorIcons.x,
                    size: 24,
                    color: Color(0xFF1E293B),
                  ))
              : const SizedBox.shrink(),
          // : IconButton(
          //     onPressed: () {
          //       print('Micro pressed');
          //     },
          //     padding: const EdgeInsets.only(left: 15),
          //     icon: const Icon(
          //       PhosphorIcons.microphone,
          //       size: 24,
          //       color: Color(0xFF1E293B),
          //     ),
          //   ),
        ),
      ),
    );
  }
}
