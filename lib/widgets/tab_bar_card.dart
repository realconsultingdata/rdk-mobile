import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class TabBarCard extends StatelessWidget with PreferredSizeWidget {
  final TabController tabController;
  final List<String> tabTitles;

  const TabBarCard(
      {Key key, @required this.tabController, @required this.tabTitles})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: TabBar(
        controller: tabController,
        isScrollable: true,
        labelColor: const Color(0xFF111112),
        unselectedLabelColor: const Color(0xFFADAFCA),
        labelStyle: Theme.of(context).textTheme.headline5,
        indicatorColor: Theme.of(context).primaryColor,
        indicatorSize: TabBarIndicatorSize.label,
        tabs: tabTitles.map((title) {
          if (MediaQuery.of(context).size.width < 768) {
            return tabTitles.length > 4
                ? Tab(text: FlutterI18n.translate(context, title))
                : Container(
                    constraints: BoxConstraints(
                        minWidth: tabTitles.length == 2
                            ? (MediaQuery.of(context).size.width / 2.4)
                            : (MediaQuery.of(context).size.width / 4.5)),
                    alignment: Alignment.center,
                    child: Wrap(
                      children: [
                        Tab(
                          text: FlutterI18n.translate(context, title),
                        ),
                      ],
                    ),
                  );
          } else {
            double minWidth;
            if (MediaQuery.of(context).size.width < 992) {
              minWidth = MediaQuery.of(context).size.width /
                  (tabTitles.length > 4
                      ? 5.5
                      : (tabTitles.length == 2 ? 2.2 : 3.5));
            } else {
              minWidth = MediaQuery.of(context).size.width /
                  (tabTitles.length > 4
                      ? 5.5
                      : (tabTitles.length == 2 ? 2.2 : 3.3));
            }
            return Container(
              constraints: BoxConstraints(
                minWidth: minWidth,
              ),
              alignment: Alignment.center,
              child: Wrap(
                children: [
                  Tab(
                    text: FlutterI18n.translate(context, title),
                  ),
                ],
              ),
            );
          }
        }).toList(),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
